Version 0.9.8.4 - ?? ?? ????
----------------------------

* Added __mod__, __imod__, __rmod__, ceil, floor, trunc, rint
  methods to cf.Data and cf.Variable

* Added ceil, floor, trunc, rint to cf.Field

* Fixed bug in which array cf.Data.array sometimes behaved like
  cf.Data.varray
	
* Fixed bug in cf.netcdf.read.read which affected reading fields
  with formula_terms.

* Refactored the test suite to use the unittest package
	
Version 0.9.8.3 - 14 July 2014
------------------------------

* Implemented multiple grid_mappings (CF trac ticket #70)

* Improved functionality and speed of field aggregation and cfa
  and cfdump command line utilities.

* Collapse methods on cf.Data object (min, max, mean, var, sd,
  sum, range, mid_range).

* Improved match/select functionality
	
Version 0.9.8.2 - 13 March 2014
-------------------------------

* Copes with PP fields with 365_day calendars

* Revamped CFA files in line with the evolving standard. CFA files
  from PP data created with a previous version will no longer work.

Version 0.9.8.1 -  December 2013
--------------------------------

Version 0.9.8 - 06 December 2013
--------------------------------

* Improved API.

* Plenty of speed and memory optimizations.

* A proper treatment of datetimes.

* WGDOS-packed PP fields are now unpacked on demand.
	
* Fixed bug in functions.py for numpy v1.7. Fixed bug when deleting
  the 'id' attribute.

* Assign a standard name to aggregated PP fields after aggregation
  rather than before (because some stash codes are too similar,
  e.g. 407 and 408).

* New subclasses of cf.Coordinate: cf.DimensionCoordinate and
  cf.AuxiliaryCoordinate.

* A cf.Units object is now immutable.
	
Version 0.9.7.1 - 26 April 2013
-------------------------------

* Fixed endian bug in CFA-netCDF files referring to PP files

* Changed default output format to NETCDF3_CLASSIC and trap error when
  when writing unsigned integer types and the 64-bit integer type to
  file formats other than NETCDF4.

* Changed unhelpful history created when aggregating
	
Version 0.9.7 - 24 April 2013
-----------------------------

* Read and write CFA-netCDF files

* Field creation interface

* New command line utilities: cfa, cfdump
	
* Redesigned repr, str and dump() output (which is shared with cfa and
  cfdump)

* Removed superceded (by cfa) command line utilities pp2cf, cf2cf

* Renamed the 'subset' method to 'select'	

* Now needs netCDF4-python 0.9.7 or later (and numpy 1.6 or later)
	
Version 0.9.6.2 - 27 March 2013
-------------------------------

* Fixed bug in cf/pp.py which caused the creation of incorrect
  latitude coordinate arrays.

Version 0.9.6.1 - 20 February 2013
----------------------------------

* Fixed bug in cf/netcdf.py which caused a failure when a file with
  badly formatted units was encountered.

Version 0.9.6 - 27 November 2012
--------------------------------

* Assignment to a field's data array with metadata-aware broadcasting,
  assigning to subspaces, assignment where data meets conditions,
  assignment to unmasked elements, etc. (setitem method)

* Proper treatment of the missing data mask, including metadata-aware
  assignment (setmask method)

* Proper treatment of ancillary data.

* Ancillary data and transforms are subspaced with their parent field.

* Much faster aggregation algorithm (with thanks to Jonathan
  Gregory). Also aggregates fields transforms, ancillary variables and
  flags.

Version 0.9.5 - 01 October 2012
-------------------------------

* Restructured documentation and package code files.

* Large Amounts of Massive Arrays (LAMA) functionality.

* Metadata-aware field manipulation and combination with
  metadata-aware broadcasting.

* Better treatment of cell measures.

* Slightly faster aggregation algorithm (a much improved one is in
  development).

* API changes for clarity.

* Bug fixes.

* Added 'TEMPDIR' to the cf.CONSTANTS dictionary

* This is a snapshot of the trunk at revision r195.

Version 0.9.5.dev - 19 September 2012
-------------------------------------

* Loads of exciting improvements - mainly LAMA functionality,
  metadata-aware field manipulation and documentation.

* This is a snapshot of the trunk at revision r185. A proper vn0.9.5
  release is imminent.

Version 0.9.4.2 - 17 April 2012
-------------------------------

* General bug fixes and code restructure

Version 0.9.4 - 15 March 2012
-----------------------------

* A proper treatment of units using the Udunits C library and the
  extra time functionality provided by the netCDF4 package.

* A command line script to do CF-netCDF to CF-netCDF via cf-python.

	
Version 0.9.3.3 - 08 February 2012
----------------------------------

* Objects renamed in line with the CF data model: 'Space' becomes
  'Field' and 'Grid' becomes 'Space'.

* Field aggregation using the CF aggregation rules is available when
  reading fields from disk and on fields in memory. The data of a
  field resulting from aggregation are stored as a collection of the
  data from the component fields and so, as before, may be file
  pointers, arrays in memory or a mixture of these two forms.

* Units, missing data flags, dimension order, dimension direction and
  packing flags may all be different between data components and are
  conformed at the time of data access.

* Files in UK Met Office PP format may now be read into CF fields.

* A command line script for PP to CF-netCDF file conversion is
  provided.

	
Version 0.9.3 - 05 January 2012
-------------------------------

* A more consistent treatment of spaces and lists of spaces (Space and
  SpaceList objects respectively).

* A corrected treatment of scalar or 1-d, size 1 dimensions in the
  space and its grid.

* Data stored in Data objects which contain metadata need to correctly
  interpret and manipulate the data. This will be particularly useful
  when data arrays spanning many files/arrays is implemented

Version 0.9.2 - 26 August 2011
-------------------------------

* Created a setup.py script for easier installation (with thanks to
  Jeff Whitaker).

* Added support for reading OPeNDAP-hosted datasets given by URLs.

* Restructured the documentation.

* Created a test directory with scripts and sample output.

* No longer fails for unknown calendar types (such as '360d').
	
Version 0.9.1 - 06 August 2011
------------------------------

* First release.
