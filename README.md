Home page
=========

* [**cf-python**](http://cfpython.bitbucket.org "cf-python home page")

----------------------------------------------------------------------

Documentation
=============

* Online documentation for the latest stable release: [**Version
  0.9.8.3 HTML**](http://cfpython.bitbucket.org/docs/0.9.8.3/
  "cf-python HTML documentation")

* Online documentation for previous releases: [**cf-python documention
  archive**](http://cfpython.bitbucket.org/docs/archive.html)

* Offline HTML documention for the installed version may be found by
  pointing a browser to ``docs/build/index.html``.


----------------------------------------------------------------------

Installation
============

Dependencies
------------

* At present, the package only runs under
  [**Linux**](http://en.wikipedia.org/wiki/Linux). However, the
  operating system dependencies are few (relating to the management of
  memory and open files) and most likely easily remedied for other
  operating systems.

* Requires a [**python**](http://www.python.org) version from 2.6 up
  to, but not including, 3.0.
 
* Requires the [**python numpy package**](http://www.numpy.org) at
  version 1.7 or newer.

* Requires the [**python netCDF4
  package**](http://code.google.com/p/netcdf4-python) at version 0.9.7
  or newer. This package requires the
  [**netCDF**](http://www.unidata.ucar.edu/software/netcdf),
  [**HDF5**](http://www.hdfgroup.org/HDF5) and
  [**zlib**](ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-4)
  libraries.

* Requires [**UNIDATA's Udunits-2
  package**](http://www.unidata.ucar.edu/software/udunits). This is a
  library which provides support for units of physical quantities.

Installation
------------

1.  Download the cf package from [**cf-python
    downloads**](https://bitbucket.org/cfpython/cf-python/downloads).
   
2.  Unpack it with:
   
        tar zxvf cf-0.9.8.3.tar.gz
   
3.  Within the newly created directory ``cf-0.9.8.3``, run one of the
    following:
   
    *  To install the cf package to a central location:
       
            python setup.py install
       
    *  To install the cf package to a locally to the user in a default
       location:
       
            python setup.py install --user
       
    *  To install the cf package in the <directory> of your choice:
       
            python setup.py install --home=<directory>

Tests
-----

The test script is in the ``test`` directory:

    python test/test.py

----------------------------------------------------------------------

Command line utilities
======================

The ``cfdump`` tool generates text representations on standard output
of the CF fields contained in the input files. 

The ``cfa`` tool creates and writes to disk the CF fields contained in
the input files.

During the installation described above, these scripts will be copied
automatically to a location given by the ``PATH`` environment
variable, otherwise they need to be copied manually from the
``scripts`` directory.

For usage instructions, use the ``-h`` option to display the manual
pages:

    cfdump -h
    cfa -h

----------------------------------------------------------------------

Code license
============

[**MIT License**](http://opensource.org/licenses/mit-license.php)

  * Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use, copy,
    modify, merge, publish, distribute, sublicense, and/or sell copies
    of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

  * The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
