from .variable import Variable


# ====================================================================
#
# CoordinateBounds object
#
# ====================================================================

class CoordinateBounds(Variable):
    '''
    
A CF coordinate's bounds object containing cell boundaries or
intervals of climatological time. The parent coordinate's
`!climatology` attribute indicates which type of bounds are present.

'''
    def contiguous(self, overlap=True, direction=None):
        '''
      
Return True if the bounds are contiguous.

Bounds are contiguous if the cell boundaries match up, or
overlap, with the boundaries of adjacent cells.

In general, it is only possible for 1 or 0 dimensional coordinates
with bounds to be contiguous, but size 1 coordinates with any number
of dimensions are always contiguous.

An exception occurs if the coordinate is multdimensional and has more
than one element.

'''
        if not self._hasData:
            return False    

        nbounds = self.shape[-1]

        if self.size == nbounds:
            return True

        if  nbounds > 2 or self.ndim > 2:
            raise ValueError(
"Can't tell if a multidimensional coordinate bounds are contiguous")

        data = self.Data
        
        if not overlap: 
            return data[1:, 0].equals(data[:-1, 1])
        else:
            if direction is None:
                b = data[(0,)*(data.ndim-1)].array
                direction =  b.item(0,) < b.item(1,)
            #--- End: if
        
            if direction:
                return (data[1:, 0] <= data[:-1, 1]).all()
            else:
                return (data[1:, 0] >= data[:-1, 1]).all()
    #--- End: def

#--- End: class
