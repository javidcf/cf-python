from .utils     import List
from .functions import RTOL, ATOL
from .functions import inspect as cf_inspect


# ====================================================================
#
# FieldList object
#
# ====================================================================

class FieldList(List):
    '''

An ordered sequence of fields.

Each element of a field list is a `cf.Field` object.

A field list has all of the callable methods, reserved attributes and
reserved CF properties that a field object has. When used with a field
list, a callable method or a reserved attribute or CF property is
applied independently to each field and, unless a method (or
assignment to a reserved attribute or CF property) carries out an
in-place change to each field, a sequence of the results is returned.
The type of sequence that may be returned will either be a new
`cf.FieldList` object or else a `cf.List` object.

In addition, a field list supports the python list-like operations
(such as indexing and methods like `!append`), but not the python list
arithmetic and comparison behaviours. Any field list arithmetic and
comparison operation is applied independently to each field element,
so all of the operators defined for a field are allowed.

'''

    def __init__(self, fields=None):
        '''

**Initialization**

:Parameters:

    fields : (sequence of) cf.Field, optional
         Create a new field list with these fields.

**Examples**

>>> fl = cf.FieldList()
>>> len(fl)
0
>>> f
<CF Field: air_temperature() K>
>>> fl = cf.FieldList(f)
>>> len(fl
1
>>> fl = cf.FieldList([f, f])
>>> len(fl)
2
>>> fl = cf.FieldList(cf.FieldList([f] * 3))
>>> len(fl)
3
'''
        super(FieldList, self).__init__(fields)
    #--- End: def

    def __delattr__(self, attr):
        '''

x.__delattr__(attr) <==> del x.attr

'''
        for f in self._list:
            delattr(f, attr)
    #--- End: def

    def __repr__(self):
        '''
x.__repr__() <==> repr(x)

'''
        return repr(self._list).replace('>, ', '>,\n ')
    #--- End: def

    def __str__(self):
        '''
x.__str__() <==> str(x)

'''
        return '\n'.join([str(x) for x in self._list])
    #--- End: def

#    def __eq__(self, other):
#        '''
#x.__eq__(y) <==> x==y
#
#'''
#        return type(self)([f.__eq__(other) for f in self._list])
#    #--- End: def
#
#    def __ne__(self, other):
#        '''
#x.__ne__(y) <==> x!=y
#
#'''
#        return type(self)([f.__ne__(other) for f in self._list])
#    #--- End: def
#
#    def __neg__(self):
#        '''
#x.__neg__() <==> -x
#
#'''
#        return type(self)([-f for f in self._list])
#    #--- End: def
#
#    def __pos__(self):
#        '''
#x.__pos__() <==> +x
#
#'''
#        return type(self)([+f for f in self._list])
#    #--- End: def
#
#    def __abs__(self):
#         '''
#x.__abs__() <==> abs(x)
#
#'''
#         return type(self)([abs(f) for f in self._list])
#    #--- End: def
#
#    def __invert__(self):
#         '''
#
#x.__invert__() <==> ~x
#
#'''
#         return type(self)([~f for f in self._list])
#    #--- End: def

#    def __add__(self, other):
#        '''
#
#The binary arithmetic operation ``+``
#
#x.__add__(y) <==> x+y
#
#``x+y`` is equivalent to ``cf.FieldList(f+y for f in x)``.
#
#'''
#        return type(self)([f.__add__(other) for f in self._list])
#    #--- End: def
#
#    def __sub__(self, other):
#        '''
#
#x.__sub__(y) <==> x-y
#
#'''
#        return type(self)([f.__sub__(other) for f in self._list])
#    #--- End: def
#
#    def __mul__(self, other):
#        '''
#x.__mul__(y) <==> x*y
#
#'''
#        return type(self)([f.__mul__(other) for f in self._list])
#    #--- End: def
#
#    def __div__(self, other):
#        '''
#x.__div__(y) <==> x/y
#'''
#        return type(self)([f.__div__(other) for f in self._list])
#    #--- End: def
#
#    def __floordiv__(self, other):
#        '''
#x.__floordiv__(y) <==> x//y
#'''
#        return type(self)([f.__floordiv__(other) for f in self._list])
#    #--- End: def
#
#    def __truediv__(self, other):
#        '''
#x.__truediv__(y) <==> x/y
#'''
#        return type(self)([f.__truediv__(other) for f in self._list])
#    #--- End: def
#
#    def __radd__(self, other):
#        '''
#x.__radd__(y) <==> y+x
#'''
#        return type(self)([f.__radd__(other) for f in self._list])
#    #--- End: def
#
#    def __rmul__(self, other):
#        '''
#x.__rmul__(y) <==> y*x
#'''
#        return type(self)([f.__rmul__(other) for f in self._list])
#    #--- End: def
#
#    def __iadd__(self, other):
#        '''
#x.__iadd__(y) <==> x+=y
#'''
#        for f in self._list:
#            f.__iadd__(other)
#
#        return self
#    #--- End def
#
#    def __isub__(self, other):
#        '''
#x.__isub__(y) <==> x-=y
#'''
#        for f in self._list:
#            f.__isub__(other)
#
#        return self
#    #--- End def
#
#    def __imul__(self, other):
#        '''
#x.__imul__(y) <==> x*=y
#'''
#        for f in self._list:
#            f.__imul__(other)
#
#        return self
#    #--- End def
#
#    def __idiv__(self, other):
#        '''
#x.__idiv__(y) <==> x/=y
#'''
#        for f in self._list:
#            f.__idiv__(other)
#        return self
#    #--- End def
#
#    def __ifloordiv__(self, other):
#        '''
#x.__ifloordiv__(y) <==> x//=y
#'''
#        for f in self._list:
#            f.__ifloordiv__(other)
#
#        return self
#    #--- End def
#
#    def __itruediv__(self, other):
#        '''
#x.__itruediv__(y) <==> x/=y
#'''
#        for f in self._list:
#            f.__itruediv__(other)
#
#        return self
#    #--- End def

    # ----------------------------------------------------------------
    # Attribute: ancillary_variables
    # ----------------------------------------------------------------
    @property
    def ancillary_variables(self):
        '''

The `~cf.Field.ancillary_variables` attribute of each field.

``fl.ancillary_variables`` is equivalent to
``cf.List(f.ancillary_variables for f in fl)``.

``fl.ancillary_variables = value`` is equivalent to ``for f in fl:
f.ancillary_variables = value``.

``del fl.ancillary_variables`` is equivalent to ``for f in fl: del
f.ancillary_variables``.

See `cf.Field.ancillary_variables` for details.

'''
        return List([f.ancillary_variables for f in self._list])
    #--- End: def
    @ancillary_variables.setter
    def ancillary_variables(self, value):
        for f in self._list:
            f.ancillary_variables = value
    #--- End: def
    @ancillary_variables.deleter
    def ancillary_variables(self):
        for f in self._list:
            del f.ancillary_variables
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: array
    # ----------------------------------------------------------------
    @property
    def array(self):
        '''

The `~cf.Field.array` attribute for each field.

``fl.array`` is equivalent to ``cf.List(f.array for f in fl)``.

See `cf.Field.array` for details.

:Returns:

    out : cf.List of numpy arrays
        The numpy array deep copy of the data array for each field.

**Examples**

See `cf.Field.array`.

'''
        return List([f.array for f in self._list])    
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: data (read only)
    # ----------------------------------------------------------------
    @property
    def data(self):
        '''

The `~cf.Field.data` attribute of each field.

``fl.data`` is equivalent to ``cf.List(f.data for f in fl)``.

See `cf.Field.data` for details.

'''
        return List([f.data for f in self._list])
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: Data
    # ----------------------------------------------------------------
    @property
    def Data(self):
        '''

The `~cf.Field.Data` attribute of each field.

``fl.Data`` is equivalent to ``cf.List(f.Data for f in fl)``.

``fl.Data = value`` is equivalent to ``for f in fl: f.Data = value``.

``del fl.Data`` is equivalent to ``for f in fl: del f.Data``.

See `cf.Field.Data` for details.

'''
        return List([f.Data for f in self._list])
    #--- End: def
    @Data.setter
    def Data(self, value):
        for f in self._list:
            f.Data = value
    #--- End: def
    @Data.deleter
    def Data(self):
        for f in self._list:
            del f.Data
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: Flags
    # ----------------------------------------------------------------
    @property
    def Flags(self):
        '''

The `~cf.Field.Flags` attribute of each field.

``fl.Flags`` is equivalent to ``cf.List(f.Flags for f in fl)``.

``fl.Flags = value`` is equivalent to ``for f in fl: f.Flags =
value``.

``del fl.Flags`` is equivalent to ``for f in fl: del f.Flags``.

See `cf.Field.Flags` for details.

'''
        return List([f.Flags for f in self._list])
    #--- End: def
    @Flags.setter
    def Flags(self, value):
        for f in self._list:
            f.Flags = value
    #--- End: def
    @Flags.deleter
    def Flags(self):
        for f in self._list:
            del f.Flags
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: domain
    # ----------------------------------------------------------------
    @property
    def domain(self):
        '''

The `~cf.Field.domain` attribute of each field.

``fl.domain`` is equivalent to ``cf.List(f.domain for f in fl)``.

``fl.domain = value`` is equivalent to ``for f in fl: f.domain =
value``.

``del fl.domain`` is equivalent to ``for f in fl: del f.domain``.

See `cf.Field.domain` for details.

'''
        return List([f.domain for f in self._list])
    #--- End: def
    @domain.setter
    def domain(self, value):
        for f in self._list:
            f.domain = value
    #--- End: def
    @domain.deleter
    def domain(self):
        for f in self._list:
            del f.domain
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: dtype
    # ----------------------------------------------------------------
    @property
    def dtype(self):
        '''

The `~cf.Field.dtype` attribute for each field.

``fl.dtype`` is equivalent to ``cf.List(f.dtype for f in fl)``.

``fl.dtype = value`` is equivalent to ``for f in fl: f.dtype =
value``.

``del fl.dtype`` is equivalent to ``for f in fl: del f.dtype``.

See `cf.Field.dtype` for details.

'''
        return List([f.dtype for f in self._list])
    #--- End: def
    @dtype.setter
    def dtype(self, value):
        for f in self._list:
            f.dtype = value
    #--- End: def
    @dtype.deleter
    def dtype(self):
        for f in self._list:
            del f.dtype
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: isscalar (read only)
    # ----------------------------------------------------------------
    @property
    def isscalar(self):
        '''

The `~cf.Field.isscalar` attribute of each field.

``fl.isscalar`` is equivalent to ``cf.List(f.isscalar for f in fl)``.

See `cf.Field.isscalar` for details.

'''
        return List([f.isscalar for f in self._list])
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: hardmask (read/write only)
    # ----------------------------------------------------------------
    @property
    def hardmask(self):
        '''

The `~cf.Field.hardmask` attribute of each field.

``fl.hardmask`` is equivalent to ``cf.List(f.hardmask for f in fl)``.

``fl.hardmask = value`` is equivalent to ``for f in fl: f.hardmask =
value``.

See `cf.Field.hardmask` for details.

'''
        return List([f.hardmask for f in self._list])
    #--- End: def
    @hardmask.setter
    def hardmask(self, value):
        for f in self._list:
            f.hardmask = value
    #--- End: def
    @hardmask.deleter
    def hardmask(self):
        for f in self._list:
            del f.hardmask
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: mask (read only)
    # ----------------------------------------------------------------
    @property
    def mask(self):
        '''

The `~cf.Field.mask` attribute of each field.

``fl.mask`` is equivalent to ``cf.FieldList(f.mask for f in fl)``.

See `cf.Field.mask` for details.

'''
        return type(self)([f.mask for f in self._list])
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: properties (read only)
    # ----------------------------------------------------------------
    @property
    def properties(self):
        '''

The `~cf.Field.properties` attribute of each field.

``fl.properties`` is equivalent to ``cf.List(f.properties for f in
fl)``.

See `cf.Field.properties` for details.

:Returns:

    out : cf.List of dicts
        A dictionary of the CF properties for each field.

**Examples**

See `cf.Field.properties`.
'''
        return List([f.properties for f in self._list])    
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: ndim (read only)
    # ----------------------------------------------------------------
    @property
    def ndim(self):
        '''

The `~cf.Field.ndim` attribute for each field.

``fl.ndim`` is equivalent to ``cf.List(f.ndim for f in fl)``.

See `cf.Field.ndim` for details.

:Returns:

    out : cf.List of ints
        The number of data array axes for each field.

**Examples**

>>> len(fl
4
>>> fl.ndim
[3, 2, 0, 3]

'''
        return List([f.ndim for f in self._list])    
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: shape (read only)
    # ----------------------------------------------------------------
    @property
    def shape(self):
        '''

The `~cf.Field.shape` attribute for each field.

``fl.shape`` is equivalent to ``cf.List(f.shape for f in fl)``.

See `cf.Field.shape` for details.

:Returns:

    out : cf.List of tuples
        The data array shape for each field.

**Examples**

>>> fl.ndim
[2, 0, 4]
>>> fl.shape
[(73, 96), (), (12, 19, 96, 73)]

'''
        return List([f.shape for f in self._list])    
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: size (read only)
    # ----------------------------------------------------------------
    @property
    def size(self):
        '''

The `~cf.Field.size` attribute for each field.

``fl.size`` is equivalent to ``cf.List(f.size for f in fl)``.

See `cf.Field.size` for details.

:Returns:

    out : cf.List of ints
        The number of data array elements for each field.

**Examples**

>>> fl.shape
[(73, 96), (), (12, 19, 96, 73)]
>>> fl.size
[7008, 1, 1597824]

'''
        return List([f.size for f in self._list])    
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: Units
    # ----------------------------------------------------------------
    @property
    def Units(self):
        '''

The `~cf.Field.Units` attribute of each field.

``fl.Units`` is equivalent to ``cf.List(f.Units for f in fl)``.

``fl.Units = value`` is equivalent to ``for f in fl: f.Units =
value``.

``del fl.Units`` is equivalent to ``for f in fl: del f.Units``.

See `cf.Field.Units` for details.

'''
        return List([f.Units for f in self._list])
    #--- End: def
    @Units.setter
    def Units(self, value):
        for f in self._list:
            f.Units = value
    #--- End: def
    @Units.deleter
    def Units(self):
        for f in self._list:
            del f.Units
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: varray
    # ----------------------------------------------------------------
    @property
    def varray(self):
        '''

The `~cf.Field.varray` attribute for each field.

``fl.varray`` is equivalent to ``cf.List(f.varray for f in fl)``.

See `cf.Field.varray` for details.

:Returns:

    out : cf.List of numpy arrays
        The numpy array view of the data array for each field.

**Examples**

See `cf.Field.varray`.

'''
        return List([f.varray for f in self._list])    
    #--- End: def

    #--------------------------------------------------------
    # CF property: flag_masks
    # ----------------------------------------------------------------
    @property
    def flag_masks(self):
        '''

The `~cf.Field.flag_masks` CF property of each field.

``fl.flag_masks`` is equivalent to ``cf.List(f.flag_masks for f in
fl)``.

``fl.flag_masks = value`` is equivalent to ``for f in fl: f.flag_masks
= value``.

``del fl.flag_masks`` is equivalent to ``for f in fl: del
f.flag_masks``.

See `cf.Field.flag_masks` for details.

'''
        return List([f.flag_masks for f in self._list])    
    #--- End: def
    @flag_masks.setter
    def flag_masks(self, value): 
        for f in self._list:
            f.flag_masks = value
    #--- End: def
    @flag_masks.deleter
    def flag_masks(self):
        for f in self._list:
            del f.flag_masks
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: flag_meanings
    # ----------------------------------------------------------------
    @property
    def flag_meanings(self):
        '''

The `~cf.Field.flag_meanings` CF property of each field.

``fl.flag_meanings`` is equivalent to ``cf.List(f.flag_meanings for f
in fl)``.

``fl.flag_meanings = value`` is equivalent to ``for f in fl:
f.flag_meanings = value``.

``del fl.flag_meanings`` is equivalent to ``for f in fl: del
f.flag_meanings``.

See `cf.Field.flag_meanings` for details.

'''
        return List([f.flag_meanings for f in self._list])    
    #--- End: def
    @flag_meanings.setter
    def flag_meanings(self, value): 
        for f in self._list:
            f.flag_meanings = value
    #--- End: def
    @flag_meanings.deleter
    def flag_meanings(self):
        for f in self._list:
            del f.flag_meanings
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: flag_values
    # ----------------------------------------------------------------
    @property
    def flag_values(self):
        '''

The `~cf.Field.flag_values` CF property of each field.

``fl.flag_values`` is equivalent to ``cf.List(f.flag_values for f in
fl)``.

``fl.flag_values = value`` is equivalent to ``for f in fl:
f.flag_values = value``.

``del fl.flag_values`` is equivalent to ``for f in fl: del
f.flag_values``.

See `cf.Field.flag_values` for details.

'''
        return List([f.flag_values for f in self._list])    
    #--- End: def
    @flag_values.setter
    def flag_values(self, value): 
        for f in self._list:
            f.flag_values = value
    #--- End: def
    @flag_values.deleter
    def flag_values(self):
        for f in self._list:
            del f.flag_values
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: comment
    # ----------------------------------------------------------------
    @property
    def comment(self):
        '''

The `~cf.Field.comment` CF property of each field.

``fl.comment`` is equivalent to ``cf.List(f.comment for f in fl)``.

``fl.comment = value`` is equivalent to ``for f in fl: f.comment =
 value``.

``del fl.comment`` is equivalent to ``for f in fl: del f.comment``.

See `cf.Field.comment` for details.

'''
        return List([f.comment for f in self._list])    
    #--- End: def
    @comment.setter
    def comment(self, value): 
        for f in self._list:
            f.comment = value
    #--- End: def
    @comment.deleter
    def comment(self):
        for f in self._list:
            del f.comment
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: _FillValue
    # ----------------------------------------------------------------
    @property
    def _FillValue(self):
        '''

The `~cf.Field._FillValue` CF property of each field.

``fl._FillValue`` is equivalent to ``cf.List(f._FillValue for f in
fl)``.

``fl._FillValue = value`` is equivalent to ``for f in fl: f._FillValue
= value``.

``del fl._FillValue`` is equivalent to ``for f in fl: del
f._FillValue``.

See `cf.Field._FillValue` for details.

'''
        return List([f._FillValue for f in self._list])    
    #--- End: def
    @_FillValue.setter
    def _FillValue(self, value): 
        for f in self._list:
            f._FillValue = value
    #--- End: def
    @_FillValue.deleter
    def _FillValue(self):
        for f in self._list:
            del f._FillValue
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: history
    # ----------------------------------------------------------------
    @property
    def history(self):
        '''

The `~cf.Field.history` CF property of each field.

``fl.history`` is equivalent to ``cf.List(f.history for f in fl)``.

``fl.history = value`` is equivalent to ``for f in fl: f.history =
value``.

``del fl.history`` is equivalent to ``for f in fl: del f.history``.

See `cf.Field.history` for details.

'''
        return List([f.history for f in self._list])    
    #--- End: def
    @history.setter
    def history(self, value): 
        for f in self._list:
            f.history = value
    #--- End: def
    @history.deleter
    def history(self):
        for f in self._list:
            del f.history
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: add_offset
    # ----------------------------------------------------------------
    @property
    def add_offset(self):
        '''

The `~cf.Field.add_offset` CF property of each field.

``fl.add_offset`` is equivalent to ``cf.List(f.add_offset for f in
fl)``.

``fl.add_offset = value`` is equivalent to ``for f in fl: f.add_offset
= value``.

``del fl.add_offset`` is equivalent to ``for f in fl: del
f.add_offset``.

See `cf.Field.add_offset` for details.

'''
        return List([f.add_offset for f in self._list])    
    #--- End: def
    @add_offset.setter
    def add_offset(self, value): 
        for f in self._list:
            f.add_offset = value
    #--- End: def
    @add_offset.deleter
    def add_offset(self):
        for f in self._list:
            del f.add_offset
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: calendar
    # ----------------------------------------------------------------
    @property
    def calendar(self):
        '''

The `~cf.Field.calendar` CF property of each field.

``fl.calendar`` is equivalent to ``cf.List(f.calendar for f in fl)``.

``fl.calendar = value`` is equivalent to ``for f in fl: f.calendar =
value``.

``del fl.calendar`` is equivalent to ``for f in fl: del f.calendar``.

See `cf.Field.calendar` for details.

'''
        return List([f.calendar for f in self._list])    
    #--- End: def
    @calendar.setter
    def calendar(self, value): 
        for f in self._list:
            f.calendar = value
    #--- End: def
    @calendar.deleter
    def calendar(self):
        for f in self._list:
            del f.calendar
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: cell_methods
    # ----------------------------------------------------------------
    @property
    def cell_methods(self):
        '''

The `~cf.Field.cell_methods` CF property of each field.

``fl.cell_methods`` is equivalent to ``cf.List(f.cell_methods for f in
fl)``.

``fl.cell_methods = value`` is equivalent to ``for f in fl:
f.cell_methods = value``.

``del fl.cell_methods`` is equivalent to ``for f in fl: del
f.cell_methods``.

See `cf.Field.cell_methods` for details.

'''
        return List([f.cell_methods for f in self._list])    
    #--- End: def
    @cell_methods.setter
    def cell_methods(self, value): 
        for f in self._list:
            f.cell_methods = value
    #--- End: def
    @cell_methods.deleter
    def cell_methods(self):
        for f in self._list:
            del f.cell_methods
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: Conventions
    # ----------------------------------------------------------------
    @property
    def Conventions(self):
        '''

The `~cf.Field.Conventions` CF property of each field.

``fl.Conventions`` is equivalent to ``cf.List(f.Conventions for f in
fl)``.

``fl.Conventions = value`` is equivalent to ``for f in fl:
f.Conventions = value``.

``del fl.Conventions`` is equivalent to ``for f in fl: del
f.Conventions``.

See `cf.Field.Conventions` for details.

'''
        return List([f.Conventions for f in self._list])    
    #--- End: def
    @Conventions.setter
    def Conventions(self, value): 
        for f in self._list:
            f.Conventions = value
    #--- End: def
    @Conventions.deleter
    def Conventions(self):
        for f in self._list:
            del f.Conventions
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: institution
    # ----------------------------------------------------------------
    @property
    def institution(self):
        '''

The `~cf.Field.institution` CF property of each field.

``fl.institution`` is equivalent to ``cf.List(f.institution for f in
fl)``.

``fl.institution = value`` is equivalent to ``for f in fl:
f.institution = value``.

``del fl.institution`` is equivalent to ``for f in fl: del
f.institution``.

See `cf.Field.institution` for details.

'''
        return List([f.institution for f in self._list])    
    #--- End: def
    @institution.setter
    def institution(self, value): 
        for f in self._list:
            f.institution = value
    #--- End: def
    @institution.deleter
    def institution(self):
        for f in self._list:
            del f.institution
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: leap_month
    # ----------------------------------------------------------------
    @property
    def leap_month(self):
        '''

The `~cf.Field.leap_month` CF property of each field.

``fl.leap_month`` is equivalent to ``cf.List(f.leap_month for f in
fl)``.

``fl.leap_month = value`` is equivalent to ``for f in fl: f.leap_month
= value``.

``del fl.leap_month`` is equivalent to ``for f in fl: del
f.leap_month``.

See `cf.Field.leap_month` for details.

'''
        return List([f.leap_month for f in self._list])    
    #--- End: def
    @leap_month.setter
    def leap_month(self, value): 
        for f in self._list:
            f.leap_month = value
    #--- End: def
    @leap_month.deleter
    def leap_month(self):
        for f in self._list:
            del f.leap_month
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: leap_year
    # ----------------------------------------------------------------
    @property
    def leap_year(self):
        '''

The `~cf.Field.leap_year` CF property of each field.

``fl.leap_year`` is equivalent to ``cf.List(f.leap_year for f in
fl)``.

``fl.leap_year = value`` is equivalent to ``for f in fl: f.leap_year =
value``.

``del fl.leap_year`` is equivalent to ``for f in fl: del
f.leap_year``.

See `cf.Field.leap_year` for details.

'''
        return List([f.leap_year for f in self._list])    
    #--- End: def
    @leap_year.setter
    def leap_year(self, value): 
        for f in self._list:
            f.leap_year = value
    #--- End: def
    @leap_year.deleter
    def leap_year(self):
        for f in self._list:
            del f.leap_year
    #--- End: def


    # ----------------------------------------------------------------
    # CF property: long_name
    # ----------------------------------------------------------------
    @property
    def long_name(self):
        '''

The `~cf.Field.long_name` CF property of each field.

``fl.long_name`` is equivalent to ``cf.List(f.long_name for f in
fl)``.

``fl.long_name = value`` is equivalent to ``for f in fl: f.long_name =
value``.

``del fl.long_name`` is equivalent to ``for f in fl: del
f.long_name``.

See `cf.Field.long_name` for details.

'''
        return List([f.long_name for f in self._list])    
    #--- End: def
    @long_name.setter
    def long_name(self, value): 
        for f in self._list:
            f.long_name = value
    #--- End: def
    @long_name.deleter
    def long_name(self):
        for f in self._list:
            del f.long_name
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: missing_value
    # ----------------------------------------------------------------
    @property
    def missing_value(self):
        '''

The `~cf.Field.missing_value` CF property of each field.

``fl.missing_value`` is equivalent to ``cf.List(f.missing_value for f
in fl)``.

``fl.missing_value = value`` is equivalent to ``for f in fl:
f.missing_value = value``.

``del fl.missing_value`` is equivalent to ``for f in fl: del
f.missing_value``.

See `cf.Field.missing_value` for details.

'''
        return List([f.missing_value for f in self._list])    
    #--- End: def
    @missing_value.setter
    def missing_value(self, value): 
        for f in self._list:
            f.missing_value = value
    #--- End: def
    @missing_value.deleter
    def missing_value(self):
        for f in self._list:
            del f.missing_value
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: month_lengths
    # ----------------------------------------------------------------
    @property
    def month_lengths(self):
        '''

The `~cf.Field.month_lengths` CF property of each field.

``fl.month_lengths`` is equivalent to ``cf.List(f.month_lengths for f
in fl)``.

``fl.month_lengths = value`` is equivalent to ``for f in fl:
f.month_lengths = value``.

``del fl.month_lengths`` is equivalent to ``for f in fl: del
f.month_lengths``.

See `cf.Field.month_lengths` for details.

'''
        return List([f.month_lengths for f in self._list])    
    #--- End: def
    @month_lengths.setter
    def month_lengths(self, value): 
        for f in self._list:
            f.month_lengths = value
    #--- End: def
    @month_lengths.deleter
    def month_lengths(self):
        for f in self._list:
            del f.month_lengths
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: references
    # ----------------------------------------------------------------
    @property
    def references(self):
        '''

The `~cf.Field.references` CF property of each field.

``fl.references`` is equivalent to ``cf.List(f.references for f in
fl)``.

``fl.references = value`` is equivalent to ``for f in fl: f.references
= value``.

``del fl.references`` is equivalent to ``for f in fl: del
f.references``.

See `cf.Field.references` for details.

'''
        return List([f.references for f in self._list])    
    #--- End: def
    @references.setter
    def references(self, value): 
        for f in self._list:
            f.references = value
    #--- End: def
    @references.deleter
    def references(self):
        for f in self._list:
            del f.references
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: scale_factor
    # ----------------------------------------------------------------
    @property
    def scale_factor(self):
        '''

The `~cf.Field.scale_factor` CF property of each field.

``fl.scale_factor`` is equivalent to ``cf.List(f.scale_factor for f in
fl)``.

``fl.scale_factor = value`` is equivalent to ``for f in fl:
f.scale_factor = value``.

``del fl.scale_factor`` is equivalent to ``for f in fl: del
f.scale_factor``.

See `cf.Field.scale_factor` for details.

'''
        return List([f.scale_factor for f in self._list])    
    #--- End: def
    @scale_factor.setter
    def scale_factor(self, value): 
        for f in self._list:
            f.scale_factor = value
    #--- End: def
    @scale_factor.deleter
    def scale_factor(self):
        for f in self._list:
            del f.scale_factor
    #--- End: def


    # ----------------------------------------------------------------
    # CF property: source
    # ----------------------------------------------------------------
    @property
    def source(self):
        '''

The `~cf.Field.source` CF property of each field.

``fl.source`` is equivalent to ``cf.List(f.source for f in fl)``.

``fl.source = value`` is equivalent to ``for f in fl: f.source =
value``.

``del fl.source`` is equivalent to ``for f in fl: del f.source``.

See `cf.Field.source` for details.

'''
        return List([f.source for f in self._list])    
    #--- End: def
    @source.setter
    def source(self, value): 
        for f in self._list:
            f.source = value
    #--- End: def
    @source.deleter
    def source(self):
        for f in self._list:
            del f.source
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: standard_error_multiplier
    # ----------------------------------------------------------------
    @property
    def standard_error_multiplier(self):
        '''

The `~cf.Field.standard_error_multiplier` CF property of each field.

``fl.standard_error_multiplier`` is equivalent to
``cf.List(f.standard_error_multiplier for f in fl)``.

``fl.standard_error_multiplier = value`` is equivalent to ``for f in
fl: f.standard_error_multiplier = value``.

``del fl.standard_error_multiplier`` is equivalent to ``for f in fl:
del f.standard_error_multiplier``.

See `cf.Field.standard_error_multiplier` for details.

'''
        return List([f.standard_error_multiplier for f in self._list])    
    #--- End: def
    @standard_error_multiplier.setter
    def standard_error_multiplier(self, value): 
        for f in self._list:
            f.standard_error_multiplier = value
    #--- End: def
    @standard_error_multiplier.deleter
    def standard_error_multiplier(self):
        for f in self._list:
            del f.standard_error_multiplier
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: standard_name
    # ----------------------------------------------------------------
    @property
    def standard_name(self):
        '''

The `~cf.Field.standard_name` CF property of each field.

``fl.standard_name`` is equivalent to ``cf.List(f.standard_name for f
in fl)``.

``fl.standard_name = value`` is equivalent to ``for f in fl:
f.standard_name = value``.

``del fl.standard_name`` is equivalent to ``for f in fl: del
f.standard_name``.

See `cf.Field.standard_name` for details.

'''
        return List([f.standard_name for f in self._list])    
    #--- End: def
    @standard_name.setter
    def standard_name(self, value): 
        for f in self._list:
            f.standard_name = value
    #--- End: def
    @standard_name.deleter
    def standard_name(self):
        for f in self._list:
            del f.standard_name
    #--- End: def


    # ----------------------------------------------------------------
    # CF property: title
    # ----------------------------------------------------------------
    @property
    def title(self):
        '''

The `~cf.Field.title` CF property of each field.

``fl.title`` is equivalent to ``cf.List(f.title for f in fl)``.

``fl.title = value`` is equivalent to ``for f in fl: f.title =
value``.

``del fl.title`` is equivalent to ``for f in fl: del f.title``.

See `cf.Field.title` for details.

'''
        return List([f.title for f in self._list])    
    #--- End: def
    @title.setter
    def title(self, value): 
        for f in self._list:
            f.title = value
    #--- End: def
    @title.deleter
    def title(self):
        for f in self._list:
            del f.title
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: units
    # ----------------------------------------------------------------
    @property
    def units(self):
        '''

The `~cf.Field.units` CF property of each field.

``fl.units`` is equivalent to ``cf.List(f.units for f in fl)``.

``fl.units = value`` is equivalent to ``for f in fl: f.units =
value``.

``del fl.units`` is equivalent to ``for f in fl: del f.units``.

See `cf.Field.units` for details.

'''
        return List([f.units for f in self._list])    
    #--- End: def
    @units.setter
    def units(self, value): 
        for f in self._list:
            f.units = value
    #--- End: def
    @units.deleter
    def units(self):
        for f in self._list:
            del f.units
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: valid_max
    # ----------------------------------------------------------------
    @property
    def valid_max(self):
        '''

The `~cf.Field.valid_max` CF property of each field.

``fl.valid_max`` is equivalent to ``cf.List(f.valid_max for f in
fl)``.

``fl.valid_max = value`` is equivalent to ``for f in fl: f.valid_max =
value``.

``del fl.valid_max`` is equivalent to ``for f in fl: del
f.valid_max``.

See `cf.Field.valid_max` for details.

'''
        return List([f.valid_max for f in self._list])    
    #--- End: def
    @valid_max.setter
    def valid_max(self, value): 
        for f in self._list:
            f.valid_max = value
    #--- End: def
    @valid_max.deleter
    def valid_max(self):
        for f in self._list:
            del f.valid_max
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: valid_min
    # ----------------------------------------------------------------
    @property
    def valid_min(self):
        '''

The `~cf.Field.valid_min` CF property of each field.

``fl.valid_min`` is equivalent to ``cf.List(f.valid_min for f in
fl)``.

``fl.valid_min = value`` is equivalent to ``for f in fl: f.valid_min =
value``.

``del fl.valid_min`` is equivalent to ``for f in fl: del
f.valid_min``.

See `cf.Field.valid_min` for details.

'''
        return List([f.valid_min for f in self._list])    
    #--- End: def
    @valid_min.setter
    def valid_min(self, value): 
        for f in self._list:
            f.valid_min = value
    #--- End: def
    @valid_min.deleter
    def valid_min(self):
        for f in self._list:
            del f.valid_min
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: valid_range
    # ----------------------------------------------------------------
    @property
    def valid_range(self):
        '''

The `~cf.Field.valid_range` CF property of each field.

``fl.valid_range`` is equivalent to ``cf.List(f.valid_range for f in
fl)``.

``fl.valid_range = value`` is equivalent to ``for f in fl:
f.valid_range = value``.

``del fl.valid_range`` is equivalent to ``for f in fl: del
f.valid_range``.

See `cf.Field.valid_range` for details.

'''
        return List([f.valid_range for f in self._list])    
    #--- End: def
    @valid_range.setter
    def valid_range(self, value): 
        for f in self._list:
            f.valid_range = value
    #--- End: def
    @valid_range.deleter
    def valid_range(self):
        for f in self._list:
            del f.valid_range
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: subspace (read only)
    # ----------------------------------------------------------------
    @property
    def subspace(self):
        '''

Subspace each field in the list, returning a new list of fields.

Subspaing by data array indices or by coordinate values are both
allowed.

:Returns:

    out : cf.FieldList

**Examples**

>>> fl
[<CF Variable: air_temperature(73, 96)>,
 <CF Variable: air_temperature(73, 96)>]
>>> fl.subspace[0,0]
[<CF Variable: air_temperature(1,1)>,
 <CF Variable: air_temperature(1,1)>]
>>> fl.slice(longitude=0, latitude=0)
[<CF Variable: air_temperature(1,1)>,
 <CF Variable: air_temperature(1,1)>]

'''
        return SubspaceFieldList(self)
    #--- End: def

    def aux(self, *args, **kwargs):
        '''

Return an auxiliary coordinate object, or its domain identifier, from
each field.

``fl.aux(*args, **kwargs)`` is equivalent to ``cf.List(f.aux(*args,
**kwargs) for f in fl)``.

See `cf.Field.aux` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.aux`.
  
:Returns:

    out : cf.List   
        For each field, the unique auxiliary coordinate object or its
        domain identifier or, if there isn't a unique item, None.

'''
        return List([f.aux(*args, **kwargs) for f in self._list])
    #--- End: def

    def auxs(self, *args, **kwargs):
        '''

Return auxiliary coordinate objects from each field.

``fl.auxs(*args, **kwargs)`` is equivalent to ``cf.List(f.auxs(*args,
**kwargs) for f in fl)``.

See `cf.Field.auxs` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.auxs`.
  
:Returns:

    out : cf.List of dicts
        For each field, a dictionary whose keys are domain item
        identifiers with corresponding values of auxiliary coordinate
        objects.

'''
        return List([f.auxs(*args, **kwargs) for f in self._list])
    #--- End: def

    def axes(self, *args, **kwargs):
        '''

Return domain axis identifiers from each field.

``fl.axes(*args, **kwargs)`` is equivalent to ``cf.List(f.axes(*args,
**kwargs) for f in fl)``.

See `cf.Field.axes` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.axes`.
  
:Returns:

    out : cf.List
        For each field, a set of domain axis identifiers, or a list if
        ordered is True.
'''
        return List([f.axes(*args, **kwargs) for f in self._list])
    #--- End: def

    def binary_mask(self):
        '''

Return a binary (0 and 1) missing data mask for each field.

``fl.binary_mask()`` is equivalent to ``cf.List(f.binary_mask() for f
in fl)``.

See `cf.Field.binary_mask` for details.

:Returns:

    out : cf.FieldList
        The binary missing data mask for each field.

'''
        return List([f.binary_mask() for f in self._list])
    #--- End: def

    def chunk(self, *args, **kwargs):
        '''

Partition the data array of each field.

``fl.chunk(*args, **kwargs)`` is equivalent to ``for f in fl:
f.chunk(*args, **kwargs)``.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.chunk`.

:Returns:

    None

'''
        for f in self._list:
            f.chunk(*args, **kwargs)
    #--- End: def

    def clip(self, *args, **kwargs):
        '''

Clip (limit) the values in the data array of each field in place.

``fl.clip(*args, **kwargs)`` is equivalent to ``for f in fl:
f.clip(*args, **kwargs)``.

See `cf.Field.clip` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.clip`.

:Returns:

    None

'''
        for f in self._list:
            f.clip(*args, **kwargs)
    #--- End: def

    def close(self):
        '''

Close all referenced open data files from each field.

``fl.close()`` is equivalent to ``for f in fl: f.close()``.

See `cf.Field.close` for details.

:Returns:

    None

'''  
        for f in self._list:
            f.close()
    #--- End: def

    def cm(self, *args, **kwargs):
        '''

Return a cell measure object, or its domain identifier, from each
field.

``fl.cm(*args, **kwargs)`` is equivalent to ``cf.List(f.cm(*args,
**kwargs) for f in fl)``.

See `cf.Field.cm` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.cm`.
  
:Returns:

    out : cf.List   
        For each field, the unique cell measure object or its domain
        identifier or, if there isn't a unique item, None.

'''
        return List([f.cm(*args, **kwargs) for f in self._list])
    #--- End: def

    def cms(self, *args, **kwargs):
        '''

Return cell measure objects from each field.

``fl.cms(*args, **kwargs)`` is equivalent to ``cf.List(f.cms(*args,
**kwargs) for f in fl)``.

See `cf.Field.cms` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.cms`.
  
:Returns:

    out : cf.List of dicts
        For each field, a dictionary whose keys are domain item
        identifiers with corresponding values of cell measure objects.

'''
        return List([f.cms(*args, **kwargs) for f in self._list])
    #--- End: def

    def coord(self, *args, **kwargs):
        '''

Return a dimension or auxiliary coordinate object, or its domain
identifier, from each field.

``fl.coord(*args, **kwargs)`` is equivalent to
``cf.List(f.coord(*args, **kwargs) for f in fl)``.

See `cf.Field.coord` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.coord`.
  
:Returns:

    out : cf.List   
        For each field, the unique dimension or auxiliary coordinate
        object or its domain identifier or, if there isn't a unique
        item, None.

'''
        return List([f.coord(*args, **kwargs) for f in self._list])
    #--- End: def

    def coords(self, *args, **kwargs):
        '''

Return dimension or auxiliary coordinate objects from each field.

``fl.coords(*args, **kwargs)`` is equivalent to
``cf.List(f.coords(*args, **kwargs) for f in fl)``.

See `cf.Field.coords` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.coords`.
  
:Returns:

    out : cf.List of dicts
        For each field, a dictionary whose keys are domain item
        identifiers with corresponding values of dimension or
        auxiliary coordinate objects.

'''
        return List([f.coords(*args, **kwargs) for f in self._list])
    #--- End: def

    def copy(self):
        '''

Return a deep copy.

``f.copy()`` is equivalent to ``copy.deepcopy(f)``.

:Returns:

    out : 
        The deep copy.

**Examples**

>>> g = f.copy()

'''
        new = type(self)()

        new_list = new._list
        for f in self._list:
            new_list.append(f.copy())
        
        return new
    #--- End: def

    def cos(self, i=False):
        '''

Take the trigonometric cosine of the data array of each field.

``fl.cos(....)`` is equivalent to ``cf.FieldList(f.cos(....) for f in
fl)``.

See `cf.Field.cos` for details.

:Returns:

    out : cf.FieldList

'''
        if i:
            fl = self
        else:
            fl = self.copy()

        for f in fl._list:
            f.cos(i=True)

        return fl
    #--- End: def

    def data_axes(self):
        '''

Return the axes of a domain item from each field.

``fl.data_axes()`` is equivalent to ``cf.List(f.data_axes() for f in
fl)``.

See `cf.Field.data_axes` for details.
  
:Return:

    out : cf.List
        For each field, the ordered list of data axes, or, if there is
        no data array, None.

'''
        return List([f.data_axes() for f in self._list])
    #--- End: def

    def datum(self, *index):
        '''

Return an element of the data array of each field as a standard Python
scalar.

``fl.datum(*index)`` is equivalent to ``cf.List(f.datum(*index) for f
in fl)``.

See `cf.Field.datum` for details.
  
:Return:

    out : cf.FieldList
        For each field, the requested element of the data array.

'''
        return type(self)([f.datum(*index) for f in self._list])
    #--- End: def

    def dim(self, *args, **kwargs):
        '''

Return a dimension coordinate object, or its domain identifier, from
each field.

``fl.dim(*args, **kwargs)`` is equivalent to ``cf.List(f.dim(*args,
**kwargs) for f in fl)``.

See `cf.Field.dim` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.dim`.
  
:Returns:

    out : cf.List   
        For each field, the unique dimension coordinate object or its
        domain identifier or, if there isn't a unique item, None.

'''
        return List([f.dim(*args, **kwargs) for f in self._list])
    #--- End: def

    def dims(self, *args, **kwargs):
        '''

Return dimension coordinate objects from each field.

``fl.dims(*args, **kwargs)`` is equivalent to ``cf.List(f.dims(*args,
**kwargs) for f in fl)``.

See `cf.Field.dims` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.dims`.
  
:Returns:

    out : cf.List of dicts
        For each field, a dictionary whose keys are domain item
        identifiers with corresponding values of dimension coordinate
        objects.

'''
        return List([f.dims(*args, **kwargs) for f in self._list])
    #--- End: def

    def delprop(self, prop):
        '''

Delete a CF property from each field.

``fl.delprop(prop)`` is equivalent to ``for f in fl:
f.delprop(prop)``.

:Parameters:

    prop : str
        The name of the CF property.

:Returns:

    None

.. seealso:: `cf.Field.delprop`, `getprop`, `hasprop`, `setprop`

'''
        for f in self._list:
            f.delprop(prop)
    #--- End: def
    
    def expand_dims(self, position=0, axis=None, i=False, **kwargs):
        '''

Insert a size 1 axis into the data array of each field in place.

``fl.expand_dims(....)`` is equivalent to
``cf.FieldList(f.expand_dims(....) for f in fl)``.

See `cf.Field.expand_dims` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.expand_dims`.
  
:Returns:

    out : cf.FieldList

'''
        if i:
            fl = self
        else:
            fl = self.copy()

        for f in fl._list:
            f.expand_dims(position=position, axis=axis, i=True, **kwargs)

        return fl
    #--- End: def

    def equals(self, other, rtol=None, atol=None, traceback=False):
        '''

'''
        if self is other:
            return True
         
        # Check that each instance is the same type
        if self.__class__ != other.__class__:
            if traceback:
                print("%s: Different types: %s, %s" %
                      (self.__class__.__name__,
                       self.__class__.__name__,
                       other.__class__.__name__))
            return False
        #--- End: if

        # Check that the lists have the same number of fields
        if len(self) != len(other): 
            if traceback:
                print("%s: Different numbers of fields: %d, %d" %
                      (self.__class__.__name__, len(self), len(other)))
            return False
        #--- End: if

        if rtol is None:
            rtol = RTOL()
        if atol is None:
            atol = ATOL()

        # Check the fields pairwise
        for f, g in zip(self._list, other._list):
            if not f.equals(g, rtol=rtol, atol=atol, traceback=traceback):
                if traceback:
                    print("%s: Different fields: %r, %r" %
                          (self.__class__.__name__, f, g))
                return False
        #--- End: for

        # Still here? Then we have equality.
        return True
    #--- End: def
 
    def dump(self, complete=False, display=True, level=0, title='Field', q='='):
        '''

Return a full description of each the field.

See `cf.Field.dump` for details.

:Parameters:

:Returns:

    out : None or cf.List
        The description of each field.

'''
        if display:
            for f in self._list:
                f.dump(complete=complete, display=True, level=level,
                       title=title, q=q)
        else:
            return List([f.dump(complete=complete, display=False, level=level,
                                title=title, q=q)
                         for f in self._list])
    #--- End: def

    def fill_value(self):
        '''

Return the data array missing data value for each field.

``fl.fill_value()`` is equivalent to ``cf.List(f.fill_value() for f in
fl)``.

See `cf.Field.fill_value` for details.

:Returns:

    out :
        For each field, the missing data value, or None if one hasn't
        been set.

'''
        return List([f.fill_value() for f in self._list])
    #--- End: def
                   
    def flip(self, axes=None, i=False, **kwargs):
        '''

Flip axes the data array of each field.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.flip`.
  
:Returns:

    out : cf.FieldList

'''
        if i:
            fl = self
        else:
            fl = self.copy()

        for f in fl._list:
            f.flip(axes=axes, i=True, **kwargs)

        return fl
    #--- End: def

    def getprop(self, prop, *default):
        '''

Get a CF property from each field.

When a default argument is given, it is returned when the attribute
doesn't exist; without it, an exception is raised in that case.

``fl.getprop(prop, *default)`` is equivalent to
``cf.List(f.getprop(prop, *default) for f in fl)``.

:Parameters:

    prop : str
        The name of the CF property.

    default : optional
        Return *default* if and only if the variable does not have the
        named property.

:Returns:

    out : cf.List
        The value of the named property, or the default value, for
        each field.

.. seealso:: `cf.Field.getprop`, `delprop`, `hasprop`, `setprop`

'''
        return List([f.getprop(prop, *default) for f in self._list])
    #--- End: def

    def hasprop(self, prop):
        '''

For each field, return True if a CF property exists, otherise False.

``fl.hasprop(prop)`` is equivalent to ``cf.List(f.hasprop(prop) for f
in fl)``.

:Parameters:

    prop : str
        The name of the property.

:Returns:

    out : cf.List
        True or False for each field.

.. seealso:: `cf.Field.hasprop`, `delprop`, `getprop`, `setprop`

'''
        return List([f.hasprop(prop) for f in self._list])
    #--- End: def

    def identity(self, default=None):
        '''

Return the identity of each field.

``fl.identity()`` is equivalent to ``cf.List(f.identity() for f in
fl)``.

See `cf.Field.identity` for details.

:Parameters:

    default : *optional*
        See `cf.Field.identity`.
  
:Returns:

    out :
        For each field, its identity.

'''
        return List([f.identity(default) for f in self._list])
    #--- End: def

    def indices(self, *exact, **kwargs):
        '''

For each field, return the data array indices which correspond to item
values.

``fl.indices(*exact, **kwargs)`` is equivalent to
``cf.List(f.indices(*exact, **kwargs) for f in fl)``.

See `cf.Field.indices` for details.

:Parameters:

    exact, kwargs : *optional*
        See `cf.Field.indices`.
  
:Returns:

    out : cf.List of tuples
        
'''
        return List([f.indices(*exact, **kwargs) for f in self._list])
    #--- End: def

    def insert_data(self, *args, **kwargs):
        '''

Insert a new data array into the only field in place.

An exception is raised if there is not exactly one field in the list.

``fl.insert_data(*args, **kwargs)`` is equivalent to ``if len(fl) ==
1: fl[0].insert_data(*args, **kwargs)``.

See `cf.Field.insert_data` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.insert_data`.
  
:Returns:

    None
        
'''
        if len(self._list) == 1:
            self._list[0].insert_data(*args, **kwargs)
        else:
            raise TypeError(
                "Can only insert data for field lists with one element")
    #--- End: def

    def inspect(self):
        '''

Inspect the object for debugging.

.. seealso:: `cf.inspect`

:Returns: 

    None

'''
        print cf_inspect(self)
    #--- End: def

    def item(self, *args, **kwargs):
        '''

Return a domain item, or its domain identifier, from each field.

``fl.item(*args, **kwargs)`` is equivalent to ``cf.List(f.item(*args,
**kwargs) for f in fl)``.

See `cf.Field.item` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.item`.
  
:Returns:

    out : cf.List
        For each field, the item or its domain identifier or, if there
        isn't a unique item, None.

'''
        return List([f.item(*args, **kwargs) for f in self._list])
    #--- End: def

    def item_axes(self, item=None, **kwargs):
        '''

Return the axes of a domain item from each field.

``fl.item_axes(*args, **kwargs)`` is equivalent to
``cf.List(f.item_axes(*args, **kwargs) for f in fl)``.

See `cf.Field.item_axes` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.item_axes`.
  
:Return:

    out : cf.List
        For each field, the ordered list of item axes, or, if there
        isn't a unique item or the item is a transform, None.

'''
        return List([f.item_axes(*args, **kwargs) for f in self._list])
    #--- End: def

    def items(self, *args, **kwargs):
        '''

Return domain items from each field.

``fl.items(*args, **kwargs)`` is equivalent to
``cf.List(f.items(*args, **kwargs) for f in fl)``.

See `cf.Field.items` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.items`.
  
:Returns:

    out : cf.List of dicts
        For each field, a dictionary whose keys are domain item
        identifiers with corresponding values of items of the
        domain.

'''
        return List([f.items(*args, **kwargs) for f in self._list])
    #--- End: def

    def iter(self, name, *args, **kwargs):
        '''

Return an iterator over the results of a method applied to the field
list.

``fl.iter(name, *args, **kwargs)`` is equivalent to
``iter(getattr(fl, name)(*args, **kwargs))``.


:Parameters:

    name : str
        The name of the method to apply to each element.

    args, kwargs : *optional*
        The arguments to be used in the call to the named method.

:Returns:

    out : generator
        An iterator over the results of the named method applied to
        each element.

.. seealso:: `method`

**Examples**

>>> fl.getprop('standard_name')
['air_temperature', 'eastward_wind']
>>> for x in fl.iter('getprop', 'standard_name'):
...     print x
...
'air_temperature'
'eastward_wind'

>>> fl.iter('squeeze')
[None, None]

'''
        return iter(getattr(self, name)(*args, **kwargs))
    #--- End: def

    def match(self, *args, **kwargs):
        '''

Test whether or not each field satisfies the given conditions.

``fl.match(*args, **kwargs)`` is equivalent to
``cf.List(f.match(*args, **kwargs) for f in fl)``.

See `cf.Field.match` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.match`.
  
:Returns:

    out : cf.List of bools
        For each field, True if it satisfies the given criteria, False
        otherwise.

'''
        return List([f.match(*args, **kwargs) for f in self._list])
    #--- End: def

    def method(self, name, *args, **kwargs):
        '''

Return the results of a method applied to each field.

``fl.method(name, *args, **kwargs)`` is equivalent to ``fl.name(*args,
**kwargs)``.

:Parameters:

    name : str
        The name of the method to apply to each element.

    args, kwargs : *optional*
        The arguments to be used in the call to the named method.

:Returns:

    out : cf.FieldList or cf.List  
        The results of the named method applied to each element.

.. seealso:: `iter`

**Examples**

>>> fl.getprop('standard_name')
['air_temperature', 'eastward_wind']
>>> fl.method('getprop', 'standard_name')
['air_temperature', 'eastward_wind']

>>> fl.method('squeeze')
[None, None]

'''
        return getattr(self, name)(*args, **kwargs)
    #--- End: def

    def name(self, default=None, identity=False):
        '''

Return a name for each field.

``fl.name()`` is equivalent to ``cf.List(f.name() for f in fl)``.

See `cf.Field.name` for details.

:Parameters:

    default : *optional*
        See `cf.Field.name`.

    identity : bool, optional
        See `cf.Field.name`.
  
:Returns:

    out : cf.List
       For each field, a name.

'''
        return List([f.name(default, identity=identity) for f in self._list])
    #--- End: def

    def override_units(self, *args, **kwargs):
        '''

Override the data array units in place.

``fl.override_units(*args, **kwargs)`` is equivalent to ``for f in fl:
f.override_units(*args, **kwargs)``.

See `cf.Field.override_units` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.override_units`.
  
:Returns:

    out : None

'''
        for f in self._list:
            f.override_Units(*args, **kwargs)
    #--- End: def

    def remove_axes(self,*args, **kwargs):
        '''
    
Remove and return axes from each field.

``fl.remove_axes(*args, **kwargs)`` is equivalent to
``cf.List(f.remove_axes(*args, **kwargs) for f in fl)``.

See `cf.Field.remove_axes` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.remove_axes`.
  
:Returns:

    out : cf.List
    
'''
        return List([f.remove_axes(*args, **kwargs) for f in self._list])
    #--- End: def
    
    def remove_axis(self, *args, **kwargs):
        '''
    
Remove and return an axis from each field.

``fl.remove_axis(*args, **kwargs)`` is equivalent to
``cf.List(f.remove_axis(*args, **kwargs) for f in fl)``.

See `cf.Field.remove_axis` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.remove_axis`.
  
:Returns:

    out : cf.List
    
'''
        return List([f.remove_axis(*args, **kwargs) for f in self._list])
    #--- End: def
    
    def remove_data(self):
        '''

Remove and return the data array from each field.

``fl.remove_data()`` is equivalent to ``cf.List(f.remove_data() for f
in fl)``.

See `cf.Field.remove_data` for details.

:Returns:

    out : cf.List
        For each field, its data array, or None if it doesn't have
        one.

'''
        return List([f.remove_data() for f in self._list])
    #--- End: def

    def remove_item(self, *args, **kwargs):
        '''

Remove and return a domain item from each field.

An item is either a dimension coordinate, an auxiliary coordinate, a
cell measure or a transform object of the domain.

``fl.remove_item(*args, **kwargs)`` is equivalent to
``cf.List(f.remove_item(*args, **kwargs) for f in fl)``.

See `cf.Field.remove_item` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.remove_item`.
  
:Returns:

    out : cf.List
        For each field, the removed item, or None if no unique item
        could be found.

'''
        return List([f.remove_item(*args, **kwargs) for f in self._list])
    #--- End: def

    def remove_items(self, *args, **kwargs):
        '''

Remove and return domain items from each field.

An item is either a dimension coordinate, an auxiliary coordinate, a
cell measure or a transform object of the domain. 

``fl.remove_items(*args, **kwargs)`` is equivalent to
``cf.List(f.remove_items(*args, **kwargs) for f in fl)``.

See `cf.Field.remove_items` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.remove_items`.
  
:Returns:

    out : cf.List of lists
        For each field, a list of the removed items.

'''
        return List([f.remove_items(*args, **kwargs) for f in self._list])
    #--- End: def

    def set_equals(self, other, rtol=None, atol=None, traceback=False):
        '''

True if two instances are set-wise equal, False otherwise.

Two instances are set-wise equal if their attributes are equal and
their elements are equal set-wise (i.e. the order of the lists is
irrelevant).

:Parameters:

    other : 
        The object to compare for equality.

    atol : float, optional
        The absolute tolerance for all numerical comparisons, By
        default the value returned by the `ATOL` function is used.

    rtol : float, optional
        The relative tolerance for all numerical comparisons, By
        default the value returned by the `RTOL` function is used.

    traceback : bool, optional
        If True then print a traceback highlighting where the two
        instances differ.

:Returns: 

    out : bool
        Whether or not the two instances are equal.

**Examples**


'''
        # Check for object identity
        if self is other:
            return True

        # Check that each instance is the same type
        if self.__class__ != other.__class__:
            if traceback:
                print("%s: Different type: %s, %s" %
                      (self.__class__.__name__,
                       self.__class__.__name__,
                       other.__class__.__name__))
            return False
        #--- End: if

        # Check for the same number of fields
        if len(self) != len(other):
            if traceback:
                print("%s: Different numbers of fields" %
                      self.__class__.__name__)
            return False
        #--- End: if
        
        # Group the fields by identity
        self_identity  = {}
        other_identity = {}        
        for fl, d in zip((self, other),
                         (self_identity, other_identity)):
            for f in fl:
                identity = f.identity()
                if identity in d:
                    d.append(f)
                else:
                    d[identity] = [f]
        #--- End: def

        # Check that there are the same identities
        if set(self_identity) != set(other_identity):
            if traceback:
                print("%s: Different identities: %s, %s" %
                      (self.__class__.__name__,
                       set(self_identity), set(other_identity)))
            return False
        #--- End: if

        # Check that there are the same number of fields for each
        # identity
        for identity, lf in self_identity.iteritems():
            lg = other_identity[identity]
            if len(lf) != len(lg):
                if traceback:
                    print("%s: Different numbers of %r fields: %d, %d" %
                          (self.__class__.__name__, identity,
                           len(lf), len(lg)))
                return False
        #--- End: for
            
        # For each identity, check that there are matching pairs of
        # equal fields.
        for identity, lf in self_identity.iteritems():
            lg = other_identity[identity]

            found_match = False
            for f in lf:
                for i, g in enumerate(lg):
                    if f.equals(g, rtol=rtol, atol=atol, traceback=False):
                        found_match = True
                        break
                #--- End: for
                if found_match:
                    lg.remove(i)
                    continue
                elif traceback:                        
                    print("%s: No equal field: %r" %
                          (self.__class__.__name__, f))
                    return False
            #--- End: for
        #--- End: for

        # Still here? Then we have set equality.
        return True
    #--- End: def

    def setdata(self, *args, **kwargs):
        '''

Set data array elements depending on a condition.

``fl.setdata(*args, **kwargs)`` is equivalent to ``for f in fl:
f.setdata(*args, **kwargs)``.

See `cf.Field.setdata` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.setdata`.
    
:Returns:

    None

'''
        for f in self._list:
            f.setdata(*args, **kwargs)
    #--- End: def

    def setprop(self, prop, value):
        '''

Set a CF property on each field.

``fl.setprop(prop, value)`` is equivalent to ``for f in fl:
f.setprop(prop, value)``.

:Parameters:

    prop : str
        The name of the CF property.

    value :
        The value for the property.
  
:Returns:

    out : None

.. seealso:: `cf.Field.setprop`, `delprop`, `getprop`, `hasprop`

'''
        for f in self._list:
            f.setprop(prop, value)
    #--- End: def

    def select(self, match={}, cvalue={}, csize={}, rank=None, exact=False,
               match_all=True, regex=False, inverse=False):
        '''

Return the fields which satisfy the given conditions.

The conditions are defined in the same manner as for `cf.Field.match`,
which tests whether or not a field satisfies the given conditions.

``fl.select(**kwargs)`` is equivalent to ``cf.FieldList(f for f in fl
if f.match(**kwargs))``.

:Parameters:

    match : *optional*
        Set conditions on field property values. See the *match*
        parameter of `cf.Field.match` for details.

    cvalue : dict, optional
        A dictionary which identifies field coordinate objects with
        corresponding tests on their data array values. See the
        *cvalue* parameter of `cf.Field.match` for details.
      
    csize :  dict, optional
        A dictionary which identifies field coordinate objects with
        corresponding tests on their cell sizes. See the *csize*
        parameter of `cf.Field.match` for details.

    rank : int or cf.Comparison, optional
        Specify a condition on field domain rank, where the domain
        rank is the number of domain axes. See the *rank* parameter of
        `cf.Field.match` for details.

    exact : bool, optional
        The *exact* argument applies to the interpretion of particular
        conditions given by values of the *match* argument and by keys
        of the *cvalue* and *csize* arguments. See the *exact*
        parameter of `cf.Field.match` for details.
     
    match_all : bool, optional
        The *match_all* argument applies to the interpretion of all
        conditions (properties, coordinate object values, coordinate
        object cell sizes, domain rank and data array rank). See the
        *match_all* parameter of `cf.Field.match` for details.

    regex : bool, optional
        The *regex* argument applies to the interpretion of
        string-valued conditions. See the *regex* parameter of
        `cf.Field.match` for details.

    inverse : bool, optional
        If True then a field matches field if it does not satisfy the
        given conditions. By default a field matches field if it
        satisfies the given conditions.

:Returns:

    out : cf.FieldList
        Each field that matches the given conditions, or if *inverse*
        is True, doesn't match the conditions.

.. seealso:: `match`

**Examples**

'''
        return type(self)([f for f in self._list
                           if f.match(match=match, cvalue=cvalue,
                                      csize=csize, rank=rank,
                                      exact=exact, match_all=match_all,
                                      regex=regex, inverse=inverse)])
    #--- End: def

    def sin(self, i=False):
        '''

Take the trigonometric sine of the data array of each field.

``fl.sin(....)`` is equivalent to ``cf.FieldList(f.sin(....) for f in
fl)``.

See `cf.Field.sin` for details.

:Returns:

    out : cf.FieldList

'''
        if i:
            fl = self
        else:
            fl = self.copy()

        for f in fl._list:
            f.sin(i=True)

        return fl
    #--- End: def

    def sort(self, cmp=None, key=None, reverse=False):
        '''

Sort the fields in place.

This method has exactly the same functionality as the built-in
`!list.sort` method.

The sort is stable in that if multiple fields have the same *key*
value then their original order is preserved.

:Parameters:

    cmp : *optional*
       See the built-in `!list`.

    key : *optional*
       See the built-in `!list`.

    reverse : bool, optional
       See the built-in `!list`.

:Returns:

    None

**Examples**

Two ways of sorting by standard name:

>>> fl.sort(key=lambda f: f.standard_name)

>>> from operator import attrgetter
>>> fl.sort(key=attrgetter('standard_name'))

Place the fields in reverse standard name order:

>>> from operator import attrgetter
>>> fl.sort(key=attrgetter('standard_name'), reverse=True)

Sort by standard name then by the value of the first element of the
data array:

>>> from operator import attrgetter
>>> fl.sort(key=attrgetter('standard_name', 'first_datum'))

Sort by standard name then by the value of the second element of the
data array:

>>> fl.sort(key=lambda f: (f.standard_name, f.subspace[..., 1].first_datum))

'''
        self._list.sort(cmp=cmp, key=key, reverse=reverse)
    #--- End: def

    def squeeze(self, axes=None, i=False, **kwargs):
        '''

Insert size 1 axes into the data array of each field.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.squeeze`.
  
:Returns:

    out : cf.FieldList

'''
        if i:
            fl = self
        else:
            fl = self.copy()

        for f in fl._list:
            f.squeeze(axes=axes, i=True, **kwargs)

        return fl
    #--- End: def

    def setitem(self, *args, **kwargs):
        raise NotImplementedError(
"cf.%s.setitem is dead. Use cf.%s.subspace or cf.%s.setdata instead." % 
(self.__class__.__name__, self.__class__.__name__, self.__class__.__name__))

    def setmask(self, *args, **kwargs):
        raise NotImplementedError(
"cf.%s.setmask is dead. Use cf.%s.subspace or cf.%s.setdata instead." %
(self.__class__.__name__, self.__class__.__name__, self.__class__.__name__))

    def subset(self, *args, **kwargs):
        raise NotImplementedError(
"cf.%s.subset is dead. Use cf.%s.select instead." %
(self.__class__.__name__, self.__class__.__name__))

    def tan(self, i=False):
        '''

Take the trigonometric tangent of the data array of each field.

``fl.tan(....)`` is equivalent to ``cf.FieldList(f.tan(....) for f in
fl)``.

See `cf.Field.tan` for details.

:Returns:

    out : cf.FieldList

'''
        if i:
            fl = self
        else:
            fl = self.copy()

        for f in fl._list:
            f.tan(i=True)

        return fl
    #--- End: def

    def transform(self, *args, **kwargs):
        '''

Return a transform object, or its domain identifier, from
each field.

``fl.transform(*args, **kwargs)`` is equivalent to
``cf.List(f.transform(*args, **kwargs) for f in fl)``.

See `cf.Field.transform` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.transform`.
  
:Returns:

    out : cf.List   
        For each field, the unique transform object or its domain
        identifier or, if there isn't a unique item, None.

'''
        return List([f.transform(*args, **kwargs) for f in self._list])
    #--- End: def

    def transforms(self, *args, **kwargs):
        '''

Return transform objects from each field.

``fl.transforms(*args, **kwargs)`` is equivalent to
``cf.List(f.transforms(*args, **kwargs) for f in fl)``.

See `cf.Field.transforms` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.transforms`.
  
:Returns:

    out : cf.List of dicts
        For each field, a dictionary whose keys are domain item
        identifiers with corresponding values of transform objects.

'''
        return List([f.transforms(*args, **kwargs) for f in self._list])
    #--- End: def

    def transpose(self, axes=None, i=False, **kwargs):
        '''

Permute the dimensions of the data array of each field in place.

``fl.transpose(....)`` is equivalent to
``cf.FieldList(f.transpose(....) for f in fl)``.

See `cf.Field.transpose` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.transpose`.
  
:Returns:

    out : cf.FieldList

'''
        if i:
            fl = self
        else:
            fl = self.copy()

        for f in fl._list:
            f.transpose(axes=axes, i=True, **kwargs)

        return fl
    #--- End: def

    def unsqueeze(self, axes=None, i=False, **kwargs):
        '''

Insert size 1 axes into the data array of each field.

``fl.unsqueeze(....)`` is equivalent to
``cf.FieldList(f.unsqueeze(....) for f in fl)``.

See `cf.Field.unsqueeze` for details.

:Parameters:

    args, kwargs : *optional*
        See `cf.Field.unsqueeze`.
  
:Returns:

    out : cf.FieldList

'''
        if i:
            fl = self
        else:
            fl = self.copy()

        for f in fl._list:
            f.unsqueeze(axes=axes, i=True, **kwargs)

        return fl
    #--- End: def

#--- End: class

## ====================================================================
##
## List of Fields slice object
##
## ====================================================================
#
#class SubspaceFieldList(SubspaceVariableList):
#    '''
#
#'''
#    __slots__ = []
#
#    def __call__(self, *arg, **kwargs):
#        '''
#
#Slice by fields by coordinate value.
#
#'''
#        fieldlist = self.variablelist
#        return type(fieldlist)([f.subspace(*arg, **kwargs) for f in fieldlist])
#    #--- End: def
#
##--- End: class

# ====================================================================
#
# List of Fields slice object
#
# ====================================================================

class SubspaceFieldList(object):
    '''

'''
    __slots__ = ('fieldlist',)

    def __init__(self, fieldlist):
        '''

Set the contained list of fields.

'''
        self.fieldlist = fieldlist
    #--- End: def

    def __call__(self, *args, **kwargs):
        '''

x.__call__(*args, **kwargs) <==> x(*args, **kwargs)

'''
        fieldlist = self.fieldlist
        return type(fieldlist)([f.subspace(*args, **kwargs)
                                for f in fieldlist._list])
    #--- End: def

    def __getitem__(self, index):
        '''
x.__getitem__(index) <==> x[index]

'''
        fieldlist = self.fieldlist
        return type(fieldlist)([f.subspace[index] for f in fieldlist._list])
    #--- End: def

    def __setitem__(self, index, value):
        '''
x.__setitem__(index, value) <==> x[index]=value

'''
        for f in self.fieldlist._list:
            f.subspace[index] = value
    #--- End: def

#--- End: class
