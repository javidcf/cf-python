import collections

from copy import deepcopy
from re import sub    as re_sub
from re import search as re_search

from .utils     import List, Dict
from .functions import equals

from .data.data import Data

# ====================================================================
#
# CellMethods object
#
# ====================================================================

class CellMethods(collections.MutableSequence):
    '''

A CF cell methods object to describe the characteristic of a field
that is represented by cell values.

Each cell method is stored in a dictionary and these dictionaries are
stored in a list-like object. Similarly to a CF cell_methods string,
the order of cell methods in the list is important.

The dictionary representing each cell method recognizes the following
keys (where all keys are optional; the referenced sections are from
the NetCDF Climate and Forecast (CF) Metadata Conventions and words in
double quotes ("...") refer to CF cell_methods components in the
referenced sections):


**Statistics for a combination of axes**

The names of the dimensions involved in a combination of axes are
given as a list (although their order is immaterial), with
corresponding dimension identifiers:

>>> c
<CF CellMethods: latitude: longitude: mean>
>>> list(c)
[{'method'   : 'mean',
  'name'     : ['latitude', 'longitude'],     
  'dim'      : ['dim2, 'dim3']}]

If the string 'area' is used to indicate variation over horizontal
area, then the corresponding dimension is None:

>>> c
<CF CellMethods: area: mean>
>>> list(c)
[{'method'   : 'mean',
  'name'     : ['area'],     
  'dim'      : [None]}]


**Cell methods when there are no coordinates**

These have name strings which are standard names or the string 'area',
with corresponding dimension identifiers of None:

>>> c
<CF CellMethods: time: max>
>>> list(c)
[{'method'   : 'max',
  'name'     : ['time'],     
  'dim'      : [None]}]


>>> c
<CF CellMethods: lat: longitude: mean>
>>> list(c)
[{'method'   : 'mean',
  'name'     : ['lat', 'longitude','],     
  'dim'      : ['dim1', None]}]

>>> c
<CF CellMethods: area: mean>
>>> list(c)
[{'method'   : 'minimum',
  'name'     : ['area'],     
  'dim'      : [None]}]

In the case of 'area', there is no distinction between this cell
method and one for which there appropriate dimensions exist, but the
two cases may be discerned by inspection of the field's domain.

'''

    def __init__(self, cell_methods=None):
        '''

**Initialization**

:Parameters:

    string : str, optional
        Initialize new instance from a CF-netCDF-like cell methods
        string. See the `parse` method for details. By default an
        empty cell methods is created.

**Examples**

>>> c = CellMethods()
>>> c = CellMethods('time: max: height: mean')

'''               
        if not cell_methods:
            self._list = []
        elif isinstance(cell_methods, basestring):
            self._list = []
            self.parse(cell_methods)
        else:
            self._list = list(cell_methods)

#        super(CellMethods, self).__init__()
#
#        if cell_methods is None:
#            return
#        elif isinstance(cell_methods, basestring):
#            self.parse(cell_methods)
#        else:
#            for cell_method in cell_methods:
#                self._list.append(cell_method.copy())
    #--- End: def

    def __delitem__(self, index):
        '''

x.__delitem__(index) <==> del x[index]

'''
        del self._list[index]
    #--- End: def

    def __deepcopy__(self, memo):
        '''
Used if copy.deepcopy is called on the variable.

'''
        return self.copy()
    #--- End: def

    def __getitem__(self, index):
        '''

x.__getitem__(index) <==> x[index]

'''     
        if isinstance(index, (int, long)):
            # index is an integer so return the element
            return self._list[index]
        else:
            # index is a slice, so return a List of the elements
            return type(self)(self._list[index])

#        out = self._list[index]
#        try:
#            int(index)
#        except TypeError:
#            # index is a slice instance, so return a CfList instance
#            out = type(self)(out)
#
#        return out
    #--- End: def

    def __hash__(self):
        '''
x.__hash__() <==> hash(x)

'''
        return hash(self.string)
    #--- End: if

    def __len__(self):
        '''

x.__len__() <==> len(x)

'''
        return len(self._list)
    #--- End: def

    def __repr__(self):
        '''
x.__repr__() <==> repr(x)

'''
        return '<CF %s: %s>' % (self.__class__.__name__, 
                                ' '.join(self.strings()))
    #--- End: def

    def __setitem__(self, index, value):        
        '''

x.__setitem__(index, value) <==> x[index]=value

'''
        if not isinstance(value, self.__class__):
            self._list[index] = value
        else:
            self._list[index] = value._list
    #--- End: def

    def __str__(self):
        '''
x.__str__() <==> str(x)

'''        
        return ' '.join(self.strings())
    #--- End: def

    def __eq__(self, other):
        '''

x.__eq__(y) <==> x==y <==> x.equals(y)

'''
        return self.equals(other)
    #--- End: def

    def __ne__(self, other):
        '''

x.__ne__(y) <==> x!=y <==> not x.equals(y)

'''
        return not self.__eq__(other)
    #--- End: def

    def __add__(self, other):
        '''

x.__add__(y) <==> x+y

'''
        new = self.copy()
        new.extend(other)
        return new
    #--- End: def

    def __mul__(self, other):
        '''

x.__mul__(n) <==> x*n

'''
        return type(self)(self._list * other)
    #--- End: def

    def __rmul__(self, other):
        '''

x.__rmul__(n) <==> n*x

'''
        return self * other
    #--- End: def

    def __iadd__(self, other):
        '''

x.__iadd__(y) <==> x+=y

'''
        self.extend(other)
        return self
    #--- End: def

    def __imul__(self, other):
        '''

x.__imul__(n) <==> x*=n

'''
        self._list = self._list * other
        return self
    #--- End: def

    @property
    def string(self):
        '''

'''
        return ' '.join(self.strings())
    #--- End: def

    def copy(self):
        '''

Return a deep copy.

``c.copy()`` is equivalent to ``copy.deepcopy(c)``.

:Returns:

    out : 
        The deep copy.

**Examples**

>>> gl = fl.copy()

'''
        return type(self)(deepcopy(self._list))
    #--- End: def

    def dump(self, display=True, prefix=None):
         '''

Return a string containing a full description of the instance.

If a cell methods 'name' is followed by a '*' then that cell method is
relevant to the data in a way which may not be precisely defined its
corresponding dimension or dimensions.

:Parameters:

    display : bool, optional
        If False then return the description as a string. By default
        the description is printed, i.e. ``c.dump()`` is equivalent to
        ``print c.dump(display=False)``.

    prefix : str, optional
       Set the common prefix of component names. By default the
       instance's class name is used.

:Returns:

    out : None or str
        A string containing the description.

**Examples**

'''
         if prefix is None:
             prefix = self.__class__.__name__
                              
         string = []
         
         for i, s in enumerate(self.strings()):
             string.append('%s[%d] -> %s' % (prefix, i, s))
             
         string = '\n'.join(string)
         
         if display:
             print string
         else:
             return string
    #--- End: def

    def equals(self, other, rtol=None, atol=None, traceback=False):
        '''

True if two cell methods are equal, False otherwise.

The *dim* attribute is ignored in the comparison.

:Parameters:

    other : 
        The object to compare for equality.

    atol : float, optional
        The absolute tolerance for all numerical comparisons, By
        default the value returned by the `ATOL` function is used.

    rtol : float, optional
        The relative tolerance for all numerical comparisons, By
        default the value returned by the `RTOL` function is used.

    traceback : bool, optional
        If True then print a traceback highlighting where the two
        instances differ.

:Returns: 

    out : bool
        Whether or not the two instances are equal.

**Examples**

'''
        if self is other:
            return True

        # Check that each instance is the same type
        if self.__class__ != other.__class__:
            if traceback:
                print("%s: Different types: %s, %s" %
                      (self.__class__.__name__,
                       self.__class__.__name__,
                       other.__class__.__name__))
            return False
        #--- End: if

        if len(self) != len(other):
            if traceback:
                print("%s: Different numbers of methods: %d, %d" %
                      (self.__class__.__name__,
                       len(self), len(other)))
            return False
        #--- End: if

        for cm0, cm1 in zip(self, other):
            keys0 = set(cm0)
            keys1 = set(cm1)
         
            if keys0 != keys1: 
                if traceback:
                    print("%s: Different keys: %s" %
                          (self.__class__.__name__,
                           keys0.symmetric_difference(keys1)))
                return False
            #--- End: if

            name0 = cm0['name']
            name1 = cm1['name']
            if len(name0) != len(name1):
                if traceback:
                    print("%s: Different names: %r, %r" %
                          (self.__class__.__name__, name0, name1))
                return False
            #--- End: if
            if None in name0 or None in name1:
                if traceback:
                    print("%s: Missing name: %r, %r" %
                          (self.__class__.__name__, name0, name1))
                return False
            #--- End: if

            keys0.remove('dim')
            keys1.remove('dim')
            keys0.remove('name')
            keys1.remove('name')

            if 'interval' in keys0:
                keys0.remove('interval')

                interval0 = cm0['interval'][:]
                interval1 = cm1['interval'][:]

                # Make sure that both lists of intervals are the same
                # length. (If they're not, then the shorter one will
                # have length 1.)
                if len(interval0) < len(name0):
                    interval0 = interval0 * len(name0)
                elif len(interval1) < len(name1):
                    interval1 = interval1 * len(name1)

#                for x, y in zip(interval0, interval1):
#                    if not x.equivalent(y, rtol=rtol, atol=atol):
#                        if traceback:
#                            print("%s: Different interval values: %s, %s" %
#                                  (self.__class__.__name__,
#                                   repr(x), repr(y)))
#                        return False
            #--- End: if

            for key in keys0:
                for x, y in zip(cm0[key], cm1[key]):
                    if not equals(x, y, rtol=rtol, atol=atol,
                                  traceback=traceback):
                        if traceback:
                            print("%s: Different %s values: %r, %r" %
                                  (self.__class__.__name__, key, x, y))
                        return False
        #--- End: for

        return True
    #--- End: def

    def equivalent(self, other, rtol=None, atol=None, traceback=False):
        '''

True if two cell methods are equivalent, False otherwise.

The *dim* attribute is ignored in the comparison.

:Parameters:

    other : 
        The object to compare for equality.

    atol : float, optional
        The absolute tolerance for all numerical comparisons, By
        default the value returned by the `ATOL` function is used.

    rtol : float, optional
        The relative tolerance for all numerical comparisons, By
        default the value returned by the `RTOL` function is used.

:Returns: 

    out : bool
        Whether or not the two instances are equivalent.

**Examples**

'''
        if self is other:
            return True

        # Check that each instance is the same type
        if self.__class__ != other.__class__:
            return False

        if len(self) != len(other):
            return False

        for cm0, cm1 in zip(self, other):

            keys0 = set(cm0)
            keys1 = set(cm1)

            if keys0 != keys1: 
                if traceback:
                    print("%s: Nonequivalent keys: %r, %r" %
                          (self.__class__.__name__, x, y))
                return False

            name0 = cm0['name']
            name1 = cm1['name']
            if len(name0) != len(name1) or (None in name0 or None in name1):
                return False
            
            if 'interval' in keys0:
                keys0.remove('interval')

                interval0 = cm0['interval'][:]
                interval1 = cm1['interval'][:]

                # Make sure that both lists of intervals are the same
                # length. (If they're not, then the shorter one will
                # have length 1.)
                if len(interval0) < len(name0):
                    interval0 = interval0 * len(name0)
                elif len(interval1) < len(name1):
                    interval1 = interval1 * len(name1)

                for x, y in zip(interval0, interval1):
                    if not x.allclose(y, rtol=rtol, atol=atol):
                        if traceback:
                            print("%s: Nonequivalent 'interval' values: %r, %r" %
                                  (self.__class__.__name__, x, y))
                        return False
            #--- End: if

            keys0.discard('dim')

            key = 'method'
            if key in keys0:
                x = cm0[key]
                y = cm1[key]                
                if not equals(x, y, rtol=rtol, atol=atol, traceback=False):
                    if traceback:
                        print("%s: Nonequivalent '%s' values: %r, %r" %
                              (self.__class__.__name__, key, x, y))
                    return False
                
                keys0.discard(key)
            #--- End: if
                   

            for key in keys0:
                for x, y in zip(cm0[key], cm1[key]):
                    if not equals(x, y, rtol=rtol, atol=atol,
                                  traceback=False):
                        if traceback:
                            print("%s: Nonequivalent '%s' values: %r, %r" %
                                  (self.__class__.__name__, key, x, y))
                        return False
        #--- End: for

        return True
    #--- End: def

    def has_cellmethod(self, other):
        '''

Return True if and only if this cell methods is a super set of another.

:Parameters:
    other : CellMethods
        The other cell methods for comparison.

:Returns:
    out : bool
        Whether or not this cell methods is a super set of the other.

**Examples**

'''
        if len(other) != 1:
            return False

        found_match = False

        for i in range(len(self)):
            if other.equivalent(self[i:i+1]):
                found_match = True
                break
        #--- End: for

        return found_match
    #--- End: def

    def insert(self, index, value):
        # Insert an object before index.
        self._list.insert(index, value)
    #--- End: def

    def parse(self, string=None):
        '''

Parse a CF cell_methods string into this CellMethods instance in
place.

:Parameters:

    string : str, optional
        The CF cell_methods string to be parsed into the CellMethods
        object. By default the cell methods will be empty.

:Returns:

    None

**Examples**

>>> c = cf.CellMethods()
>>> c = c.parse('time: minimum within years time: mean over years (ENSO years)')    
>>> print c
Cell methods    : time: minimum within years
                  time: mean over years (ENSO years)

'''
        if not string:
            self._list[:] = []
            return

        # Split the cell_methods string into a list of strings ready
        # for parsing into the result list. E.g.
        #   'lat: mean (interval: 1 hour)'
        # maps to 
        #   ['lat:', 'mean', '(', 'interval:', '1', 'hour', ')']
        cell_methods = re_sub('\((?=[^\s])' , '( ', string)
        cell_methods = re_sub('(?<=[^\s])\)', ' )', cell_methods).split()

        while cell_methods:
#            self.region = False

            # Create a new element for this 
            self.append(Dict())            

            # List of dimensions
            self[-1]['dim'] = []

            # List of names
            self[-1]['name'] = []

            while cell_methods:
                if not cell_methods[0].endswith(':'):
                    break

                self[-1]['name'].append(cell_methods.pop(0)[:-1])            
                self[-1]['dim'].append(None)
            #--- End: while

            if not cell_methods:
                break

            # Method
            self[-1]['method'] = cell_methods.pop(0)

            if not cell_methods:
                break

            # Climatological statistics and statistics which apply to
            # portions of cells
            while cell_methods[0] in ('within', 'where', 'over'):
                term = cell_methods.pop(0)
                self[-1][term] = cell_methods.pop(0)
                if not cell_methods:
                    break
            #--- End: while

            if not cell_methods: 
                break

            # interval and comment
            if cell_methods[0].endswith('('):
                cell_methods.pop(0)

                if not (re_search('^(interval|comment):$', cell_methods[0])):
                    cell_methods.insert(0, 'comment:')
                           
                while not re_search('^\)$', cell_methods[0]):
                    term = cell_methods.pop(0)[:-1]

                    if term == 'interval':
                        if 'interval' not in self[-1]:
                            self[-1]['interval'] = List()
                        interval = cell_methods.pop(0)
                        try:
                            interval = float(interval)
                        except ValueError:
                            pass

                        if cell_methods[0] != ')':
                            units = cell_methods.pop(0)
                        else:
                            units = None

                        try:
                            self[-1]['interval'].append(Data(interval, units))
                        except ValueError:
                            del self[-1]['interval']                            
                            print(
"WARNING: Ignoring unknown interval in %s initialization: %s %s" % \
(self.__class__.__name__, interval, units))
                            
                        continue
                    #--- End: if

                    if term == 'comment':
                        comment = List()
                        while cell_methods:
                            if cell_methods[0].endswith(')'):
                                break
                            if cell_methods[0].endswith(':'):
                                break
                            comment.append(cell_methods.pop(0))
                        #--- End: while
                        self[-1]['comment'] = ' '.join(comment)
                    #--- End: if

                #--- End: while 

                if cell_methods[0].endswith(')'):
                    cell_methods.pop(0)
            #--- End: if

#            x = [self[-1]['name']]
#            if 'interval' in self[-1]:
#                x.append(self[-1]['interval'])
#
#            x = zip(*sorted(zip(*x)))
##
#            self[-1]['name'] = x[0]
#            if 'interval' in self[-1]:
#                self[-1]['interval'] = x[1]

        #--- End: while

    #--- End: def

    def strings(self):
        '''

Return a list of a CF-netCDF-like string of each cell method.

Note that if the intention is to concatenate the output list into a
string for creating a CF-netCDF cell_methods attribute, then the cell
methods "name" components may need to be modified, where appropriate,
to reflect netCDF variable names.

:Returns:

    out : list
        A string for each cell method.

**Examples**

>>> c = cf.CellMethods('time: minimum within years time: mean over years (ENSO years)')
>>> c.strings()
['time: minimum within years',
 'time: mean over years (ENSO years)']

'''
        strings = []

        for cm in self:

            string = []

#            if sorted(cm['name']) == ['latitude', 'longitude']:
#                string.append('area:')
#            else:
            x = []
            for dim, name in zip(cm['dim'], cm['name']):
                if name is None:
                    if dim is not None:
                        name = dim
                    else:
                        name = '?'

                x.append('%s:' % name)
            #--- End: for
            string.extend(x)
#             #--- End: if

            string.append(cm['method'])

            for portion in ('within', 'where', 'over'):
                if portion in cm:
                    string.extend((portion, cm[portion])) 
            #--- End: for

            if 'interval' in cm:
                x = ['(']

                y = []
                for interval in cm['interval']:
#                    y.append('interval: %s' % interval.first_datum)
                    y.append('interval: %s' % interval.datum(0))
                    if interval.Units:
                        y.append(interval.Units.units)
                #--- End: for
                x.append(' '.join(y))

                if 'comment' in cm:
                    x.append(' comment: %s' % cm['comment'])

                x.append(')')

                string.append(''.join(x))

            elif 'comment' in cm:
                string.append('(%s)' % cm['comment'])
            
            strings.append(' '.join(string))
        #--- End: for

        return strings
    #--- End: def

    def netcdf_translation(self, f):
        '''

Translate netCDF variable names.

:Parameters:

    f : Field
        The field which provides the translation.

:Returns:

    out : CellMethods
        A new cell methods instance with translated names.

**Examples**

>>> c = CellMethods('time: mean lon: mean')
>>> d = c.netcdf_translation(f)

'''
        cell_methods = self.copy()

        domain = f.domain

        # Change each 'name' value to a standard_name (or domain
        # coordinate identifier) and create the 'dim' key
            
        # From the CF conventions (1.5): In the specification of this
        # attribute, name can be a dimension of the variable, a scalar
        # coordinate variable, a valid standard name, or the word
        # 'area'.
        for cm in cell_methods:

            # Reset dim
            cm['dim'] = []

            if cm['name'] == ['area']:
                cm['dim'].append(None)
                continue
            #--- End: if

            dim_coords = f.dims()

            # Still here?
            for i, name in enumerate(cm['name']):
                axis = None
                for axis, ncdim in domain.nc_dimensions.iteritems():
                    if name == ncdim:
                        break
                    
                    axis = None
                #--- End: for                    

                if axis is not None:
                    # name is a netCDF dimension name (including
                    # scalar coordinates).
                    cm['dim'].append(axis)
#                    if dim in domain:
#                    if dim in domain.dim: 
                    if axis in dim_coords:
##                        cm['name'][i] = domain[axis].name('domain:%s' % axis)
#                        cm['name'][i] = domain.dim[axis].name('domain:%s' % axis)
                        cm['name'][i] = dim_coords[axis].name('domain:%s' % axis)
                    else:
                        cm['name'][i] = None
                else:                    
                    # name must be a standard name
#                    cm['dim'].append(
#                        f.item({'standard_name': name}, role='dim',
#                               dimensions=True, one_d=True))
                    cm['dim'].append(domain.axis({'standard_name': name}, 
                                                 role='d', exact=True))
            #--- End: for
        #--- End: for
    
        return cell_methods
    #--- End: def

#--- End: class
