from itertools import izip, izip_longest, chain
from re        import search as re_search

from .comparison import gt, le, Comparison
from .functions  import RTOL, ATOL, equals, allclose
from .functions  import inspect as cf_inspect
from .units      import Units
from .transform  import Transform

#_valid_ctypes = ('T', 'X', 'Y', 'Z')

# ====================================================================
#
# Domain object
#
# ====================================================================

class Domain(object):
    '''

Completely describe a field's coordinate system (domain).

It contains the dimensions constructs, auxiliary coordinate
constructs, cell measure constructs and transform constructs defined
by the CF data model.

The domain is a dictionary-like object whose key/value pairs identify
dimension coordinate, auxiliary coordinate and cell measure constructs
and store them as `cf.DimensionCoordinate`, `cf.AuxiliaryCoordinate`
and `cf.CellMeasure` objects respectively.

'''

    def __init__(self, axes={}, assign_axes={}, dim=None, aux=None, cm=None,
                 trans=None, copy=True):
        '''

**Initialization**

.. note:: `cf.DimensionCoordinate`, `cf.AuxiliaryCoordinate` and
          `cf.Coordinate` objects may almost always be used
          interchangably in the initialization, but there may be
          performance improvements if the appropriate type is
          provided.

:Parameters:

    dim : (sequence or dict of) cf.DimensionCoordinate, optional
        Initialize dimension coordinates.

        If sequence of dimension coordinates is given then identifiers
        will be automatically initialized as ``'dim0'``, ``'dim1'``,
        etc. for the first to the last dimension coordinates in the
        ordered sequence. Giving a `cf.DimensionCoordinate` object is
        equivalent to a single element sequence.

        If a dictionary is given then the keys are dimension
        coordinate identifiers (``'dimN'``) and values are dimension
        coordinate objects. The ``N`` part of each key identifier
        should be replaced by an arbitrary integer greater then or
        equal to zero. Careful selection of dimension coordinate
        identifiers (``'dimN'``) may prevent the need to set the
        *axes* parameter.

        No meaning should be inferred from the integer part of the
        identifiers, which need not include zero nor be consecutive.

    aux : (sequence or dict of) cf.AuxiliaryCoordinate, optional
        Initialize auxiliary coordinates.

        If a sequence of auxiliary coordinates is given then
        identifiers will be automatically initialized as ``'aux0'``,
        ``'aux1'``, etc. for the first to the last auxiliary
        coordinates in the ordered sequence. Giving an
        `cf.AuxiliaryCcoordinate` object is equivalent to a single
        element sequence.

        If a dictionary is given then the keys are auxiliary
        coordinate identifiers (``'auxN'``) and values are auxiliary
        coordinate instances. The ``N`` part of each key identifier
        should be replaced by an arbitrary integer greater then or
        equal to zero.

        No meaning should be inferred from the integer part of the
        identifiers, which need not include zero nor be consecutive.


    cm : (sequence of) cf.CellMeasure, optional
        Initialize cell measures.

        If a sequence of cell measures is given then identifiers will
        be automatically initialized as ``'cm0'``, ``'cm1'``, etc. for
        the first to the last cell measures in the ordered
        sequence. Giving a `cf.CellMeasure` object is equivalent to a
        single element sequence.

        If a dictionary is given then the keys are cell measure
        identifiers (``'cmN'``) and values are auxiliary coordinate
        instances. The ``N`` part of each key identifier should be
        replaced by an arbitrary integer greater then or equal to
        zero.

        No meaning should be inferred from the integer part of the
        identifiers, which need not include zero nor be consecutive.

    assign_axes : dict, optional
        Initialize the mapping of which axes span each component of
        the domain. The dictionary is stored in the domain's
        `~cf.Domain.dimensions` attribute. By default it is
        intitialized as follows:

           * For each dimension coordinate, a key/value pair is
             created for which the key is the dimension coordinate's
             identifier (``'dimN'``) and the value is a single element
             list of the same identifier (``'dimN'``). I.e. a
             dimension coordinate identifier is identical to the
             identifier of the dimension which it spans. Consequently,
             it is never necessary to map a dimension coordinate to
             its dimension.
        .. 
           
           * For each auxiliary coordinate and cell measure, a
             key/value pair is created for which the key is the
             auxiliary coordinate's or cell measure's identifier
             (``'auxN'`` or ``'cmN'`` respectively ) and the value is
             a list of dimension identifiers (``'dimN'``), stating the
             assumed dimensions, in order, of the construct
             concerned. These dimension identifiers will be the
             sequence of consecutive dimension identifiers ``'dim0'``
             up to ``'dimM'``, where ``M-1`` is the number of
             dimensions of the construct. An assumed dimension will be
             initialized with the correct size if it has not been
             specified elsewhere (by the *kwargs*, *dims* or
             *dimension_sizes* parameters).

        Explicitly setting a particular construct's dimensions does
        not alter the default behaviour for other constructs.

        Any dimension given in a value list that has not been
        specified elsewhere (by the *kwargs*, *dims* or
        *dimension_sizes* parameters) will be initialized with the
        correct size.

        Note that it is possible to set the mapping of which
        dimensions relate to a field's data array by creating the key
        ``'data'`` whose value is a list of dimension identifiers
        (``'dimN'``), stating the dimensions, in order, of the field's
        data array.

    axes : dict, optional
        Initialize axes of the domain. By default the axes are
        inferred from the dimension coordinate, auxiliary coordinate
        and cell measure constructs.

        An axis is set by providing a key/value pair for which the key
        is the dimension's identifier (``'dimN'``) and the value is
        the dimension's integer size. The ``N`` part of the key
        identifier should be replaced by an arbitrary integer greater
        then or equal to zero.

        No meaning should be inferred from the integer part of the
        identifiers, which need not include zero nor be consecutive.

        Explicitly setting a dictionary key does not alter the default
        behaviour for other axes.

        Note that a dimension may be intialized which is not spanned
        by any of the given constructs, and a dimension spanned by a
        given construct may also be initialized here, providing that
        there they are consistent.

    assign_axes : dict, optional
        Initialize the mapping of which axes span each component of
        the domain. The dictionary is stored in the domain's
        `~cf.Domain.dimensions` attribute. By default it is
        intitialized as follows:

           * For each dimension coordinate, a key/value pair is
             created for which the key is the dimension coordinate's
             identifier (``'dimN'``) and the value is a single element
             list of the same identifier (``'dimN'``). I.e. a
             dimension coordinate identifier is identical to the
             identifier of the dimension which it spans. Consequently,
             it is never necessary to map a dimension coordinate to
             its dimension.
        .. 
           
           * For each auxiliary coordinate and cell measure, a
             key/value pair is created for which the key is the
             auxiliary coordinate's or cell measure's identifier
             (``'auxN'`` or ``'cmN'`` respectively ) and the value is
             a list of dimension identifiers (``'dimN'``), stating the
             assumed dimensions, in order, of the construct
             concerned. These dimension identifiers will be the
             sequence of consecutive dimension identifiers ``'dim0'``
             up to ``'dimM'``, where ``M-1`` is the number of
             dimensions of the construct. An assumed dimension will be
             initialized with the correct size if it has not been
             specified elsewhere (by the *kwargs*, *dims* or
             *dimension_sizes* parameters).

        Explicitly setting a particular construct's dimensions does
        not alter the default behaviour for other constructs.

        Any dimension given in a value list that has not been
        specified elsewhere (by the *kwargs*, *dims* or
        *dimension_sizes* parameters) will be initialized with the
        correct size.

        Note that it is possible to set the mapping of which axes
        relate to a field's data array by creating the key ``'data'``
        whose value is a list of axis identifiers (``'dimN'``),
        stating the axes, in order, of the field's data array.

    trans : (sequence or dict of) cf.Transform, optional
        Initialize transforms.

        If a sequence of transforms is given then identifiers will be
        automatically initialized as ``'trans0'``, ``'trans1'``,
        etc. for the first to the last transforms in the ordered
        sequence. Giving a `cf.Transform` object is equivalent to a
        single element sequence.

        If a dictionary is given then the keys are transform
        identifiers (``'transN'``) and values are auxiliary coordinate
        instances. The ``N`` part of each key identifier should be
        replaced by an arbitrary integer greater then or equal to
        zero.

        No meaning should be inferred from the integer part of the
        identifiers, which need not include zero nor be consecutive.

**Examples**

In this example, four dots (``....``) refers to appropriate
initializations of the coordinate, cell measure and transform
constructs, which are omitted here for clarity.

>>> dim_coord_A = cf.DimensionCoordinate(....)
>>> dim_coord_B = cf.DimensionCoordinate(....)
>>> dim_coord_A.size
73
>>> dim_coord_B.size 
96
>>> aux_coord_A = cf.AuxiliaryCoordinate(....)
>>> aux_coord_A.shape
(96, 73)
>>> cell_measure_A = cf.CellMeasure(....)
>>> cell_measure_A.shape
(73, 96)
>>> transform_A = cf.Transform(name='latitude_longitude', ....)
>>> d = cf.Domain(dim=(dim_coord_A, dim_coord_B),
...               aux=aux_coordA,
...               cm=[cell_measure_A],
...               trans=transform_A,
...               assign_axes={'aux0': ['dim1', 'dim0']})
...
>>> d.dimensions
{'aux0': ['dim1', 'dim0'],
 'cm0' : ['dim0', 'dim1'],
 'dim1': ['dim1'],
 'dim0': ['dim0']}
>>> d.transforms
{'trans0' : <CF Transform: latitude_longitude>}

It was necessary to specifiy the dimension mapping for ``'aux0'``
because the auxiliary coordinate doesn't have the default dimension
order of ``['dim0', 'dim1']``. Equivalent ways of initializing the
same domain are as follows:

>>> d = cf.Domain(dim=[dim_coordA, dim_coordB],
...               aux=aux_coordA,
...               cm=cell_measureA,
...               trans=transformA,
...               assign_axes={'aux0': ['dim1', 'dim0']}
...               transform_map={'trans0': 'aux0'})

Note that we may (for clarity) specify an entry in the *dimensions*
dictionary, even if the default behaviour aplies:
 
>>> d = cf.Domain(dim={'dim1': dim_coord_B, 'dim0': dim_coord_A},
...               aux0={'aux0': aux_coord_A},
...               cm0={'cm0': cell_measure_A},
...               trans={'trans0': transform_A},,
...               assign_axes={'aux0': ['dim1', 'dim0'],
...                            'cm0' : ['dim0', 'dim1']})

We may change default identifier names:

>>> d = cf.Domain(dim=[dim_coord_A, dim_coord_B],
...               aux={'aux5': aux_coord_A},
...               cm=[cell_measureA],
...               trans=transform_A,
...               assign_axes={'aux5': ['dim1', 'dim0']})

>>> d = cf.Domain(dim={'dim21': dim_coord_A, 'dim3': dim_coord_B},
...               aux={'aux5': aux_coord_A},
...               cm=cell_measureA,
...               trans=[transform_A],
...               assign_axes={'aux5': ['dim3' , 'dim21'],
...                            'cm0' : ['dim21', 'dim3']})

In the last case it was necessary to include the cell measure
construct in the dimension map because we had altered the default
dimension names. If we had *not* included ``'cm0'`` in the dimension
map then the cell measure's dimension would have initialized by the
default values ``['dim0', 'dim1']`` and these two new axes would have
created in addition to ``'dim21'``, and ``'dim3'``.

'''
        self.dimension_sizes = axes.copy()
        self.dimensions      = assign_axes.copy()
        
        self.d = {}
        self.a = {}        
        self.c = {}
        self.t = {}
        self._map = {}

        # ------------------------------------------------------------
        # Initialize dimension coordinates
        # ------------------------------------------------------------
        if dim:
            if isinstance(dim, dict):
                for key, coord in dim.iteritems():
                    self.insert_dim(coord, key=key, copy=copy)
            else:
                for N, coord in enumerate(dim):
                    key = 'dim%d' % N
                    self.insert_dim(coord, key=key, copy=copy)
        #--- End: if

        # ------------------------------------------------------------
        # Initialize auxiliary coordinates
        # ------------------------------------------------------------
        if aux: 
            if isinstance(aux, dict):
                for key, coord in aux.iteritems():
                    dimensions = self.dimensions.get(key, None)
                    self.insert_aux(coord, key=key, axes=dimensions, copy=copy)
            else:
                for N, coord in enumerate(aux):
                    key = 'aux%d' % N   
                    dimensions = self.dimensions.get(key, None)
                    self.insert_aux(coord, key=key, axes=dimensions, copy=copy)
        #--- End: if

        # ------------------------------------------------------------
        # Initialize cell measures
        # ------------------------------------------------------------
        if cm: 
            if isinstance(cm, dict):
                for key, cm in cm.iteritems():
                    dimensions = self.dimensions.get(key, None)
                    self.insert_cm(cm, key=key, axes=dimensions, copy=copy)
            else:
                for N, coord in enumerate(cm):
                    key = 'cm%d' % N   
                    dimensions = self.dimensions.get(key, None)
                    self.insert_cm(cm, key=key, axes=dimensions, copy=copy)
        #--- End: if

        # ------------------------------------------------------------
        # Initialize transforms
        # ------------------------------------------------------------
        if trans:
            if isinstance(trans, Transform):
                self.insert_transform(trans, key='trans0', copy=copy)

            elif isinstance(trans, dict):
                for key, transform in trans.iteritems():
                    self.insert_transform(transform, key=key, copy=copy)

            else:
                for N, transform in enumerate(trans):
                    key = 'trans%d' % N 
                    self.insert_transform(transform, key=key, copy=copy)
        #--- End: if
    #--- End: def

    def __repr__(self):
        '''

x.__repr__() <==> repr(x)

'''
        return '<CF %s: %s>' % (self.__class__.__name__, 
                                tuple(self.dimension_sizes.values()))
    #--- End: def

    def __str__(self):
        '''
x.__str__() <==> str(x)

'''
        mmm = {}
        def _print_coord(domain, key, variable, dimension_coord):
            '''Private function called by __str__'''

            if dimension_coord:
                name = "%s(%d)" % (domain.axis_name(key),
                                   domain.dimension_sizes[key])
                mmm[key] = name
                if key not in domain.d:
                    return name
                else:
                    variable = domain.d[key]
                
                x = [name]
            #--- End: if

            # Still here?
#            variable = domain[key]

            if not dimension_coord:               
                # Auxiliary coordinate
                shape = [mmm[dim] for dim in domain.dimensions[key]]
                shape = str(tuple(shape)).replace("'", "")
                shape = shape.replace(',)', ')')
                x = [variable.name('domain:'+key)]
                x.append(shape)
            #--- End: if

            try:
                variable.compress
            except AttributeError:
                if variable._hasData:
                    x.append(' = ')
                    x.append(str(variable.Data))
            else:
                x.append(' -> compressed ')
                compressed = []
                for unc in domain[key].compress:
                    shape = str(unc.size)
                    compressed.append(unc.name('unc')+'('+shape+')')
                x.append(', '.join(compressed))

            return ''.join(x)
        #--- End: def

        string = []

        x = [_print_coord(self, dim, None, True)
             for dim in sorted(self.dimension_sizes)]
        if x:
#           string.append('Dimensions     : ')
            string.append('Axes           : ')
            string.append('\n               : '.join(x))

        x = [_print_coord(self, aux, v, False) 
             for aux, v in sorted(self.a.items())]
        if x:
            string.append('\n')
#           string.append('Auxiliary coords: ')
            string.append('Aux coords     : ')
            string.append('\n               : '.join(x))

        # Cell measures
        x = [_print_coord(self, cm, v, False)
             for cm, v in sorted(self.c.items())]
        if x:
            string.append('\n')
            string.append('Cell measures  : ')
            string.append('\n               : '.join(x))

        # Transforms
        x = [repr(transform) for transform in self.t.values()]
        if x:
            string.append('\n')
            string.append('Transforms     : ')
            string.append('\n               : '.join(x))
        #--- End: if

        return ''.join(string)
    #--- End: def

    def _conform_transform(self, transform):
        '''

:Parameters:

    transform : cf.Transform

:Returns:

    None

**Examples**

>>> d._conform_transform(t)

'''
        role = ('d', 'a')

        coordinates = transform.coords
        coords      = self.items(role=role) 

        for pointer in coordinates.copy():
            if pointer in coords:
                continue

            key = self.item(pointer, role=role, exact=True, key=True)

            if key is not None or pointer in coords:
                transform.change_coord(pointer, key)
                
#                for term in transform.coord_terms:
#                    if transform[term] == pointer:
#                        transform[term] = key
#
#                for term, value in transform.items():
#                    if equals(value, pointer):
#                        transform[term] = key
#                #--- End: for##
#
#                coordinates.remove(pointer)
#                coordinates.add(key)
    #--- End: def

    def _replace_transforms_coord_identifier(self, key):
        '''

Replace a coordinate identifier with the coordinate object's identity
in all transforms.

If the coordinate object has no identity then the coordinate object is
effectively removed.

:Parameters:

    key : str
        A domain coordinate identifier.

:Returns:

    None

**Examples**

>>> d._replace_transforms_coord_identifier('dim1')
>>> d._replace_transforms_coord_identifier('aux0')

'''
        name = self.get(key).identity(None)
        for transform in self.t.itervalues():
            transform.change_coord(key, name)
    #--- End: def

    def _set(self, key, value):
        '''

Set the item of a pre-existing identifier of the domain.

An item is either a dimension coordinate, an auxiliary coordinate, a
cell measure or a transform object.

.. note:: Consistency is NOT checked.

:Parameters:

    key : str
        A domain identifier.
    
    value :
        The new item corresponding to the domain identifier given by
        *key*.
    
:Returns:

    None

.. seealso:: `get`, `has`

**Examples**

>>> d.items().keys()
['dim0', 'aux0', 'aux1', 'trans0']
>>> d._set('aux1', cf.AuxiliaryCoordinate(....))
>>> d._set('trans0', cf.Transform(....))

'''
        getattr(self, self._map[key])[key] = value
    #--- End: def

    def _equal_transform(self, t, u, domain, rtol=None, atol=None, 
                         pointer_map={}, traceback=False):
        '''

:Parameters:

    t : cf.Transform

    u : cf.Transform

    domain : cf.Domain
        The domain which contains *u*.

    pointer_map : dict

    traceback : bool, optional
        If True then print a traceback highlighting where the two
        instances differ.

:Returns:

    out : bool

**Examples**

>>>

'''
        if rtol is None:
            rtol = RTOL()
        if atol is None:
            atol = ATOL()              

        if not t.equals(u, rtol=rtol, atol=atol, traceback=traceback):
            if traceback:
                print(
"%s: Unequal transforms (%r != %r)" % (self.__class__.__name__, t, u))
            return False

        # If transforms have coordinate-valued terms - check
        # them                        
        for term in t.coord_terms:
            if u[term] in pointer_map:
                if t[term] == pointer_map[u[term]]:
                    continue
            elif u[term] == t[term]:
                continue    

            # Still here?
            if traceback:
                print(
"%s: Unequal %s transform '%s' term" % (self.__class__.__name__, t.name, term))

            return False
        #--- End: for

        return True
    #--- End: def

    def equivalent(self, other, rtol=None, atol=None, traceback=False):
        '''

True if and only if two domains are logically equivalent.

:Parameters:

    other :
        The object to compare for equivalence.

    atol : float, optional
        The absolute tolerance for all numerical comparisons, By
        default the value returned by the `ATOL` function is used.

    rtol : float, optional
        The relative tolerance for all numerical comparisons, By
        default the value returned by the `RTOL` function is used.

    traceback : bool, optional
        If True then print a traceback highlighting where the two
        objects differ.

:Returns: 

    out : bool
        Whether or not the two objects are equivalent.
      



'''
        if sorted(self.dimension_sizes.values()) != sorted(self.dimension_sizes.values()):
            if traceback:
                print("%s: Different axes: %s != %s" %
                      (self.__class__.__name__, 
                       sorted(self.dimension_sizes.values()),
                       sorted(other.dimension_sizes.values())))
            return False

        s = self.analyse()
        t = other.analyse()
        
        if set(s['id_to_coord']) != set(t['id_to_coord']):
            if traceback:
                print(
"%s: Non-matching axes: %s" %
(self.__class__.__name__, set(s['id_to_coord']).difference(t['id_to_coord'])))
            return False
        
        for identity, coord0 in s['id_to_coord'].iteritems():
            coord1 = t['id_to_coord'][identity]
            if not coord0._equivalent_data(coord1, rtol=rtol, atol=atol):
                if traceback:
                    print(
"%s: Non-equivalent 1-d coordinate data array: %s" %
(self.__class__.__name__, identity))
                    return False
        #--- End: for
                
        keys1 = other.t.keys()
        for transform0 in self.t.itervalues():
            found_match = False
            for key1 in keys1:
                transform1 = other_t[key1]
                
                if self.equivalent_transform(transform0, transform1,
                                             domain=other, traceback=False):
                    found_match = True
                    transforms1.remove(key1)
                    break
            #--- End: for

            if not found_match:
                if traceback:
                    print(
"%s: Missing transform: %r" % (self.__class__.__name__, transform0))
                return False
        #--- End: for                    

        return True
    #--- End: def

    def equivalent_transform(self, t, u, domain, atol=None, rtol=None,
                             traceback=False):
        '''

:Parameters:

    t : cf.Transform

    u : cf.Transform

    domain : cf.Domain
        The domain which contains *u*.

    traceback : bool, optional
        If True then print a traceback highlighting where the two
        instances differ.

:Returns:

    out : bool

**Examples**

>>>

'''
        if not t.equivalent(u, rtol=rtol, atol=atol, traceback=traceback):
            if traceback:
                print(
"%s: Unequivalent transforms (%r != %r)" % (self.__class__.__name__, t, u))
            return False

        t_coord_terms = t.coord_terms.copy()
        u_coord_terms = u.coord_terms.copy()

        for term in t_coord_terms.intersection(u_coord_terms):
            # Term is coordiante-valued in both t and u
            t_coord_terms.remove(term)
            u_coord_terms.remove(term)

            tcoord = self.item(t[term], role='da', exact=True)
            ucoord = domain.item(u[term], role='da', exact=True)

            if (tcoord is None or ucoord is None or 
                not tcoord._equivalent_data(ucoord, rtol=rtol, atol=atol)):
                if traceback:
                    print(
"%s: Unequal transform '%s' term" % (self.__class__.__name__, term))
                return False
        #--- End: for

        for term in t_coord_terms:  
            # Term is coordiante-valued in t but missing from u
            coord = self.item(t[term], role='da', exact=True)            
            default = t.default(term)
            if default is None or coord is None or not allclose(coord, default):
                if traceback:
                    print(
"%s: Unequivalent transform '%s' term" % (self.__class__.__name__,  term))
                return False
        #--- End: for

        for term in u_coord_terms:  
            # Term is coordiante-valued in u but missing from t
            coord = self.item(u[term], role='da', exact=True)
            default = u.default(term)
            if default is None or coord is None or not allclose(coord, default):
                if traceback:
                    print(
"%s: Unequivalent transform '%s' term" % (self.__class__.__name__,  term))
                return False
        #--- End: for

        # Still here?
        return True
    #--- End: def

    def canonical_transform(self, transform):
        '''
'''
        transform = transform.copy()
        
        for term, value in transform.iteritems():
            data = getattr(value, '__data__', None)
            if data is None:
                # Value has no units
                continue

            units = transform.canonical_units(term)
            if units is None:
                continue
                
            if isinstance(units, basestring):
                # units is a standard_name of a coordinate
                coord = self.item(units, role='da', exact=True)
                if coord is None:
                    continue
                
                units = coord.Units
            #--- End: if            

            if units.equivalent(data.Units):
                data.Units = units
            else:
                raise ValueError("asdddddddddddddd 87236768")
        #--- End: for

        return transform
    #--- End: def

    def analyse(self):
        '''

'''
        a = {}

        # ------------------------------------------------------------
        # Map each axis identity to its domain identifier, if such a
        # mapping exists.
        #
        # For example:
        # >>> id_to_axis
        # {'time': 'dim0', 'height': dim1'}
        # ------------------------------------------------------------
        id_to_axis = {}

        # ------------------------------------------------------------
        # For each dimension that is identified by a 1-d auxiliary
        # coordinate, map its dimension's its domain identifier.
        #
        # For example:
        # >>> id_to_aux
        # {'region': 'aux0'}
        # ------------------------------------------------------------
        id_to_aux = {}

        # ------------------------------------------------------------
        # 
        #
        # For example:
        # >>> id_to_key
        # {'region': 'aux0'}
        # ------------------------------------------------------------
        id_to_key = {}

        # ------------------------------------------------------------
        # Map each dimension's identity to the coordinate which
        # provides that identity.
        #
        # For example:
        # >>> id_to_coord
        # {'time': <CF Coordinate: time(12)>}
        # ------------------------------------------------------------
        id_to_coord = {}

        axis_to_coord = {}
        aux_to_coord = {}

        # ------------------------------------------------------------
        #
        # ------------------------------------------------------------
        aux_coords = {}
        aux_coords['N-d'] = {}

        cell_measures = {}
        cell_measures['N-d'] = {}

        # ------------------------------------------------------------
        # List the dimensions which are undefined, in that no unique
        # identity can be assigned to them.
        #
        # For example:
        # >>> undefined_axes
        # ['dim2']
        # ------------------------------------------------------------
        undefined_axes = []

        # ------------------------------------------------------------
        #
        # ------------------------------------------------------------
        warnings = []

        for axis in self.dimension_sizes:

            # Find this dimension's 1-d and N-d auxiliary coordinates
            aux_coords[axis]        = {}
            aux_coords[axis]['1-d'] = {}
            aux_coords[axis]['N-d'] = {}
            for aux, coord in self.items(role='a', axes=axis).iteritems():
                if coord.ndim > 1:
                    aux_coords['N-d'][aux] = coord
                    aux_coords[axis]['N-d'][aux] = coord
                else:
                    aux_coords[axis]['1-d'][aux] = coord
            #--- End: for

            # Find this dimension's 1-d and N-d cell measures
            cell_measures[axis]        = {}
            cell_measures[axis]['1-d'] = {}
            cell_measures[axis]['N-d'] = {}
            for cm, cell_measure in self.items(role='c', axes=axis).iteritems():
                if cell_measure.ndim > 1:
                    cell_measures['N-d'][cm]       = cell_measure
                    cell_measures[axis]['N-d'][cm] = cell_measure
                else:
                    cell_measures[axis]['1-d'][cm] = cell_measure
            #--- End: for

            if axis in self.d:
                # This dimension of the domain has a dimension
                # coordinate
                dim_coord = self.d[axis]
                identity = dim_coord.identity()
                if identity is not None and dim_coord._hasData:
                    if identity in id_to_axis:
                        warnings.append(
                            "Domain has more than one '%s' axis" % identity)

                    id_to_axis[identity]  = axis
                    id_to_key[identity]   = axis
                    id_to_coord[identity] = dim_coord
                    axis_to_coord[axis]   = dim_coord
#                    dim_to_coord[axis]    = dim_coord
                    continue

            elif len(aux_coords[axis]['1-d']) == 1:
                # This axis of the domain does not have a dimension
                # coordinate but it does have exactly one 1-d
                # auxiliary coordinate, so that will do.
                aux       = list(aux_coords[axis]['1-d'])[0]
                aux_coord = self.a[aux]
                
                identity = aux_coord.identity()
                if identity is not None and aux_coord._hasData:
                    if identity in id_to_axis:
                        warnings.append(
                            "Domain has more than one '%s' axis" % identity)

                    id_to_aux[identity]   = aux
                    id_to_key[identity]   = aux
                    id_to_axis[identity]  = axis
                    id_to_coord[identity] = aux_coord
                    axis_to_coord[axis]   = aux_coord
#                    aux_to_coord[axis]    = aux_coord
                    continue
            #--- End: if

            # Still here? Then this axis is undefined
            undefined_axes.append(axis)
        #--- End: for

        # ------------------------------------------------------------
        # Invert the mapping between dimensions and identities
        # ------------------------------------------------------------
        axis_to_id = {}
        for k, v in id_to_axis.iteritems():
            axis_to_id[v] = k

        return {'aux_coords'    : aux_coords,
#                'aux_to_coord'  : aux_to_coord,
                'axis_to_id'    : axis_to_id,
                'axis_to_coord' : axis_to_coord,
                'cell_measures' : cell_measures,
                'id_to_aux'     : id_to_aux,
                'id_to_coord'   : id_to_coord,
                'id_to_axis'    : id_to_axis,
                'id_to_key'     : id_to_key,
                'undefined_axes': undefined_axes,
                'warnings'      : warnings,                
                }    
    #--- End def 

    def attach_to_transform(transform, arg, term=None, **kwargs):
        '''

Attach a coordinate construct to a transform.

:Parameters:

    transform : cf.Transform

    arg : 
        The coordinate to attach, identified as for the `item`
        method. See *kwargs*.
    
    term : sequence of str, optional

    kwargs : *optional*
        These keyword arguments are used internally by the `item`
        method to customise finding the coordinate.
    
:Returns:

    None

**Examples**

>>> t
<CF Transform: asdkjhfskjfhgslghldkjgf>
>>> d.attach_to_transform(t, 'dim2')
>>> d.attach_to_transform(t, 'longitude', term=['b1'])
>>> d.attach_to_transform(t,
...                       {'standard_name': 'latitude', 
...                        'long_name': 'latitude'},
...                       exact=True, match_all=False)
  
'''
        kwargs['key']  = True
        kwargs['role'] = ('d', 'a')
        key = self.item(arg, **kwargs)
        if key is None:
            raise ValueError("Can't find %r coordinate object to attach to %r" % 
                             (arg, transform))

#        transform.coords.add(key)

        if term:
            for t in term:
                transform[t] = key
        else:
            transform.coords.add(key)

    #--- End: def

    def get(self, key, *default):        
        '''

Return the item corresponding to an internal identifier.

An item is a dimension coordinate, an auxiliary coordinate, a cell
measure or a transform object.

:Parameters:

    key : str        
        An internal identifier.

    default : *optional*
        Return *default* if and only if the domain does not have the
        given *key*.

:Returns:

    out :
        The item of the domain with the given internal identifier. If
        none exists and *default* is set then *default* is returned.

.. seealso:: `has`, `item`

**Examples**

>>> d.get('dim0')
<CF DimensionCoordinate: atmosphere_hybrid_height_coordinate(1)>
>>> d.get('aux1')
<CF AuxiliaryCoordinate: latitude(10, 9) degree_N>
>>> d.get('cm0')
<CF CellMeasure: area(9, 10) km 2>
>>> d.get('trans0')
<CF Transform: rotated_latitude_longitude>
>>> d.get('bad_id')
ValueError: Domain doesn't have internal identifier 'bad_id'
>>> print d.get('bad_id', None)
None

'''  
        if key in self._map:
            return getattr(self, self._map[key])[key]
        elif default:
            return default[0]
        else:
            raise ValueError(
                "Domain doesn't have internal identifier '%s'" % key)
     #--- End: def

    def has(self, key):        
        '''

True if the domain has the given internal identifier.

:Parameters:

    key : str        
        An internal identifier.

:Returns:

    out : bool
        Whether or not the domain has the internal identifier.

.. seealso:: `get`, 'item`, `items`

**Examples**

>>> d.items().keys()
['dim0', 'aux0', 'aux1', 'trans0']
>>> d.has('dim0')
True
>>> d.has('aux2')
False

'''
        return key in self._map
     #--- End: def

    def axis_name(self, axis=None, **kwargs):
        '''

Return the canonical name for an axis.

:Parameters:

    axis, kwargs : optional
        Select the axis which would be selected by this call of the
        domain's `~cf.Domain.axis` method: ``d.axis(axis,
        **kwargs)``. See `cf.Domain.axis` for details.

:Returns:

    out : str
        The canonical name for the axis.

**Examples**

>>> d.axis_name('dim0')
'time'
>>> d.axis_name('dim1')
'domain:dim1'
>>> d.axis_name('dim2')
'ncdim:lat'

'''        
        axis = self.axis(axis, **kwargs)
        if axis is None:
            raise ValueError("No unique axis could be identified")

        dim = self.item(role='d', axes=axis, rank=1, strict_axes=True)
        if dim is not None:
            # Get the name from the dimension coordinate
            return dim.name('domain:%s' % axis) 

        aux = self.item(role='a', axes=axis, rank=1, strict_axes=True)
        if aux is not None:
            # Get the name from the unique 1-d auxiliary coordinate
            return aux.name('domain:%s' % axis)
        else:
            try:
                return 'ncdim:%s' % self.nc_dimensions[axis]
            except (KeyError, AttributeError):
                return 'domain:%s' % axis


       #axis = self.axis(arg, role=('d', 'a', 'c'))
       #if axis is None:
       #    raise ValueError("No axis is identified by '%s'" % arg)    
       #
       #if axis in self.d:
       #    # Get the name from the dimension coordinate
       #    return self.d[axis].name(default='domain:%s' % axis)                
       #
       ## Still here? Then see if there is a unique 1-d auxiliary
       ## coordinate which can provide a name.
       #aux = self.item(role='a', axes=axis, rank=1)
       #if aux is not None:
       #    return aux.name(default='domain:%s' % axis)
       #else:
       #    try:
       #        return 'ncdim:%s' % self.nc_dimensions[axis]
       #    except (KeyError, AttributeError):
       #        return 'domain:%s' % axis
    #--- End: def

    def axis_identity(self, axis=None, **kwargs):
        '''

Return the canonical name for an axis.

:Parameters:

    axis, kwargs : optional
        Select the axis which would be selected by this call of the
        domain's `~cf.Domain.axis` method: ``d.axis(axis,
        **kwargs)``. See `cf.Domain.axis` for details.

:Returns:

    out : str
        The canonical name for the axis.

**Examples**

'''        
        axis = self.axis(axis, **kwargs)
        if axis is None:
            raise ValueError("No unique axis could be identified")

        dim = self.item(role='d', axes=axis, rank=1, strict_axes=True)
        if dim is not None:
            # Get the identity from the dimension coordinate
            return dim.identity()

        aux = self.item(role='a', axes=axis, rank=1, strict_axes=True)
        if aux is not None:
            # Get the identity from the unique 1-d auxiliary coordinate
            return aux.identity()

        return None
    #--- End: def

    def direction(self, axis):
        '''

Return True if an axis is increasing, otherwise return False.

An axis is considered to be increasing if its dimension coordinate
values are increasing in index space or if it has no dimension
coordinate.

:Parameters:

    axis : str
        A domain axis identifier, such as ``'dim0'``.

:Returns:

    out : bool
        Whether or not the axis is increasing.
        
.. seealso:: `directions`

**Examples**

>>> d.dimension_sizes
{'dim0': 3, 'dim1': 1, 'dim2': 2, 'dim3': 2, 'dim4': 99}
>>> d.dimensions
{'dim0': ['dim0'],
 'dim1': ['dim1'],
 'aux0': ['dim0'],
 'aux1': ['dim2'],
 'aux2': ['dim3'],
}
>>> d['dim0'].array
array([  0  30  60])
>>> d.direction('dim0')
True
>>> d['dim1'].array
array([15])
>>> d['dim1'].bounds.array
array([  30  0])
>>> d.direction('dim1')
False
>>> d['aux1'].array
array([0, -1])
>>> d.direction('dim2')
True
>>> d['aux2'].array
array(['z' 'a'])
>>> d.direction('dim3')
True
>>> d.direction('dim4')
True

'''
        if axis in self.d:
            return self.d[axis].direction()
        
        return True        
    #--- End: def
  
    def directions(self):
        '''

Return a dictionary mapping axes to their directions.

:Returns:

    out : dict
        A dictionary whose key/value pairs are axis identifiers and
        their directions.

.. seealso:: `direction`

**Examples**

>>> d.directions()
{'dim1': True, 'dim0': False}

'''
        self_direction = self.direction

        directions = {}
        for axis in self.dimension_sizes:
            directions[axis] = self_direction(axis)

        return directions
    #--- End: def

    def map_axes(self, other):
        '''

Map the axis identifiers of the domain to their equivalent axis
identifiers of another.

:Parameters:

    other : cf.Domain

:Returns:

    out : dict
        A dictionary whose keys are the axis identifiers of the domain
        with corresponding values of axis identifiers of the of other
        domain.

**Examples**

>>> d.map_axes(e)
{'dim0': 'dim1',
 'dim1': 'dim0',
 'dim2': 'dim2'}

'''
        s = self.analyse()
        t = other.analyse()
        
        out = {}
        
        for identity, dim in s['id_to_axis'].iteritems():
            if identity in t['id_to_axis']:
                out[dim] = t['id_to_axis'][identity]
        #--- End: for

        return out
    #--- End: def

    def insert_axis(self, size, key=None, replace=True):
        '''

Insert an axis into the domain in place.

This method has exactly the same interface, functionality and outputs
as `cf.Field.insert_axis`. Therefore see `cf.Field.insert_axis` for
the full documentation details.

:Parameters:

    size : int
        See `cf.Field.insert_axis`.

    key : str, optional
        See `cf.Field.insert_axis`.
  
    replace : bool, optional
        See `cf.Field.insert_axis`.

:Returns:

    out :
        The domain identifier of the new axis.

.. seealso:: `insert_aux`, insert_cm`, insert_dim`, insert_transform`


**Examples**

See `cf.Field.insert_axis`.

'''
        if key is not None:
            if (key in self.dimension_sizes and not replace and 
                self.dimension_sizes[key] != size):
                raise ValueError(
"Can't insert axis: Existing axis %r has different size (%d, %d)" %
(key, size, self.dimension_sizes[key]))

            self.dimension_sizes[key] = size
            return key

        # Still here? Then no identifier was specified for the
        # dimension, so create a new one.
        key = self.new_axis_identifier()
        self.dimension_sizes[key] = size

        return key
    #--- End: def

    def new_axis_identifier(self):
        '''

Return a new, unique axis identifier for the domain.

The domain is not updated.

:Returns:

    out : str
        The new identifier.

.. seealso:: `new_aux_identifier`, `new_cm_identifier`,
             `new_dim_identifier`, `new_transform_identifier`

**Examples**

>>> d.dimension_sizes.keys()
['dim2', 'dim0']
>>> d.new_axis_identifier()
'dim3'

>>> d.dimension_sizes.keys()
[]
>>> d.new_axis_identifier()
'dim0'

'''
        dimensions = self.dimension_sizes

        n = len(dimensions)
        new_key = 'dim%d' % n

        while new_key in dimensions:
            n += 1
            new_key = 'dim%d' % n
        #--- End: while

        return new_key
    #--- End: def

    def new_aux_identifier(self):
        '''

Return a new, unique auxiliary coordinate identifier for the domain.

The domain is not updated.

:Returns:

    out : str
        The new identifier.

.. seealso:: `new_cm_identifier`, `new_dimemsion_identifier`,
             `new_transform_identifier`

**Examples**

>>> d.items(role='a').keys()
['aux2', 'aux0']
>>> d.new_aux_identifier()
'aux3'

>>> d.items(role='a').keys()
[]
>>> d.new_aux_identifier()
'aux0'

'''
        keys = self.a

        n = len(keys)
        new_key = 'aux%d' % n

        while new_key in keys:
            n += 1
            new_key = 'aux%d' % n
        #--- End: while

        return new_key
    #--- End: def

    def new_cm_identifier(self):
        '''

Return a new, unique cell measure identifier for the domain.

The domain is not updated.

:Returns:

    out : str
        The new identifier.

.. seealso:: `new_aux_identifier`, `new_axis_identifier`,
             `new_transform_identifier`

**Examples**

>>> d.items(role='c').keys()
['cm2', 'cm0']
>>> d.new_cm_identifier()
'cm3'

>>> d.items(role='c').keys()
[]
>>> d.new_cm_identifier()
'cm0'


'''
        keys = self.c

        n = len(keys)
        new_key = 'cm%d' % n

        while new_key in keys:
            n += 1
            new_key = 'cm%d' % n
        #--- End: while

        return new_key
    #--- End: def

    def new_transform_identifier(self):
        '''

Return a new, unique transform identifier for the domain.

The domain is not updated.

:Returns:

    out : str
        The new identifier.

.. seealso:: `new_aux_identifier`, `new_axis_identifier`,
             `new_cm_identifier`

**Examples**

>>> d.items(role='t').keys()
['trans1']
>>> d.new_transform_identifier()
'trans2'

>>> d.items(role='t').keys()
[]
>>> d.new_transform_identifier()
'trans0'

'''
        if not self.t:
            return 'trans0'
        
        keys = self.t

        n = len(keys)
        new_key = 'trans%d' % n

        while new_key in keys:
            n += 1
            new_key = 'trans%d' % n
        #--- End: while

        return new_key
    #--- End: def

    def insert_coord(self, variable, key, copy=True, axes=None):
        '''

Insert a dimension coordinate or auxiliary coordinate into the domain
in place.

:Parameters:

    variable : cf.AuxiliaryCoordinate or cf.DimensionCoordinate or cf.Coordinate
        The new dimension coordinate or auxiliary coordinate. If
        required, it will be converted to the appropiate object type
        (dimension coordinate or auxiliary).

    key : str
        The identifier for the new coordinate. The identifier must
        start with the string ``dim`` or ``aux``, corresponding to
        whether the *variable* is to be a dimension coordinate or an
        auxiliary coordinate respectively.

    axes : list, optional
        If the *variable* is an auxiliary coordinate then the
        identities of its dimensions must be provided. Ignored if the
        *variable* is a dimension coordinate.

    copy: bool, optional
        If False then the *variable* is not copied before
        insertion. By default it is copied.

:Returns:

    None

**Examples**

>>>

'''
        if key.startswith('d'):
            self.insert_dim(variable, key=key, copy=copy)
            
        elif key.startswith('a'):
            self.insert_aux(variable, key=key, axes=axes, copy=copy)

        else:
            raise ValueError("bad key in insert_coord: '%s'" % key)
    #--- End: def

    def insert_dim(self, item, key=None, copy=True, replace=True):
        '''

Insert a dimension coordinate to the domain in place.

:Parameters:

    item: cf.DimensionCoordinate or cf.Coordinate or cf.AuxiliaryCoordinate
        The new coordinate. If not a dimension coordinate object then
        it will be converted to one.

    key : str, optional
        The identifier for the new dimension coordinate. The
        identifier is of the form ``'dimN'`` where the ``N`` part
        should be replaced by an arbitrary integer greater then or
        equal to zero. By default a unique identifier will be
        generated.

    copy: bool, optional
        If False then the dimension coordinate is not copied before
        insertion. By default it is copied.
      
    replace : bool, optional
        If False then do not replace an existing dimension coordinate
        with the same identifier. By default an existing dimension
        coordinate with the same identifier is replaced with *coord*.
    
:Returns:

    out : str
        The identifier for the new dimension coordinate (see the *key*
        parameter).

**Examples**

>>>

'''
        item = item.asdimension(copy=copy)            

        if key is None:
            key = self.insert_axis(item.size)
        else:
            if key in self.d and not replace:
                raise ValueError(
"Can't insert dimension coordinate object: replace=%s and %r identifier already exists" %
(replace, key))

            self.insert_axis(item.size, key, replace=False)
        #--- End: if

        dimensions = self.dimensions

        dimensions[key] = [key]

        # ------------------------------------------------------------
        # Turn a scalar dimension coordinate into size 1, 1-d
        # ------------------------------------------------------------
        if item.isscalar:
            item.expand_dims(0, i=True)

        self.d[key] = item

        self._map[key] = 'd'

        transforms = self.t
        if transforms:
            for transform in transforms.itervalues():
                self._conform_transform(transform)

        return key
    #--- End: def

    def _insert_item(self, variable, key, role, axes=None, copy=True,
                     replace=True):
        '''

Insert a new auxiliary coordinate into the domain in place, preserving
internal consistency.

:Parameters:

    coord :cf.Coordinate
        The new auxiliary coordinate.

    key : str
        The identifier for the auxiliary coordinate or cell
        measure.

    role : str

    axes : sequence, optional
        The ordered axes of the new coordinate. Ignored if the
        coordinate is a dimension coordinate. Required if the
        coordinate is an auxiliary coordinate.

    copy: bool, optional
        If False then the auxiliary coordinate is not copied before
        insertion. By default it is copied.

    replace : bool, optional
        If False then do not replace an existing dimension coordinate
        with the same identifier. By default an existing dimension
        coordinate with the same identifier is replaced with *coord*.

:Returns:

    out : str
        The identifier for the new auxiliary coordinate (see the 'key'
        parameter).


**Examples**

>>>

'''
        if key in self.dimensions and not replace:
            raise ValueError(
                "Can't insert %s: '%s' identifier already exists" %
                (role, key))

        if not axes:
            # try default axes
            axes = ['dim%d' % n for n in range(variable.ndim)]
            if not axes:
                axes = ['dim0']

            for axis, size in izip_longest(axes, variable.shape, fillvalue=1):
                if axis not in self.dimension_sizes:
                    raise ValueError(
                        "Can't insert %s: Default dimension '%s' doesn't exist"
                        % (role, axis))

                if self.dimension_sizes[axis] != size:
                    raise ValueError(
                        "Can't insert %s: Default axis %r has wrong size (%d)" %
                        (role, axis, size))

        else:
            # Axes have been provided
            keys = []                                   
            for axis, size in  izip_longest(axes, variable.shape, fillvalue=1):
                k = self.axis(axis)
                if k is None:                    
                    raise ValueError("Can't insert %s: No %r axis " %
                                     (role, axis))

                self.insert_axis(size, key=k, replace=False)
                keys.append(k)
            #--- End: for
            axes = keys
        #--- End: if

        n_axes = len(set(axes))
        ndim   = variable.ndim
        if not (ndim == n_axes or (ndim == 0 and n_axes == 1)):
            raise ValueError(
                "Can't insert %s: Mismatched number of axes (%d != %d)" % 
                (role, n_axes, ndim))

        self.dimensions[key] = axes

        if not ndim:
            # Turn a scalar item into size 1, 1-d, copying it
            # required.
            variable = variable.expand_dims(0, i=(not copy))
        elif copy:
            # Copy the variable
            variable = variable.copy()

        return variable
    #--- End: def

    def inspect(self):
        '''

Inspect the object for debugging.

.. seealso:: `cf.inspect`

:Returns: 

    None

'''
        print cf_inspect(self)
    #--- End: def

    def items(self, items=None, role=None, axes=None, ctype=None, exact=False,
              inverse=False, match_all=True, rank=None, regex=False,
              strict_axes=False):
        '''

Return items of the domain.

This method has exactly the same interface, functionality and outputs
as `cf.Field.items`. Therefore see `cf.Field.items` for the full
documentation details.

:Parameters:

    items : *optional*
        See `cf.Field.items`.

    role : (sequence of) str, optional
        See `cf.Field.items`.

    ctype : (sequence of) str, optional
        See `cf.Field.items`.

    axes : *optional*
        See `cf.Field.items`.

    rank : int or cf.Comparison, optional
        See `cf.Field.items`.

    exact : bool, optional
        See `cf.Field.items`.

    inverse : bool, optional
        See `cf.Field.items`.

    match_all : bool, optional
        See `cf.Field.items`.

    regex : bool, optional
        See `cf.Field.items`.

    strict_axes : bool, optional
        See `cf.Field.items`.

:Returns:

    out : dict
       See `cf.Field.items`.
 
.. seealso:: `axes`, `item`, `remove_items`

**Examples**

See `cf.Field.items`.

All of these examples are for the same domain, whose complete
dictionary of items is shown in the first example.

>>> d.items()
{'dim0': <CF DimensionCoordinate: grid_latitude(111) degrees>,
 'dim1': <CF DimensionCoordinate: grid_longitude(106) degrees>,
 'dim2': <CF DimensionCoordinate: time(12) days since 1997-1-1>,
 'aux0': <CF AuxiliaryCoordinate: longitude(111, 106) degrees_E>,
 'aux1': <CF AuxiliaryCoordinate: latitude(111, 106) degrees_N>,
 'aux2': <CF AuxiliaryCoordinate: forecast_reference_time(12) days since 1997-1-1>
 'cm0': <CF CellMeasure: area(111, 106) m2>,
 'trans0': <CF Transform: rotated_latitude_longitude>}

>>> d.items(axes='grid_latitude')
{'dim0': <CF DimensionCoordinate: grid_latitude(111) degrees>,
 'aux0': <CF AuxiliaryCoordinate: longitude(111, 106) degrees_E>,
 'aux1': <CF AuxiliaryCoordinate: latitude(111, 106)> degrees_N,
 'cm0': <CF CellMeasure: area(111, 106) m2>}
>>> d.items(axes='grid_latitude', rank=1)
{'dim0': <CF DimensionCoordinate: grid_latitude(111) degrees>}
>>> d.items(axes='grid_latitude', strict_axes=True)
{'dim0': <CF DimensionCoordinate: grid_latitude(111) degrees>}

>>> d.items(axes='time')
{'dim2': <CF DimensionCoordinate: time(12) days since 1997-1-1>,
 'aux2': <CF AuxiliaryCoordinate: forecast_reference_time(12) days since 1997-1-1>}
>>> d.items(axes='time', role='d')
{'dim2': <CF DimensionCoordinate: time(12) days since 1997-1-1>}

>>> d.items(axes='area')
{'aux0': <CF AuxiliaryCoordinate: longitude(111, 106) degrees_E>,
 'aux1': <CF AuxiliaryCoordinate: latitude(111, 106) degrees_N>,
 'cm0': <CF CellMeasure: area(111, 106) m2>}
>>> d.items(axes=['grid_latitude', 'grid_longitude'])
{'aux0': <CF AuxiliaryCoordinate: longitude(111, 106) degrees_E>,
 'aux1': <CF AuxiliaryCoordinate: latitude(111, 106) degrees_N>,
 'cm0': <CF CellMeasure: area(111, 106) m2>}

>>> d.items(axes=d.axes(ctype='T'))
{'dim2': <CF DimensionCoordinate: time(12) days since 1997-1-1>,
 'aux2': <CF AuxiliaryCoordinate: forecast_reference_time(12) days since 1997-1-1>}
>>> d.items(axes={'units': 'hrs since 1-1-1'})
{'dim2': <CF DimensionCoordinate: time(12) days since 1997-1-1>,
 'aux2': <CF AuxiliaryCoordinate: forecast_reference_time(12) days since 1997-1-1>}

>>> d.items(ctype='T')
{'dim2': <CF DimensionCoordinate: time(12) days since 1997-1-1>,
 'aux2': <CF AuxiliaryCoordinate: forecast_reference_time(12) days since 1997-1-1>}

>>> d.items('grid')
{'dim0': <CF DimensionCoordinate: grid_latitude(111) degrees>,
 'dim1': <CF DimensionCoordinate: grid_longitude(106) degrees>}
>>> d.items('grid', exact=True)
{}

>>> d.items({'units': 'degrees_E'})
{'dim0': <CF DimensionCoordinate: grid_latitude(111) degrees>,
 'dim1': <CF DimensionCoordinate: grid_longitude(106) degrees>,
 'aux0': <CF AuxiliaryCoordinate: longitude(111, 106) degrees_E>,
 'aux1': <CF AuxiliaryCoordinate: latitude(111, 106)> degrees_N}
>>> d.items({'units': 'degrees_E'}, exact=True)
{'aux0': <CF AuxiliaryCoordinate: longitude(111, 106) degrees_E>}

>>> d.items({'units': 'radians', 'standard_name': 'time'})
{}
>>> d.items({'units': 'radians', 'standard_name': 'time'}, maximal_match=False)
{'dim0': <CF DimensionCoordinate: grid_latitude(111) degrees>,
 'dim1': <CF DimensionCoordinate: grid_longitude(106) degrees>,
 'dim2': <CF DimensionCoordinate: time(12) days since 1997-1-1>,
 'aux0': <CF AuxiliaryCoordinate: longitude(111, 106) degrees_E>,
 'aux1': <CF AuxiliaryCoordinate: latitude(111, 106)> degrees_N}
>>> d.items({'units': 'radians', 'standard_name': 'time'}, maximal_match=False, exact=True)
{'dim2': <CF DimensionCoordinate: time(12) days since 1997-1-1>}

>>> set(d.items(role='da')) == set(d.items(role='ct', inverse=True))
True

'''
        # ------------------------------------------------------------
        # Select items which have a given role
        # ------------------------------------------------------------
        if role is None:
            role = ('d', 'a', 'c', 't')

        rd = {}
        for r in role:
            rd.update(getattr(self, r))

        if axes is not None:           
            # --------------------------------------------------------
            # Select items which span specified axes
            # --------------------------------------------------------
            # Identify the axes
            axes = self.axes(axes)

            if not axes:
                rd = {}
            else:
                dimensions = self.dimensions
                
                d = {}
                if rank:
                    if not strict_axes:
                        # --------------------------------------------
                        # Select items which span at least one of the
                        # given axes, and possibly others.
                        # --------------------------------------------
                        for key, value in rd.iteritems():
                            x = dimensions.get(key, ())
                            if rank == len(x) and axes.intersection(x):
                                d[key] = value
#                    elif subset_axes:
#                        # --------------------------------------------
#                        # Select items which span exactly the given
#                        # axis
#                        # --------------------------------------------
#                        for key, value in rd.iteritems():
#                            x = dimensions.get(key, ())
#                            if rank == len(x) and axes.issuperset(x):
#                                d[key] = value
                    else:
                        # --------------------------------------------
                        # Select items which span exactly the given
                        # axis
                        # --------------------------------------------
                        for key, value in rd.iteritems():
                            x = dimensions.get(key, ())
                            if rank == len(x) and axes == set(x):
                                d[key] = value
                else:
                    
                    if not strict_axes:
                        # --------------------------------------------
                        # Select items which span at least one of the
                        # given axes, and possibly others.
                        # --------------------------------------------
                        for key, value in rd.iteritems():
                            if axes.intersection(dimensions.get(key, ())):
                                d[key] = value
#                    elif subset_axes:
#                        # --------------------------------------------
 #                       # Select items which span exactly the given
 #                       # axes
 #                       # --------------------------------------------
 #                       for key, value in rd.iteritems():
 #                           if axes.issuperset(dimensions.get(key, ())):
#                                d[key] = value
                    else:
                        # --------------------------------------------
                        # Select items which span exactly the given
                        # axes
                        # --------------------------------------------
                        for key, value in rd.iteritems():
                            if axes == set(dimensions.get(key, ())):
                                d[key] = value
                 #--- End: if
                rd = d
            #--- End: if

        elif rank:
            dimensions = self.dimensions
            
            d = {}
            for key, item in rd.iteritems():
                if rank == len(dimensions.get(key, ())):
                    d[key] = item
            #--- End: for
            rd = d
        #--- End: if

        if ctype is not None and rd:
            # --------------------------------------------------------
            # Select items which have a given coordinate type
            # --------------------------------------------------------
            ctype = tuple(ctype)
            d = {}
            for key, item in rd.iteritems():
                for ct in ctype:
#                    f = getattr(item, ct, None)
                    #if f is None:
                    #    # This item is not a dimension or auxiliary
                    #    # coordinate
                    #    break
                    #elif f:
                    #    # This item is a dimension or auxiliary
                    #    # coordinate of one of the specified types
                    #    d[key] = item
                    #    break
                    if getattr(item, ct):
                        # This item is a dimension or auxiliary
                        # coordinate of one of the specified types
                        d[key] = item
                        break
                #-- End: for
            #-- End: for
            rd = d 
        #--- End: if

        if rd and items is not None:
            # --------------------------------------------------------
            # Select items whose properties satisfy conditions
            # --------------------------------------------------------
            d = {}

            if isinstance(items, (basestring, dict, Comparison)):
                items= (items,)

            if items:
                match = []
                for m in items:
                    if m.__hash__ and m in rd:
                        # m is a domain item identifier
                        d[m] = rd.pop(m)
                    else:                    
                        match.append(m)
                #--- End: for
                
                if match and rd:                
                    for key, item in rd.iteritems():
                        for m in match:

#                            if m in _valid_ctypes:
#                                if getattr(item, m, False): 
#                                    # This item has the specified
#                                    # coordinate type
#                                    d[key] = item           
#                                    break   

                            if item.match(m, exact=exact, regex=regex,
                                          match_all=match_all):
                                # This item matches the at least one
                                # of the critieria
                                d[key] = item
                                break                            
                #--- End: if
            #--- End: if

            rd = d
        #--- End: if

        if inverse:
            # --------------------------------------------------------
            # Select items other than those previously selected
            # --------------------------------------------------------
            d = {}
            for r in ('d', 'a', 'c', 't'):
                d.update(getattr(self, r))

            for key in rd:
                del d[key]
                
            rd = d
        #--- End: if

        # ------------------------------------------------------------
        # Return the selected items
        # ------------------------------------------------------------
        return rd
    #--- End: def

    def transform_axes(self, key):
        '''

Return the axes spanned by the coordinate object inputs of a
transform.

:Parameters:

    key : str
        An transform identifier of the domain.

:Returns:

    out : set
        A set of the domain identifiers of the axes spanned by the
        transform's coordinates.

**Examples**

>>> t = d.item('rotated_latitude_longitude', key=True)
>>> d.transform_axes(t)
set(['dim2', 'dim1'])

'''
        axes = self.dimensions

        taxes = []
        for ckey in self.t[key].coords:
            taxes.extend(axes.get(ckey, ()))

        return set(taxes)
    #--- End: def

    def insert_aux(self, coord, key=None, axes=None, copy=True, replace=True):
        '''

Insert a auxiliary coordinate into the domain in place.

:Parameters:

    coord : cf.AuxiliaryCoordinate or cf.Coordinate or cf.DimensionCoordinate 
        The new coordinate. If not an auxiliary coordinate object then
        it will be converted to one.

    key : str, optional
        The identifier for the new dimension coordinate. The
        identifier is of the form ``'auxN'`` where the ``N`` part
        should be replaced by an arbitrary integer greater then or
        equal to zero. By default a unique identifier will be
        generated.

    axes : list, optional
        The ordered axes of the new coordinate. Ignored if the
        coordinate is a dimension coordinate. Required if the
        coordinate is an auxiliary coordinate.

    copy: bool, optional
        If False then the auxiliary coordinate is not copied before
        insertion. By default it is copied.

    replace : bool, optional
        If False then do not replace an existing auxiliary coordinate
        with the same identifier. By default an existing auxiliary
        coordinate with the same identifier is replaced with *coord*.

:Returns:

    out : str
        The identifier for the new auxiliary coordinate (see the *key*
        parameter).


**Examples**

>>>

'''
        coord = coord.asauxiliary(copy=copy)

        if not key:
            key = self.new_aux_identifier()

        coord = self._insert_item(coord, key, 'auxiliary coordinate', axes=axes,
                                  copy=False)

        self.a[key] = coord

        self._map[key] = 'a'

        transforms = self.t
        if transforms:
            for transform in transforms.itervalues():
                self._conform_transform(transform)

        return key
    #--- End: def

    def insert_cm(self, cm, key=None, axes=None, copy=True, replace=True):
        '''

Insert a cell measure into the domain in place.

:Parameters:

    cm : cf.CellMeasure
        The new cell measure.

    key : str, optional
        The identifier for the new cell measure. The identifier is of
        the form ``'cmN'`` where the ``N`` part should be replaced by
        an arbitrary integer greater then or equal to zero. By default
        a unique identifier will be generated.

    axes : sequence, optional
        The ordered axes of the new cell measure.

    copy : bool, optional
        If False then the cell measure is not copied before
        insertion. By default it is copied.

    replace : bool, optional
        If False then do not replace an existing cell measure with the
        same identifier. By default an existing cell measure with the
        same identifier is replaced with *cm*.

:Returns:

    out : str
        The identifier for the new cell measure (see the *key*
        parameter).

**Examples**

>>>

'''
        if key is None:
            key = self.new_cm_identifier()

        cm = self._insert_item(cm, key, 'cell measure', axes=axes, copy=copy)

        self.c[key] = cm

        self._map[key] = 'c'

        return key
    #--- End: def

    def insert_transform(self, transform, key=None, copy=True, replace=False):
        '''

Insert a transform object into the domain in place.

:Parameters:

    transform : cf.Transform
        The new transform object.

    key : str, optional
        The identifier for the new transform object. By default a
        unique identifier will be generated.

    copy : bool, optional
        If False then the transform object is not copied before
        insertion. By default it is copied.

    replace : bool, optional
        If True then replace an existing transform object with the
        same identifier. By default an exception is raised if there is
        an existing transform object with the same identifier.

:Returns:

    out : str
        The internal identifier of the new transform object.

**Examples**

>>>

'''
        if key is None:
            key = self.new_transform_identifier()
        elif not replace and key in self.t:
            raise ValueError(
"Can't insert transform object: replace=%s and %r identifier already exists" %
(replace, key))
        
        if copy:
            transform = transform.copy()

        self._conform_transform(transform)

        self.t[key] = transform

        self._map[key] = 't'

        return key
    #--- End: def

    def remove_axes(self,  axes=None, **kwargs):
        '''

Remove and return axes from the domain.

This method has exactly the same interface, functionality and outputs
as `cf.Field.remove_axes`. Therefore see `cf.Field.remove_axes` for
the full documentation details.

:Parameters:

    axes, kwargs : *optional*
        See `cf.Field.remove_axes`.

:Returns:

    out : list
        The removed axes. The list may be empty.

.. seealso:: `axes`, `remove_axis`, `remove_item`, `remove_items`

**Examples**

See `cf.Field.remove_axes`.

'''
        d = self

        # ------------------------------------------------------------
        # Find the domain axis identifiers
        # ------------------------------------------------------------
        axes = d.axes(axes, **kwargs)
        if not axes:
            return []

        if axes.intersection(d.dimensions.get('data', ())):
            raise ValueError(
                "Can't remove an axis which is spanned by the data array")

        axes_sizes = d.dimension_sizes

        for axis in axes:
            if (axes_sizes[axis] > 1 and
                d.items(role=('d', 'a', 'c'), rank=gt(1), axes=axis)):
                raise ValueError(
"Can't remove an axis with size > 1 which is spanned by a multidimensional item")
        #--- End: for

        items = d.items(role=('d', 'a', 'c'), axes=axes)
        for key, item in items.iteritems():
            item_axes = d.dimensions[key]

            # Remove the item if it only spans removed axes
            if axes.issuperset(item_axes):
                d.remove_item(key)
                continue

            # Still here? Then squeeze removed axes from the
            # multidimensional item.
            iaxes = [item_axes.index(axis) for axis in axes
                     if axis in item_axes]
            item.squeeze(iaxes, i=True)
#            item.squeeze(axes.intersection(item_axes))
#            if not item.ndim:
#                # Remove the multidimensional item if it doesn't span
#                # any axes after being squeezed
#                d.remove_item(key)
#            else:

            # Remove the removed axes from the multidimensional item's
            # list of axes
            for axis in axes.intersection(item_axes):
                item_axes.remove(axis)
                ##--- End: for
                #if not item_axes:  
                #    # Remove the item if it doesn't span any axes
                #    # after being squeezed
                #    d.remove_item(key)
        #--- End: for

        # ------------------------------------------------------------
        # Remove the axes
        # ------------------------------------------------------------
        for axis in axes:
            del axes_sizes[axis]

        return list(axes)
    #--- End: def

    def remove_axis(self, axis=None, **kwargs):
        '''

Remove and return an axis from the domain.

This method has exactly the same interface, functionality and outputs
as `cf.Field.remove_axis`. Therefore see `cf.Field.remove_axis` for
the full documentation details.

:Parameters:

    axis, kwargs : *optional*
        See `cf.Field.remove_axis`.

:Returns:

    out : 
        The domain identifier of the removed axis, or None if there
        isn't one.

.. seealso:: `axis`, `remove_axes`, `remove_item`, `remove_items`

**Examples**

See `cf.Field.remove_axis`.

'''
        axis = self.axis(axis, **kwargs)
        if axis is None:
            return

        return self.remove_axes(axis)[0]
    #--- End: def

    def remove_item(self, item=None, **kwargs):
        '''

Remove and return an item from the domain.

This method has exactly the same interface, functionality and outputs
as `cf.Field.remove_item`. Therefore see `cf.Field.remove_item` for
the full documentation details.

:Parameters:

    item, kwargs : *optional*
        See `cf.Field.remove_item`.

:Returns:

    out : 
        The removed item, or None if no unique item could be found.

.. seealso:: `item`, `remove_axes`, `remove_axis`, `remove_items`

**Examples**

See `cf.Field.remove_item`.

>>> d.items()
{'dim0': <CF DimensionCoordinate: grid_latitude(111) degrees>,
 'dim1': <CF DimensionCoordinate: grid_longitude(106) degrees>,
 'dim2': <CF DimensionCoordinate: time(12) days since 1997-1-1>,
 'aux0': <CF AuxiliaryCoordinate: longitude(111, 106) degrees_E>,
 'aux1': <CF AuxiliaryCoordinate: latitude(111, 106) degrees_N>,
 'aux2': <CF AuxiliaryCoordinate: forecast_reference_time(12) days since 1997-1-1>
 'cm0': <CF CellMeasure: area(111, 106) m2>,
 'trans0': <CF Transform: rotated_latitude_longitude>}
>>> d.remove_item('grid_long')
>>> d.remove_item('aux1')
>>> d.remove_item(ctype='T')
>>> d.remove_item('longitude', role='a', exact=True)
>>> d.remove_item('rotated_latitude_longitude')
>>> d.remove_item({None: 'area', 'units': 'km2'})
>>> d.items()
{'dim0': <CF DimensionCoordinate: grid_latitude(111) degrees>}

'''
        d = self

        items = d.items(item, **kwargs)
        if not items:
            return d

        key = items.popitem()[0]
        if items:
            return

        return d.remove_items(key)[0]
    #--- End: def

    def remove_items(self, items=None, **kwargs):
        '''

Remove and return items from the domain.

This method has exactly the same interface, functionality and outputs
as `cf.Field.remove_items`. Therefore see `cf.Field.remove_items` for
the full documentation details.

:Parameters:

    items, kwargs : *optional*
        See `cf.Field.remove_items`.

:Returns:

    out : list
        The removed items. The list may be empty.

.. seealso:: `items`, `remove_axes`, `remove_axis`, `remove_item`

**Examples**

See `cf.Field.remove_items`.

'''
        d = self

        out = []
        for key, item in d.items(items, **kwargs).iteritems():
            x = d._map[key]
            
            if x in 'da':
                d._replace_transforms_coord_identifier(key)
                
            del d._map[key]
      
            d.dimensions.pop(key, None)

            del getattr(d, x)[key]

            out.append(item)
        #--- End: if        

        return out
    #--- End: def

    def copy(self):
        '''

Return a deep copy.

``d.copy()`` is equivalent to ``copy.deepcopy(d)``.

:Returns:

    out : 
        The deep copy.

**Examples**

>>> e = d.copy()

'''
        X = type(self)
        new = X.__new__(X)

        new.dimensions = {}
        for key, value in self.dimensions.iteritems():
            new.dimensions[key] = value[:]
            
        new.dimension_sizes = self.dimension_sizes.copy()

        new._map = self._map.copy()

        new.d = {}
        for key, value in self.d.iteritems():
            new.d[key] = value.copy()

        new.a = {}
        for key, value in self.a.iteritems():
            new.a[key] = value.copy()

        new.c = {}
        for key, value in self.c.iteritems():
            new.c[key] = value.copy()

        new.t = {}
        for key, value in self.t.iteritems():
            new.t[key] = value.copy()

        return new
    #--- End: def

    def close(self):
        '''

Close all referenced open data files.

:Returns:

    None

**Examples**

>>> d.close()

'''
        for item in self.items().itervalues():
            item.close()
    #--- End: def

    def item(self, item=None, key=False, **kwargs):
        '''

Return an item of the domain, or its domain identifier.

This method has exactly the same interface, functionality and outputs
as `cf.Field.item`. Therefore see `cf.Field.item` for the full
documentation details.

:Parameters:

    item, kwargs : *optional*
        See `cf.Field.item`.

    key : bool, option
        See `cf.Field.item`.

:Returns:

    out : 
        See `cf.Field.item`.
     
.. seealso:: `axis`, `items`, `remove_item`

**Examples**

See `cf.Field.items`.

The following examples are base on the following domain:

>>> d.items()
{'dim0': <CF DimensionCoordinate: grid_latitude(73)
 'dim1': <CF DimensionCoordinate: grid_longitude(96)>,
 'dim2': <CF DimensionCoordinate: time(12)>,
 'aux0': <CF AuxiliaryCoordinate: latitude(73, 96)>,
 'aux1': <CF AuxiliaryCoordinate: longitude(73, 96)>,
 'cm0':  <CF CellMeasure: area(96, 73)>,
 'trans0': <CF Transform: rotated_latitude_longitude>}

>>> d.item('longitude')
<CF DimensionCoordinate: longitude(360)>
>>> d.item('long')
<CF DimensionCoordinate: longitude(360)>
>>> d.item('long', key=True)
'dim2'

>>> d.item('lon', exact=True)
None
>>> d.item('longitude', exact=True)
<CF DimensionCoordinate: longitude(360)>

>>> d.item('cm0')
<CF CellMeasure: area(96, 73)>

>>> d.item({'units': 'degrees'})
None
>>> d.item({'units': 'degreeN'})
<CF AuxiliaryCoordinate: latitude(73, 96)>

>>> d.item(axes='time')
<CF DimensionCoordinate: time(12)>
>>> d.item(axes='grid_latitude')
None
>>> d.item(axes='grid_latitude', strict_axes=True)
<CF DimensionCoordinate: grid_latitude(73)
>>> d.item(axes='grid_longitude', rank=1, key=True)
'dim1'

'''    
        d = self.items(item, **kwargs)
        if not d:
            return

        items = d.popitem()

        if d:
            return

        if key:
            return items[0]
        else:
            return items[1]
    #--- End: def

    def item_axes(self, item=None, **kwargs):
        '''

Return the axes of a domain item.

This method has exactly the same interface, functionality and outputs
as `cf.Field.item_axes`. Therefore see `cf.Field.item_axes` for the
full documentation details.

:Parameters:

    item, kwargs : *optional*
         See `cf.Field.item_axes`.

:Returns:

    out : list or None
        The ordered list of axes for the item or, if there is no
        unique item or the item is a transform, then None is returned.
       
.. seealso:: `axes`, `data_axes`, `item`

**Examples**

See `cf.Field.item_axes`.

'''    
        kwargs['key'] = True
        key = self.item(item, **kwargs)

        if key is not None and self._map[key] != 't':
            return self.dimensions[key][:]
    #--- End: def

    def data_axes(self):
        '''

Return the axes of the field's data array.

This method has exactly the same interface, functionality and outputs
as `cf.Field.data_axes`. Therefore see `cf.Field.data_axes` for the
full documentation details.

:Returns:

    out : list or None
        The ordered axes of the field's data array. If there is no
        data array then None is returned.
       
.. seealso:: `axes`, `item_axes`

**Examples**

See `cf.Field.data_axes`.

'''    
        axes = self.dimensions.get('data', None)
        if axes is not None:
            return axes[:]
    #--- End: def

    def axes(self, axes=None, ordered=False, size=None, **kwargs):
        '''

Return domain axis identifiers.

This method has exactly the same interface, functionality and outputs
as `cf.Field.axes`.

See `cf.Field.axes` for details.

:Parameters:

    axes : *optional*
        See `cf.Field.axes`.

    ordered : bool, optional
        See `cf.Field.axes`.

    size : int or cf.Comparison, optional
        See `cf.Field.axes`.

    kwargs : *optional*
        See `cf.Field.axes`.

:Returns:

    out : set or list
        A set of domain axis identifiers, or a list if *ordered* is
        True. The set or list may be empty.

.. seealso:: `axis`, `items`, `remove_axes`

**Examples**

See `cf.Field.axes`.

'''
        def _axes(self, axes, size, item_axes, axes_sizes, kwargs):

            a = None

            if axes is not None:
                if axes.__hash__:
                    if axes in axes_sizes:
                        # ------------------------------------------------
                        # match is a domain axis identifier
                        # ------------------------------------------------
                        a = (axes,)
                    elif axes in item_axes and not kwargs:
                        # ------------------------------------------------
                        # match is a domain item identifier
                        # ------------------------------------------------
                        a = item_axes[axes]
                    elif isinstance(axes, slice):                       
                        a = item_axes.get('data', ())[axes]
                    else:
                        try:
                            a = (item_axes.get('data', ())[axes],)
                        except IndexError:
                            a = ()
                        except TypeError:
                            pass
                #--- End: if

            elif not kwargs:
                a = tuple(axes_sizes)
            #--- End: if

            if a is None:
                # --------------------------------------------------------
                # Assume that match is a value accepted by the items
                # method
                # --------------------------------------------------------
                a = [] 
                for key in self.items(axes, **kwargs):                
                    a += item_axes.get(key, ())
            #--- End: if

            if size:
                a = [axis for axis in a if size == axes_sizes[axis]]

            return a
        #--- End: def

        if kwargs:
            kwargs['axes'] = None

        item_axes  = self.dimensions
        axes_sizes = self.dimension_sizes

        if axes is None or isinstance(axes, (basestring, dict, slice, int, long)):
            # --------------------------------------------------------
            # axes is not a sequence or a set
            # --------------------------------------------------------
            a = _axes(self, axes, size, item_axes, axes_sizes, kwargs)
        else:   
            # --------------------------------------------------------
            # axes is a sequence or a set
            # --------------------------------------------------------
            a = []
            for x in axes:
                a += _axes(self, x, size, item_axes, axes_sizes, kwargs)
        #--- End: if

        if not ordered:
            return set(a)
        else:
            return list(a)
    #--- End: def
        
    def axis(self, axis=None, **kwargs):
        '''

Return a domain axis identifier.

The axis may be selected with the keyword arguments. When multiple
criteria are given, the axis will be the intersection of the
selections. If no unique axis can be found then None is returned.

:Parameters:

    axis, kwargs : *optional*
         Select the unique axis (it it exists) which would be selected
         by this call of the domain's `~cf.Domain.axes` method:
         ``d.axes(match=match, **kwargs)``. See `cf.Domain.axes` for
         details.

:Returns:

    out : str
        The unique domain axis identifier. If there isn't a unique
        axis then None is returned.

.. seealso:: `axes`, `item`, `items`, `remove_item`

**Examples**

'''
        axes = self.axes(axis, **kwargs)
        if not axes:
            return

        axis = axes.pop()

        if not axes:
            return axis
        else:
            return
    #--- End: def

    def axes_sizes(self, axes=None, size=None, **kwargs):
        '''
'''
        out = {}
        
        axes = self.axes(axes, size=size, **kwargs)

        for axis in axes:
            out[axis] = self.dimension_sizes[axis]
                
        if 1: #name:
            out2 = {}
            for axis, size in out.iteritems():
                out2[self.axis_name(axis)] = size

            return out2            

        return out
    #--- End: def

    def axis_size(self,  axis=None, **kwargs):
        '''
'''
        axis = self.axis(axis, **kwargs)
        if axis is None:
            return None

        return self.dimension_sizes[axis]
    #--- End: def

    def expand_dims(self, coord=None, size=1, copy=True):
        '''

Expand the domain with a new dimension in place.

The new dimension may by of any size greater then 0.

:Parameters:

    coord : cf.Coordinate, optional
        A dimension coordinate for the new dimension. The new
        dimension's size is set to the size of the coordinate's array.

    size : int, optional
        The size of the new dimension. By default a dimension of size
        1 is introduced. Ignored if *coord* is set.

:Returns:

    None

**Examples**

>>> d.expand_dims()
>>> d.expand_dims(size=12)
>>> c
<CF DimensionCoordinate: >
>>> d.expand_dims(coord=c)

'''
        if coord:            
            self.insert_dim(coord, copy=copy)
        else:
            self.insert_axis(size)
    #--- End: def

    def dump_axes(self, display=True, level=0):
        '''
        
Return a string containing a description of the domain.

:Parameters:

    display : bool, optional
        If False then return the description as a string. By default
        the description is printed.

    level : int, optional

:Returns:

    out : str
        A string containing the description.

**Examples**

'''
        indent1 = '    ' * level
        indent2 = '    ' * (level+1)

        string = ['%sAxes:' % indent1]
        
        x = sorted(['%s%s(%d)' % (indent2,
                                  self.axis_name(axis),
                                  self.dimension_sizes[axis])
                    for axis in self.dimension_sizes])
        string.extend(x)

        string = '\n'.join(string)
       
        if display:
            print string
        else:
            return string
    #--- End: def
         
    def dump_components(self, complete=False, display=True, level=0):
        '''
        
Return a string containing a full description of the domain.

:Parameters:

    complete : bool, optional

    display : bool, optional
        If False then return the description as a string. By default
        the description is printed.

    level : int, optional

:Returns:

    out : str
        A string containing the description.

**Examples**

'''
        indent1 = '    ' * level

        string = []
         
        # Dimension coordinates
        for key, value in sorted(self.d.iteritems()):
            string.append('')
            string.append('%sDimension coordinate: %s' %
                          (indent1, value.name('')))
            string.append(value.dump(display=False, domain=self, key=key, level=level+1))

        # Auxiliary coordinates
        for key, value in sorted(self.a.iteritems()):
            string.append('')
            string.append('%sAuxiliary coordinate: %s' % 
                          (indent1, value.name('')))
            string.append(value.dump(display=False, domain=self, key=key,
                                     level=level+1))
               
        # Cell measures
        for key, value in sorted(self.c.iteritems()):
            string.append('')
            string.append(value.dump(display=False, domain=self, key=key,
                                     level=level))

        # Transforms
        for key, value in sorted(self.t.iteritems()):
            string.append('')
            string.append(value.dump(display=False, complete=complete,
                                     domain=self, level=level))

        return '\n'.join(string)
    #--- End: def

    def dump(self, complete=False, display=True, level=0):
        '''

Return a string containing a full description of the domain.

:Parameters:

    complete : bool, optional
        Output a complete dump. Fields contained in transforms are
        themselves described with their dumps.

    display : bool, optional
        If False then return the description as a string. By default
        the description is printed, i.e. ``d.dump()`` is equivalent to
        ``print d.dump(display=False)``.

    level : int, optional

:Returns:

    out : None or str
        A string containing the description.

complete : bool, optional

**Examples**

'''
        string = (self.dump_axes(display=False, level=level),
                  self.dump_components(complete=complete, display=False,
                                       level=level),
                  )

        string = '\n'.join(string)
       
        if display:
            print string
        else:
            return string
    #--- End: def

    def equals(self, other, rtol=None, atol=None, traceback=False):
        '''

True if two domains are equal, False otherwise.

Equality is defined as follows:

* There is one-to-one correspondence between dimensions and dimension
  sizes between the two domains.

* For each domain component type (dimension coordinate, auxiliary
  coordinate and cell measures), the set of constructs in one domain
  equals that of the other domain. The component identifiers need not
  be the same.

* The set of transforms in one domain equals that of the other
  domain. The transform identifiers need not be the same.

Equality of numbers is to within a tolerance.

:Parameters:

    other :
        The object to compare for equality.

    atol : float, optional
        The absolute tolerance for all numerical comparisons, By
        default the value returned by the `ATOL` function is used.

    rtol : float, optional
        The relative tolerance for all numerical comparisons, By
        default the value returned by the `RTOL` function is used.

    traceback : bool, optional
        If True then print a traceback highlighting where the two
        instances differ.

:Returns: 

    out : bool
        Whether or not the two instances are equal.

**Examples**

>>> d.equals(s)
True

>>> d.equals(t)
False

>>> d.equals(t, traceback=True)


'''
        if self is other:
            return True
        
        # Check that each instance is the same type
        if type(self) != type(other):
            print("%s: Different types: %s, %s" %
                  (self.__class__.__name__,
                   self.__class__.__name__,
                   other.__class__.__name__))
            return False
        #--- End: if

        if (sorted(self.dimension_sizes.values()) != 
            sorted(other.dimension_sizes.values())):
            # There is not a 1-1 correspondence between dimensions and
            # dimension sizes between the two domains.           
            if traceback:
                print("%s: Different ranks: %s, %s" %
                      (self.__class__.__name__,
                       sorted(self.dimension_sizes.values()),
                       sorted(other.dimension_sizes.values())))
            return False
        #--- End: if

        if rtol is None:
            rtol = RTOL()
        if atol is None:
            atol = ATOL()              

        # ------------------------------------------------------------
        # Test the coordinates and cell measures. Don't worry about
        # transforms yet - we'll do so later.
        # ------------------------------------------------------------
#        key_map = {}
##        for self_keys, other_keys in izip(sorted(self.d),  sorted(self.a),  sorted(self.c),
##                                          sorted(other.d), sorted(other.a), sorted(other.c)):
#        for self_keys, other_keys in izip((sorted(self.d),  sorted(self.a),  sorted(self.c)),
#                                          (sorted(other.d), sorted(other.a), sorted(other.c))):
#            for key0 in self_keys:
#                found_match = False
#                for key1 in other_keys: 
#                    if self.get(key0).equals(other.get(key1), rtol=rtol, atol=atol,
#                                             traceback=False):
#                        found_match = True
#                        key_map[key1] = key0
#                        other_keys.remove(key1)
#                        break
#                #--- End: for
#
#                if not found_match:
#                    if traceback:
#                        print("%s: Different coordinate: %s" %
#                              (self.__class__.__name__, repr(self[key0])))
#                    return False
#            #--- End: for
#        #--- End: for

        key_map = {}
        for self_keys, other_keys in izip((self.d,  self.a,  self.c),
                                          (other.d, other.a, other.c)):
            self_items  = self_keys.items()
            other_items = other_keys.items()
            for key0, value0 in self_items:
                found_match = False
                for i, (key1, value1) in enumerate(other_items):
                    if value0.equals(value1, rtol=rtol, atol=atol,
                                     traceback=False):
                        found_match = True
                        key_map[key1] = key0
                        other_items.pop(i)
                        break
                #--- End: for

                if not found_match:
                    if traceback:
                        print("%s: Different coordinate: %r" %
                              (self.__class__.__name__, value0))
                    return False
            #--- End: for
        #--- End: for

        # ------------------------------------------------------------
        # Test the transforms
        # ------------------------------------------------------------
        self_t  = self.t
        other_t = other.t
        if not self_t:
            if other_t:
                # Self doesn't have any transforms but other does
                if traceback:
                    print(
"%s: Different numbers of transforms: 0 != %d" %
(self.__class__.__name__, len(other_t)))
                return False
        else:
            if not other_t:
                # Other doesn't have any transforms but self does
                if traceback:
                    print(
"%s: Different numbers of transforms: %d != 0" %
(self.__class__.__name__, len(self_t)))
                return False
            #--- End: if

            transforms1 = other_t.keys()

            for key0, transform0 in self_t.iteritems():
                found_match = False

                for key1 in transforms1:
                    transform1 = other_t[key1]

                    if self._equal_transform(transform0, transform1,
                                             domain=other, pointer_map=key_map,
                                             traceback=False): 
                        # This transform is also in other
                        found_match = True
                        transforms1.remove(key1)
                        break
                #--- End: for

                if not found_match:
                    # This transform was not found in other
                    if traceback:
                        print("%s: Missing transform: %r" %
                              (self.__class__.__name__, transform0))
                    return False
            #--- End: for                    
        #--- End: if

        # ------------------------------------------------------------
        # Still here? Then the two domains are equal
        # ------------------------------------------------------------
        return True
    #--- End: def

#--- End: class
