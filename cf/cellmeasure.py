from .variable import Variable

# ====================================================================
#
# CellMeasure object
#
# ====================================================================

class CellMeasure(Variable):
    '''
    
A CF cell measure construct containing information that is needed
about the size, shape or location of the field's cells.

It is a variable which contains a data array and metadata comprising
properties to describe the physical nature of the data.


**Attributes**

==========  =======  =================================================
Attribute   Type     Description
==========  =======  =================================================
`!measure`  ``str``  The spatial measure being represented. Either
                     ``'area'`` or ``'volume'`` are allowed by the CF
                     conventions.
==========  =======  =================================================

'''   
    def dump(self, display=True, domain=None, key=None, level=0):
        '''

Return a string containing a full description of the cell measure.

:Parameters:

    display : bool, optional
        If False then return the description as a string. By default
        the description is printed, i.e. ``c.dump()`` is equivalent to
        ``print c.dump(display=False)``.

    level : int, optional

:Returns:

    out : None or str
        A string containing the description.

**Examples**

'''
        indent1 = '    ' * level
        indent2 = '    ' * (level+1)

        if hasattr(self, 'measure'):
            string = ['%sCell measure: %s' % (indent1, self.measure)]
        elif hasattr(self.Units, 'units'):
            string = ['%sCell measure: %s' % (indent1, self.units)]
        else:
            string = ['%sCell measure: %s' % (indent1, self.name(default=''))]

        if self._hasData:
            if domain:
                x = ['%s(%d)' % (domain.axis_name(dim),
                                 domain.dimension_sizes[dim])
                     for dim in domain.dimensions[key]]
                string.append('%sData(%s) = %s' % (indent2, ', '.join(x), str(self.Data)))
            else:
                x = [str(s) for s in self.shape]
                string.append('%sData(%s) = %s' % (indent2, ', '.join(x), str(self.Data)))
        #--- End: if

        if self._simple_properties():
            string.append(self._dump_simple_properties(level=level+1))
          
        string = '\n'.join(string)
       
        if display:
            print string
        else:
            return string
    #--- End: def

    def identity(self, default=None):
        '''

Return the cell measure's identity.

The identity is first found of:

* The `!measure` attribute.

* The `standard_name` CF property.

* The `!id` attribute.

* The value of the *default* parameter.

:Parameters:

    default : optional
        If none of `measure`, `standard_name` and `!id` exist then
        return *default*. By default, *default* is None.

:Returns:

    out :
        The identity.

**Examples**

'''
        return super(CellMeasure, self).identity(default)
    #--- End: def

    def name(self, default=None, identity=False):
        '''

Return a name for the cell measure.

Returns the the first found of:

* The `!measure` attribute.

* The `standard_name` CF property.

* The `!id` attribute.

* If the *identity* parameter is False, the `long_name` CF property,
  preceeded by the string ``'long_name:'``.

* If the *identity* parameter is False, the `!ncvar` attribute,
  preceeded by the string ``'ncvar:'``.

* The value of the *default* parameter.

Note that ``c.name(identity=True)`` is equivalent to ``c.identity()``.

:Parameters:

    default : *optional*
        If no name can be found then return the value of the *default*
        parameter. By default the default is None.

    identity : bool, optional
        If True then only return ....

:Returns:

    out : str
        A  name for the variable.

.. seealso:: `identity`

**Examples**

>>> f.standard_name = 'air_temperature'
>>> f.long_name = 'temperature of the air'
>>> f.ncvar = 'tas'
>>> f.name()
'air_temperature'
>>> del f.standard_name
>>> f.name()
'long_name:temperature of the air'
>>> del f.long_name
>>> f.name()
'ncvar:tas'
>>> del f.ncvar
>>> f.name()
None
>>> f.name('no_name')
'no_name'
>>> f.standard_name = 'air_temperature'
>>> f.name('no_name')
'air_temperature'

'''
        n = getattr(self, 'measure', None)
        if n is not None:
            return n

        return super(CellMeasure, self).name(default, identity=identity)
    #--- End: def

#--- End: class
