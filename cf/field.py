from numpy import argmax as numpy_argmax
from numpy import size   as numpy_size
from numpy import where  as numpy_where

from copy      import deepcopy
from itertools import izip
from operator  import mul as operator_mul

from .ancillaryvariables import AncillaryVariables
from .cellmeasure        import CellMeasure
from .cellmethods        import CellMethods
from .comparison         import Comparison, gt
from .constants          import masked as cf_masked
from .domain             import Domain
from .fieldlist          import FieldList
from .flags              import Flags
from .functions          import parse_indices, CHUNKSIZE, equals
from .units              import Units
from .variable           import Variable, SubspaceVariable

from .data.data import Data

_units_radians = Units('radians')
_units_m  = Units('m')
_units_m2 = Units('m2')

# --------------------------------------------------------------------
# Map each allowed input collapse method name to its corresponding
# cf.Data method
# --------------------------------------------------------------------
_collapse_methods = {'avg'               : 'mean',
                     'average'           : 'mean',
                     'mean'              : 'mean',
                     'max'               : 'max',
                     'maximum'           : 'max',
                     'mid_range'         : 'mid_range',
                     'min'               : 'min',
                     'minimum'           : 'min',
                     'range'             : 'range',
                     'sd'                : 'sd',
                     'standard_deviation': 'sd',
                     'sum'               : 'sum',
                     'var'               : 'var',
                     'variance'          : 'var',
                     }

# --------------------------------------------------------------------
# Map each cf.Data method to its corresponding cell_method name
# --------------------------------------------------------------------
_collapse_cell_methods = {'max'      : 'maximum',
                          'mean'     : 'mean',
                          'mid_range': 'mid_range',
                          'min'      : 'minimum',
                          'range'    : 'range',
                          'sd'       : 'standard_deviation',
                          'sum'      : 'sum',
                          'var'      : 'variance',
                          }

# --------------------------------------------------------------------
# Map each cf.Data method to its corresponding minimum number of
# elements
# --------------------------------------------------------------------
_collapse_min_size = {'max'      : 1,
                      'mean'     : 1,
                      'mid_range': 1,
                      'min'      : 1,
                      'range'    : 1,
                      'sd'       : 2,
                      'sum'      : 1,
                      'var'      : 2,
                      }

# --------------------------------------------------------------------
# These cf.Data methods may be weighted
# --------------------------------------------------------------------
_collapse_weighted_methods = set(('mean', 'sd', 'var'))

# --------------------------------------------------------------------
# These cf.Data methods may specify a number of degrees of freedom
# --------------------------------------------------------------------
_collapse_ddof_methods = set(('sd', 'var'))

# ====================================================================
#
# Field object
#
# ====================================================================

class Field(Variable):
    '''

A field construct according to the CF data model.

A field is a container for a data array and metadata comprising
properties to describe the physical nature of the data and a
coordinate system (called a domain) which describes the positions of
each element of the data array.

The field's domain may contain dimensions and auxiliary coordinate and
cell measure objects (which themselves contain data arrays and
properties to describe them) and transform objects.

All components of a field are optional.

**Miscellaneous**

Field objects are picklable.

'''

    _special_properties = Variable._special_properties.union(        
        ('ancillary_variables',
         'cell_methods',
         'flag_values',
         'flag_masks',
         'flag_meanings')
         )
    
    def __init__(self, properties={}, attributes={}, data=None, domain=None,
                 flags=None, ancillary_variables=None, axes=None, 
                 copy=True):
        '''

**Initialization**

:Parameters:

    properties : dict, optional
        Provide the new field with CF properties from the dictionary's
        key/value pairs.

    data : cf.Data, optional
        Provide the new field with an N-dimensional data array in a
        `cf.Data` object.

    domain : cf.Domain, optional
        Provide the new field with a coordinate system in a
        `cf.Domain` object. By default an empty domain is created.

    attributes : dict, optional
        Provide the new field with attributes from the dictionary's
        key/value pairs.

    flags : cf.Flags, optional
        Provide the new field with self-describing flag values in a
        `cf.Flags` object.

    ancillary_variables : cf.AncillaryVariables, optional
        Provide the new field with ancillary variable fields in a
        `cf.AncillaryVariables` object.

    axes : sequence of str, optional
        A list of domain axis identifiers (``'dimN'``), stating the
        axess, in order, of field's data array. By default these axis
        identifiers will be the sequence of consecutive axis
        identifiers ``'dim0'`` up to ``'dimM'``, where ``M`` is the
        number of axes of the data array, or an empty sequence if the
        data array is a scalar.

        If the field's domain already defines any of these axes, then
        their identifiers and sizes must correspond to those of the
        field's data array. Otherwise, new axes will be created in the
        field's domain.

    copy : bool, optional
        If False then do not deep copy arguments prior to
        initialization. By default arguments are deep copied.

'''
        # Initialize the new field with attributes and CF properties
        super(Field, self).__init__(properties=properties,
                                    attributes=attributes, copy=copy)   
    
        # Domain
        if domain is not None:
            if not copy:
                self.domain = domain
            else:
                self.domain = domain.copy()
        else:
            # A field always has a domain
            self.domain = Domain()

        # Data array
        if data is not None:
            self.insert_data(data, axes=axes, copy=copy)
           
        # Flags
        if flags is not None:
            if not copy:
                self.Flags = flags
            else:
                self.Flags = flags.copy()
        #--- End: if

        # Ancillary variables
        if ancillary_variables is not None:
            if not copy:
                self.ancillary_variables = ancillary_variables
            else:
                self.ancillary_variables = ancillary_variables.copy()
        #--- End: if

        # Cyclic axes
        self.setcyclic()
    #--- End: def

    def broadcastable(self, g):
        '''
'''
        # ------------------------------------------------------------
        # Analyse each domain
        # ------------------------------------------------------------
        s = self.domain.analyse()
        v = g.domain.analyse()

        if s['warnings'] or v['warnings']:
            return False

        matching_size_gt1_ids = []
        for x, coord0 in s['id_to_coord']:
            size0 = coord0.size
            if size0 == 1:
                continue
            
            if x in v['id_to_coord']:
                coord1 = v['id_to_coord']['x']
                size1 = coord1.size
                if size1 == 1:
                    continue
                if size0 != size1:
                    return False

                matching_size_gt1_ids.append(x)
        #--- End: for                    

        for x, coord1 in v['id_to_coord']:
            if x in matching_size_gt1_ids:
                continue
            
            size1 = coord1.size
            if size1 == 1:
                continue
            
            if x in s['id_to_coord']:
                coord0 = s['id_to_coord']['x']
                size0 = coord0.size
                if size0 == 1:
                    continue
                if size0 != size1:
                    return False

                matching_size_gt1_ids.append(x)
        #--- End: for                    

        # Check that at most one field has undefined axes
        if s['undefined_axes'] and v['undefined_axes']:
            raise ValueError(
"Can't combine fields: Both fields have undefined axes")

        # Find the axis names which are present in both fields
        matching_ids = set(s['id_to_axis']).intersection(v['id_to_axis'])
        
        # Check that any matching axes defined by an auxiliary
        # coordinate are done so in both fields.
        for identity in set(s['id_to_aux']).symmetric_difference(v['id_to_aux']):
            if identity in matching_ids:
                raise ValueError(
"Can't combine fields: %r axis defined by auxiliary in only 1 field" %
standard_name) ########~WRONG
        #--- End: for


        #-------------------------------------------------------------
        #
        #-------------------------------------------------------------
        for identity in matching_size_gt1_ids:
            coord0 = s['id_to_coord'][identity]
            coord1 = v['id_to_coord'][identity]

            # Check that the defining coordinate data arrays are
            # compatible
            if not coord0._equivalent_data(coord1):
                # Can't broadcast: The defining coordinates have
                # unequivalent data arrays and are both size > 1.
                return False

            # Still here? Then the defining coordinates have
            # equivalent data arrays

            # If the defining coordinates are attached to
            # transforms then check that those transforms are
            # equivalent
            key0 = s['id_to_key'][identity]                
            key1 = v['id_to_key'][identity]

            equivalent_transforms = True
            for transform0 in self.transforms().itervalues():
                if key0 not in transform0.coords:
                    continue

                equivalent_transforms = False
                for transform1 in g.transforms().itervalues():
                    if key1 not in transform1.coords:
                        continue

                    # Each defining coordinate is referenced by a
                    # transform ...
                    if self.domain.equivalent_transform(transform0,
                                                        transform1,
                                                        g.domain):
                        # ... and those transforms are equivalent
                        equivalent_transforms = True
                    #--- End: if

                    break
                #--- End: for

                break
            #--- End: for

            if not equivalent_transforms:
                # Can't broadcast: Defining coordinates have
                # incompatible transformsare and are both size >
                # 1.
                return False
        #--- End: for

        # --------------------------------------------------------
        # Still here? Then the two fields are broadcastable!
        # --------------------------------------------------------
        return True
    #--- End: def 

    def _binary_operation(self, other, method):
        '''

Implement binary arithmetic and comparison operations on the master
data array with metadata-aware broadcasting.

It is intended to be called by the binary arithmetic and comparison
methods, such as `__sub__`, `__imul__`, `__rdiv__`, `__lt__`, etc.

:Parameters:

    other : standard Python scalar object or cf.Field or cf.Comaparison or cf.Data

    method : str
        The binary arithmetic or comparison method name (such as
        ``'__idiv__'`` or ``'__ge__'``).

:Returns:

    out : cf.Field
        The new field, or the same field if the operation was an in
        place augmented arithmetic assignment.

**Examples**

>>> h = f._binary_operation(g, '__add__')
>>> h = f._binary_operation(g, '__ge__')
>>> f._binary_operation(g, '__isub__')
>>> f._binary_operation(g, '__rdiv__')

'''
        if (isinstance(other, (float, int, long, bool, basestring)) or
            other is self):
            # ========================================================
            # Combine the field with one of the following:
            #
            #   * A python scalar
            #   * Itself
            #
            # These cases are special because they don't involve any
            # changes to the field's domain and so can use the
            # metadata-UNaware Variable._binary_operation method.
            # ========================================================
            return super(Field, self)._binary_operation(other, method)
        #--- End: if

        if isinstance(other, Data) and other.size == 1:
            # ========================================================
            # Combine the field with a size 1 Data object
            #
            # This case is special because it doesn't involve any
            # changes to the field's domain and so can use the
            # metadata-UNaware Variable._binary_operation method.
            # ========================================================
#            other = other.copy()
            other = other.squeeze() # necessary ??
            return super(Field, self)._binary_operation(other, method)
        #--- End: if

        if isinstance(other, Comparison):
            # ========================================================
            # Combine the field with a Comparison object
            # ========================================================
            return NotImplemented
        #--- End: if

        if not isinstance(other, self.__class__):
            raise ValueError(
                "Can't combine %r with %r" %
                (self.__class__.__name__, other.__class__.__name__))
        #--- End: if

        # ============================================================
        # Still here? Then combine the field with another field
        # ============================================================

        # ------------------------------------------------------------
        # Analyse each domain
        # ------------------------------------------------------------
        s = self.domain.analyse()
        v = other.domain.analyse()

        if s['warnings'] or v['warnings']:
            raise ValueError("Can't combine fields: %s" % 
                             (s['warnings'] or v['warnings']))

        # Check that at most one field has undefined axes
        if s['undefined_axes'] and v['undefined_axes']:
            raise ValueError(
"Can't combine fields: Both fields have undefined axes")

        # Find the axis names which are present in both fields
        matching_ids = set(s['id_to_axis']).intersection(v['id_to_axis'])
        
        # Check that any matching axes defined by an auxiliary
        # coordinate are done so in both fields.
        for identity in set(s['id_to_aux']).symmetric_difference(v['id_to_aux']):
            if identity in matching_ids:
                raise ValueError(
"Can't combine fields: %r axis defined by auxiliary in only 1 field" %
standard_name) ########~WRONG
        #--- End: for

        # ------------------------------------------------------------
        # For matching dimension coordinates check that they have
        # consistent transforms and that one of the following is
        # true:
        #
        # 1) They have equal size > 1 and their data arrays are
        #    equivalent
        #
        # 2) They have unequal sizes and one of them has size 1
        #
        # 3) They have equal size = 1. In this case, if the data
        #    arrays are not equivalent then the axis will be omitted
        #    from the result field's domain.
        #-------------------------------------------------------------

        # List of size 1 axes to be completely removed from the result
        # field. Such an axis's size 1 defining coordinates have
        # unequivalent data arrays.
        #
        # For example:
        # >>> remove_size1_axes
        # ['dim2']
        remove_size1_axes = []

        # List of matching axes with equivalent defining dimension
        # coordinate data arrays.
        #
        # Note that we don't need to include matching axes with
        # equivalent defining *auxiliary* coordinate data arrays.
        #
        # For example:
        # >>> 
        # [('dim2', 'dim0')]
        matching_axes_with_equivalent_data = []

        # For each field, list those of its matching axes which need
        # to be broadcast against the other field. I.e. those axes
        # which are size 1 but size > 1 in the other field.
        #
        # For example:
        # >>> s['broadcast_axes']
        # ['dim1']
        s['broadcast_axes'] = []
        v['broadcast_axes'] = []

        # Map axes in field1 to axes in field0 and vice versa
        #
        # For example:
        # >>> axis1_to_axis0
        # {'dim1': 'dim0', 'dim2': 'dim1', 'dim0': 'dim2'}
        # >>> axis0_to_axis1
        # {'dim0': 'dim1', 'dim1': 'dim2', 'dim2': 'dim0'}
        axis1_to_axis0 = {}
        axis0_to_axis1 = {}

        for identity in matching_ids:
            coord0 = s['id_to_coord'][identity]
            coord1 = v['id_to_coord'][identity]

            axis0  = s['id_to_axis'][identity]
            axis1  = v['id_to_axis'][identity]

            axis1_to_axis0[axis1] = axis0
            axis0_to_axis1[axis0] = axis1

            # Check the sizes of the defining coordinates
            size0 = coord0.size
            size1 = coord1.size
            if size0 != size1:
                # Defining coordinates have different sizes
                if size0 == 1:
                    # Can broadcast
                    s['broadcast_axes'].append(s['id_to_axis'][identity])
                elif size1 == 1:
                    # Can broadcast
                    v['broadcast_axes'].append(v['id_to_axis'][identity])
                else:
                    # Can't broadcast
                    raise ValueError(
"Can't combine fields: Can't broadcast %r axes with sizes %d and %d" %
(identity, size0, size1))

                continue
            #--- End: if

            # Still here? Then these defining coordinates have the
            # same size.

            # Check that the defining coordinate data arrays are
            # compatible
            if coord0._equivalent_data(coord1):
                # The defining coordinates have equivalent data
                # arrays

                # If the defining coordinates are attached to
                # transforms then check that those transforms are
                # equivalent
                key0 = s['id_to_key'][identity]                
                key1 = v['id_to_key'][identity]

                equivalent_transforms = True
                for transform0 in self.transforms().itervalues():
                    if key0 not in transform0.coords:
                        continue

                    equivalent_transforms = False
                    for transform1 in other.transforms().itervalues():
                        if key1 not in transform1.coords:
                            continue

                        # Each defining coordinate is referenced by a
                        # transform ...
                        if self.domain.equivalent_transform(transform0,
                                                            transform1,
                                                            other.domain):
                            # ... and those transforms are equivalent
                            equivalent_transforms = True
                        #--- End: if

                        break
                    #--- End: for

                    break
                #--- End: for

                if not equivalent_transforms:
                    # The defining coordinates have incompatible
                    # transforms
                    if coord0.size > 1:
                        # They are both size > 1
                        raise ValueError(
"Can't combine fields: Incompatible transforms for %r coordinates" % identity)
                    else:
                        # They are both size 1 so flag this axis to be
                        # omitted from the result field
                        remove_size1_axes.append(axis0)

                elif identity not in s['id_to_aux']:
                    # The defining coordinates 1) are both dimension
                    # coordinates, 2) have equivalent data arrays and
                    # 3) have compatible transforms (if any).
                    matching_axes_with_equivalent_data.append((axis0, axis1))

            else:
                # The defining coordinates have unequivalent data
                # arrays
                if coord0.size > 1:
                    # They are both size > 1
                    raise ValueError(
"Can't combine fields: Incompatible %r coordinate data arrays" % identity)
                else:
                    # They are both size 1 so flag this axis to be
                    # omitted from the result field
                    remove_size1_axes.append(axis0)
        #--- End: for

        # --------------------------------------------------------
        # Still here? Then the two fields are combinable!
        # --------------------------------------------------------

        # ------------------------------------------------------------
        # 2.1 Create copies of the two fields, unless it is an in
        #     place combination, in which case we don't want to copy
        #     self)
        # ------------------------------------------------------------
        field1 = other.copy()

        inplace = method[2] == 'i'
        if not inplace:
            field0 = self.copy()
        else:
            field0 = self

        # Aliases for the field's dOmain and data array
        domain0 = field0.domain
        domain1 = field1.domain

        # 
        s['new_axes'] = []
            
#        for axis1 in domain1.dimension_sizes:
#            if axis1 in v['axis_to_id']:
#                identity = v['axis_to_id'][axis1]
#                if identity in matching_ids:
#                    axis0 = s['id_to_axis'][identity]
#                    axis1_to_axis0[axis1] = axis0
#                    axis0_to_axis1[axis0] = axis1
#        #--- End: for

        # ------------------------------------------------------------
        # Permute the axes of the data array of field0 so that:
        #
        # * All of the matching axes are the inner (fastest varying)
        #   axes
        #
        # * All of the undefined axes are the outer (slowest varying)
        #   axes
        #
        # * All of the defined but unmatched axes are in the middle
        # ------------------------------------------------------------
        data_axes0 = domain0.data_axes()
        axes_unD = []                     # Undefined axes
        axes_unM = []                     # Defined but unmatched axes
        axes0_M  = []                     # Defined and matched axes
        for axis0 in data_axes0:
            if axis0 in axis0_to_axis1:
                # Matching axis                
                axes0_M.append(axis0)
            elif axis0 in s['undefined_axes']:
                # Undefined axis
                axes_unD.append(axis0)
            else:
                # Defined but unmatched axis
                axes_unM.append(axis0)
        #--- End: for

        field0.transpose(axes_unD + axes_unM + axes0_M, i=True)

        end_of_undefined0   = len(axes_unD)
        start_of_unmatched0 = end_of_undefined0
        start_of_matched0   = start_of_unmatched0 + len(axes_unM)

        # ------------------------------------------------------------
        # Permute the axes of the data array of field1 so that:
        #
        # * All of the matching axes are the inner (fastest varying)
        #   axes and in corresponding positions to data0
        #
        # * All of the undefined axes are the outer (slowest varying)
        #   axes
        #
        # * All of the defined but unmatched axes are in the middle
        # ------------------------------------------------------------
        data_axes1 = domain1.data_axes()
        axes_unD = []
        axes_unM = []
        axes1_M  = [axis0_to_axis1[axis0] for axis0 in axes0_M]
        for  axis1 in data_axes1:          
            if axis1 in axes1_M:
                pass
            elif axis1 in axis1_to_axis0:
                # Matching axis
                axes_unM.append(axis1)
            elif axis1 in v['undefined_axes']:
                # Undefined axis
                axes_unD.append(axis1) 
            else:
                # Defined but unmatched axis
                axes_unM.append(axis1)
        #--- End: for

        field1.transpose(axes_unD + axes_unM + axes1_M, i=True)

        start_of_unmatched1 = len(axes_unD)
        start_of_matched1   = start_of_unmatched1 + len(axes_unM)
        undefined_indices1  = slice(None, start_of_unmatched1)
        unmatched_indices1  = slice(start_of_unmatched1, start_of_matched1)

        # ------------------------------------------------------------
        # Make sure that each pair of matching axes run in the same
        # direction 
        #
        # Note that the axis0_to_axis1 dictionary currently only maps
        # matching axes
        # ------------------------------------------------------------
        for axis0, axis1 in axis0_to_axis1.iteritems():
             if domain1.direction(axis1) != domain0.direction(axis0):
                field1.flip(axis1, i=True)
        #--- End: for
    
        # ------------------------------------------------------------
        # 2f. Insert size 1 axes into the data array of field0 to
        #     correspond to defined but unmatched axes in field1
        #
        # For example, if   field0.Data is      1 3         T Y X
        #              and  field1.Data is          4 1 P Z   Y X
        #              then field0.Data becomes 1 3     1 1 T Y X
        # ------------------------------------------------------------
        unmatched_axes1 = data_axes1[unmatched_indices1]
        if unmatched_axes1:
            for axis1 in unmatched_axes1:
#                axis0 = field0.expand_dims(end_of_undefined0, i=True)
                field0.expand_dims(end_of_undefined0, i=True)

                axis0 = set(field0.data_axes()).difference(data_axes0).pop()

                axis1_to_axis0[axis1] = axis0
                axis0_to_axis1[axis0] = axis1
                s['new_axes'].append(axis0)

                start_of_unmatched0 += 1
                start_of_matched0   += 1 

                data_axes0 = domain0.data_axes()
            #--- End: for
#            data_axes0 = domain0.data_axes()
        #--- End: if

        # ------------------------------------------------------------
        # Insert size 1 axes into the data array of field1 to
        # correspond to defined but unmatched axes in field0
        #
        # For example, if   field0.Data is      1 3     1 1 T Y X
        #              and  field1.Data is          4 1 P Z   Y X 
        #              then field1.Data becomes     4 1 P Z 1 Y X 
        # ------------------------------------------------------------
        unmatched_axes0 = data_axes0[start_of_unmatched0:start_of_matched0]
        if unmatched_axes0:
            for axis0 in unmatched_axes0:
#                axis1 = field1.expand_dims(start_of_matched1, i=True)
                field1.expand_dims(start_of_matched1, i=True)
               
                axis1 = set(field1.data_axes()).difference(data_axes1).pop()

                axis0_to_axis1[axis0] = axis1
                axis1_to_axis0[axis1] = axis0

                start_of_unmatched1 += 1

                data_axes1 = field1.data_axes()
            #--- End: for
#            data_axes1 = domain1.data_axes()
         #--- End: if

        # ------------------------------------------------------------
        # Insert size 1 axes into the data array of field0 to
        # correspond to undefined axes (of any size) in field1
        #
        # For example, if   field0.Data is      1 3     1 1 T Y X
        #              and  field1.Data is          4 1 P Z 1 Y X 
        #              then field0.Data becomes 1 3 1 1 1 1 T Y X
        # ------------------------------------------------------------
        axes1 = data_axes1[undefined_indices1]
        if axes1:
            for axis1 in axes1:
#                axis0 = field0.expand_dims(end_of_undefined0, i=True)

                field0.expand_dims(end_of_undefined0, i=True)

                axis0 = set(field0.data_axes()).difference(data_axes0).pop()

                axis0_to_axis1[axis0] = axis1
                axis1_to_axis0[axis1] = axis0
                s['new_axes'].append(axis0)

                data_axes0 = field0.data_axes()
            #--- End: for
        #--- End: if

        # ============================================================
        # 3. Combine the data objects
        #
        # Note that, by now, field0.ndim >= field1.ndim.
        # ============================================================
        field0.Data = getattr(field0.Data, method)(field1.Data)

        # ============================================================
        # 4. Adjust the domain of field0 to accommodate its new data
        # ============================================================
        insert_dim = {}
        insert_aux = {}
        remove_aux = []

        # ------------------------------------------------------------
        # 4a. Remove any size 1 axes which are matching axes but with
        #     different coordinate data array values
        # ------------------------------------------------------------
        field0.remove_axes(remove_size1_axes)

        # ------------------------------------------------------------
        # 4b. If broadcasting has grown any size 1 axes in domain0
        #     then replace their size 1 coordinates with the
        #     corresponding size > 1 coordinates from domain1.
        # ------------------------------------------------------------
        transforms1 = field1.transforms()
        transforms = []

        for axis0 in s['broadcast_axes'] + s['new_axes']:        
            axis1 = axis0_to_axis1[axis0]
            size = domain1.dimension_sizes[axis1]
            domain0.insert_axis(size, key=axis0, replace=True)

#            domain0.dimension_sizes[axis0] = domain1.dimension_sizes[axis1]

#            for tkey, transform in other.transform.iteritems():
##            for tkey, transform in field1.transforms():
            for tkey in transforms1:
                if axis1 in domain1.transform_axes(tkey):
                    transforms.append(tkey)

            # Copy the domain1 dimension coordinate to
            # domain0, if it exists.
            if axis1 in domain1.d:
                insert_dim[axis1] = axis0

            # Remove any domain0 1-d auxiliary coordinates for
            # this axis
            if axis0 in s['aux_coords']:
                for aux0 in s['aux_coords'][axis0]['1-d'].keys():
                    remove_aux.append(aux0)
                    del s['aux_coords'][axis0]['1-d'][aux0]
            #--- End: if

            # Copy to domain0 any domain1 1-d auxiliary coordinates
            # for this axis
            if axis1 in v['aux_coords']:
                for aux1 in v['aux_coords'][axis1]['1-d']:
                    insert_aux[aux1] = [axis0]
        #--- End: for

        # ------------------------------------------------------------
        # Consolidate any 1-d auxiliary coordinates for matching axes
        # whose defining dimension coordinates have equivalent data
        # arrays.
        #
        # A domain0 1-d auxiliary coordinate is retained if there is a
        # corresponding domain1 1-d auxiliary with the same standard
        # name and equivalent data array.
        # ------------------------------------------------------------
        for axis0, axis1 in matching_axes_with_equivalent_data:

            for aux0, coord0 in s['aux_coords'][axis0]['1-d'].iteritems():
                if coord0.identity() is None:
                    # Remove this domain0 1-d auxiliary coordinate
                    # because it has no identity
                    remove_aux.append(aux0)
                    continue

                # Still here?
                aux0_has_equivalent_pair = False

                for aux1, coord1 in v['aux_coords'][axis1]['1-d'].items():
                    if coord1.identity() is None:
                        continue
                    
                    if coord0._equivalent_data(coord1): 
                        del v['aux_coords'][axis1]['1-d'][aux1]
                        aux0_has_equivalent_pair = True
                        break
                #--- End: for

                if not aux0_has_equivalent_pair:
                    # Remove this domain0 1-d auxiliary coordinate
                    # becuase it has no equivalent in domain1
                    remove_aux.append(aux0)                    
        #--- End: for

        # ------------------------------------------------------------
        # Consolidate N-d auxiliary coordinates for matching axes
        # which have the same size
        # ------------------------------------------------------------
        # Remove any N-d auxiliary coordinates which span broadcasted
        # axes
        for broadcast_axes, aux_coords, domain in izip((s['broadcast_axes'], v['broadcast_axes']),
                                                       (s['aux_coords']    , v['aux_coords']),
                                                       (domain0            , domain1)):
            for axis in broadcast_axes:
                if axis not in aux_coords:
                    continue

                for aux in aux_coords[axis]['N-d']:
                    del aux_coords['N-d'][aux]
                    if domain is domain0:
                        remove_aux.append(aux)
        #--- End: for

        # Remove any N-d auxiliary coordinates which span a mixture of
        # matching and non-matching axes
        for aux_coords, domain, axis_to_id in izip((s['aux_coords'], v['aux_coords']),
                                                   (domain0         , domain1         ),
                                                   (s['axis_to_id'] , v['axis_to_id'] )):
            for aux in aux_coords['N-d'].keys():
                # Count how many of this N-d auxiliary coordinate's
                # axes are matching axes
                n_matching_dims = len([True for axis in domain.dimensions[aux]
                                       if axis_to_id[axis] in matching_ids])
                
                if 1 <= n_matching_dims < len(domain.dimensions[aux]):
                    # At least one axis is a matching axis and at
                    # least one axis isn't => so remove this domain0
                    # auxiliary coordinate
                    del aux_coords['N-d'][aux]
                    if domain is domain0:
                        remove_aux.append(aux)
            #--- End: for
        #--- End: for

        # Forget about
        for aux0 in s['aux_coords']['N-d'].keys():
             n_matching_axes = len(s['aux_coords']['N-d'][aux0])
             if not n_matching_axes:
                 del s['aux_coords']['N-d'][aux0]
        #--- End: for

        # Copy to domain0 any domain1 N-d auxiliary coordinates which
        # do not span any matching axes
        for aux1, coord1 in v['aux_coords']['N-d'].items():
             n_matching_axes = len(v['aux_coords']['N-d'][aux1])
             if not n_matching_axes:
                 axes = [axis1_to_axis0[axis1] for axis1 in domain1.dimensions[aux1]]
                 insert_auxs[aux1] = axes
                 del v['aux_coords']['N-d'][aux1]
        #--- End: for

        # By now, aux_coords0['N-d'] contains only those N-d auxiliary
        # coordinates which span equal sized matching axes.
 
        # Remove from domain0 any N-d auxiliary coordinates which span
        # same-size matching axes and do not have an equivalent N-d
        # auxiliary coordinate in domain1 (i.e. one which spans the
        # same axes, has the same standard name and has equivalent
        # data)
        for aux0, coord0 in s['aux_coords']['N-d'].iteritems():

            # Remove domain0 N-d auxiliary coordinate if it has no
            # standard name
            if coord0.identity() is None:
                remove_aux.append(aux0)
                continue

            # Still here?
            aux0_has_equivalent_pair = False
            for aux1, coord1 in v['aux_coords']['N-d'].items():
                if coord1.identity() is None:
                    continue

                copy = True
                axes1 = domain1.item_axes(aux1)
                transpose_axes = [axes1.index(axis0_to_axis1[axis0])
                                  for axis0 in domain1.item_axes(aux0)]
                if transpose_axes != range(coord1.ndim):
#                    coord1 = coord1.copy()
                    coord1 = coord1.transpose(transpose_axes)
                    copy = False  # necessary?

                if coord0._equivalent_data(coord1, copy=copy):
                    del v['aux_coords']['N-d'][aux1]
                    aux0_has_equivalent_pair = True
                    break
            #--- End: for

            # Remove domain0 N-d auxiliary coordinate if it has no
            # equivalent in domain1
            if not aux0_has_equivalent_pair:
                remove_aux.append(aux0)
        #--- End: for
                
        key1_to_key0 = {}

        for axis1, axis in insert_dim.iteritems():
            axis0 = domain0.insert_dim(domain1.d[axis1], key=axis)
            key1_to_key0[axis1] = axis0
            
        for aux1, axes in insert_aux.iteritems():
            aux0 = domain0.insert_aux(domain1.a[aux1], axes=axes)
            key1_to_key0[aux1] = aux0

        domain0.remove_items(set(remove_aux))

        # Transforms from domain1 -> domain0
        for tkey in set(transforms):
            new_transform = other.transform[tkey].copy()
            for key1 in transform.coords:
                new_transform.rename_pointer(key1, key1_to_key0.get(key1, None))
                    
            domain0.insert_transform(new_transform, copy=False)
        #--- End: for

        return field0
    #--- End: def
    
    def _conform_for_assignment(self, other):
        '''
    
Conform `other` so that it is ready for metadata-unaware assignment
broadcasting across `self`.

`other` is not changed in place.

:Parameters:

    other : cf.Field
        The field to conform.

:Returns:

    out : cf.Field
        The conformed version of *other*.

**Examples**

>>> new = _conform_for_assignment(f, value, 'setitem')

'''
        # Analyse each domain
        domain0 = self.domain
        domain1 = other.domain
        s = domain0.analyse()
        v = domain1.analyse()
    
        if s['warnings'] or v['warnings']:
            raise ValueError("Can't setitem: %s" % (s['warnings'] or v['warnings']))
    
        # Find the set of matching axes
        matching_ids = set(s['id_to_axis']).intersection(v['id_to_axis'])
        if not matching_ids:
             raise ValueError("Can't assign: No matching axes")
    
        # ------------------------------------------------------------
        # Check that any matching axes defined by auxiliary
        # coordinates are done so in both fields.
        # ------------------------------------------------------------
        for identity in matching_ids:
            if (identity in s['id_to_aux']) + (identity in v['id_to_aux']) == 1:
                raise ValueError(
"Can't assign: %r axis defined by auxiliary in only 1 field" %
identity)
        #--- End: for
    
        copied = False
    
        # ------------------------------------------------------------
        # Check that 1) all undefined axes in other have size 1 and 2)
        # that all of other's unmatched but defined axes have size 1
        # and squeeze any such axes out of its data array.
        #
        # For example, if   self.Data is        P T     Z Y   X   A
        #              and  other.Data is     1     B C   Y 1 X T
        #              then other.Data becomes            Y   X T
        # ------------------------------------------------------------
        squeeze_axes1 = []
        for axis1 in v['undefined_axes']:
            if domain1.dimension_sizes[axis1] != 1:            
                raise ValueError(
                    "Can't assign: Can't broadcast size %d undefined axis" %
                    domain1.dimension_sizes[axis1])

            squeeze_axes1.append(axis1)
        #--- End: for

        for identity in set(v['id_to_axis']).difference(matching_ids):
            axis1 = v['id_to_axis'][identity]
            if domain1.dimension_sizes[axis1] != 1:
               raise ValueError(
                    "Can't assign: Can't broadcast size %d %r axis" %
                    (domain1.dimension_sizes[axis1], identity))
            
            squeeze_axes1.append(axis1)    
        #--- End: for

        if squeeze_axes1:
            if not copied:
                other = other.copy()
                copied = True

            other.squeeze(squeeze_axes1, i=True)
        #--- End: if

        # ------------------------------------------------------------
        # Permute the axes of other.Data so that they are in the same
        # orer as their matching counterparts in self.Data
        #
        # For example, if   self.Data is       P T Z Y X   A
        #              and  other.Data is            Y X T
        #              then other.Data becomes   T   Y X
        # ------------------------------------------------------------
        data_axes0 = domain0.data_axes()
        data_axes1 = domain1.data_axes()
        transpose_axes1 = []       
        for axis0 in data_axes0:
            identity = s['axis_to_id'][axis0]
            if identity in matching_ids:
                axis1 = v['id_to_axis'][identity]                
                if axis1 in data_axes1:
                    transpose_axes1.append(axis1)
        #--- End: for

        if transpose_axes1 != data_axes1: 
            if not copied:
                other = other.copy()
                copied = True

            other.transpose(transpose_axes1, i=True)
        #--- End: if

        # ------------------------------------------------------------
        # Insert size 1 axes into other.Data to match axes in
        # self.Data which other.Data doesn't have.
        #
        # For example, if   self.Data is       P T Z Y X A
        #              and  other.Data is        T   Y X
        #              then other.Data becomes 1 T 1 Y X 1
        # ------------------------------------------------------------
        expand_positions1 = []
        for i, axis0 in enumerate(data_axes0):
            identity = s['axis_to_id'][axis0]
            if identity in matching_ids:
                axis1 = v['id_to_axis'][identity]
                if axis1 not in data_axes1:
                    expand_positions1.append(i)
            else:     
                expand_positions1.append(i)
        #--- End: for

        if expand_positions1:
            if not copied:
                other = other.copy()
                copied = True

            for i in expand_positions1:
                other.expand_dims(i, i=True)
        #--- End: if

        # ----------------------------------------------------------------
        # Make sure that each pair of matching axes has the same
        # direction
        # ----------------------------------------------------------------
        flip_axes1 = []
        for identity in matching_ids:
            axis1 = v['id_to_axis'][identity]
            axis0 = s['id_to_axis'][identity]
            if domain1.direction(axis1) != domain0.direction(axis0):
                flip_axes1.append(axis1)
         #--- End: for

        if flip_axes1:
            if not copied:
                other = other.copy()
                copied = True

            other = other.flip(flip_axes1, i=True)
        #--- End: if

        return other
    #--- End: def

    def equivalent(self, other, rtol=None, atol=None, traceback=False):
        '''

True if and only if two fields are logically equivalent.

Equivalence is defined as:

* Both fields must have the same dentity as returned by the `identity`
  methods.

* Both fields' data arrays being the same after accounting for
  different but equivalent:

    * units

    * size one dimensions (if *squeeze* is True), 
    
    * dimension directions (if *use_directions* is True) 
    
    * dimension orders (if *transpose* is set to a dictionary).

* Both fields' domains must have the same dimensionality and where a
  dimension in one field has an identity inferred a 1-d coordinate,
  the other field has a matching dimension whose identity inferred is
  inferred from a 1-d coordinate with an equivalent data array.

:Parameters:

    other :
        The object to compare for equivalence.

    atol : float, optional
        The absolute tolerance for all numerical comparisons, By
        default the value returned by the `cf.ATOL` function is used.

    rtol : float, optional
        The relative tolerance for all numerical comparisons, By
        default the value returned by the `cf.RTOL` function is used.

    traceback : bool, optional
        If True then print a traceback highlighting where the two
        objects differ.

:Returns: 

    out : bool
        Whether or not the two objects are equivalent.
      

'''
        if not self.equivalent_domain(other, rtol=rtol, atol=atol,
                                      traceback=traceback):
            if traceback:
                print("%s: Nonequivalent domains: %r, %r" % 
                      (self.__class__.__name__,
                       self.domain, other.domain))
            return False

        if not self.equivalent_data(other, rtol=rtol, atol=atol,
                                    traceback=False):
            if traceback:
                print("%s: Nonequivalent data arrays: %r, %r" % 
                      (self.__class__.__name__,
                       getattr(self, 'data', None),
                       getattr(other, 'data', None)))
            return False
                
        return True
    #--- End_def

    def equivalent_domain(self, other, rtol=None, atol=None,
                          traceback=False):
        '''
'''
        return self.domain.equivalent(other.domain, rtol=rtol,
                                      atol=atol, traceback=traceback)
    #--- End_def

    def equivalent_data(self, other, rtol=None, atol=None, traceback=False):
        '''

Equivelence is defined as both fields having the same data arrays
after accounting for different but equivalent units, size one
dimensions, different dimension directions and different dimension
orders.

:Parameters:

    other : cf.Field

    atol : float, optional
        The absolute tolerance for all numerical comparisons, By
        default the value returned by the `cf.ATOL` function is used.

    rtol : float, optional
        The relative tolerance for all numerical comparisons, By
        default the value returned by the `cf.RTOL` function is used.

:Returns:

    out : bool
        Whether or not the two fields' data arrays are equivalent.

**Examples**

>>> f.equivalent_data(g)

'''
        if self._hasData != other._hasData:
            if traceback:
                print("%s: Only one field has data: %s, %s" %
                      (self.__class__.__name__, self._hasData, other._hasData))
            return False
        
        if not self._hasData:
            # Neither field has a data array
            return True

        if self.size != other.size:
            if traceback:
                print("%s: Different data array sizes (%d, %d)" %
                      (self.__class__.__name__, self.size, other.size))
            return False

        s = self.domain.analyse()
        t = other.domain.analyse()

        data0 = self.data
        data1 = other.data
        if 1 in data0._shape:
            data0 = data0.squeeze()
            
        copy = True
        if 1 in data1._shape:
            data1 = data1.squeeze()
            copy = False

        data_axes0 = self.domain.data_axes()
        data_axes1 = other.domain.data_axes()

        transpose_axes = []
        for axis0 in data_axes0:
            axis1 = t['id_to_axis'].get(s['axis_to_id'][axis0], None)
            if axis1 is not None:
                transpose_axes.append(data_axes1.index(axis1))
            else:
                if traceback:
                    print("%s: woooooooooooooooo" % self.__class__.__name__)
                return False
        #--- End: for
       
        if transpose_axes != range(other.ndim):
            if copy:
                data1 = data1.copy()
                copy = False

            data1.transpose(transpose_axes, i=True)
        #--- End: if

        if self.size > 1:            
            self_directions  = self.domain.directions()
            other_directions = other.domain.directions()

            flip_axes = [i for i, (axis1, axis0) in enumerate(izip(data_axes1,
                                                                   data_axes0))
                         if other_directions[axis1] != self_directions[axis0]]
        
            if flip_axes:
                if copy:
                    data1 = data1.copy()                
                    copy = False

                data1.flip(flip_axes, i=True)
        #--- End: if

        return data0.equals(data1, rtol=rtol, atol=atol, ignore_fill_value=True)
    #--- End: def
 
    def allclose(self, y, rtol=None, atol=None):
        '''

Returns True if two broadcastable fields have have equal array values
to within numerical tolerance, False otherwise.

The follwowing are accounted for:

* Units
* Axis order
* Axis direction

:Parameters:

    y : 

    atol : float, optional
        The absolute tolerance for all numerical comparisons, By
        default the value returned by the `cf.ATOL` function is used.

    rtol : float, optional
        The relative tolerance for all numerical comparisons, By
        default the value returned by the `cf.RTOL` function is used.

:Returns:

    out : bool
        Whether or not the two fields' data arrays are equivalent.

.. seealso:: `~cf.Field.equals`

**Examples**

'''
        if isinstance(y, self.__class__):
            if self._hasData != y._hasData:
                # Exactly one field has a data array
                return False            

            if not self._hasData:
                # Neither field has a data array
                return True

            y = self._conform_for_assignment(y)
        #--- End: if

        return super(Field, self).allclose(y, atol=atol, rtol=rtol)
    #--- End: def
 
#    def _parse_axes(self, axes, method, default=(), dims=False, ignore=False):
#        '''
#
#:Parameters:
#
#    axes : (sequence of) str or int
#
#    method : str
#
#    default : sequence of str or int, optional
#
#    dims : bool, optional
#
#    ignore : bool, optional
#        If True then ignore any dimensions which do not span the
#        field's data array. By default an exception is raised if a
#        specified dimension does not span the field's data array.
#
#:Returns:
#    
#    out : list of ints (, list of strs)
#        Return a list of integer positions of the field's data
#        array. If *dims* is True then also return the domain dimension
#        identifiers corresponding to the integer positions.
#
#**Examples**
#
#>>> f._parse_axes()
#
#'''
#        domain          = self.domain
#        dimensions      = domain.dimensions
#        data_axes = dimensions['data']
#
#        if axes is None: #not axes and axes is not 0:
#            axes = default
#
#        # Convert axes to a list of integer positions
#        if isinstance(axes, (str, int, long)):
#            axes = (axes,)
#        else:
#            axes = tuple(axes)
#
#        axes2 = []
#        for axis in axes:
#
#
#            axes2.extend(self.domain.axis(axis, **kwargs))
#
#
#            if axis in data_axes:
#                axes2.append(data_axes.index(axis))
#            elif isinstance(axis, (int, long)):
#                if axis < 0:
#                    axis += self.ndim
#                    if axis < 0:
#                        raise ValueError(
#                            "Can't %s: Can't determine dimension from %d" %
#                            (method, axis-self.ndim))
#                elif axis >= self.ndim:
#                    raise ValueError(
#                        "Can't %s: Can't determine dimension from %d" %
#                        (method, axis))
#                #--- End: if
#                axes2.append(axis)
#            else:
#                dim = self.domain.item_axes(axis, role='dac', rank=1)
#                if dim is None:
#                    raise ValueError(
#                        "Can't %s: Can't determine dimension from %s" %
#                        (method, repr(axis)))
#
#                if dim in data_axes:
#                    axes2.append(data_axes.index(dim))
#                elif not ignore:
#                    # Do not ignore dimensions which don't span the
#                    # data array
#                    raise ValueError(
#                        "Can't %s: %s dimension not spanned by data array" %
#                        (method, repr(axis)))
#        #--- End: for
#
#        if axes2:
#            # Check for duplicate axes
#            if len(axes2) != len(set(axes2)):
#                raise ValueError("Can't %s: Repeated axis: %s" %
#                                 (method, repr(axes2)))
#            
##            # Check for out of range axes
##            if max(axes2) >= self.ndim:
##                raise ValueError("Can't %s: Invalid axis for this array: %d" %
##                             (method, max(axes2)))
#        #--- End: if
#
#        if dims:
#            return axes2, [data_axes[i] for i in axes2]
#        else:
#            return axes2
#    #--- End: def

    def __repr__(self):
        '''

x.__repr__() <==> repr(x)

'''
        if self._hasData:
            domain = self.domain
            x = ['%s(%d)' % (domain.axis_name(axis),
                             domain.dimension_sizes[axis])
                 for axis in domain.data_axes()]
            axis_names = '(%s)' % ', '.join(x)
        else:
            axis_names = ''
        #--- End: if
            
        # Field units
        units = getattr(self, 'units', '')
        calendar = getattr(self, 'calendar', None)
        if calendar:
            units += '%s calendar' % calendar

        return '<CF Field: %s%s %s>' % (self.name(''), axis_names, units)
    #--- End: def

    def __str__(self):
        '''

x.__str__() <==> str(x)

'''
        string = ["%s field summary" % self.name('')]
        string.append(''.ljust(len(string[0]), '-'))

        # Units
        units = getattr(self, 'units', '')
        calendar = getattr(self, 'calendar', None)
        if calendar:
            units += ' %s calendar' % calendar

        domain = self.domain

        # Data
        if self._hasData:
            x = ['%s(%d)' % (domain.axis_name(axis),
                             domain.dimension_sizes[axis])
                 for axis in domain.data_axes()]
            
            string.append('Data           : %s(%s) %s' % (self.name(''),
                                                          ', '.join(x), units))
        elif units:
            string.append('Data           : %s' % units)

        # Cell methods
        cell_methods = getattr(self, 'cell_methods', None)
        if cell_methods is not None:
            string.append('Cell methods   : %s' % str(cell_methods))

        # Domain
        if domain:
            string.append(str(domain))
            
        # Ancillary variables
        ancillary_variables = getattr(self, 'ancillary_variables', None)
        if ancillary_variables is not None:
            y = ['Ancillary vars : ']
            y.append('\n               : '.join(
                    [repr(a) for a in ancillary_variables]))
            string.append(''.join(y))

        string.append('')

        return '\n'.join(string)
    #--- End def

    # ----------------------------------------------------------------
    # Attribute: ancillary_variables
    # ----------------------------------------------------------------
    @property
    def ancillary_variables(self):
        '''

A `cf.AncillaryVariables` object containing CF ancillary data.

**Examples**

>>> f.ancillary_variables
[<CF Field: >]

'''
        return self._get_special_attr('ancillary_variables')
    #--- End: def
    @ancillary_variables.setter
    def ancillary_variables(self, value):
        self._set_special_attr('ancillary_variables', value)
    @ancillary_variables.deleter
    def ancillary_variables(self):
        self._del_special_attr('ancillary_variables')

    # ----------------------------------------------------------------
    # Attribute: Flags
    # ----------------------------------------------------------------
    @property
    def Flags(self):
        '''

A `cf.Flags` object containing self-describing CF flag values.

Stores the `flag_values`, `flag_meanings` and `flag_masks` CF
properties in an internally consistent manner.

**Examples**

>>> f.Flags
<CF Flags: flag_values=[0 1 2], flag_masks=[0 2 2], flag_meanings=['low' 'medium' 'high']>

'''
        return self._get_special_attr('Flags')
    @Flags.setter
    def Flags(self, value):
        self._set_special_attr('Flags', value)
    @Flags.deleter
    def Flags(self):
        self._del_special_attr('Flags')

    # ----------------------------------------------------------------
    # Attribute: mask (read only)
    # ----------------------------------------------------------------
    @property
    def mask(self):
        '''

A field containing the mask of the data array.

**Examples**

>>> f
<CF Field: air_temperature(time(12), latitude(73), longitude(96)) K>
>>> m = f.mask
>>> m
<CF Field: long_name:air_temperature_mask(time(12), latitude(73), longitude(96)) >
>>> m.data
<CF Data: [[[True, ..., False]]] >

'''       
        long_name = self.identity()
        if long_name is not None:
            long_name+= '_mask'
        else:
            long_name  = 'mask'

        return type(self)(properties={'long_name': long_name},
                          domain=self.domain.copy(),
                          data=self.Data.mask,
                          copy=False)
    #--- End: def
    @mask.setter
    def mask(self, value):
        Variable.mask.fset(self, value)
    @mask.deleter
    def mask(self): 
        Variable.mask.fdel(self)

    # ----------------------------------------------------------------
    # CF property: flag_values
    # ----------------------------------------------------------------
    @property
    def flag_values(self):
        '''

The flag_values CF property.

Stored as a 1-d numpy array but may be set as any array-like object.

**Examples**

>>> f.flag_values = ['a', 'b', 'c']
>>> f.flag_values
array(['a', 'b', 'c'], dtype='|S1')
>>> f.flag_values = numpy.arange(4)
>>> f.flag_values
array([1, 2, 3, 4])
>>> del f.flag_values

>>> f.setprop('flag_values', 1)
>>> f.getprop('flag_values')
array([1])
>>> f.delprop('flag_values')

'''
        try:
            return self.Flags.flag_values
        except AttributeError:
            raise AttributeError(
                "%s doen't have CF property 'flag_values'" %
                self.__class__.__name__)
    #--- End: def
    @flag_values.setter
    def flag_values(self, value):
        try:
            flags = self.Flags
        except AttributeError:
            self.Flags = Flags(flag_values=value)
        else:
            flags.flag_values = value
    #--- End: def
    @flag_values.deleter
    def flag_values(self):
        try:
            del self.Flags.flag_values
        except AttributeError:
            raise AttributeError(
                "Can't delete non-existent %s CF property 'flag_values'" %
                self.__class__.__name__)
        else:
            if not self.Flags:
                del self.Flags
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: flag_masks
    # ----------------------------------------------------------------
    @property
    def flag_masks(self):
        '''
The flag_masks CF property.

Stored as a 1-d numpy array but may be set as array-like object.

**Examples**

>>> f.flag_masks = numpy.array([1, 2, 4], dtype='int8')
>>> f.flag_masks
array([1, 2, 4], dtype=int8)
>>> f.flag_masks = (1, 2, 4, 8)
>>> f.flag_masks
array([1, 2, 4, 8], dtype=int8)
>>> del f.flag_masks

>>> f.setprop('flag_masks', 1)
>>> f.getprop('flag_masks')
array([1])
>>> f.delprop('flag_masks')

'''
        try:
            return self.Flags.flag_masks
        except AttributeError:
            raise AttributeError(
                "%s doen't have CF property 'flag_masks'" %
                self.__class__.__name__)
    #--- End: def
    @flag_masks.setter
    def flag_masks(self, value):
        try:
            flags = self.Flags
        except AttributeError:
            self.Flags = Flags(flag_masks=value)
        else:
            flags.flag_masks = value
    #--- End: def
    @flag_masks.deleter
    def flag_masks(self):
        try:
            del self.Flags.flag_masks
        except AttributeError:
            raise AttributeError(
                "Can't delete non-existent %s CF property 'flag_masks'" %
                self.__class__.__name__)
        else:
            if not self.Flags:
                del self.Flags
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: flag_meanings
    # ----------------------------------------------------------------
    @property
    def flag_meanings(self):
        '''

The flag_meanings CF property.

Stored as a 1-d numpy string array but may be set as a space delimited
string or any array-like object.

**Examples**

>>> f.flag_meanings = 'low medium      high'
>>> f.flag_meanings
array(['low', 'medium', 'high'],
      dtype='|S6')
>>> del flag_meanings

>>> f.flag_meanings = ['left', 'right']
>>> f.flag_meanings
array(['left', 'right'],
      dtype='|S5')

>>> f.flag_meanings = 'ok'
>>> f.flag_meanings
array(['ok'],
      dtype='|S2')

>>> f.setprop('flag_meanings', numpy.array(['a', 'b'])
>>> f.getprop('flag_meanings')
array(['a', 'b'],
      dtype='|S1')
>>> f.delprop('flag_meanings')

'''
        try:
            return self.Flags.flag_meanings
        except AttributeError:
            raise AttributeError(
                "%s doen't have CF property 'flag_meanings'" %
                self.__class__.__name__)
    #--- End: def
    @flag_meanings.setter
    def flag_meanings(self, value): 
        try:
            flags = self.Flags
        except AttributeError:
            self.Flags = Flags(flag_meanings=value)
        else:
            flags.flag_meanings = value
    #--- End: def
    @flag_meanings.deleter
    def flag_meanings(self):
        try:
            del self.Flags.flag_meanings
        except AttributeError:
            raise AttributeError(
                "Can't delete non-existent %s CF property 'flag_meanings'" %
                self.__class__.__name__)
        else:
            if not self.Flags:
                del self.Flags
    #--- End: def

    # ----------------------------------------------------------------
    # CF property: cell_methods
    # ----------------------------------------------------------------
    @property
    def cell_methods(self):
        '''

The `cf.CellMethods` object containing the CF cell methods of the data
array.

**Examples**

>>> f.cell_methods
<CF CellMethods: time: mean (interval: 1.0 month)>

'''
        return self._get_special_attr('cell_methods')
    #--- End: def
    @cell_methods.setter
    def cell_methods(self, value):
        self._set_special_attr('cell_methods', value)
    @cell_methods.deleter
    def cell_methods(self):
        self._del_special_attr('cell_methods')

    # ----------------------------------------------------------------
    # CF property: Conventions	
    # ----------------------------------------------------------------
    @property
    def Conventions(self):
        '''

The Conventions CF property.

**Examples**

>>> f.Conventions = 'CF-1.5'
>>> f.Conventions
'CF-1.5'
>>> del f.Conventions

>>> f.setprop('Conventions', 'CF-1.5')
>>> f.getprop('Conventions')
'CF-1.5'
>>> f.delprop('Conventions')

'''
        return self.getprop('Conventions')
    #--- End: def

    @Conventions.setter
    def Conventions(self, value): self.setprop('Conventions', value)
    @Conventions.deleter
    def Conventions(self):        self.delprop('Conventions')

    # ----------------------------------------------------------------
    # CF property: institution (a simple attribute)
    # ----------------------------------------------------------------
    @property
    def institution(self):
        '''

The institution CF property.

**Examples**

>>> f.institution = 'University of Reading'
>>> f.institution
'University of Reading'
>>> del f.institution

>>> f.setprop('institution', 'University of Reading')
>>> f.getprop('institution')
'University of Reading'
>>> f.delprop('institution')

'''
        return self.getprop('institution')
    #--- End: def
    @institution.setter
    def institution(self, value): self.setprop('institution', value)
    @institution.deleter
    def institution(self):        self.delprop('institution')

    # ----------------------------------------------------------------
    # CF property: references (a simple attribute)
    # ----------------------------------------------------------------
    @property
    def references(self):
        '''

The references CF property.

**Examples**

>>> f.references = 'some references'
>>> f.references
'some references'
>>> del f.references

>>> f.setprop('references', 'some references')
>>> f.getprop('references')
'some references'
>>> f.delprop('references')

'''
        return self.getprop('references')
    #--- End: def
    @references.setter
    def references(self, value): self.setprop('references', value)
    @references.deleter
    def references(self):        self.delprop('references')

    # ----------------------------------------------------------------
    # CF property: standard_error_multiplier	
    # ----------------------------------------------------------------
    @property
    def standard_error_multiplier(self):
        '''

The standard_error_multiplier CF property.

**Examples**

>>> f.standard_error_multiplier = 2.0
>>> f.standard_error_multiplier
2.0
>>> del f.standard_error_multiplier

>>> f.setprop('standard_error_multiplier', 2.0)
>>> f.getprop('standard_error_multiplier')
2.0
>>> f.delprop('standard_error_multiplier')

'''
        return self.getprop('standard_error_multiplier')
    #--- End: def

    @standard_error_multiplier.setter
    def standard_error_multiplier(self, value):
        self.setprop('standard_error_multiplier', value)
    @standard_error_multiplier.deleter
    def standard_error_multiplier(self):
        self.delprop('standard_error_multiplier')

    # ----------------------------------------------------------------
    # CF property: source	
    # ----------------------------------------------------------------
    @property
    def source(self):
        '''

The source CF property.

**Examples**

>>> f.source = 'radiosonde'
>>> f.source
'radiosonde'
>>> del f.source

>>> f.setprop('source', 'surface observation')
>>> f.getprop('source')
'surface observation'
>>> f.delprop('source')

'''
        return self.getprop('source')
    #--- End: def

    @source.setter
    def source(self, value): self.setprop('source', value)
    @source.deleter
    def source(self):        self.delprop('source')

    # ----------------------------------------------------------------
    # CF property: title	
    # ----------------------------------------------------------------
    @property
    def title(self):
        '''

The title CF property.

**Examples**

>>> f.title = 'model data'
>>> f.title
'model data'
>>> del f.title

>>> f.setprop('title', 'model data')
>>> f.getprop('title')
'model data'
>>> f.delprop('title')

'''
        return self.getprop('title')
    #--- End: def

    @title.setter
    def title(self, value): self.setprop('title', value)
    @title.deleter
    def title(self):        self.delprop('title')

    # ----------------------------------------------------------------
    # Attribute: domain
    # ----------------------------------------------------------------
    @property
    def domain(self):
        '''

The `cf.Domain` object containing the field's domain.

**Examples**

>>> f.domain
<CF Domain: (12, 19, 73, 96)>

'''
        return self._get_special_attr('domain')
    #--- End: def
    @domain.setter
    def domain(self, value):
        self._set_special_attr('domain', value)
    @domain.deleter
    def domain(self):
        self._del_special_attr('domain')

    # ----------------------------------------------------------------
    # Attribute: subspace (read only)
    # ----------------------------------------------------------------
    @property
    def subspace(self):
        '''

Return a new object which will get or set a subspace of the field.

The returned object is a `!SubspaceField` object which may be indexed
to select a subspace by axis index values (``f.subspace[indices]``) or
called to select a subspace by coordinate object array values
(``f.subspace(**coordinate_values)``).

**Subspacing by indexing**

Subspacing by indices allows a subspaced field to be defined via index
values for the axes of the field's data array.

Indices to the returned `!SubspaceField` object have an extended
Python slicing syntax, which is similar to :ref:`numpy array indexing
<numpy:arrays.indexing>`, but with two important extensions:

* Size 1 axes are never removed.

  An integer index i takes the i-th element but does not reduce the
  rank of the output array by one:

* When advanced indexing is used on more than one axis, the advanced
  indices work independently.

  When more than one axis's slice is a 1-d boolean sequence or 1-d
  sequence of integers, then these indices work independently along
  each axis (similar to the way vector subscripts work in Fortran),
  rather than by their elements:

**Subspacing by coordinate values**

Subspacing by values of one dimensionsal coordinate objects allows a
subspaced field to be defined via coordinate values of its domain.

Coordinate objects and their values are provided as keyword arguments
to a call to the returned `!SubspaceField` object.  Coordinate objects
may be identified by their identities, as returned by their
`!identity` methods. See `cf.Field.indices` for details, since
``f.subspace(**coordinate_values)`` is exactly equivalent to
``f.subspace[f.indices(**coordinate_values)]``.

**Assignment to subspaces**

Elements of a field's data array may be changed by assigning values to
a subspace of the field.

Assignment is only possible to a subspace defined by indices of the
returned `!SubspaceField` object. For example, ``f.subspace[indices] =
0`` is possible, but ``f.subspace(**coordinate_values) = 0`` is *not*
allowed. However, assigning to a subspace defined by coordinate values
may be done as follows: ``f.subspace[f.indices(**coordinate_values)] =
0``.

**Missing data**

The treatment of missing data elements during assignment to a subspace
depends on the value of field's `hardmask` attribute. If it is True
then masked elements will notbe unmasked, otherwise masked elements
may be set to any value.

In either case, unmasked elements may be set, (including missing
data).

Unmasked elements may be set to missing data by assignment to the
`cf.masked` constant or by assignment to a value which contains masked
elements.

.. seealso:: `cf.masked`, `hardmask`, `indices`, `setdata`

**Examples**

>>> print f
Data            : air_temperature(time(12), latitude(73), longitude(96)) K
Cell methods    : time: mean
Dimensions      : time(12) = [15, ..., 345] days since 1860-1-1
                : latitude(73) = [-90, ..., 90] degrees_north
                : longitude(96) = [0, ..., 356.25] degrees_east
                : height(1) = [2] m

>>> f.shape
(12, 73, 96)
>>> f.subspace[...].shape
(12, 73, 96)
>>> f.subspace[slice(0, 12), :, 10:0:-2].shape
(12, 73, 5)
>>> lon = f.coord('longitude').array
>>> f.subspace[..., lon<180]

>>> f.shape
(12, 73, 96)
>>> f.subspace[0, ...].shape
(1, 73, 96)
>>> f.subspace[3, slice(10, 0, -2), 95].shape
(1, 5, 1)

>>> f.shape
(12, 73, 96)
>>> f.subspace[:, [0, 72], [5, 4, 3]].shape
(12, 2, 3)

>>> f.subspace().shape
(12, 73, 96)
>>> f.subspace(latitude=0).shape
(12, 1, 96)
>>> f.subspace(latitude=cf.wi(-30, 30)).shape
(12, 25, 96)
>>> f.subspace(long=cf.ge(270, 'degrees_east'), lat=cf.set([0, 2.5, 10])).shape
(12, 3, 24)
>>> f.subspace(latitude=cf.lt(0, 'degrees_north'))
(12, 36, 96)
>>> f.subspace(latitude=[cf.lt(0, 'degrees_north'), 90])
(12, 37, 96)
>>> import math
>>> f.subspace(longitude=cf.lt(math.pi, 'radian'), height=2)
(12, 73, 48)
>>> f.subspace(height=cf.gt(3))
IndexError: No indices found for 'height' values gt 3

>>> f.subspace(dim2=3.75).shape
(12, 1, 96)

>>> f.subspace[...] = 273.15
    
>>> f.subspace[f.indices(longitude=cf.wi(210, 270, 'degrees_east'),
...                      latitude=cf.wi(-5, 5, 'degrees_north'))] = cf.masked

>>> index = f.indices(longitude=0)
>>> f.subspace[index] = f.suspace[index] * 2

'''
        return SubspaceField(self)
    #--- End: def

    @property
    def year(self):
        '''

The year of each data array element.

Only applicable for reference time units.

'''
        if self._hasData:
            return type(self)(properties={'long_name': 'year'},
                              data=self.data.year, 
                              domain=self.domain.copy(),
                              copy=False)

        raise ValueError(
            "ERROR: Can't get %ss when there is no data array" % x)        
    #--- End: def

    @property
    def month(self):
        '''

The month of each data array element.

Only applicable for reference time units.

'''
        if self._hasData:
            return type(self)(properties={'long_name': 'month'},
                              data=self.data.month, 
                              domain=self.domain.copy(),
                              copy=False)

        raise ValueError(
            "ERROR: Can't get %ss when there is no data array" % x)        
    #--- End: def

    @property
    def day(self):
        '''

The day of each data array element.

Only applicable for reference time units.

'''
        if self._hasData:
            return type(self)(properties={'long_name': 'day'},
                              data=self.data.day, 
                              domain=self.domain.copy(),
                              copy=False)

        raise ValueError(
            "ERROR: Can't get %ss when there is no data array" % x)        
    #--- End: def

    @property
    def hour(self):
        '''

The hour of each data array element.

Only applicable for reference time units.

'''
        if self._hasData:
            return type(self)(properties={'long_name': 'hour'},
                              data=self.data.hour,
                              domain=self.domain.copy(),
                              copy=False)

        raise ValueError(
            "ERROR: Can't get %ss when there is no data array" % x)        
    #--- End: def

    @property
    def minute(self):
        '''

The minute of each data array element.

Only applicable for reference time units.

'''
        if self._hasData:
            return type(self)(properties={'long_name': 'minute'},
                              data=self.data.minute, 
                              domain=self.domain.copy(),
                              copy=False)

        raise ValueError(
            "ERROR: Can't get %ss when there is no data array" % x)        
    #--- End: def

    @property
    def second(self):
        '''

The second of each data array element.

Only applicable for reference time units.

'''
        if self._hasData:
            return type(self)(properties={'long_name': 'second'},
                              data=self.data.second, 
                              domain=self.domain.copy(),
                              copy=False)

        raise ValueError(
            "ERROR: Can't get %ss when there is no data array" % x)        
    #--- End: def
       
    def cell_area(self, radius=6371229.0, insert=False, force=False):
        '''

:Parameters:

    radius : data-like, optional
        The radius used for calculating spherical surface areas when
        both of the horizontal axes are part of a spherical polar
        coordinate system. By default *radius* has a value of
        6371229 metres. If units are not specified then units of
        metres are assumed.

    insert : bool, optional
        If True then the calculated cell areas are inserted in place
        as a area cell measure object. An existing area cell measure
        object for the horizontal axes will not be overwritten.

    force : bool, optional
        If True the always calculate the cell areas. By default if
        there is already an area cell measure object for the
        horizontal axes then it will provide the area values.
        
:Returns:

    out : cf.Field

**Examples**

>>> a = f.cell_area()
>>> a = f.cell_area(insert=True)
>>> a = f.cell_area(force=True)
>>> a = f.cell_area(radius=cf.Data(3389.5, 'km'))

        '''
        area_cm = self.cm('area', axes=('X', 'Y'))

        if not force and area_cm:
            w = self.weights('area')
        else:
            x = self.dim('X')
            y = self.dim('Y')
            if (x is None or y is None or 
                not x.Units.equivalent(_units_radians) or
                not y.Units.equivalent(_units_radians)):
                raise ValueError("sd---------------------")
            
            # Got x and y coordinates in radians, so we can calc.
    
            # Parse the radius of the planet
            radius = Data.asdata(radius).squeeze()
            radius.dtype = float
            if radius.size != 1:
                raise ValueError("Multiple radii: radius=%r" % radius)
            if not radius.Units:
                radius.override_units(_units_m, i=True)
            elif not radius.Units.equivalent(_units_m):
                raise ValueError(
                    "Invalid units for radius: %r" % radius.Units)
                    
            w = self.weights('area')
            radius **= 2
            w *= radius
            w.override_units(radius.Units, i=True)   
        #--- End: if               

        if insert:
            # ----------------------------------------------------
            # Insert new cell measure
            # ----------------------------------------------------
            if area_cm:
                raise ValueError(
                    "Can't overwrite an existing area cell measure object")

            cm = CellMeasure(data=w.data, copy=True)
            cm.measure = 'area'
            map_axes = w.domain.map_axes(self.domain)
            data_axes = w.data_axes()
            axes = (map_axes[data_axes[0]], map_axes[data_axes[1]])
            self.insert_cm(cm, axes=axes, copy=False)
        #--- End: if               

        w.standard_name = 'area'
        w.long_name     = 'area'

        return w
    #--- End: def

    def close(self):
        '''

Close all files referenced by the field.

Note that a closed file will be automatically reopened if its contents
are subsequently required.

:Returns:

    None

**Examples**

>>> f.close()

'''
        new = super(Field, self).close()

        self.domain.close()

        ancillary_variables = getattr(self, 'ancillary_variables', None)
        if ancillary_variables is not None:
            ancillary_variables.close()
    #--- End: def

    def iscyclic(self, axis, **kwargs):
        '''

:Parameters:

    axis, kwargs : 

:Returns:

    out : bool

.. seealso:: `cyclic`

**Examples**

'''
        axis = self.domain.axis(axis, **kwargs)
        if axis is None:
            raise ValueError("_98y345 zi7yaw47b")

        return axis in self.cyclic()
    #--- End: def

    def cyclic(self, axis=None, iscyclic=True, period=None, **kwargs):
        '''

:Parameters:

    axis, kwargs : 

    iscyclic : bool, optional

    period : data-like or None

:Returns:

    out : list

.. seealso:: `iscyclic`

**Examples**

'''
        data_axes = self.data_axes()
        old = [data_axes[i] for i in self.Data.cyclic()]

        if axis is None and not kwargs:            
            return old
        
        axis = self.domain.axis(axis, **kwargs)
        if axis is None:
            raise ValueError("879534 k.j asdm,547`")

        try:
            self.Data.cyclic(data_axes.index(axis), iscyclic)
        except IndexError:
            pass

        if iscyclic:
            dim = self.dim(axis)
            if dim is not None:
                if period is not None:
                    dim.period(period)
                elif dim.period() is None:
                    raise ValueError(
                        "Must set a period for cyclic dimension coordinates")                
        #--- End: if

        return old
    #--- End: def

    def weights(self, weights=None, scale=False, squeeze=False,
                components=False):
        '''
'''
        def _weights_field(data, domain=None, axes=None):
            '''
    :Parameters:
    
        data : cf.Data

        domain : cf.Domain, optional

        axes : list, optional

    :Returns:

        out : cf.Field

    '''
            w = type(self)(domain=domain, data=data, axes=axes, copy=False)
            w.long_name = 'weight'
            w.comment   = 'Weights for %r' % self
            return w
        #--- End: def

        if equals(weights, None) or equals(weights, 'equal'):
            # --------------------------------------------------------
            # All equal weights
            # --------------------------------------------------------
            if components:
                # Return an empty components dictionary
                return {}
            
            # Return a field containing a single weight of 1
            return _weights_field(Data(1.0, '1'))
        #--- End: if

        # Still here?

        comp = {}
        
        domain    = self.domain
        data_axes = domain.data_axes()
        calc_area = False

        weights_axes = set()

        fields        = []
        axes          = []

        if isinstance(weights, basestring) and weights in ('area', 'volume'):
            cell_measures = (weights,)
        else:
            cell_measures = []
            for w in tuple(weights):
                if isinstance(w, self.__class__):
                    fields.append(w)
                elif w in ('area', 'volume'):
                    cell_measures.append(w)
                else:
                    axes.append(w)
        #--- End: if

        # ------------------------------------------------------------
        # Field weights
        # ------------------------------------------------------------
        for f in fields:           
            s = self.domain.analyse()
            t = f.domain.analyse()

            if t['undefined_axes']:
                if t.axes(size=gt(1)).intersection(t['undefined_axes']):
                    raise ValueError("345jn456jn")

            f = f.squeeze()
            
            axes_map = {}
            
            for axis1 in f.data_axes():
                identity = t['axis_to_id'].get(axis1, None)
                if identity is None:
                    raise ValueError(
"Input weights field has unmatched, size > 1 %r axis" % f.axis_name(axis1))
                
                axis0 = s['id_to_axis'].get(identity, None)
                if axis0 is None:
                    raise ValueError(
"Input weights field has unmatched, size > 1 %r axis" % identity)
                                
                axes_map[axis1] = axis0

                if f.axis_size(axis1) != self.axis_size(axis0):
                    raise ValueError(
"Input weights field has incorrectly sized %r axis (%d != %d)" % 
(identity, f.axis_size(axis1), self.axis_size(axis0)))

                # Check that the defining coordinate data arrays are
                # compatible
                coord0 = s['axis_to_coord'][axis0]
                coord1 = t['axis_to_coord'][axis1]

                if not coord0._equivalent_data(coord1):
                    raise ValueError(
"Input weights field has incompatible %r coordinates" % identity)

                # Still here? Then the defining coordinates have
                # equivalent data arrays

                # If the defining coordinates are attached to
                # transforms then check that those transforms are
                # equivalent
                key0 = s['id_to_key'][identity]                
                key1 = v['id_to_key'][identity]

                equivalent_transforms = True
                for transform0 in self.transforms().itervalues():
                    if key0 not in transform0.coords:
                        continue

                    equivalent_transforms = False
                    for transform1 in g.transforms().itervalues():
                        if key1 not in transform1.coords:
                            continue

                        # Each defining coordinate is referenced by a
                        # transform ...
                        if self.domain.equivalent_transform(transform0,
                                                            transform1,
                                                            g.domain):
                            # ... and those transforms are equivalent
                            equivalent_transforms = True
                        #--- End: if

                        break
                    #--- End: for

                    break
                #--- End: for
   
                if not equivalent_transforms:
                    raise ValueError(
                        "Input weights field has incompatible transforms")
            #--- End: for

            f_axes = tuple([axes_map[axis1] for axis1 in f.data_axes()])
        
            for axis1 in f_axes:
                if axis1 in weights_axes:
                    raise ValueError(
                        "Multiple weights specified for %r axis" % 
                        self.axis_name(axes_map[axis1]))
            #--- End: if

            comp[f_axes] = f.Data
        
            weights_axes.update(f_axes)
        #--- End: for

        # ------------------------------------------------------------
        # Cell measure weights
        # ------------------------------------------------------------
        for measure in ('volume', 'area'):
            if measure not in cell_measures:
                continue
            
            cms = domain.items(measure, role='c', exact=True)
            if not cms:
                if measure == 'area':
                    calc_area = True
                    continue
    
                raise ValueError(
                    "Can't get weights: No %r cell measure" % measure)
            
            key, cm = cms.popitem()    
            
            if cms:
                raise ValueError("Multiple %r cell measures" % measure)
            
            cm_axes0 = domain.item_axes(key)
            
            if squeeze:
                cm_axes = [axis for axis, n in izip(cm_axes0, cm.shape)
                           if n > 1]
            else:
                cm_axes = cm_axes0

            for axis in cm_axes:
                if axis in weights_axes:
                    raise ValueError(
                        "Multiple weights specifications for %r axis" % 
                        domain.axis_name(axis))

            cm = cm.Data.copy()
            if cm_axes != cm_axes0:
                iaxes = [cm_axes0.index(axis) for axis in cm_axes]
                cm.squeeze(iaxes, i=True)

            comp[tuple(cm_axes)] = cm
            
            weights_axes.update(cm_axes)
        #--- End: for
        
        # ------------------------------------------------------------
        # Calculated area weights from X and Y 1-d coordinates
        # ------------------------------------------------------------
        if calc_area:
            xdims = self.dims({None: 'X', 'units': 'radians'})
            ydims = self.dims({None: 'Y', 'units': 'radians'})

            if not (xdims and ydims):
                raise ValueError("Can't get calculate area weights")
            
            xaxis, xcoord = xdims.popitem()
            yaxis, ycoord = ydims.popitem()
            
            if xdims or ydims:
                raise ValueError("sb769234 ddb")
            
            for axis in (xaxis, yaxis):
                if axis in weights_axes:
                    raise ValueError(
                        "Multiple weights specifications for %r axis" % 
                        self.axis_name(axis))

            if not squeeze or xcoord.size > 1:
                comp[(xaxis,)] = xcoord.cell_size(units=_units_radians)
                print 'x cell size = ',xcoord.cell_size(units=_units_radians).array
                weights_axes.add(xaxis)
                
            if not squeeze or ycoord.size > 1:                  
                if not ycoord._hasbounds:
                    ycoord = ycoord.copy()
                    ycoord.get_bounds(create=True, insert=True)
                    print 'y.bounds.array=', ycoord.bounds.array
                    ycoord.clip(-90, 90, units=Units('degrees'), i=True)
                    print 'y.bounds.array=', ycoord.bounds.array
                ycoord = ycoord.sin()                    
                print 'y cell size = ',ycoord.cell_size()
                comp[(yaxis,)] = ycoord.cell_size()
                weights_axes.add(yaxis)
            #--- End: if
        #--- End: if

        # ------------------------------------------------------------
        # 1-d linear weights from 1-d coordinates
        # ------------------------------------------------------------
        for axis in axes:
            d = self.dims(axis)
            if not d:
                raise ValueError("Can't find axis matching %r" % axis)

            key, coord = d.popitem()

            if d:
                raise ValueError("Multiple axes matching %r" % axis)
            
            axis = domain.item_axes(key)[0]

            if axis in weights_axes:
                raise ValueError(
                    "Multiple weights specifications for %r axis" % 
                    domain.axis_name(axis))

            if squeeze and domain.axis_size(axis) == 1:
                # Ignore size 1 weights component
                continue

            comp[(axis,)] = coord.cell_size()

            weights_axes.add(axis)
        #--- End: for

        # ------------------------------------------------------------
        # Scale weights
        # ------------------------------------------------------------
        if scale:
            # What to do about -ve weights? dch
            for key, w in comp.items(): 
                wmin = w.data.setdata(cf_masked, w, w<=0).min()    
                if wmin:
                    wmin.dtype = float
                    w /= wmin
                    comp[key] = w
        #--- End: if

        if components:
            # --------------------------------------------------------
            # Return a dictionary of component weights, which may be
            # empty.
            # --------------------------------------------------------
            components = {}
            for key, v in comp.iteritems():
                key = tuple([data_axes.index(axis) for axis in key])
                components[key] = v
            #--- End: for

            return components

        if not comp:
            # --------------------------------------------------------
            # No component weights have been defined so return an
            # equal weights field
            # --------------------------------------------------------
            return _weights_field(Data(1.0, '1'))
        
        # ------------------------------------------------------------
        # Return a weights field which is the outer product of the
        # component weights
        # ------------------------------------------------------------
        pp = sorted(comp.items())       
        waxes, wdata = pp.pop(0)
        while pp:
            a, y = pp.pop(0)
            wdata.outerproduct(y, i=True)
            waxes += a
        #--- End: while
            
        wdomain = self.domain.copy()
        
        asd = wdomain.axes().difference(weights_axes)
        
        wdomain.dimensions.pop('data', None)
        wdomain.remove_items(wdomain.items(axes=asd))
        wdomain.remove_axes(asd)            

        return _weights_field(wdata, wdomain, waxes)
    #--- End: def

    def collapse(self, method, axes=None, squeeze=True, mtol=1, weights=None,
                 ddof=1, a=None, i=False, **kwargs):
        r'''

Collapse axes by statistical calculations.

Missing data array elements and those with zero weight are omitted
from the calculation.

The following collapse methods are available over any subset of the
field's axes:

==================  ==================================================
Method              Notes
==================  ==================================================
Maximum             The maximum of the values.

Minimum             The minimum of the values.
                             
Sum                 The sum of the values.

Mid-range           The average of the maximum and the minimum of the
                    values.

Range               The absolute difference between the maximum and
                    the minimum of the values.

Mean                The unweighted mean, :math:`m`, of :math:`N`
                    values :math:`x_i` is

                    .. math:: m=\frac{1}{N}\sum_{i=1}^{N} x_i

                    The weighted mean, :math:`\tilde{m}`, of :math:`N`
                    values :math:`x_i` with corresponding weights
                    :math:`w_i` is

                    .. math:: \tilde{m}=\frac{1}{\sum_{i=1}^{N} w_i}
                                        \sum_{i=1}^{N} w_i x_i                  

Standard deviation  The unweighted standard deviation, :math:`s`, of
                    :math:`N` values :math:`x_i` with mean :math:`m`
                    and with :math:`N-ddof` degrees of freedom
                    (:math:`ddof\ge0`) is

                    .. math:: s=\sqrt{\frac{1}{N-ddof} \sum_{i=1}^{N}
                                (x_i - m)^2}

                    The weighted standard deviation,
                    :math:`\tilde{s}_N`, of :math:`N` values
                    :math:`x_i` with corresponding weights
                    :math:`w_i`, weighted mean :math:`\tilde{m}` and
                    with :math:`N` degrees of freedom is

                    .. math:: \tilde{s}_N=\sqrt{\frac{1}
                                          {\sum_{i=1}^{N} w_i}
                                          \sum_{i=1}^{N} w_i(x_i -
                                          \tilde{m})^2}
                  
                    The weighted standard deviation,
                    :math:`\tilde{s}`, of :math:`N` values :math:`x_i`
                    with corresponding weights :math:`w_i` and with
                    :math:`N-ddof` degrees of freedom (:math:`ddof>0`)
                    is
                  
                    .. math:: \tilde{s}=\sqrt{\frac{a \sum_{i=1}^{N}
                                         w_i}{a \sum_{i=1}^{N} w_i -
                                         ddof}} \tilde{s}_N
                  
                    where :math:`a` is the smallest positive number
                    whose product with each weight is an
                    integer. :math:`a \sum_{i=1}^{N} w_i` is the size
                    of a new sample created by each :math:`x_i` having
                    :math:`aw_i` repeats. In practice, :math:`a` may
                    not exist or may be difficult to calculate, so
                    :math:`a` is either set to a predetermined value
                    or an approximate value is calculated. The
                    approximation is the smallest positive number
                    whose products with the smallest and largest
                    weights and the sum of the weights are all
                    integers, where a positive number is considered to
                    be an integer if its decimal part is sufficiently
                    small (no greater than 10\ :sup:`-8` plus 10\
                    :sup:`-5` times its integer part). This
                    approximation will never overestimate :math:`a`,
                    so :math:`\tilde{s}` will never be underestimated
                    when the approximation is used. If the weights are
                    all integers which are collectively coprime then
                    setting :math:`a=1` will guarantee that
                    :math:`\tilde{s}` is exact.

Variance            The variance is the square of the standard
                    deviation.
==================  ==================================================


:Parameters:

    method : str or cf.Cellmethods
        Define the collapse method. All of the axes specified by the
        *axes* parameter are collapsed simultaneously by this
        method. Each method is given by one of the following strings:

          ======================  ====================================
          Method                  Possible strings
          ======================  ====================================
          Maximum                 ``'max'``, ``'maximum'``
          Minimum                 ``'min'``, ``'minimum'``                
          Sum                     ``'sum'``
          Mid-range               ``'mid_range'``
          Range                   ``'range'``
          Mean                    ``'mean'``, ``'average'``, ``'avg'``    
          Standard deviation      ``'sd'``, ``'standard_deviation'``
          Variance                ``'var'``, ``'variance'``
          ======================  ====================================

        An alternative form is to provide a CF cell methods-like
        string or a `cf.CellMethods` object equivalent to such a
        string. In this case an ordered sequence of collapses may be
        defined and collapse methods and the axes to which they apply
        are both provided. The axes are interpreted as for the *axes*
        parameter.
          
          **Example:**
          
          >>> g = f.collapse('time: max X: Y: mean dim3: sd')
          
          is equivalent to:
          
          >>> g = f.collapse('max', axes='time')
          >>> g = g.collapse('mean', axes=['X', 'Y'])
          >>> g = g.collapse('sd', axes='dim3')

    axes, kwargs : optional 
        The axes to be collapsed. The axes are those that would be
        selected by this call of the field's `axes` method:
        ``f.axes(axes, **kwargs)``. See `cf.Field.axes` for
        details. If an axis has size 1 then it is ignored. By default
        all axes with size greater than 1 are collapsed.

    weights : optional

    squeeze : bool, optional
        If True then collapsed axes are removed from the data
        array. By default the axes which are collapsed are left in the
        result's data array as axes with size 1.

    mtol : number, optional
        For each element in the output data array, the fraction of
        contributing input array elements which is allowed to contain
        missing data. Where this fraction exceeds *mtol*, missing
        data is returned. The default is 1, meaning a missing datum in
        the output array only occurs when its contributing input array
        elements are all missing data. A value of 0 means that a
        missing datum in the output array occurs whenever any of its
        contributing input array elements are missing data. Any
        intermediate value is permitted.

    ddof : number, *optional*
        The delta degrees of freedom in the calculation of a standard
        deviation or variance. The number of degrees of freedom used
        in the calculation is (N-*ddof*) where N represents the number
        of elements. By default *ddof* is 1, meaning the standard
        deviation of the population is estimated according to the
        usual formula with (N-1) in the denominator to avoid the bias
        caused by the use of the sample mean (Bessel's correction).

    a : *optional*
        Specify the value of :math:`a` in the calculation of a
        weighted standard deviation or variance when *ddof* is greater
        than 0. See the notes above for details. *a* must be a field
        of values which is broadcastable to the collapsed field or
        else a number. By default approximate values are calculated.

    i : bool, optional
        If True then update the field in place. By default a new field
        is created.

:Returns:

    out : cf.Field
        The collapsed field.

'''
        if i:
            f = self
        else:
            f = self.copy()

        # If method is a cell methods string (such as 'area: mean
        # time: max') then convert it to a CellMethods object
        if ':' in method:
            method = CellMethods(method)

        if isinstance(method, CellMethods):
            if axes is not None:
                raise ValueError(
                    "Can't collapse: Can't set both axes and cell methods")

            methods = [cm['method'] for cm in method]
            axes    = [cm['name']   for cm in method]
            for name, m in zip(axes, methods):
                if None in name:
                    raise ValueError(
                        "Can't collapse: Invalid %s cell method name: %s" %
                        (m, name))
            #--- End: for
 
            # Recursively call the collapse method for each cell
            # method
            while methods:
                f.collapse(methods.pop(0), axes=axes.pop(0),
                           squeeze=squeeze, weights=weights,
                           mtol=mtol, ddof=ddof, f=f, i=True)
            #--- End: while

            return f
        #--- End: if

        # Still here?

        # ------------------------------------------------------------
        # Find which axes have been requested for collapsing
        # ------------------------------------------------------------
        domain = f.domain
    
        kwargs['size']    = gt(1)
        kwargs['ordered'] = True        
        collapse_axes = domain.axes(axes, **kwargs)
    
        if not collapse_axes:
            # Do nothing if the requested axes do not exist in field's
            # domain
            return f

        method = _collapse_methods[method]

        size = reduce(operator_mul, domain.axes_sizes(collapse_axes).values())
        min_size = _collapse_min_size[method]
        if size < min_size:
            raise ValueError(
                "Can't calculate %s from fewer than %d elements" %
                (_collapse_cell_methods[method], min_size))

        # ------------------------------------------------------------
        # Find which axes actually spanned by the data array
        # ------------------------------------------------------------    
        data_axes = domain.data_axes()
        iaxes = [data_axes.index(axis) for axis in collapse_axes]
    
        # Initialise keyword arguments to the cf.Data collapse methods
        if method in _collapse_ddof_methods:
            kwargs2 = {'ddof': ddof, 'a': a}
        else:
            kwargs2 = {}

        # ------------------------------------------------------------
        # Calculate weights, if required
        # ------------------------------------------------------------
        if weights and method in _collapse_weighted_methods:
            weights = f.weights(weights=weights, squeeze=True,
                                scale=True, components=True)
            if weights:
                kwargs2['weights'] = weights
        #--- End: if

        # ------------------------------------------------------------
        # Collapse the data array
        # ------------------------------------------------------------
        print '.DCH@', method
        getattr(f.Data, method)(axes=iaxes, squeeze=squeeze, mtol=mtol, i=True,
                                **kwargs2)
    
        if squeeze:
            # Remove the collapsed axes from the field's list of data
            # array axes
            domain.dimensions['data'] = [axis for axis in data_axes
                                         if axis not in collapse_axes]
    
        # ------------------------------------------------------------
        # Update ancillary variables
        # ------------------------------------------------------------
        ancillary_variables = getattr(f, 'ancillary_variables', None)
        if ancillary_variables:
            new_av = []

            for av in ancillary_variables:
                keep = True

                axis_map = av.domain.map_axes(domain)
                for axis in av.axes(size=gt(1)):
                    if axis not in axis_map or axis_map[axis] in collapse_axes:
                        # Don't keep this ancillary variable because
                        # it has an axis of size > 1 which is either a
                        # collapse axis or which doesn't match any
                        # axis in the parent field
                        keep = False
                        break
                #--- End: for

                if keep:
                    new_av.append(av)
            #--- End: for

            if new_av:
                f.ancillary_variables = AncillaryVariables(new_av)
            else:
                del f.ancillary_variables 
        #--- End: if        

        # ------------------------------------------------------------
        # Update fields in transforms
        # ------------------------------------------------------------
        for coordinate_reference in f.transforms().itervalues():
            for term, value in coordinate_reference.items():
                if not isinstance(value, Field):
                    # Keep the term because it's not a field
                    continue 

                axis_map = value.domain.map_axes(domain)
                for axis in value.axes(size=gt(1)):
                    if axis not in axis_map or axis_map[axis] in collapse_axes:
                        # Don't keep this term because it has a size >
                        # 1 axis which is either a collapse axis or
                        # which doesn't match any axis in the parent
                        # field
                        coordinate_reference[term] = None
                        break
                #--- End: for
            #--- End: for
        #--- End: for
    
        #-------------------------------------------------------------
        # Update dimension coordinates, auxiliary coordinates and cell
        # measures
        # -------------------------------------------------------------
        for axis in collapse_axes:
            # Ignore axes which are already size 1
            if domain.axis_size(axis) == 1:
                continue
            
            # Remove all size > 1 auxiliary coordinates and cell
            # measures which span this axis
            domain.remove_items(role=('a', 'c'), axes=axis)

            dim_coord = domain.item(axis)
            if dim_coord is None:
                continue
    
            if dim_coord.hasbounds:
                bounds = [dim_coord.bounds.datum(0), dim_coord.bounds.datum(-1)]
            else:
                bounds = [dim_coord.datum(0), dim_coord.datum(-1)]
    
            units = dim_coord.Units
            data   = Data([(bounds[0] + bounds[1])*0.5], units)
            bounds = Data([bounds], units)
    
            dim_coord.insert_data(data, bounds=bounds, copy=False)
    
            # Put the new dimension coordinate into the domain
            domain.insert_axis(1, key=axis, replace=True)
            domain.insert_dim(dim_coord, key=axis, copy=False, replace=True)      
        #--- End: for
    
        # ------------------------------------------------------------
        # Update the cell methods
        # ------------------------------------------------------------
        if not hasattr(f, 'cell_methods'):
            f.cell_methods = CellMethods()

        collapse_axes = sorted(collapse_axes)
        
        name = []
        for axis in collapse_axes:
            item = domain.item(axis)
            if item is not None:
                name.append(item.identity(default=axis))
            else:
                name.append(axis)
        #--- End: for

        string = '%s: %s' % (': '.join(name), _collapse_cell_methods[method])

        cell_method = CellMethods(string)
        
        cell_method[0]['dim'] = collapse_axes[:]
        
        if not f.cell_methods or f.cell_methods[-1] != cell_method:
            f.cell_methods += cell_method

        # ------------------------------------------------------------
        # Return the collapsed field
        # ------------------------------------------------------------
        return f
    #--- End: def

    def data_axes(self):
        '''

Return the axes of the field's data array.

:Returns:

    out : list or None
        The ordered axes of the field's data array. If there is no
        data array then None is returned.

.. seealso:: `axes`, `item_axes`

**Examples**

'''    
        return self.domain.data_axes()
    #--- End: def

    def dump(self, complete=False, display=True, level=0, title='Field', q='='):
        '''

Return a string containing a description of the field.

The description described without abbreviation with the exception of
data arrays, which are abbreviated to their first and last values, and
fields contained in transforms and ancillary variables are given as
one-line summaries.

:Parameters:
 
    complete : bool, optional
        Output a complete dump. Fields contained in transforms and
        ancillary variables are themselves described with their dumps.

    display : bool, optional
        If False then return the description as a string. By default
        the description is printed, i.e. ``f.dump()`` is equivalent to
        ``print f.dump(display=False)``.

    level : int, optional

    title : str, optional
    
    q : str, optional
    
:Returns:

    out : None or str
        A string containing the description.

**Examples**

'''       
        indent = '    '      
        indent0 = indent * level
        indent1 = indent0 + indent

        domain = self.domain

        title = '%s%s: %s' % (indent0, title, self.name(''))
        line  = '%s%s'     % (indent0, ''.ljust(len(title)-level*4, q))

        # Title
        string = [line, title, line]

        # Axes
        dimension_sizes = domain.dimension_sizes
        if dimension_sizes:
            string.extend((domain.dump_axes(display=False, level=level), ''))

        # Data
        if self._hasData:
            x = ['%s(%d)' % (domain.axis_name(axis), dimension_sizes[axis])
                 for axis in domain.data_axes()]
            string.append('%sData(%s) = %s' % (indent0, ', '.join(x),
                                               str(self.Data)))

        # Cell methods
        cell_methods = getattr(self, 'cell_methods', None)
        if cell_methods is not None:            
            string.append('%scell_methods = %s' % (indent0, str(cell_methods)))

        # Simple properties
        if self._simple_properties():
            string.extend(
                ('', self._dump_simple_properties(level=level,
                                                  omit=('Conventions',))))
            
        # Flags
        flags = getattr(self, 'Flags:', None)
        if flags is not None:            
            string.extend(('', flags.dump(display=False, level=level)))

        # Domain
        string.append(domain.dump_components(complete=complete, display=False,
                                             level=level))

        # Ancillary variables
        ancillary_variables = getattr(self, 'ancillary_variables', None)
        if ancillary_variables is not None:
            string.extend(('', '%sAncillary variables:' % indent0))
            if not complete:
                x = ['%s%r' % (indent1, f) for f in ancillary_variables]
                string.extend(x)
            else:
                for f in ancillary_variables:
                    string.append(f.dump(display=False, complete=False,
                                         level=level+1,
                                         title='Ancillary field', q='-'))
        #--- End: if

        string.append('')
        
        string = '\n'.join(string)
       
        if display:
            print string
        else:
            return string
    #--- End: def

    def equals(self, other, rtol=None, atol=None, traceback=False):
        '''

True if two fields are logically equal, False otherwise.

:Parameters:

    other :
        The object to compare for equality.

    atol : float, optional
        The absolute tolerance for all numerical comparisons, By
        default the value returned by the `cf.ATOL` function is used.

    rtol : float, optional
        The relative tolerance for all numerical comparisons, By
        default the value returned by the `cf.RTOL` function is used.

    traceback : bool, optional
        If True then print a traceback highlighting where the two
        instances differ.

:Returns: 

    out : bool
        Whether or not the two instances are equal.

**Examples**

>>> f.Conventions
'CF-1.0'
>>> g = f.copy()
>>> g.Conventions = 'CF-1.5'
>>> f.equals(g)
True

In the following example, two fields differ only by the long name of
their time coordinates. The traceback shows that they differ in their
domains, that they differ in their time coordinates and that the long
name could not be matched.

>>> g = f.copy()
>>> g.coord('time').long_name = 'something_else'
>>> f.equals(g, traceback=True)
Coordinate: Different long_name: 'time', 'something else'
Coordinate: Different long_name: 'time', 'latitude in rotated pole grid'
Coordinate: Different long_name: 'time', 'longitude in rotated pole grid'
Domain: Different coordinate: <CF Coordinate: time(12)>
Field: Different 'domain': <CF Domain: (73, 96, 12)>, <CF Domain: (73, 96, 12)>
False

'''
        return super(Field, self).equals(other, rtol=rtol, atol=atol, 
                                         traceback=traceback,
                                         ignore=('Conventions',))
    #---End: def

    def expand_dims(self, position=0, axis=None, i=False, **kwargs):
        '''

Insert a size 1 axis into the data array.

:Parameters:

    position : int, optional
        Specify the position that the new axis will have in the data
        array axes. By default the new axis has position 0, the
        slowest varying position.

    axis, kwargs : *optional*
        Specify the axis to insert. By default a new size 1 axis is
        inserted which doesn't yet exist.

        If *axis* and/or *kwargs* are set then the axis is the unique,
        size 1 axis that would be selected by this call of the field's
        `axes` method: ``f.axes(axis, size=1, **kwargs)``. See
        `cf.Field.axes` for details.

    i : bool, optional
        If True then update the field in place. By default a new field
        is created.

:Returns:

    out : cf.Field

.. seealso:: `axes`, `flip`, `squeeze`, `swapaxes`, `transpose`,
             `unsqueeze`,

**Examples**

'''
        domain = self.domain
#        data_axes  = domain.dimensions['data']

        if axis is None and not kwargs:
            axis = domain.new_axis_identifier()
        else:
            axis = domain.axis(axis, size=1, **kwargs)
            if axis is None:
                raise ValueError("Can't identify axis to insert")            
            elif axis in domain.data_axes():
                raise ValueError(
                    "Can't insert a duplicate axis: %r" % axis)
        #--- End: if
       
        # Expand the dims in the field's data array
        f = super(Field, self).expand_dims(position, i=i)

        domain = f.domain
        domain.dimensions['data'].insert(position, axis)
        domain.dimension_sizes[axis] = 1

        return f
    #--- End: def

    def indices(self, *arg, **kwargs):
        '''

Return the data array indices which correspond to one dimensional
coordinate object values.

If no coordinate values are specified for an axis then a full slice
(``slice(None)``) is assumed for that axis.

Values for size 1 axes which are not spanned by the field' data array
may be specified, but only indices for axes which span the field's
data array will be returned.

The coordinate value conditions may be given in any order.

:Parameters:

    arg : str, *optional*
        By default keywords are considered to be an abbreviations of
        one-dimensional coordinate object identities. If *arg* is
        ``'exact'`` then keywords are considered to be unabbreviated
        one-dimensional coordinate object identities. If *arg* is
        ``'regex'`` then keywords are considered to be regular
        expressions supported by the `re` module and are compared with
        one-dimensional coordinate object identities via the
        `re.search` function. If, however, a keyword is a domain
        identifier, then *arg* is ignored for that keyword.

    kwargs : *optional*
        Keyword names identify one-dimensional coordinate object and
        keyword values specify tests on their data array values. For
        each item's axis, an index is created which selects data array
        elements where the test is passed. Each keyword/value pair is
        specified as follows:

          ========  ==================================================
          keyword:  A string which selects a unique, one-dimensional
                    coordinate object by comparison with its identity,
                    as returned by its `!identity` method; or the
                    coordinate object may be selected by its domain
                    identifier (such as ``'dim1'``, ``'aux0'``, etc.).
                  
                    By default it is assumed to be an
                    abbreviation. See the *exact* argument.

          value:    Any object specifying a condition for the
                    coordinate object's to be tested against. Field
                    data array elements are selected where the
                    coordinate object's data array equals the value.
          ========  ==================================================

        By default, each data array axis without an item specification
        is assigned the full slice ``slice(None)``.

          Example: To create indices to a field's data array which
          select elements where its latitude coordinate values are
          greater than 60 degrees north you could set
          ``latitude=cf.ge(60, 'degrees_N')``.

:Returns:

    out : tuple
        
.. seealso:: `setdata`, `subspace`

**Examples**

These examples use the following field, which includes a dimension
coordinate object with no identity (``ncvar:model_level_number``) and
which has a data array which doen't span all of the domain axes:

>>> print f
air_temperature field summary
-----------------------------
Data            : air_temperature(ncvar:model_level_number(19), latitude(73), longitude(96)) K
Dimensions      : ncvar:model_level_number(19) = [1, ... 19]
                : latitude(73) = [-90.0, ..., 90.0] degrees_north
                : longitude(96) = [0.0, ..., 356.25] degrees_east
                : time(1) = [ 2004-06-01 00:00:00] noleap calendar
>>> f.items()
{'dim0': <CF DimensionCoordinate: time(1) days since 2004-01-01 noleap calendar>,
 'dim1': <CF DimensionCoordinate: ncvar:model_level_number(19)>,
 'dim2': <CF DimensionCoordinate: latitude(73), degree_north>,
 'dim3': <CF DimensionCoordinate: latitude(96), degree_east>}

>>> f.indices(lat=0.0, lon=0.0)
(slice(0, 19, 1), slice(0, 1, 1), slice(0, 1, 1))

>>> f.indices(lon=cf.wi(0, 3.75), lat=cf.lt(0.0))
(slice(0, 19, 1), slice(0, 36, 1), slice(0, 2, 1))

>>> f.indices(lat=cf.lt(0.0), lon=cf.set([0, 356.25]))
(slice(0, 19, 1), slice(0, 36, 1), slice(0, 96, 96))

>>> f.indices(lat=cf.lt(0.0), lon=cf.set([0, 356.25, 3.75]))
(slice(0, 19, 1), slice(0, 32, 1), [0, 1, 95])

>>> f.indices(dim1=cf.wi(6, 18))
(slice(5, 19, 1), slice(0, 73, 1), slice(0, 96, 1))

>>> f.indices(dim1=19, lon=cf.ne(0, 'degrees_east'))
(slice(18, 19, 1), slice(0, 73, 1), slice(1, 96, 1))

>>> f.indices('exact', latitude=cf.lt(0.0), longitude=0)
(slice(0, 19, 1), slice(0, 32, 1), slice(0, 1, 1))

>>> f.indices('exact', lat=cf.lt(0.0), long=0)
ValueError: Can't find indices: Ambiguous indentity: 'lat'

>>> f.indices('regex', lat=cf.lt(0.0), long'=0)
(slice(0, 19, 1), slice(0, 32, 1), slice(0, 1, 1))

'''
        if arg:
            if len(arg) > 1:
                raise ValueError("Can only set 0 or 1 postitional arguments")

            arg = arg[0]
            exact = (arg == 'exact')
            regex = (arg == 'regex')
            if not (exact or regex):
                raise ValueError(
                    "Unrecognized postitional argument: %r" % arg)
        else:
            exact = False
            regex = False

        domain = self.domain

        seen_axes = []

        data_axes = domain.data_axes()

        # Initialize indices
        indices = [slice(None)] * self.ndim

        # Loop round slice criteria
        for identity, value in kwargs.iteritems():
            coords = domain.items(identity, role=('d', 'a'), rank=1,
                                 exact=exact, regex=regex)

            if len(coords) != 1:
                raise ValueError(
                    "Can't find indices: Ambiguous indentity: %r" %
                    identity)

            key, coord = coords.popitem()

            axis = domain.item_axes(key)[0]

            if axis in seen_axes:
                raise ValueError(
                    "Can't find indices: Duplicate %r axis" % axis)
            else:
                seen_axes.append(axis)

            if (coord.isdimension and
                self.iscyclic(key) and 
                isinstance(value, Comparison) and
                value.operator in ('wi', 'wo')):
            
                period = coord.period()
                if period is None:
                    raise ValueError(
"Can't get cyclic indices for non-periodic %r dimension coordinates" %
coord.name())

                a = coord.array
                
                without = value.operator == 'wo'
                
                l = value.value[0] % period
                u = value.value[1] % period
            
                if coord.direction():
                    
                    mx = a[-1]
                    l -= period * (1 + (l - mx)//period)
                    u -= period * (1 + (u - mx)//period)

                    step = 1
                    if without:
                        l, u = u, l
                        start = numpy_where(a > l)[0][0]
                        stop  = numpy_where(a < u)[0][-1] + 1
                    else:
                        start = numpy_where(a >= l)[0][0]
                        stop  = numpy_where(a <= u)[0][-1] + 1
                else:
                    mx = a[0]
                    l -= period * (1 + (l - mx)//period)
                    u -= period * (1 + (u - mx)//period)

                    step = -1
                    if without:
                        l, u = u, l
                        start = numpy_where(a < u)[0][0]
                        stop  = numpy_where(a > l)[0][-1] + 1
                    else:
                        start = numpy_where(a <= u)[0][0]
                        stop  = numpy_where(a >= l)[0][-1] + 1
                #--- End: if

                if l >= u:
                    start -= a.size
                        
                index = slice(start, stop, step)

            else:
                item_match = (coord.Data == value)
                
                if not item_match.any():
                    raise IndexError(
                        "No %r axis indices found from: %r" %
                        (identity, value))
                    
                index = item_match.array
            #--- End: if

            # Add the index to the right place in the list of indices.
            if axis in data_axes:
                indices[data_axes.index(axis)] = index
        #--- End: for

        return tuple(parse_indices(self, tuple(indices), False))
    #--- End: def

    def insert_data(self, data, axes=None, copy=True, replace=True):
        '''

Insert a new data array into the field in place.

Note that the data array's missing data value, if it has one, is not
transferred to the field.

:Parameters:

    data : cf.Data
        The new data array.

    axes : sequence of str, optional
        A list of axis identifiers (``'dimN'``), stating the axes, in
        order, of the data array.

        The ``N`` part of each identifier should be replaced by an
        integer greater then or equal to zero such that either a) each
        axis identifier is the same as one in the field's domain, or
        b) if the domain has no axes, arbitrary integers greater then
        or equal to zero may be used, the only restriction being that
        the resulting identifiers are unique.

        By default the axes will either be those already defined by
        the domain for the data array, else the sequence of
        consecutive axis identifiers ``'dim0'`` up to ``'dimM'``,
        where ``M`` is the number of axes of the data array (or an
        empty sequence if the data array is a scalar).

        If an axis of the data array already exists in the domain then
        the it must have the same size as the domain axis.

    copy : bool, optional
        If False then the new data array is not deep copied prior to
        insertion. By default the new data array is deep copied.

    replace : bool, optional
        If False then do not replace an existing data array. By
        default an data array is replaced with *data*.
   
:Returns:

    None

**Examples**

>>> f.dimension_sizes
{'dim0': 1, 'dim1': 3}
>>> f.insert_data(cf.Data([[0, 1, 2]]))

>>> f.dimension_sizes
{'dim0': 1, 'dim1': 3}
>>> f.insert_data(cf.Data([[0, 1, 2]]), axes=['dim0', 'dim1'])

>>> f.dimension_sizes
{}
>>> f.insert_data(cf.Data([[0, 1], [2, 3, 4]]))
>>> f.dimension_sizes
{'dim0': 2, 'dim1': 3}

>>> f.insert_data(cf.Data(4))

>>> f.insert_data(cf.Data(4), axes=[])

>>> f.dimension_sizes
{'dim0': 3, 'dim1': 2}
>>> data = cf.Data([[0, 1], [2, 3, 4]])
>>> f.insert_data(data, axes=['dim1', 'dim0'], copy=False)

>>> f.insert_data(cf.Data([0, 1, 2]))
>>> f.insert_data(cf.Data([3, 4, 5]), replace=False)
ValueError: Can't initialize data: Data already exists
>>> f.insert_data(cf.Data([3, 4, 5]))

'''
        if self._hasData and not replace:
            raise ValueError(
                "Can't set data: Data already exists and replace=%s" %
                replace)

        domain = self.domain

        if data.isscalar:
            # --------------------------------------------------------
            # data is scalar
            # --------------------------------------------------------
            if axes: 
                raise ValueError(
                    "Can't set data: Wrong number of axes for data array: %s" %
                    axes)

            axes = []

        elif axes is not None:
            # --------------------------------------------------------
            # axes have been set
            # --------------------------------------------------------
            if len(set(axes)) != data.ndim:
                raise ValueError(
                    "Can't set data: Wrong number of axes for data array: %r" %
                    axes)
            
            for axis, size in izip(axes, data.shape):
                try:
                    domain.insert_axis(size, axis, replace=False)
                except ValueError:
                    raise ValueError(
"Can't set data: Incompatible domain size for axis %r (%d)" %
(axis, size))
            #--- End: for

            axes = list(axes)

        else:
            # --------------------------------------------------------
            # axes have not been set and data is not scalar
            # --------------------------------------------------------
            axes = domain.data_axes() #dimensions.get('data', None)
            
            if axes is not None:
                if len(set(axes)) != data.ndim:
                    raise ValueError(
"Can't set data: Wrong number of axes for data array: %s" %
axes)
            else:
                axes = ['dim%d' % N for N in range(data.ndim)]
            #--- End: if

            for axis, size in izip(axes, data.shape):
                try:
                    domain.insert_axis(size, axis, replace=False)
                except ValueError:
                    raise ValueError(
                        "Can't set data: Incompatible size for axis %r (%d)" %
                        (axis, size))
        #--- End: if

        domain.dimensions['data'] = axes

        if copy:
            data = data.copy()

        self.Data = data
    #--- End: def

    def match(self, match=None, cvalue={}, csize={}, rank=None, exact=False,
              match_all=True, regex=False, inverse=False):
        '''

Test whether or not the field satisfies the given conditions.

Different types of conditions may be set with the keyword arguments:
         
===========  =========================================================
Keyword      What gets tested
===========  =========================================================
*match*      The field's properties
             
*cvalue*     The field's coordinate object array values
             
*csize*      The field's coordinate object cell sizes
             
*rank*       The field's domain rank
===========  =========================================================

When multiple criteria are given, the field matches if it satisfies
the conditions given by each one.

**Quick start examples**

There is great flexibility in the types of test which can be
specified, and as a result the documentation is very detailed in
places. These preliminary, simple examples show that the usage need
not always be complicated and may help with understanding the keyword
descriptions.

1. Test if a field contains air temperature data:

   >>> f.match('air_temperature')

2. Test if a field has at least one longitude grid cell point on the
   Greenwich meridian:

   >>> f.match(cvalue={'longitude': 0})

3. Test if a field has latitude grid cells which all have a resolution
   of less than 1 degree:

   >>> f.match(csize={'latitude': cf.lt(1, 'degree')})

4. Test if a field has exactly 4 domain axes:

   >>> f.match(rank=4)

5. Examples 1 to 4 may be combined to test if a field has exactly 4
   domain axes, contains air temperature data, has at least one
   longitude grid cell point on the Greenwich meridian and all
   latitude grid cells have a resolution of less than 1 degree:

   >>> f.match('air_temperature',
   ...         cvalue={'longitude': 0},
   ...         csize={'latitude': cf.lt(1, 'degree')},
   ...         rank=4)

6. Test if a field contains monthly mean data array values:

   >>> f.match({'cell_methods': cf.CellMethods('time: mean')},
   ...         csize={'time': cf.wi(28, 31, 'days')})

Further examples are given within and after the description of the
arguments.

:Parameters:

    match : *optional*
        Set conditions on the field's property values. By default (see
        the *match_all* argument) the field matches if, for each
        specified property, the field's property value passes the
        given test. The *match* argument may be one of:

          * ``None`` or an empty dictionary. Always matches the
            field. This is the default.
        ..

          * A string or `cf.Comparison` object to be compared with
            field's identity, as returned by its `identity` method. If
            it is a string then the usual rules for string comparisons
            apply (see the *exact* and *regex* arguments for details,
            e.g. by default a string value is tested for being an
            abbreviation).

              Example: to match a field with an identity starting
              with "ocean" you could set ``match='ocean'``.

            This case may be specified equivalently in a dictionary
            with the special key of ``None``.

              Example: ``match='cloud_area_fraction'`` is
              equivalent to ``match={None: 'cloud_area_fraction'}``.
       ..

          * A dictionary which identifies properties of the field with
            corresponding tests on their values. Each key/value pair
            is specified as follows:

              ======  ================================================      
              key:    An unabbreviated property name (such as
                      ``'long_name'``, ``'units'``, ``'foo'``, etc).

              value:  A condition to be tested against the field's
                      property value for, in general, equality.

                      The exceptions are for string valued conditions
                      (see the *exact* and *regex* arguments for
                      details, e.g. by default a string value is
                      tested for being an abbreviation) and for values
                      of some special keys (see the following table).
              ======  ================================================
              
            The dictionary keys whose values have special
            interpretations are:

              ==================  ====================================
              Special key         Value
              ==================  ====================================
              ``None``            The value is compared (with the
                                  usual rules) against the field's
                                  identity, as returned by its
                                  `identity` method.
                                    
              ``'units'``         The value must be a string and, by
                                  default, is evaluated for
                                  equivalence with the field's `units`
                                  property. See the *exact* argument.
                            
              ``'calendar'``      The value must be a string and, by
                                  default, is evaluated for
                                  equivalence with the field's
                                  `calendar` property. See the *exact*
                                  argument.
                              
              ``'cell_methods'``  The value must be a `cf.CellMethods`
                                  object and, by default, is evaluated
                                  for being equivalent to a single
                                  cell method contained within the
                                  field's `cell_methods` property. See
                                  the *exact* argument.
              ==================  ====================================
            
              Example: To match a field with a `standard_name`
              starting with the string "air" and with `units` of
              temperature you could set ``match={'standard_name':
              'air', 'units': 'K'}``.

    cvalue : dict, optional
        A dictionary which identifies coordinate objects of the field
        with corresponding tests on their data array values. By
        default (see the *match_all* argument) the field matches if,
        for each specified coordinate object, the field has such an
        item and at least one element of its data array passes the
        given test. Each key/value pair is specified as follows:

          ======  ====================================================
          key:    A string or `cf.Comparison` object selecting a
                  unique coordinate object by comparison with its
                  identity, as returned by its `!identity` method; or
                  the coordinate object may be selected by its domain
                  identifier (such as ``'dim1'``, ``'aux0'``, etc.).
                  
                  If it is a string representing a coordinate object
                  identity then the usual rules for string comparisons
                  apply (see the *exact* and *regex* arguments for
                  details, e.g. by default a string value is tested
                  for being an abbreviation).

                  If it is a `cf.Comparison` object then it is tested
                  for equality.
                  
          value:  In general, any object specifying a condition for
                  the coordinate object's data array to be tested
                  against. The test is passed if at least one data
                  array element equals the value.

                  If the value is ``None`` then the test is always
                  passed (i.e. this case tests for coordinate object
                  existence).
          ======  ====================================================

          Example: To match a field with a depth coordinate object
          which has at least one data array element between one and
          two kilometres you could set ``cvalue={'depth': cf.wi(1000,
          2000, 'm')}``.

    csize : dict, optional
        A dictionary which identifies coordinate objects of the field
        with corresponding tests on their cell sizes. By default (see
        the *match_all* argument) the field matches if, for each
        specified coordinate object, the field has such an item and
        all of its cell sizes pass the given test. However, if a
        coordinate object has also been specified with the *cvalue*
        argument then a match occurs if all of the cells meeting the
        *cvalue* conditions pass the cell size test.  Each key/value
        pair is specified as follows:

          ======  ====================================================
          key:    A string or `cf.Comparison` object selecting a
                  unique coordinate object by comparison with its
                  identity, as returned by its `!identity` method; or
                  the coordinate object may be selected by its domain
                  identifier (such as ``'dim1'``, ``'aux0'``, etc.).
                  
                  If it is a string representing a coordinate object
                  identity then the usual rules for string comparisons
                  apply (see the *exact* and *regex* arguments for
                  details, e.g. by default a string value is tested
                  for being an abbreviation).

                  If it is a `cf.Comparison` object then it is tested
                  for equality.

          value:  In general, any object specifying a condition for
                  the coordinate object's cell sizes to be tested
                  against. The test is passed if at all of the cell
                  sizes (or all those for data array elements which
                  passed a test given by the *cvalue* argument) equal
                  the value.

                  If the value is ``None`` then the test is always
                  passed (i.e. this case tests for coordinate object
                  existence).
          ======  ====================================================

          Example: To match a field whose latitude coordinate
          object cell sizes are all strictly less than 2 degrees you
          could set ``csize={'latitude': cf.lt(2, 'degrees')}``. If
          this test were to be used in conjunction with
          ``cvalue={'latitude': cf.ge(0)}`` then it is less stringent,
          as the field would match if all of its northern hemisphere
          latitude coordinate object cell sizes were less than 2
          degrees.

    rank : int or cf.Comparison, optional
        Specifiy a condition on the field's domain rank, where the
        domain rank is the number of domain axes. The field matches if
        its number of domain axes equals *rank*.

          Example: ``rank=2`` matches a field with exactly two domain
          axes and ``rank=cf.wi(3, 4)`` matches a field with three or
          four domain axes.

    exact : bool, optional
        The *exact* argument applies to the interpretion of particular
        conditions given by values of the *match* argument and by keys
        of the *cvalue* and *csize* arguments. By default *exact* is
        False, which means that:
       
          * If the *regex* argument is False (which is the default)
            then string values are considered to be arbitrary
            abbreviations.
  
          * If the *regex* argument is True then string values are
            considered to be regular expressions, which are evaluated
            using the :py:obj:`re.search` function.

          * `units` and `calendar` values in the *match* dictionary
            are evaluated for equivalence rather then equality
            (e.g. ``'metre'`` is equivalent to ``'m'`` and to
            ``'km'``).

          * A `cell_methods` value in the *match* dictionary is
            evaluated for equivalence as follows:

            - If it contains a single cell method then it is evaluated
              for being equivalent to a single cell method contained
              within the field's cell methods.

            - If it contains two or more cell methods then it is
              evaluated for being equivalent to the field's cell
              methods.
        ..

          Example: To match a field with a `standard_name` starting
          with "air_pressure" and `units` of pressure you could set
          ``match={'standard_name': 'air_pressure', 'units': 'hPa'},
          exact=False``.

        If *exact* is True then:

          * All string values are considered to be unabbreviated and
            not to be regular expressions (even if the *regex*
            argument is True).

          * `units` and `calendar` values in the *match* dictionary
            are evaluated for exact equality rather than equivalence
            (e.g. ``'metre'`` is equal to ``'m'``, but not to
            ``'km'``).

          * A `cell_methods` value in the *match* dictionary is
            evaluated for equivalence to the field's cell methods.
        ..

          Example: To match a field with a `standard_name` of exactly
          "air_pressure" and `units` of exactly hectopascals you could
          set ``match={'standard_name': 'air_pressure', 'units':
          'hPa'}, exact=True``.

        Note that `cf.Comparison` objects provide a mechanism for
        overriding ``exact=False`` for individual values.

          Example: ``match={'standard_name': cf.eq('air_pressure'),
          'units': 'hPa'}`` will match a field with a `!standard_name`
          of exactly "air_pressure" and any `units` of pressure.
    
    match_all : bool, optional
        By default *match_all* is True and the field matches if it
        satisfies all of the conditions specified by all of the test
        types (properties, coordinate object values, coordinate object
        cell sizes, domain rank and data array rank).

        If *match_all* is False then the field will match if, for each
        test type, it satisfies at least one of the given conditions.

          Example: To match a field with a `standard_name`
          containing "ocean" and/or `units` of density you could set
          ``match={'standard_name': cf.eq('ocean', regex=True),
          'units': 'kg m-3'}, match_all=False``. If *match_all* were
          True in this case then the field would match only if it
          satisfied both conditions.
    
    regex : bool, optional
        By default *regex* is False, which means that all strings
        given by values of the *match* argument and by keys of the
        *cvalue* and *csize* arguments are considered to be arbitrary
        abbreviations or else exact (see the *exact* argument).

        If *regex* is True then such strings are considered to be
        regular expressions supported by the :py:obj:`re` module and
        are compared via the :py:obj:`re.search` function (see the
        *exact* argument).

          Example: To match a field with a `long_name` which
          starts with "a" and ends with "z" you could set
          ``match={'long_name': '^a.*z$'}, regex=True``.

        Note that `cf.Comparison` objects provide a mechanism for
        overriding *regex* for individual values.

          Example: ``match={'long_name': cf.eq('^a.*z$',
          regex=True)}`` will match a field with a `long_name` which
          starts with "a" and ends with "z" regardless of the value of
          *regex*.
    
    inverse : bool, optional
        If True then return the field matches if it does not satisfy
        the given conditions.

:Returns:

    out : bool
        True if the field satisfies the given criteria, False
        otherwise.

.. seealso:: `items`, `select`

**Examples**

Field identity starts with "air":

>>> f.match('air')

Field identity ends with with "temperature":

>>> f.match('temperature$', regex=True)

Field identity is exactly "air_temperature":

>>> f.match('air_temperature', exact=True)

Field has units of temperature:

>>> f.match({'units': 'K'}):

Field has units of exactly Kelvin:

>>> f.match({'units': 'K'}, exact=True)

Field standard name starts with "air" and has units of temperature:

>>> f.match({'standard_name': 'air',
...          'units': 'K'})

Field standard name starts with "air" and/or has units of temperature:

>>> f.match({'standard_name': 'air',
...          'units': 'K'},
...         match_all=False)

Field standard name starts with "air" and/or has units of exactly Kelvin:

>>> f.match({'standard_name': cf.eq('^air', regex=True),
...          'units': 'K'},
...         exact=True,
...         match_all=False)

Field has a height coordinate object with some values greater than
63km:

>>> f.match(cvalue={'height': cf.gt(63, 'km')})

Field has a height coordinate object with some values greater than
63km and a north polar point on its horizontal grid:

>>> f.match(cvalue={'height': cf.gt(63, 'km'),
...                 'latitude': cf.eq(90, 'degrees')})

Field has a height coordinate object with some values greater then
63km and/or a north polar point on its horizontal grid

>>> f.match(cvalue={'height': cf.gt(63, 'km'),
...                 'latitude': cf.eq(90, 'degrees')},
...         match_all=False)

Field longitude cell sizes are all 3.75:

>>> f.match(csize={'longitude': 3.75})

Field latitude cell sizes within 
a tropical region are all no greater
than 1 degree:

>>> f.match(cvalue={'latitude': cf.wi(-30, 30, 'degrees'),
...         csize={'latitude': cf.le(1, 'degrees')})

Field contains monthly mean air pressure data and all vertical levels
within the bottom 100 metres of the atmosphere have a thickness of 20
metres or less:

>>> f.match({None: cf.eq('air_pressure'),
...          'cell_methods': cf.CellMethods('time: mean')},
...         cvalue={'height': cf.le(100, 'm')},
...         csize={'height': cf.le(20, 'm'),
...                'time': cf.wi(28, 31, 'days')})

'''
        conditions_have_been_set = False
        something_has_matched    = False

        if rank is not None:
            conditions_have_been_set = True
            found_match = len(self.axes()) == rank
            if match_all and not found_match:
                return bool(inverse)

            something_has_matched = True
        #--- End: if

            
        if not match and isinstance(match, dict):
            match = None

        if match is not None:
            conditions_have_been_set = True
            
            if not isinstance(match, dict):
                match = {None: match}
            elif match:
                # ----------------------------------------------------
                # Try to match other cell methods
                # ----------------------------------------------------
                if 'cell_methods' in match:
                    f_cell_methods = self.getprop('cell_methods', None)
                    
                    if not f_cell_methods:
                        found_match = False
                    else:
                        match = match.copy()
                        cell_method = match.pop('cell_methods')
                        if not exact and len(cell_method) == 1:
                            found_match = f_cell_methods.has_cellmethod(cell_method)
                        else:
                            found_match = f_cell_methods.equivalent(cell_method)
                    #--- End: if                        
                        
                    if match_all and not found_match:
                        return bool(inverse)
                #--- End: if
                
                # ---------------------------------------------------
                # Try to match flags
                # ---------------------------------------------------
                if ('flag_masks' in match or 'flag_meanings' in match or
                    'flag_values' in match):
                    f_flags = getattr(self, Flags, None)
                    
                    if not f_flags:
                        found_match = False
                    else:   
                        match = match.copy()
                        found_match = f_flags.equals(
                            Flags(flag_masks=match.pop('flag_masks', None),
                                  flag_meanings=match.pop('flag_meanings', None),
                                  flag_values=match.pop('flag_values', None)))
                    #--- End: if
                
                    if match_all and not found_match:
                        return bool(inverse)
                #--- End: if
            #--- End: if
             
            # --------------------------------------------------------
            # Try to match other properties and attributes
            # --------------------------------------------------------
            found_match = super(Field, self).match(
                match=match, exact=exact, regex=regex,
                match_all=match_all, inverse=False)
            
            if match_all and not found_match:
                return bool(inverse)

            something_has_matched = found_match
        #--- End: if

        # ------------------------------------------------------------
        # Try to match coordinate values
        # ------------------------------------------------------------
        # Keep a note of coordinate matches to save us from having to
        # work them out again if there are cell size conditions
        coord_matches = {}

        if cvalue:
            conditions_have_been_set = True

            found_match = False
            # Some coordinate conditions have been been specified

            # Loop round coordinate criteria
            for identity, condition in cvalue.iteritems():
                c = self.items(identity, role=('d', 'a'), exact=exact,
                               regex=regex)
                if match_all and len(c) != 1:
                    # The field does not have a unique coordinate with
                    # this identity, so it does not match.
                    found_match = False
                    break

                if condition is None:
                    field_matches = True
                else:
                    key, c = c.popitem()
                    coord_match = (condition == c.Data)
                    coord_matches[key] = coord_match
                    field_matches = coord_match.any()
                #--- End: if
                
                if match_all:                    
                    if field_matches:
                        found_match = True 
                    else:
                        found_match = False
                        break
                elif field_matches:
                    found_match = True
                    break
            #--- End: for 

            if match_all and not found_match:
                return bool(inverse)

            something_has_matched = found_match
        #--- End: if

        # ------------------------------------------------------------
        # Try to match coordinate cell sizes
        # ------------------------------------------------------------
        if csize:
            # Some coordinate conditions have been been specified
            conditions_have_been_set = True
            found_match = False

            for identity, condition in csize.iteritems():
                c = self.items(identity, role=('d', 'a'), exact=exact,
                               regex=regex)
                if match_all and len(c) != 1:
                    # The field does not have a unique coordinate with
                    # this name, so it does not match.  
                    found_match = False
                    break

                if condition is None:
                    field_matches = True
                else:
                    key, c = c.popitem()
                    
                    if not c._hasbounds:
                        if condition == 0:
                            continue
                        else:
                            # The specified cell size is not zero but
                            # the coordinate does not have any bounds,
                            # so it does not match.
                            found_match = False
                            break
                    #--- End: if
                    
                    b = c.bounds.Data
                    cell_sizes = abs(b[...,1] - b[...,0])
                    cell_sizes.squeeze(-1, i=True)
                    
                    bounds_match = (cell_sizes == condition)
                    
                    if key in coord_matches: 
                        # Coordinate conditions have also been
                        # specified, so return False if any of the
                        # matching coordinate cells do not match the
                        # cell size condition.
                        coord_match = coord_matches[key]
                        field_matches = \
                            ((bounds_match & coord_match) == coord_match).all()
                    else:
                        # No coordinate conditions have been
                        # specified, so return False if any coordinate
                        # cells do not match the cell size condition.
                        # if not bounds_match.all(): return False
                        field_matches = bounds_match.all()
                #--- End: if

                if match_all:
                    if field_matches:
                        found_match = True 
                    else:
                        found_match = False
                        break
                elif field_matches:
                    found_match = True
                    break
            #--- End: for

            if match_all and not found_match:
                return bool(inverse)

            something_has_matched = True
        #--- End: if

        if conditions_have_been_set:
            if something_has_matched:            
                return not bool(inverse)
            else:
                return bool(inverse)
        else:
            return not bool(inverse)
    #--- End: def

    def flip(self, axes=None, i=False, **kwargs):
        '''

Flip (reverse the direction of) axes of the field.

:Parameters:

    axes, kwargs : *optional*
        Select the axes. The axes are those which would be selected by
        this call of the field's `axes` method: ``f.axes(axes,
        **kwargs)``. See `cf.Field.axes` for details.

    i : bool, optional
        If True then update the field in place. By default a new field
        is created.

:Returns:

    out : cf.Field
        
.. seealso:: `axes`, `expand_dims`, `squeeze`, `swapaxes`,
             `transpose`, `unsqueeze`

**Examples**

>>> f.flip()
>>> f.flip('time')
>>> f.flip(1)
>>> f.flip('dim2')
>>> f.flip(['time', 1, 'dim2'])

'''
        domain  = self.domain

        if axes is None and not kwargs:
            # Flip all the axes
            axes = domain.axes() #set(data_axes)
            iaxes = range(self.ndim)
        else:
            axes = domain.axes(axes, **kwargs)

            data_axes = domain.data_axes()
            iaxes = [data_axes.index(axis) for axis in
                     axes.intersection(data_axes)]
        #--- End: if

        # Flip the requested axes in the field's data array
        f = super(Field, self).flip(iaxes, i=i)

        # Flip any coordinate and cell measures which span the flipped
        # axes
        domain = f.domain
        domain_axes = domain.dimensions
        for key, item in domain.items(role=('d', 'a', 'c')).iteritems():
            item_axes = domain_axes[key]
            item_flip_axes = axes.intersection(item_axes)
            if item_flip_axes:
                iaxes = [item_axes.index(axis) for axis in item_flip_axes]
                item.flip(iaxes, i=True)
        #--- End: for

        return f
    #--- End: def

    def method(self, callable_method, *args, **kwargs):
        '''

Return the results of a callable method (with arguments) applied to
the field.

``f.method(callable_method, *args, **kwargs)`` is equivalent to
``f.callable_method(*args, **kwargs)``.

:Parameters:

    callable_method : str
        The name of the method to be called.

    args, kwargs : *optional*
         Any arguments accepted by the method given by the
         *callable_method* argument.

:Returns:

    out :
        The result of the application of the callable method.

**Examples**

>>> f.getprop('standard_name')
'air_temperature'
>>> f.method('getprop', 'standard_name')
'air_temperature'

>>> print f.method('squeeze')
None

'''
        return getattr(self, callable_method)(*args, **kwargs)
    #--- End: def

    def remove_data(self):
        '''

Remove and return the data array of the field.

:Returns: 

    out : cf.Data or None
        The removed data array, or None if there isn't one.

**Examples**

>>> f._hasData
True
>>> f.data
<CF Data: [0, ..., 9] m>
>>> f.remove_data()
>>> f._hasData
False

'''
        if not self._hasData:
            return

        self.domain.dimensions.pop('data', None)

        data = self.data
        del self.Data

        return data
    #--- End: def

    def select(self, match=None, cvalue={}, csize={}, rank=None, exact=False,
               match_all=True, regex=False, inverse=False):
        '''

Return the field if it satisfies the given conditions.

The conditions are defined in the same manner as for `cf.Field.match`,
which tests whether or not a field satisfies the given conditions.

``f.select(**kwargs)`` is equivalent to ``f if f.match(**kwargs) else
cf.FieldList()``.

:Parameters:

    match : *optional*
        Set conditions on field property values. See the *match*
        parameter of `cf.Field.match` for details.

    cvalue : dict, optional
        A dictionary which identifies field coordinate objects with
        corresponding tests on their data array values. See the
        *cvalue* parameter of `cf.Field.match` for details.
      
    csize :  dict, optional
        A dictionary which identifies field coordinate objects with
        corresponding tests on their cell sizes. See the *csize*
        parameter of `cf.Field.match` for details.

    rank : int or cf.Comparison, optional
        Specify a condition on field domain rank, where the domain
        rank is the number of domain axes. See the *rank* parameter of
        `cf.Field.match` for details.

    exact : bool, optional
        The *exact* argument applies to the interpretion of particular
        conditions given by values of the *match* argument and by keys
        of the *cvalue* and *csize* arguments. See the *exact*
        parameter of `cf.Field.match` for details.
     
    match_all : bool, optional
        The *match_all* argument applies to the interpretion of all
        conditions (properties, coordinate object values, coordinate
        object cell sizes, domain rank and data array rank). See the
        *match_all* parameter of `cf.Field.match` for details.

    regex : bool, optional
        The *regex* argument applies to the interpretion of
        string-valued conditions. See the *regex* parameter of
        `cf.Field.match` for details.

    inverse : bool, optional
        If True then return the field if it does not satisfy the given
        conditions. By default an empty field list is returned if it
        satisfies the given conditions.

:Returns:

    out : cf.Field or cf.FieldList
        If the field matches the given conditions then it is returned
        as an object identity. Otherwise an empty field list is
        returned.

.. seealso:: `match`

**Examples**

Select the field if has exactly four domain axes:

>>> f.select(rank=4)

See `cf.Field.match` for further examples.

'''
        if self.match(match=match, cvalue=cvalue, csize=csize, rank=rank,
                      exact=exact, match_all=match_all, regex=regex,
                      inverse=inverse):
            return self
        else:
            return FieldList()
    #--- End: def

    def anchor(self, axis, value, i=False, **kwargs):
        '''

:Parameters:

    axis : 

    axis, kwargs : *optional*

        If *axes* and/or *kwargs* are set then the new axis order is
        that which would be returned by this call of the field's
        `axes` method: ``f.axes(axes, ordered=True, **kwargs)``. See
        `cf.Field.axes` for details.

    i : bool, optional
        If True then update the field in place. By default a new field
        is created.

:Returns:

    out : cf.Field

.. seealso:: `roll`

**Examples**

''' 
        axis = self.domain.axis(axis, **kwargs)
        if axis is None:
            raise ValueError("Can't shift: Bad axis specification")

        if i:
            f = self
        else:
            f = self.copy()
        
        domain = f.domain

        try:
            iaxis = domain.data_axes().index(axis)
        except ValueError:
            return f

        if domain.axis_size(axis) <= 1:
            return f
        
        dim = domain.item(axis)
        if dim is None:
            raise ValueError("Can't shift non-cyclic axis")
        
        period = dim.period()

        if period is None:
            raise ValueError("Can't shift non-cyclic %r axis" % dim.name())

        value = Data.asdata(value)
        if not value.Units:
            value = value.override_units(dim.Units)
        elif not value.Units.equivalent(dim.Units):
            raise ValueError("saaaa dduuuuuuuu 8754 mnb \d ")

        d = dim.data

        centre = dim._centre(period)

        hasbounds = dim.hasbounds
        if hasbounds:
            bounds = dim.bounds

        while value <= dim._centre(period)-period:
            print 'A',  repr(value),repr(period), repr(dim._centre(period)), repr(dim._centre(period)-period)
            d += period            
            if hasbounds:
                bounds += period
        #--- End: while

        while value >= dim._centre(period)+period:
            d -= period
            if hasbounds:
                bounds -= period
        #--- End: while

        value = value.datum()
        a     = dim.array
        if value < a.min():
            n = 1 + (a.min()-value)//period
            value += n * period
        elif value > a.max():
            n = -((value-a.min())//period)
            value += n * period
        else:
            n = 0


        if index and n:
            dim += n * period

        return f.roll(iaxis, index, i=True)

#            while a.min() <= value <= a.min():
                

        print  value, 'a=', a
        if dim.direction():
            index = -numpy_argmax(a > value) + 1
        else:
            if value > 0:
                index = numpy_argmax(a < a[-1] + value)
            else:
                index = numpy_argmax(a < a[0] + value)
        print index
        return f.roll(iaxis, index, i=True)
    #--- End: def
    
    def setcyclic(self):
        '''
'''
        dims = self.dims('X')
        if len(dims) != 1:
            return

        key, dim = dims.popitem()

        if dim.getprop('standard_name', None) not in ('longitude', 'grid_longitude'):
            self.cyclic(key, iscyclic=False)
            return

        if not dim.hasbounds:
            self.cyclic(key, iscyclic=False)
            return
        
        bounds = dim.bounds
        if not bounds._hasData:
            self.cyclic(key, iscyclic=False)
            return 
        
        period = Data(360, 'degrees')
        if abs(bounds.datum(-1) - bounds.datum(0)) != period:
            self.cyclic(key, iscyclic=False)
            return

        self.cyclic(key, iscyclic=True, period=period)
    #--- End: def

    def sort(self, *args, **kwargs):
        '''

Sort of the field in place.

This method is null in that it always makes no changes to the
field. It exists to provide compatibility with `cf.FieldList` objects,
for which sorting the order of fields is a meaningful operation.

See `cf.FieldList.sort` for details.

:Parameters:

    args, kwargs: *optional*
       See `cf.FieldList.sort`.

:Returns:

    None

**Examples**

>>> g = f.copy()
>>> f.sort()
>>> f.equals(g)
True

'''
        return
    #--- End: def

#    def spans(self, identities, spans_all=False):
#        '''
#'''
#        s = self.domain.analyse()
#        
#        if not spans_all:
#            for identity in identities:
#                if identity in s['id_to_axis']:
#                    return True
#
#            return False
#        else:
#            for identity in identities:
#                if identity not in s['id_to_axis']:
#                    return False
#
#            return True
#    #--- End: def

    def squeeze(self, axes=None, i=False, **kwargs):
        '''

Remove size 1 axes from the data array.

By default all size 1 axes are removed from the field's data array,
but particular axes may be selected with the keyword arguments.

Squeezed axes are not squeezed from the domain's coordinate and cell
measure objects, nor are they removed from the field. To completely
remove axes, use the field's `remove_axes` method.

:Parameters:

    axes, kwargs : *optional*
        Select the axes. The axes are those which would be selected by
        this call of the field's `axes` method: ``f.axes(axes, size=1,
        **kwargs)``. See `cf.Field.axes` for details.

    i : bool, optional
        If True then update the field in place. By default a new field
        is created.

:Returns:

    out : cf.Field

.. seealso:: `axes`, `expand_dims`, `flip`, `remove_axes`, `swapaxes`,
             `transpose`, `unsqueeze`

**Examples**

>>> f.squeeze()
>>> f.squeeze('time')
>>> f.squeeze(1)
>>> f.squeeze('dim2')
>>> f.squeeze([1, 'time', 'dim2'])
>>> f.squeeze({'units': 'Pa'})

'''
        domain = self.domain
        data_axes = domain.data_axes() #dimensions['data']

        if axes is None and not kwargs:
            axes_sizes = domain.dimension_sizes
            axes = [axis for axis in data_axes if axes_sizes[axis] == 1]
        else:
            axes = domain.axes(axes, **kwargs).intersection(data_axes)

        if not axes:
            return
 
        iaxes = [data_axes.index(axis) for axis in axes]      

        # Squeeze the field's data array
        f = super(Field, self).squeeze(iaxes, i=i)

        f.domain.dimensions['data'] = [axis for axis in data_axes
                                       if axis not in axes]

        return f
    #--- End: def

    def transpose(self, axes=None, i=False, **kwargs):
        '''

Permute the axes of the data array in place.

:Parameters:

    axes, kwargs : *optional*
        Set the new axis order. By default the existing axis order is
        reversed.

        If *axes* and/or *kwargs* are set then the new axis order is
        that which would be returned by this call of the field's
        `axes` method: ``f.axes(axes, ordered=True, **kwargs)``. See
        `cf.Field.axes` for details.

    i : bool, optional
        If True then update the field in place. By default a new field
        is created.

:Returns:

    out : cf.Field

.. seealso:: `axes`, `expand_dims`, `flip`, `squeeze`, `swapaxes`,
             `unsqueeze`

**Examples**

>>>f.items()
{'dim0': <CF DimensionCoordinate: time(12) noleap>,
 'dim1': <CF DimensionCoordinate: latitude(64) degrees_north>,
 'dim2': <CF DimensionCoordinate: longitude(128) degrees_east>,
 'dim3': <CF DimensionCoordinate: height(1) m>}
>>> f.data_axes()
['dim0', 'dim1', 'dim2']
>>> f.transpose()
>>> f.transpose(['latitude', 'time', 'longitude'])
>>> f.transpose([1, 0, 2])
>>> f.transpose((1, 'time', 'dim2'))

''' 
        domain = self.domain
        data_axes = domain.data_axes() #dimensions['data']

        if axes is None and not kwargs:
            axes = data_axes[::-1]
            iaxes = range(self.ndim-1, -1, -1)
        else:
            axes = domain.axes(axes, ordered=True, **kwargs)
            if set(axes) != set(data_axes):
                raise ValueError("Can't transpose: Bad axis specification")
            
            iaxes = [data_axes.index(axis) for axis in axes]
        #---- End: if
            
        # Transpose the field's data array
        f = super(Field, self).transpose(iaxes, i=i)

        # Reorder the list of axes in the domain
        f.domain.dimensions['data'] = axes

        return f
    #--- End: def

    def unsqueeze(self, axes=None, i=False, **kwargs):
        '''

Insert size 1 axes into the data array of the field.

By default all size 1 axes of the domain which are not spanned by the
field's data array are inserted, but particular axes may be selected
with the keyword arguments.

The axes are inserted into the slowest varying data array positions.

:Parameters:

    axes, kwargs : *optional*
        Select the axes. The axes are those which would be selected by
        this call of the field's `axes` method: ``f.axes(axes, size=1,
        **kwargs)``. See `cf.Field.axes` for details.

    i : bool, optional
        If True then update the field in place. By default a new field
        is created.

:Returns:

    out : cf.Field

.. seealso:: `axes`, `expand_dims`, `flip`, `squeeze`, `swapaxes`,
             `transpose`,

**Examples**

>>> print f
Data            : air_temperature(time, latitude, longitude)
Cell methods    : time: mean
Dimensions      : time(1) = [15] days since 1860-1-1
                : latitude(73) = [-90, ..., 90] degrees_north
                : longitude(96) = [0, ..., 356.25] degrees_east
                : height(1) = [2] m
Auxiliary coords:
>>> f.unsqueeze()
>>> print f
Data            : air_temperature(height, time, latitude, longitude)
Cell methods    : time: mean
Dimensions      : time(1) = [15] days since 1860-1-1
                : latitude(73) = [-90, ..., 90] degrees_north
                : longitude(96) = [0, ..., 356.25] degrees_east
                : height(1) = [2] m
Auxiliary coords:

'''
        domain = self.domain

        data_axes = domain.data_axes() #dimensions['data']
        axes = domain.axes(axes, size=1, **kwargs).difference(data_axes)

        if i:
            f = self
        else:
            f = self.copy()

        for axis in axes:
            f.expand_dims(0, axis, i=True)

        return f
    #--- End: def

    def aux(self, item=None, key=False, **kwargs):
        '''

Return an auxiliary coordinate object of the domain, or its domain
identifier.

``f.aux(item, key=key, **kwargs)`` is equivalent to ``f.item(item,
key=key, role='a', **kwargs)``. See `cf.Field.item` for details.

.. seealso:: `auxs`, `cm`, `coord`, `dim`, `item`, `remove_aux`,
             `transform`

**Examples**

'''
        kwargs['role'] = 'a'
        return self.domain.item(item, key=key, **kwargs) 
    #--- End: def

    def cm(self, item=None, key=False, **kwargs):
        '''

Return a cell measure object of the domain, or its domain identifier.

``f.cm(item, key=key, **kwargs)`` is equivalent to ``f.item(item,
key=key, role='c', **kwargs)``. See `cf.Field.item` for details.

.. seealso:: `aux`, `cms`, `coord`, `dim`, `item`, `remove_cm`,
             `transform`

**Examples**

'''
        kwargs['role'] = 'c'
        return self.domain.item(item, key=key, **kwargs)
    #--- End: def
    
 #   @classmethod
 #   def concatenate(cls, fields, axis=0, **kwargs):
 #       '''
 #       '''
 #       new = fields[0].copy(_omit_Data=True)
 #
 #       axis = self.domain.axis(axis, **kwargs)
 #       if axis is None:
 #           raise ValueError("3458567")
 #           
 #       try:
 #           iaxis = self.data_axes().index(axis)
 #       except ValueError:
 #           raise ValueError("asd 356 '\x {}[ah i876ibq %*")
 #
 #       new.Data = Data.concatenate(data, axis=iaxis)
 #
 #       # 
 #       for key, item in new.domain.items(axes=iaxis).iteritems():
 #           item.Data = Data.concatenate(data, axis=new.item_axes(key).index(axis))
 #          
 #       return new
 #   #--- End: def
        

    def coord(self, item=None, key=False, **kwargs):
        '''

Return a dimension or auxiliary coordinate object of the domain, or
its domain identifier.

``f.coord(item, key=key, **kwargs)`` is equivalent to ``f.item(item,
key=key, role='da', **kwargs)``. See `cf.Field.item` for details.

.. seealso:: `aux`, `cm`, `coords`, `dim`, `item`, `remove_coord`,
             `transform`

**Examples**

'''
        kwargs['role'] = 'da'
        return self.domain.item(item, key=key, **kwargs) 
    #--- End: def

    def dim(self, item=None, key=False, **kwargs):
        '''

Return a dimension coordinate object of the domain, or its domain
identifier.

``f.dim(item, key=key, **kwargs)`` is equivalent to ``f.item(item,
key=key, role='d', **kwargs)``. See `cf.Field.item` for details.

.. seealso:: `aux`, `cm`, `coord`, `dims`, `item`, `transform`

**Examples**

'''
        kwargs['role'] = 'd'
        return self.domain.item(item, key=key, **kwargs) 
    #--- End: def

    def transform(self, item=None, key=False, **kwargs):
        '''

Return a transform object of the domain, or its domain identifier.

``f.transform(item, key=key, **kwargs)`` is equivalent to
``f.item(item, key=key, role='t', **kwargs)``. See `cf.Field.item` for
details.

.. seealso:: `aux`, `cm`, `coord`, `dim`, `item`, `remove_transform`,
             `transforms`

**Examples**

'''
        kwargs['role'] = 't'
        return self.domain.item(item, key=key, **kwargs)
    #--- End: def

    def auxs(self, items=None, **kwargs):
        '''

Return auxiliary coordinate objects of the domain.

``f.auxs(items, **kwargs)`` is equivalent to ``f.items(items,
role='a', **kwargs)``. See `cf.Field.items` for details.

.. seealso:: `aux`, `axes`, `cms`, `coords`, `dims`, `items`,
             `remove_auxs`, `transforms`

**Examples**

'''
        kwargs['role'] = 'a'
        return self.domain.items(items, **kwargs)
    #--- End: def

    def cms(self, items=None, **kwargs):
        '''

Return cell measure objects of the domain.

``f.cms(items, **kwargs)`` is equivalent to ``f.items(items, role='c',
**kwargs)``. See `cf.Field.items` for details.

.. seealso:: `auxs`, `axes`, `cm`, `coords`, `dims`, `items`,
             `remove_cms`, `transforms`

**Examples**

'''
        kwargs['role'] = 'c'
        return self.domain.items(items, **kwargs)
    #--- End: def

    def coords(self, items=None, **kwargs):
        '''

Return dimension and auxiliary coordinate objects of the domain.

``f.coords(items, **kwargs)`` is equivalent to ``f.items(items,
role='da', **kwargs)``. See `cf.Field.items` for details.

.. seealso:: `auxs`, `axes`, `cms`, `coord`, `dims`, `items`,
             `remove_coords`, `transforms`

**Examples**

'''
        kwargs['role'] = 'da'
        return self.domain.items(items, **kwargs)
    #--- End: def

    def dims(self, items=None, **kwargs):
        '''

Return dimension coordinate objects of the domain.

``f.dims(items, **kwargs)`` is equivalent to ``f.items(items,
role='d', **kwargs)``.

See `cf.Field.items` for details.

.. seealso:: `auxs`, `axes`, `cms`, `coords`, `dim`, `items`,
             `remove_dims`, `transforms`

**Examples**

'''
        kwargs['role'] = 'd'
        return self.domain.items(items, **kwargs)
    #--- End: def

    def transforms(self, items=None, **kwargs):
        '''

Return transforms of the domain.

``f.transforms(items, **kwargs)`` is equivalent to ``f.items(items,
role='t', **kwargs)``. See `cf.Field.items` for details.

.. seealso:: `auxs`, `axes`, `cms`, `coords`, `dims`, `items`,
             `remove_transforms`, `transform`

**Examples**

'''
        kwargs['role'] = 't'
        return self.domain.items(items, **kwargs)
    #--- End: def

    def item(self, items=None, role=None, axes=None, ctype=None, exact=False,
             inverse=False, match_all=True, rank=None, regex=False,
             strict_axes=False, key=False):
        '''

Return an item, or its domain identifier, from the field.

An item is either a dimension coordinate, an auxiliary coordinate, a
cell measure or a transform object.

The item may be selected with the keyword arguments. When multiple
criteria are given, the item will be the intersection of the
selections. If no unique item can be found then None is returned.

A returned item is not a copy, so in-place changes to it are stored in
the domain.

Note that::

   k = f.item(items, key=True, **args)
   v = f.item(items, key=False, **args)

is equivalent to::

   d = f.items(items, **args)
   if len(d) == 1:
      k, v = d.popitem()
   else:
      k, v = None, None

:Parameters:

    items : *optional*
        Select items whose properties satisfy the given conditions.
        Set as for the *items* parameter of `cf.Field.items`.

    role : (sequence of) str, optional
        Select items of the given roles. Set as for the *role*
        parameter of `cf.Field.items`.

    ctype : (sequence of) str, optional
        Select dimension and auxiliary coordinate object items of the
        given CF types. Set as for the *ctype* parameter of
        `cf.Field.items`.

    axes : *optional*
        Select items which span at least one of the specified axes,
        taken in any order (as well as possibly spanning other,
        unspecified axes). Set as for the *axes* parameter of
        `cf.Field.items`.

    rank : int or cf.Comparison, optional
        Select items whose ranks satisfy the given condition, where an
        item's rank is the number of axes which it spans. Set as for
        the *rank* parameter of `cf.Field.items`.

    exact : bool, optional
        The *exact* argument applies to the interpretion of particular
        conditions given by values of the *items* argument. Set as for
        the *exact* parameter of `cf.Field.items`.
       
    inverse : bool, optional
        If True then select items other than those selected by all
        other criteria.

    match_all : bool, optional
        The *match_all* argument applies to the interpretion of
        dictionaries given by the *items* argument. By default
        *match_all* is True and items are selected if they satisfy all
        of the conditions specified by a dictionary of the *items*
        argmuent.

        If *match_all* is False then items are selected if they
        satisfy at least one of the specified by a dictionary of the
        *items* argmuent.

          Example: To select items with a `!standard_name` containing
          "temp" and/or `!units` of temperature you could set
          ``items={'standard_name': cf.eq('temp', regex=True),
          'units': 'K'}, match_all=False``. If *match_all* were True
          in this case then the field would match only if it satisfied
          both conditions.

    regex : bool, optional
        By default *regex* is False and all strings given by values of
        the *items* argument are considered to be arbitrary
        abbreviations or else exact (see the *exact* argument).

        If *regex* is True then such strings are considered to be
        regular expressions supported by the :py:obj:`re` module and
        are compared via the :py:obj:`re.search` function (see the
        *exact* argument).

          Example: To select items with a `!long_name` which starts
          with "a" and ends with "z" you could set
          ``items={'long_name': '^a.*z$'}, regex=True``.

        Note that `cf.Comparison` objects provide a mechanism for
        overriding *regex* for individual values.

          Example: ``items={'long_name': cf.eq('^a.*z$',
          regex=True)}`` will select items a `!long_name` which starts
          with "a" and ends with "z" regardless of the value of
          *regex*.

    strict_axes : bool, optional
        The *strict_axes* argument applies to the interpretion of
        *strict_axes* argument. Set as for the *axes* parameter of
        `cf.Field.items`.

    key : bool, optional
        If True then return the domain's identifier for the item,
        rather than the item itself.

:Returns:

    out : 

        The unique item or its domain identifier or, if there is no
        unique item, None.

.. seealso:: `aux`, `cm`, `coord`, `dim`, `item_axes`, `items`,
             `remove_item`, `transform`

**Examples**

'''
        return self.domain.item(item=items, role=role, axes=axes, ctype=ctype,
                                exact=exact, inverse=inverse,
                                match_all=match_all, rank=rank, regex=regex,
                                strict_axes=strict_axes, key=key)
    #--- End: def
    
    def axis_name(self, axis=None, **kwargs):
        '''

Return the canonical name for an axis.

:Parameters:

    axis, kwargs : optional
        Select the axis which would be selected by this call of the
        field's `~cf.Field.axis` method: ``f.axis(axis,
        **kwargs)``. See `cf.Field.axis` for details.

:Returns:

    out : str
        The canonical name for the axis.

**Examples**

>>> f.axis_name('dim0')
'time'
>>> f.axis_name('dim1')
'domain:dim1'
>>> f.axis_name('dim2')
'ncdim:lat'

'''        
        return self.domain.axis_name(axis, **kwargs)
    #-- End: def

    def axis_size(self, axis=None, size=None, **kwargs):
        '''
'''
        return self.domain.axis_size(axis=axis, size=size, **kwargs)

    def axes_sizes(self, axes=None, size=None, **kwargs):
        '''
'''
        return self.domain.axes_sizes(axes=axes, size=size, **kwargs)

        
        

    def axes(self, axes=None, ordered=False, size=None, **kwargs):
        '''

Return domain axis identifiers from the field.

The output is a set of domain axis identifiers, which may be empty.

By default all axes of the domain are returned, but particular axes
may be selected with the keyword arguments. There are two types of
selection:

==============  ======================================================
Selection type  Method
==============  ======================================================
Explicit        Select axes by domain axis identifiers, positions in
                the list of the field's data array axes or axis size.

Implicit        Select axes which are spanned by particular items of
                the domain. An item is a dimension coordinate, an
                auxiliary coordinate, a cell measure or a transform
                object.
==============  ======================================================

When multiple criteria have been specified, the returned items are the
intersection of the selections.

:Parameters:

    axes : *optional*
        Select axes. The *axes* parameter may be one, or a sequence,
        of:

          * ``None``. If there no *kwargs* arguments have been set
            then selects all axes.
        ..

          * A domain axis identifier (such as ``'dim1'``). Explicitly
            selects this axis.
        ..

          * An integer or :py:obj:`slice` object. Explcitly selects
            the axes coresponding to the given positions in the list
            of axes of the field's data array.
        ..

          * Any value accepted by the *items* argument of the field's
            `items` method. Used in conjunction with the *kwargs*
            arguments to implicitly select the axes which span the
            items that would be identified by this call of the field's
            `items` method: ``f.items(items=axes, axes=None,
            **kwargs)``. See `cf.Field.items` for details.

        If *axes* is a sequence of any combination of the above then
        the selected axes are the union of those selected by each
        element of the sequence.

          Example:

          >>> x = f.axes(['dim2', 'time', {'units': 'degree_north'}])
          >>> y = set()
          >>> for axes in ['dim2', 'time', {'units': 'degree_north'}]:
          ...     y.update(f.axes(axes))
          ...
          >>> x == y
          True

        If the sequence is empty then no axes are selected.
 
    ordered : bool, optional
        Return an ordered list of axes instead of an unordered
        set. The order of the list will reflect any ordering specified
        by the selection keywords *axes* and *kwargs*.

          Example: If the data array axes, as returned by the field's
          `data_axes` method, are ``['dim0', 'dim1', 'dim2']``, then
          ``f.axes([2, 0, 1, 2])`` will return ``set(['dim0', 'dim1',
          'dim2'])``, but ``f.axes([2, 0, 1, 2], ordered=True)`` will
          return ``['dim2', 'dim0', 'dim1', 'dim2']``.

    size : int or cf.Comparison, optional
        Select axes whose sizes equal *size*. Axes with a range of
        sizes may be selected if *size* is a `cf.Comparison` object.

          Example: ``size=1`` selects size 1 axes and
          ``size=cf.ge(2)`` selects axes with sizes greater than 1.

    kwargs : *optional*
        For each implicit selection of the *axes* argument, any extra
        keyword arguments are used to modify the item selection.

          Example: ``f.axes('grid_latitude', exact=True, rank=1)``
          will select the axes spanned by all one dimensionsal items
          whose identities are exactly "grid_latitude".

        For each explicit selection of the *axes* argument, *kwargs*
        are ignored.

          Example: ``f.axes(-1, ctype='T', rank=1)`` will select only
          fastest varying axis of the field's data array, the keyword
          arguments being ignored in this case.

        If *axes* has not been set then any extra keyword arguments
        are used to implicitly select the axes which span the items
        that would be identified by this call of the field's `items`
        method: ``f.items(items=None, axes=None, **kwargs)``. See
        `cf.Field.items` for details. For example:

          Example: ``f.axes(ctype='T', rank=1)`` will select the axes
          spanned by one dimensionsal CF time coordinate objects.

:Returns:

    out : set or list
        A set of domain axis identifiers, or a list if *ordered* is
        True. The set or list may be empty.

.. seealso:: `data_axes`, `item_axes`, `items`, `remove_axes`

**Examples**

All axes and their identities:

>>> f.axes()
set(['dim0', 'dim1', 'dim2', 'dim3'])
>>> dict([(axis, f.domain.axis_name(axis)) for axis in f.axes()])
{'dim0': time(12)
 'dim1': height(19)
 'dim2': latitude(73)
 'dim3': longitude(96)}

Axes which are not spanned by the data array:

>>> f.axes().difference(f.data_axes())

'''
        return self.domain.axes(axes, size=size, ordered=ordered, **kwargs)
    #--- End: def

    def insert_axis(self, size, key=None, replace=True):
        '''

Insert an axis into the domain in place.

:Parameters:

    size : int
        The size of the new axis.

    key : str, optional
        The domain identifier for the new axis. By default a new,
        unique idenfier is generated.
  
    replace : bool, optional
        If False then do not replace an existing axis with the same
        identifier but a different size. By default an existing axis
        with the same identifier is changed to have the new size.

:Returns:

    out :
        The domain identifier of the new axis.

.. seealso:: `insert_aux`, `insert_cm`, `insert_data`, `insert_dim`,
             `insert_transform`

**Examples**

>>> f.insert_axis(1)
>>> f.insert_axis(90, key='dim4')
>>> f.insert_axis(23, key='dim0', replace=False)

'''
        return self.domain.insert_axis(size, key=key, replace=replace)
    #--- End: def

    def insert_aux(self, item, key=None, axes=None, copy=True, replace=True):
        '''

Insert an auxiliary coordinate object into the domain in place.

:Parameters:

    item : cf.AuxiliaryCoordinate or cf.Coordinate or cf.DimensionCoordinate
        The new auxiliary coordinate object. If it is not already a
        auxiliary coordinate object then it will be converted to one.

    key : str, optional
        The domain identifier for the *item*. By default a new, unique
        identifier will be generated.

    axes : sequence of str, optional
        The ordered list of axes for the *item*. Each axis is given by
        its domain identifier. By default the axes are assumed to be
        ``'dim0'`` up to ``'dimM-1'``, where ``M-1`` is the number of
        axes spanned by the *item*.

    copy: bool, optional
        If False then the *item* is not copied before insertion. By
        default it is copied.
      
    replace : bool, optional
        If False then do not replace an existing auxiliary coordinate
        object of domain which has the same identifier. By default an
        existing auxiliary coordinate object with the same identifier
        is replaced with *item*.
    
:Returns:

    out : 
        The domain identifier for the inserted *item*.

.. seealso:: `insert_axis`, `insert_cm`, `insert_data`, `insert_dim`,
             `insert_transform`

**Examples**

>>>

'''
        return self.domain.insert_aux(item, key=key, axes=axes, copy=copy,
                                      replace=replace)
    #--- End: def

    def insert_cm(self, item, key=None, axes=None, copy=True, replace=True):
        '''

Insert an cell measure object into the domain in place.

:Parameters:

    item : cf.CellMeasure
        The new cell measure object.

    key : str, optional
        The domain identifier for the *item*. By default a new, unique
        identifier will be generated.

    axes : sequence of str, optional
        The ordered list of axes for the *item*. Each axis is given by
        its domain identifier. By default the axes are assumed to be
        ``'dim0'`` up to ``'dimM-1'``, where ``M-1`` is the number of
        axes spanned by the *item*.

    copy: bool, optional
        If False then the *item* is not copied before insertion. By
        default it is copied.
      
    replace : bool, optional
        If False then do not replace an existing cell measure object
        of domain which has the same identifier. By default an
        existing cell measure object with the same identifier is
        replaced with *item*.
    
:Returns:

    out : 
        The domain identifier for the *item*.

.. seealso:: `insert_axis`, `insert_aux`, `insert_data`, `insert_dim`,
             `insert_transform`

**Examples**

>>>

'''
        return self.domain.insert_cm(item, key=key, axes=axes, copy=copy,
                                     replace=replace)
    #--- End: def

    def insert_dim(self, item, key=None, copy=True, replace=True):
        '''

Insert a dimension coordinate object into the domain in place.

:Parameters:

    item : cf.DimensionCoordinate or cf.Coordinate or cf.AuxiliaryCoordinate
        The new dimension coordinate object. If it is not already a
        dimension coordinate object then it will be converted to one.

    key : str, optional
        The domain identifier for the *item*. By default a new, unique
        identifier will be generated.

    copy: bool, optional
        If False then the *item* is not copied before insertion. By
        default it is copied.
      
    replace : bool, optional
        If False then do not replace an existing dimension coordinate
        object of domain which has the same identifier. By default an
        existing dimension coordinate object with the same identifier
        is replaced with *item*.
    
:Returns:

    out : 
        The domain identifier for the inserted *item*.

.. seealso:: `insert_aux`, `insert_axis`, `insert_cm`, `insert_data`,
             `insert_transform`

**Examples**

>>>

'''
        key = self.domain.insert_dim(item, key=key, copy=copy, replace=replace)
        self.setcyclic()
        return key
    #--- End: def

    def insert_transform(self, item, key=None, copy=True, replace=True):
        '''

Insert a transform object into the domain in place.

:Parameters:

    item : cf.Transform
        The new transform object.

    key : str, optional
        The domain identifier for the *item*. By default a new, unique
        identifier will be generated.

    copy: bool, optional
        If False then the *item* is not copied before insertion. By
        default it is copied.
      
    replace : bool, optional
        If False then do not replace an existing transform object of
        domain which has the same identifier. By default an existing
        transform object with the same identifier is replaced with
        *item*.
    
:Returns:

    out : 
        The domain identifier for the *item*.

.. seealso:: `insert_axis`, `insert_aux`, `insert_cm`, `insert_data`,
             `insert_dim`
             

**Examples**

>>>

'''
        return self.domain.insert_transform(item, key=key, copy=copy,
                                            replace=replace) 
    #--- End: def

    def item_axes(self, item=None, **kwargs):
        '''

Return the axes of a domain item of the field.

An item is a dimension coordinate, an auxiliary coordinate, a cell
measure or a transform object.

:Parameters:

    item, kwargs : *optional*
         Select the item which would be selected by this call of the
         field's `item` method: ``d.item(item, **kwargs)``. See
         `cf.Field.item` for details.

:Returns:

    out : list or None
        The ordered list of axes for the item or, if there is no
        unique item or the item is a transform, then None is returned.

.. seealso:: `axes`, `data_axes`, `item`

**Examples**

'''    
        return self.domain.item_axes(item, **kwargs)
    #--- End: def

    def items(self, items=None, role=None, axes=None, ctype=None, exact=False,
              inverse=False, match_all=True, rank=None, regex=False,
              strict_axes=False):
        '''

Return domain items from the field.

An item is a dimension coordinate, an auxiliary coordinate, a cell
measure or a transform object.

The output is a dictionary whose key/value pairs are domain
identifiers with corresponding values of items of the domain.

By default all items of the domain are returned, but particular items
may be selected with the keyword arguments:

=======  =============================================================
Keyword  Selection
=======  =============================================================
*items*  Items whose properties satisfy given conditions
         
*role*   Items with particular roles
         
*axes*   Items which span particular axes
         
*ctype*  Coordinate object items of particular types
         
*rank*   Items with particular ranks
=======  =============================================================

When multiple keyword arguments are given, the returned items are the
intersection of the selections.

The returned items are not copies, so in-place changes to them are
stored in the field's domain.

**Quick start examples**

There is great flexibility in the types of item selection which can be
specified, and as a result the documentation is very detailed. These
preliminary, simple examples show that the usage need not always be
complicated and may help with understanding the keyword descriptions.

1. Select all items whose identities (as returned by their `!identity`
   methods) start "height":

   >>> f.items('height')

2. Select all items which span only one axis:

   >>> f.items(rank=1)

3. Select all cell measure objects:

   >>> f.items(role='c')

4. Select all items which span the "time" axis:

   >>> f.items(axes='time')

5. Select all CF latitude coordinate objects:

   >>> f.items(ctype='Y')

6. Select all multidimensional dimension and auxiliary coordinate
   objects which span at least the "time" and/or "height" axes and
   whose long names contain the string "qwerty":

   >>> f.items({'long_name': cf.eq('qwerty', regex=True)}, 
   ...         role='da',
   ...         axes=['time', 'height'],
   ...         rank=cf.ge(2))

Further examples are given within and after the description of the
arguments.

:Parameters:

    items : *optional*
        Select items whose properties satisfy the given
        conditions. The *items* argument may be one, or a sequence,
        of:

          * ``None`` or an empty dictionary. All items are
            selected. This is the default.
        ..

          * A domain item identifier (such as ``'dim1'``, ``'aux0'``,
            ``'cm2'``, ``'trans0'``, etc.). Selects the corresponding
            item.
        ..

          * A string specifiying a CF coordinate type. Any one of the
            types allowed by the *ctype* argument is allowed.
          
              Example: To select a latitude coordinate object you
              could set ``'Y'``.
        ..

          * A string or `cf.Comparison` object. Selects items whose
            identities, as returned by their `!identity` methods,
            match the value. If it is a string then the usual rules
            for string comparisons apply (see the *exact* and *regex*
            arguments for details, e.g. by default a string value is
            tested for being an abbreviation). 

              Example: To select items with an identity starting with
              "grid" you could set ``items='grid'``.

            This case may be specified equivalently in a dictionary
            with the special key of ``None``. 

              Example: ``items='time'`` is equivalent to
              ``items={None: 'time'}``.
        ..

          * A dictionary which identifies properties of the items with
            corresponding tests on their values. By default (see the
            *match_all* argument) selects items which satisfy all of
            the conditions. Each key/value pair is specified as
            follows:

              ======  ================================================
              key:    An unabbreviated property name (such as
                      ``'long_name'``, ``'units'``, ``'foo'``, etc).
                      
              value:  A condition to be tested against each item's
                      property for, in general, equality.

                      The exceptions are for string valued conditions
                      (see the *exact* and *regex* arguments for
                      details, e.g. by default a string value is
                      tested for being an abbreviation) and for values
                      of some special keys (see the following table).
              ======  ================================================

            The dictionary keys whose values have special interpretations are:

              ==============  ========================================
              Special key     Value
              ==============  ========================================
              ``None``        The value is compared (with the usual
                              rules) against each item's identity, as
                              returned by its `!identity` method.
                	
              ``'units'``     The value must be a string and, by
                              default, is evaluated for equivalence
                              with items' `!units` properties. See the
                              *exact* argument.

              ``'calendar'``  The value must be a string and, by
                              default, is evaluated for equivalence
                              with items' `!calendar` properties. See
                              the *exact* argument.
              ==============  ========================================

              Example: To select items with a `!standard_name`
              starting with "air" and with `!units` of temperature you
              could set ``items={'standard_name': 'air', 'units':
              'K'}``.

        If *items* is a sequence of any combination of the above then
        the selected items are the union of those selected by each
        element of the sequence.

          Example: 

          >>> x = f.items(['aux1', 'time', {'units': 'degreeN'}, {'units': 'K'}])
          >>> y = {}
          >>> for items in ['aux1', 'time', {'units': 'degreeN'}, {'units': 'K'}]:
          ...     y.update(f.items(items))
          ...
          >>> set(x) == set(y)
          True

        If the sequence is empty then no items are selected.
 
    role : (sequence of) str, optional
        Select items of the given roles. Valid roles are:

           =======  ==================================================
           Role     Selected items
           =======  ==================================================
           ``'a'``  Auxiliary coordinate objects
           ``'c'``  Cell measure objects
           ``'d'``  Dimension coordinate objects
           ``'t'``  Transform objects
           =======  ==================================================

        Multiple roles may be specified by a multi-character string or
        a sequence.

          Example: Selecting auxiliary coordinate and cell measure
          objects may be done with any of the following values of
          *role*: ``'ac'``, ``'ca'``, ``('a', 'c')``, ``['c', 'a']``,
          ``set(['a', 'c'])``, etc.

    ctype : (sequence of) str, optional
        Select dimension and auxiliary coordinate object items of the
        given CF types. Valid types, as defined by the CF conventions,
        are:

           =======  ==================================================
           Type     Selected items
           =======  ==================================================
           ``'T'``  Time coordinate objects
           ``'X'``  Longitude coordinate objects
           ``'Y'``  Latitude coordinate objects
           ``'Z'``  Vertical coordinate objects
           =======  ==================================================

        Multiple types may be specified by a multi-character string or
        a sequence.

          Example: Selecting time and longitude coordinate objects may
          be done with any of the following values of *ctype*:
          ``'XT'``, ``'TX'``, ``('X', 'T')``, ``['T', 'X']``,
          ``set(['X', 'T'])``, etc.

    axes : *optional*
        Select items which span at least one of the specified axes,
        taken in any order (as well as possibly spanning other,
        unspecified axes). The axes are those that would be selected
        by this call of the field's `axes` method:
        ``f.axes(axes)``. See `cf.Field.axes` for details. The
        *strict_axes* argument modifies this behaviour.

        Note that more complex specifications of axes are possible,
        since the *axes* argument may take as its value any output of
        the field's `axes` method.

          Example: To specify the axes spanned by one dimensionsal CF
          time coordinate objects you could set
          ``axes=f.axes(ctype='T', rank=1)``.

    rank : int or cf.Comparison, optional
        Select items whose ranks satisfy the given condition, where an
        item's rank is the number of axes which it spans. If *rank* is
        an integer then only items with this rank are selected. Items
        with a range of ranks may be selected if *rank* is a
        `cf.Comparison` object.

          Example: ``rank=1`` selects items which span exactly one
          axis and ``rank=cf.ge(2)`` selects items which span two or
          more axes.

    exact : bool, optional
        The *exact* argument applies to the interpretion of particular
        conditions given by values of the *items* argument. By default
        *exact* is False, which means that:

          * If the *regex* argument is False (which is the default)
            then string values are considered to be arbitrary
            abbreviations.

          * If the *regex* argument is True then string values are
            considered to be regular expressions, which are evaluated
            using the :py:obj:`re.search` function.

          * `!units` and `!calendar` values are evaluated for
            equivalence rather then equality (e.g. ``'metre'`` is
            equivalent to ``'m'`` and to ``'km'``).
        ..

          Example: To select items with a `!standard_name` starting
          with "air_pressure" and `!units` of pressure you could set
          ``items={'standard_name': 'air_pressure', 'units': 'hPa'},
          exact=False``.
 
        If *exact* is True then:

          * All string values are considered to be unabbreviated and
            not to be regular expressions (even if the *regex*
            argument is True).

          * `!units` and `!calendar` values are evaluated for exact
            equality rather than equivalence (e.g. ``'metre'`` is
            equal to ``'m'``, but not to ``'km'``).
        ..

          Example: To select items with a `!standard_name` of exactly
          "air_pressure" and `!units` of exactly hectopascals you
          could set ``items={'standard_name': 'air_pressure', 'units':
          'hPa'}, exact=True``.

        Note that `cf.Comparison` objects provide a mechanism for
        overriding ``exact=False`` for individual values.

          Example: ``items={None: cf.eq('grid_latitude'), 'units':
          'degrees'}`` will select items with an identity of exactly
          "grid_latitude" and any `!units` equivalent to degrees.
       
    inverse : bool, optional
        If True then select items other than those selected by all
        other criteria.

          Example: ``f.items(role='da', inverse=True)`` selects the
          same items as ``f.items(role='ct')``.

    match_all : bool, optional
        The *match_all* argument applies to the interpretion of
        dictionaries given by the *items* argument. By default
        *match_all* is True and items are selected if they satisfy all
        of the conditions specified by a dictionary of the *items*
        argmuent.

        If *match_all* is False then items are selected if they
        satisfy at least one of the specified by a dictionary of the
        *items* argmuent.

          Example: To select items with a `!standard_name` containing
          "temp" and/or `!units` of temperature you could set
          ``items={'standard_name': cf.eq('temp', regex=True),
          'units': 'K'}, match_all=False``. If *match_all* were True
          in this case then the field would match only if it satisfied
          both conditions.

    regex : bool, optional
        By default *regex* is False and all strings given by values of
        the *items* argument are considered to be arbitrary
        abbreviations or else exact (see the *exact* argument).

        If *regex* is True then such strings are considered to be
        regular expressions supported by the :py:obj:`re` module and
        are compared via the :py:obj:`re.search` function (see the
        *exact* argument).

          Example: To select items with a `!long_name` which starts
          with "a" and ends with "z" you could set
          ``items={'long_name': '^a.*z$'}, regex=True``.

        Note that `cf.Comparison` objects provide a mechanism for
        overriding *regex* for individual values.

          Example: ``items={'long_name': cf.eq('^a.*z$',
          regex=True)}`` will select items a `!long_name` which starts
          with "a" and ends with "z" regardless of the value of
          *regex*.

    strict_axes : bool, optional
        The *strict_axes* argument applies to the interpretion of
        *axes* argument. By default *strict_axes* is False and items
        are selected which span at least one of the specified axes,
        taken in any order (as well as possibly spanning other,
        unspecified axes). 

        If *strict_axes* is True then items are selected which span
        exactly all of the specified axes, taken in any order.

:Returns:

    out : dict
        A dictionary whose keys are domain item identifiers with
        corresponding values of items of the domain. The dictionary
        may be empty.

.. seealso:: `auxs`, `axes`, `cms`, `coords`, `dims`, `item`, `match`
             `remove_items`, `transforms`

**Examples**

'''
        return self.domain.items(
            items, role=role, axes=axes, ctype=ctype, exact=exact,
            inverse=inverse, match_all=match_all, rank=rank,
            regex=regex, strict_axes=strict_axes)
    #--- End: def

    def iter(self, name, *args, **kwargs):
        '''

Return a single element iterable for the output of a method (with
arguments).

``f.iter(name, *args, **kwargs)`` is equivalent to ``[f.name(*args,
**kwargs)]``.

:Parameters:

    name : str
        The name of the method.

    args, kwargs : *optional*
        The arguments to be used in the call to the method.

:Returns:

    out : list
        A single element list containing the output of the call to the
        method.

.. seealso:: `method`

**Examples**

>>> f.getprop('standard_name'):
air_pressure
>>> f.iter('getprop', 'standard_name'):
('air_pressure',)

>>> print f.squeeze()
None
>>> print f.iter('squeeze')
(None,)

'''
        return [getattr(self, name)(*args, **kwargs)]
    #--- End: def

    def remove_item(self, item=None, **kwargs):
        '''

Remove and return a domain item from the field.

An item is either a dimension coordinate, an auxiliary coordinate, a
cell measure or a transform object of the domain.

The item may be selected with the keyword arguments. If no unique item
can be found then no items are removed and None is returned.

:Parameters:

    item, kwargs : *optional*
        Select the item which would be selected by this call of the
        field's `item` method: ``f.item(item, **kwargs)``. See
        `cf.Field.item` for details.

:Returns:

    out :
        The removed item, or None if no unique item could be found.

.. seealso:: `item`, `remove_axes`, `remove_axis`, `remove_items`

**Examples**

'''
        return self.domain.remove_item(item, **kwargs)
    #--- End: def

    def remove_axes(self, axes=None, **kwargs):
        '''

Remove and return axes from the field.

By default all axes of the domain are removed, but particular axes may
be selected with the keyword arguments.

The axis may be selected with the keyword arguments. If no unique axis
can be found then no axis is removed and None is returned.

If an axis has size greater than 1 then it is not possible to remove
it if it is spanned by the field's data array or any multidimensional
coordinate or cell measure object of the field.

:Parameters:

    axes, kwargs : *optional*
        Select the axes which would be selected by this call of the
        field's `axes` method: ``f.axes(axes, **kwargs)``. See
        `cf.Field.axes` for details.

:Returns:

    out : list
        The removed axes. The list may be empty.

.. seealso:: `axes`, `remove_axis`, `remove_item`, `remove_items`

**Examples**

'''
        domain = self.domain

        axes = domain.axes(axes, **kwargs)
        if not axes:
            return []

        size1_data_axes = []
        axes_sizes = domain.dimension_sizes
        for axis in axes.intersection(domain.data_axes()): #dimensions['data']):
            if axes_sizes[axis] == 1:
                size1_data_axes.append(axis)
            else:
                raise ValueError(
"Can't remove an axis with size > 1 which is spanned by the data array")
        #---End: for

        if size1_data_axes:
            self.squeeze(size1_data_axes, i=True)

        axes = domain.remove_axes(axes, i=True)

        return axes
    #--- End: def

    def remove_axis(self, axis=None, **kwargs):
        '''

Remove and return an axis from the field.

The axis may be selected with the keyword arguments. If no unique axis
can be found then no axis is removed and None is returned.

If the axis has size greater than 1 then it is not possible to remove
it if it is spanned by the field's data array or any multidimensional
coordinate or cell measure object of the field.

:Parameters:

    axis, kwargs : *optional*
        Select the unique axis (it it exists) which would be selected
        by this call of the field's `axes` method: ``f.axes(axis,
        **kwargs)``. See `cf.Field.axes` for details.

:Returns:

    out :
        The domain identifier of the removed axis, or None if there
        isn't one.

.. seealso:: `axes`, `remove_axes`, `remove_item`, `remove_items`

**Examples**

'''      
        axis = self.domain.axis(axis, **kwargs)
        if axis is None:
            return

        return self.remove_axes(axis)[0]
    #--- End: def

    def remove_items(self, items=None, **kwargs):
        '''

Remove and return domain items from the domain.

An item is either a dimension coordinate, an auxiliary coordinate, a
cell measure or a transform object of the domain.

By default all items of the domain are removed, but particular items
may be selected with the keyword arguments.

:Parameters:

    items, kwargs : *optional*
        Select the items which would be selected by this call of the
        field's `items` method: ``f.items(items, **kwargs)``. See
        `cf.Field.items` for details.

:Returns:

    out : list
        The removed items. The list may be empty.

.. seealso:: `items`, `remove_axes`, `remove_axis`, `remove_item`

**Examples**

'''
        return self.domain.remove_items(items, **kwargs)
    #--- End: def

    def roll(self, axis, shift, i=False, **kwargs):
        '''

:Parameters:

    axis : 

    axis, kwargs : *optional*
        Set the new axis order. By default the existing axis order is
        reversed.

        If *axes* and/or *kwargs* are set then the new axis order is
        that which would be returned by this call of the field's
        `axes` method: ``f.axes(axes, ordered=True, **kwargs)``. See
        `cf.Field.axes` for details.

    i : bool, optional
        If True then update the field in place. By default a new field
        is created.

:Returns:

    out : cf.Field

.. seealso:: `expand_dims`, `flip`, `squeeze`, `swapaxes`,
             `transpose`, `unsqueeze`

**Examples**

''' 
        axis = self.domain.axis(axis, **kwargs)
        if axis is None:
            raise ValueError("Can't roll: Bad axis specification")

        if i:
            f = self
        else:
            f = self.copy()
        
        domain = f.domain

        if domain.axis_size(axis) <= 1:
            return f
        
        dim = domain.item(axis)
        if dim is not None and dim.period() is None:
            raise ValueError(
                "Can't roll %r axis with non-periodic dimension coordinates" % 
                dim.name())

        try:
            iaxis = domain.data_axes().index(axis)
        except ValueError:
            return f

        f = super(Field, f).roll(iaxis, shift, i=True)

        item_axes = domain.item_axes
        for key, item in domain.items(role=('d', 'a', 'c')).iteritems():
            axes = item_axes(key)
            if axis in axes:
                item.roll(axes.index(axis), shift, i=True)
        #--- End: for

        return f
    #--- End: def

    def setdata(self, x, y, *arg, **kwargs):
        '''

Set data array elements depending on a condition.

Elements are set differently depending on where the condition is True
or False. Two assignment values are given. From one of them, the
field's data array is set where the condition is True and where the
condition is False, the data array is set from the other.

Each assignment value may either contain a single datum, or is an
array-like object which is broadcastable shape of the field's data
array.

**Missing data**

The treatment of missing data elements depends on the value of field's
`hardmask` attribute. If it is True then masked elements will not
unmasked, otherwise masked elements may be set to any value.

In either case, unmasked elements may be set to any value (including
missing data).

Unmasked elements may be set to missing data by assignment to the
`cf.masked` constant or by assignment to a value which contains masked
elements.

:Parameters:

    x, y :
        Specify the assignment value. Where the condition defined by
        the *arg* and *kwargs* arguments is True, set the field's data
        array from *x* and where the condition is False, set the
        field's data array from *y*. Arguments *x* and *y* are each
        one of:

          * ``None``. The appropriate elements of the field's data
            array are unchanged.
        ..

          * Any object which is broadcastable to the field's shape
            using the metadata-aware `cf` broadcasting rules (i.e. a
            `cf.Field` object or any object, ``a``, for which
            ``numpy.size(a) == 1`` is True). The appropriate elements
            of the field's data array are set to the corresponding
            values from the object broadcast to the field's data array
            shape.
        ..
          
    arg : *optional*
        Define the condition which determines how to set the field's
        data array. Depending on the presence of any optional keyword
        arguments, *arg* is interpreted in one of two ways:

          ==========  ================================================
          Keyword     *arg*
          arguments?
          ==========  ================================================
          No          Any object which is broadcastable to the field's
                      shape using the metadata-aware `cf` broadcasting
                      rules (i.e. a `cf.Field` object or any object,
                      ``a``, for which ``numpy.size(a) == 1`` is
                      True). The condition is True where the object
                      broadcast to the field's data array shape is
                      True.

          Yes         A modifier for the interpretation of the 
                      keyword arguments. See *kwargs* for details.
          ==========  ================================================

        In the first case, the array-like object explicitly describes
        where the condition is True or False (if *arg* is unset then
        it defaults to a condition of True everywhere); whilst in the
        second case, the condition is defined by the keyword
        arguments.

    kwargs : *optional*
        Define the condition which determines how to set the field's
        data array by tests on the field's one dimensional coordinate
        values. Where the tests are passed, the condition is True
        elsewhere the condition is False.

        The condition is True at the data array indices that would be
        returned by this call of the field's `indices` method:
        ``f.indices(*arg, **kwargs)``. See `cf.Field.indices` for
        details.

:Returns:

    None

.. seealso:: `cf.masked`, `hardmask`, `indices`, `subspace`

**Examples**

Set data array values to 15 everywhere:

>>> f.setdata(15, None)

Set all negative data array values to zero and leave all other
elements unchanged:

>>> f.setdata(0, None, f<0)

Multiply all positive data array elements by -1 and set other data
array elements to 3.14:

>>> f.setdata(-f, 3.14, f>0)

Set all values less than 280 and greater than 290 to missing data:

>>> f.setdata(cf.masked, None, (f < 280) | (f > 290))

Set data array elements in the northern hemisphere to missing data:

>>> f.setdata(cf.masked, None, latitude=cf.ge(0))

Set a field's polar rows to their average values:

>>> a = cf.collapse(f, 'longitude: mean')
>>> f.setdata(a, None, latitude=cf.set([-90, 90])

Set a field to values from another field within the tropics, and to
yet another field elsewhere:

>>> f
<CF Field: air_temperature(height(19), time(12), latitude(73), longitude(96)) K>
>>> g
<CF Field: air_temperature(longitude(1), latitude(73)) K>
>>> h
<CF Field: air_temperature(latitude(1), time(1), longitude(96)) K>
>>> f.setdata(g, h, latitude=cf.wi(-30, 30))

Identify a one dimensionsal coordinate object by its abbreviated
identity:

>>> f.setdata(cf.Data([23]), numpy.array(54), lat=0)

Identify a one dimensionsal coordinate object by its exact identity:

>>> f.setdata([[15]], f.subspace(height=10), cf.masked, 'exact', latitude=0)

Identify one dimensionsal coordinates by regular expressions (in this
case, for example, a one dimensionsional object of "grid_latitude" or
"latitude" would be selected):

>>> f.setdata(g, h, 'regex', lat=cf.wi(-30, 30)), lon=cf.wi(-30, 30))

'''
        if len(arg) > 1:
            raise ValueError("asdasds09989 a")          
            
        if x is None and y is None:
            return

        self_class = self.__class__

        # Find the condition
        if kwargs:
            indices = f.indices(*arg, **kwargs)
            condition = self.mask
            condition.subspace[...] = False
            condition.subspace[indices] = True
        elif not arg:
            condition = True              
        else:
            condition = arg[0]                
            if isinstance(condition, self_class):
                condition = self._conform_for_assignment(condition)
        #--- End: if

        if x is not None and isinstance(x, self_class):
            x = self._conform_for_assignment(x)
               
        if y is not None and isinstance(y, self_class):
            y = self._conform_for_assignment(y)

        super(Field, self).setdata(x, y, condition)
    #--- End: def

#--- End: class


# ====================================================================
#
# SubspaceField object
#
# ====================================================================

class SubspaceField(SubspaceVariable):
    '''

An object which will get or set a subspace of a field.

The returned object is a `!SubspaceField` object which may be indexed
to select a subspace by axis index values (``f.subspace[indices]``) or
called to select a subspace by coordinate object array values
(``f.subspace(**coordinate_values)``).

**Subspacing by indexing**

Subspacing by indices allows a subspaced field to be defined via index
values for the axes of the field's data array.

Indices have an extended Python slicing syntax, which is similar to
:ref:`numpy array indexing <numpy:arrays.indexing>`, but with two
important extensions:

* Size 1 axes are never removed.

  An integer index i takes the i-th element but does not reduce the
  rank of the output array by one:

* When advanced indexing is used on more than one axis, the advanced
  indices work independently.

  When more than one axis's slice is a 1-d boolean sequence or 1-d
  sequence of integers, then these indices work independently along
  each axis (similar to the way vector subscripts work in Fortran),
  rather than by their elements:

**Subspacing by coordinate values**

Subspacing by values of one dimensionsal coordinate objects allows a
subspaced field to be defined via coordinate values of its domain.

Coordinate objects and their values are provided as keyword arguments
to a call to a `SubspaceField` object. Coordinate objects may be
identified by their identities, as returned by their `!identity`
methods. See `cf.Field.indices` for details, since
``f.subspace(**coordinate_values)`` is exactly equivalent to
``f.subspace[f.indices(**coordinate_values)]``.

**Assignment to subspaces**

Elements of a field's data array may be changed by assigning values to
a subspace of the field.

Assignment is only possible to a subspace defined by indices of the
returned `!SubspaceField` object. For example, ``f.subspace[indices] =
0`` is possible, but ``f.subspace(**coordinate_values) = 0`` is *not*
allowed. However, assigning to a subspace defined by coordinate values
may be done as follows: ``f.subspace[f.indices(**coordinate_values)] =
0``.

**Missing data**

The treatment of missing data elements during assignment to a subspace
depends on the value of field's `hardmask` attribute. If it is True
then masked elements will notbe unmasked, otherwise masked elements
may be set to any value.

In either case, unmasked elements may be set, (including missing
data).

Unmasked elements may be set to missing data by assignment to the
`cf.masked` constant or by assignment to a value which contains masked
elements.

.. seealso:: `cf.masked`, `hardmask`, `indices`, `setdata`

**Examples**

>>> print f
Data            : air_temperature(time(12), latitude(73), longitude(96)) K
Cell methods    : time: mean
Dimensions      : time(12) = [15, ..., 345] days since 1860-1-1
                : latitude(73) = [-90, ..., 90] degrees_north
                : longitude(96) = [0, ..., 356.25] degrees_east
                : height(1) = [2] m

>>> f.shape
(12, 73, 96)
>>> f.subspace[...].shape
(12, 73, 96)
>>> f.subspace[slice(0, 12), :, 10:0:-2].shape
(12, 73, 5)
>>> lon = f.coord('longitude').array
>>> f.subspace[..., lon<180]

>>> f.shape
(12, 73, 96)
>>> f.subspace[0, ...].shape
(1, 73, 96)
>>> f.subspace[3, slice(10, 0, -2), 95].shape
(1, 5, 1)

>>> f.shape
(12, 73, 96)
>>> f.subspace[:, [0, 72], [5, 4, 3]].shape
(12, 2, 3)

>>> f.subspace().shape
(12, 73, 96)
>>> f.subspace(latitude=0).shape
(12, 1, 96)
>>> f.subspace(latitude=cf.wi(-30, 30)).shape
(12, 25, 96)
>>> f.subspace(long=cf.ge(270, 'degrees_east'), lat=cf.set([0, 2.5, 10])).shape
(12, 3, 24)
>>> f.subspace(latitude=cf.lt(0, 'degrees_north'))
(12, 36, 96)
>>> f.subspace(latitude=[cf.lt(0, 'degrees_north'), 90])
(12, 37, 96)
>>> import math
>>> f.subspace(longitude=cf.lt(math.pi, 'radian'), height=2)
(12, 73, 48)
>>> f.subspace(height=cf.gt(3))
IndexError: No indices found for 'height' values gt 3

>>> f.subspace(dim2=3.75).shape
(12, 1, 96)

>>> f.subspace[...] = 273.15
    
>>> f.subspace[f.indices(longitude=cf.wi(210, 270, 'degrees_east'),
...                      latitude=cf.wi(-5, 5, 'degrees_north'))] = cf.masked

>>> index = f.indices(longitude=0)
>>> f.subspace[index] = f.suspace[index] * 2

'''
    __slots__ = []

    def __getitem__(self, indices):
        '''

x.__getitem__(indices) <==> x[indices]

'''
        field = self.variable

        if indices is Ellipsis:
            return field.copy()

        data = field.Data
        shape = data.shape

        # Parse the index
        indices, roll = parse_indices(field, indices, True)

        if roll:
            axes = data._axes
            cyclic_axes = data._cyclic
            for iaxis, shift in roll.iteritems():
                if axes[iaxis] not in cyclic_axes:
                    raise IndexError(
                        "Can't take a cyclic slice from non-cyclic %r axis" %
                        field.axis_name(iaxis))

                field = field.roll(iaxis, shift)
            #--- End: for
            new = field
        else:            
            new = field.copy(_omit_Data=True)

#        cyclic_axes = []
#        for i, axis in field.data_axes():
#            if field.iscyclic(axis):
#                cyclic_axes.append(i)                
#
#        indices, roll = parse_indices(field, indices, cyclic_axes)
#
#        if roll:
#            for iaxis, x in roll.iteritems():
#                field = field.roll(iaxis, x)
#
#            new = field
#        else:            
#            # Initialise the output field
#            new = field.copy(_omit_Data=True)
        
        # Initialise the output field
#        new = field.copy(_omit_Data=True)

        ## Work out if the indices are equivalent to Ellipsis and
        ## return if they are.
        #ellipsis = True
        #for index, size in izip(indices, field.shape):
        #    if index.step != 1 or index.stop-index.start != size:
        #        ellipsis = False
        #        break
        ##--- End: for
        #if ellipsis:
        #    return new

        # ------------------------------------------------------------
        # Subspace the field's data
        # ------------------------------------------------------------
        new.Data = field.Data[tuple(indices)]
        
        domain = new.domain

        data_axes = domain.data_axes()

        # ------------------------------------------------------------
        # Subspace ancillary variables.
        # 
        # If this is not possible for a particular ancillary variable
        # then it will be discarded from the output field.
        # ------------------------------------------------------------
        if hasattr(field, 'ancillary_variables'):
            new.ancillary_variables = AncillaryVariables()

            for av in field.ancillary_variables:
                axis_map = av.domain.map_axes(field.domain)
                av_indices = []
                flip_axes = []

                for avaxis in av.domain.data_axes(): #dimensions['data']:
                    if av.domain.dimension_sizes[avaxis] == 1:
                        # Size 1 axes are always ok
                        av_indices.append(slice(None))
                        continue

                    if avaxis not in axis_map:
                        # Unmatched size > 1 axes are not ok
                        av_indices = None
                        break

                    faxis = axis_map[avaxis]
                    if faxis in data_axes: #field.domain.dimensions['data']:
                        # Matched axes spanning the data arrays are ok
                        i = data_axes.index(faxis)
                        av_indices.append(indices[i])
                        if av.domain.direction(avaxis) != domain.direction(faxis):
                            flip_axes.append(avaxis)
                    else:
                        av_indices = None
                        break                      
                #--- End: for

                if av_indices is not None:
                    # We have successfully matched up each axis of the
                    # ancillary variable's data array with a unique
                    # axis in the parent field's data array, so we can
                    # keep a subspace of this ancillary field
                    if flip_axes:
                        av = av.flip(flip_axes)

                    new.ancillary_variables.append(av.subspace[tuple(av_indices)])
            #--- End: for

            if not new.ancillary_variables:
                del new.ancillary_variables
        #--- End: if

        # ------------------------------------------------------------
        # Subspace fields in transforms
        # ------------------------------------------------------------
        transforms = new.transforms()
        if transforms:
                
            broken = []

            for key, transform in transforms.iteritems():
                for term, variable in transform.iteritems():
                    if not isinstance(variable, Field):
                        continue

                    # Still here? Then try to subspace a formula_terms
                    # field.
                    dim_map = variable.domain.map_axes(domain)
                    v_indices = []
                    flip_dims = []

                    for vdim in variable.domain.data_axes(): #dimensions['data']:
                        if variable.domain.dimension_sizes[vdim] == 1:
                            # We can always index a size 1 axis of the
                            # data array
                            v_indices.append(slice(None))
                            continue
                        
                        if vdim not in dim_map:
                            # Unmatched size > 1 axes are not ok
                            v_indices = None
                            break

                        axis = dim_map[vdim]
                        data_axes = domain.data_axes()
                        if axis in data_axes:
                            # We can index a matched axis which spans
                            # the data array
                            i = data_axes.index(axis)
                            v_indices.append(indices[i])
                            if variable.domain.direction(vdim) != domain.direction(axis):
                                flip_dims.append(vdim)
                        else:
                            v_indices = None
                            break                      
                    #--- End: for

                    if v_indices is not None:
                        # This term is subspaceable
                        if flip_dims:
                            variable = variable.flip(flip_dims)

                        transform[term] = variable.subspace[tuple(v_indices)]
                    else:
                        # This term is broken
                        transform[term] = None
                #--- End: for
            #--- End: for
        #--- End: if

        # ------------------------------------------------------------
        # Subspace the coordinates and cell measures
        # ------------------------------------------------------------
        for key, item in domain.items(role=('d', 'a', 'c'),
                                      axes=data_axes).iteritems():
            item_axes = domain.item_axes(key)

            dice = []
            for axis in item_axes:
                if axis in data_axes:
                    dice.append(indices[data_axes.index(axis)])
                else:
                    dice.append(slice(None))
            #--- End: for

            domain._set(key, item.subspace[tuple(dice)])
        #--- End: for

        for axis, size  in izip(data_axes, new.shape):
            domain.dimension_sizes[axis] = size
            
        return new
    #--- End: def

    def __call__(self, *arg, **kwargs):
        '''

Return a subspace of the field defined by coordinate values.

:Parameters:

    kwargs : *optional*
        Keyword names identify coordinates; and keyword values specify
        the coordinate values which are to be reinterpreted as indices
        to the field's data array.


~~~~~~~~~~~~~~ /??????
        Coordinates are identified by their exact identity or by their
        axis's identifier in the field's domain.

        A keyword value is a condition, or sequence of conditions,
        which is evaluated by finding where the coordinate's data
        array equals each condition. The locations where the
        conditions are satisfied are interpreted as indices to the
        field's data array. If a condition is a scalar ``x`` then this
        is equivalent to the `cf.Comparison` object ``cf.eq(x)``.

:Returns:

    out : cf.Field

**Examples**

>>> f.indices(lat=0.0, lon=0.0)
>>> f.indices(lon=cf.lt(0.0), lon=cf.set([0, 3.75]))
>>> f.indices(lon=cf.lt(0.0), lon=cf.set([0, 356.25]))
>>> f.indices(lon=cf.lt(0.0), lon=cf.set([0, 3.75, 356.25]))

'''
        field = self.variable

        if not kwargs:
            return field.copy()    

        return field.subspace[field.indices(*arg, **kwargs)]
    #--- End: def

    def __setitem__(self, indices, value):
        '''

x.__setitem__(indices, value) <==> x[indices]

'''
        field = self.variable

        if isinstance(value, field.__class__):
           value = field._conform_for_assignment(value)
           value = value.Data

        elif numpy_size(value) != 1:
            raise ValueError(
                "Can't assign a size %d %r to a %s data array" %
                (numpy_size(value), value.__class__.__name__,
                 field.__class__.__name__))

        elif isinstance(value, Variable):
            value = value.Data

        field.Data[indices] = value
    #--- End: def

#--- End: class
