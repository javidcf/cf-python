import os

from csv import reader as csv_reader

from .          import __file__
from .utils     import Dict
from .functions import RTOL, ATOL, equals, allclose
from .functions import inspect as cf_inspect
from .units     import Units

from .data.data import Data

# --------------------------------------------------------------------
# Map coordinate conversion names to their CF-netCDF types
# --------------------------------------------------------------------
_type = {}
_file = os.path.join(os.path.dirname(__file__),
                     'etc/coordinate_reference/type.txt')
for x in csv_reader(open(_file, 'r'), delimiter=' ', skipinitialspace=True):
    if not x or x[0] == '#':
        continue
    _type[x[0]] = x[1]

# --------------------------------------------------------------------
# Map coordinate conversion names to their
# --------------------------------------------------------------------
_coordinates = {}
_file = os.path.join(os.path.dirname(__file__),
                     'etc/coordinate_reference/coordinates.txt')
for x in csv_reader(open(_file, 'r'), delimiter=' ', skipinitialspace=True):
    if not x or x[0] == '#':
        continue
    _coordinates[x[0]] = set(x[1:])

# --------------------------------------------------------------------
# Map coordinate conversion terms to their terms default values
# --------------------------------------------------------------------
_default_values = {}
_file = os.path.join(os.path.dirname(__file__),
                     'etc/coordinate_reference/default_values.txt')
for x in csv_reader(open(_file, 'r'), delimiter=' ', skipinitialspace=True):
    if not x or x[0] == '#':
        continue
    _default_values[x[0]] = float(x[1])

# --------------------------------------------------------------------
# Map coordinate conversion terms to their canonical units
# --------------------------------------------------------------------
_canonical_units = {}
_file = os.path.join(os.path.dirname(__file__),
                     'etc/coordinate_reference/canonical_units.txt')
for x in csv_reader(open(_file, 'r'), delimiter=' ', skipinitialspace=True):
    if not x or x[0] == '#':
        continue
    try:
        _canonical_units[x[0]] = Units(x[1])
    except:
        pass

# --------------------------------------------------------------------
# Map coordinate reference names to their terms which may take
# non-constant values (i.e. pointers to coordinate objects or
# non-scalar field objects).
# --------------------------------------------------------------------
_non_constant_terms = {}
_file = os.path.join(os.path.dirname(__file__),
                     'etc/coordinate_reference/non_constant_terms.txt')
for x in csv_reader(open(_file, 'r'), delimiter=' ', skipinitialspace=True):
    if not x or x[0] == '#' or len(x) == 1:
        continue
    _non_constant_terms[x[0]] = set(x[1:])


# ====================================================================
#
# Transform object
#
# ====================================================================

_units = {}

class Transform(Dict):
    '''

A CF transform construct.

A transform construct defines a mapping from one set of coordinates
which can not geo-locate the field construct's data to another set of
coordinates that can geo-locate the field construct's data.

Transform constructs correspond to the functions of the CF-netCDF
attributes formula_terms, which describes how to compute a vertical
auxiliary coordinate variable from components and grid_mapping, which
describes how to compute true latitude and longitude auxiliary
coordinate variables from horizontal projection dimension coordinates,
or describes the figure of earth for true latitude and longitude
coordinate variables.

A transform is a dictionary-like object for which each key/value pair
comprises the name of a term of the transform and its corresponding
variable. A term that is omitted is assumed to be zero.

The each variable is one of the following:

=====================  ====================================================
Variable               Notes
=====================  ====================================================
Pointer to coordinate  A domain's coordinate identifier; or a
                       coordinate's identity, as a defined by the
                       coordinate's `~cf.Coordinate.identity` method.

Python numeric scalar  

`cf.Data`              Must have an array size of 1

`cf.Field`             

``None``               Indicates that the term is unset. Note that
                       this is different than the term being missing,
                       as in this case there is no assumption of a
                       value of zero.
=====================  ====================================================


**Attributes**

===============  ========  ======================================================
Attribute        Type      Description
===============  ========  ======================================================
`!coords`        ``set``   A set of pointers to coordinate objects (may be empty)
`!grid_mapping`  ``bool``  True if and only if the transform is a grid_mapping
`!name`          ``str``   The identity of the transform (such as 
                           ``'ocean_sigma_coordinate'`` or
                           ``'rotated_latitude_longitude'``)
===============  ========  ======================================================


**Examples**

>>> t
<CF Transform: rotated_latitude_longitude>
>>> print t.dump()
Transform: rotated_latitude_longitude
    grid_north_pole_latitude = 38.0
    grid_north_pole_longitude = 190.0

>>> print t.dump()
Transform: atmosphere_hybrid_height_coordinate
    a = 'aux0'
    b = 'aux1'
    orog = <CF Field: surface_altitude(grid_longitude(96), grid_latitude(73)) m>

'''

    def __init__(self, name=None, type=None, coord_terms=None, coords=None, **kwargs):
        '''

**Initialization**

:Parameters:

    kwargs :
        Keys and values are initialized exactly as keyword arguments
        for a built-in dict. In general, keys are transform parameter
        names (such as ``'grid_north_pole_latitude'`` or ``'sigma'``)
        with appropriate values, with three exceptions:

          * If the key ``'standard_name'`` then its value is the
            identity for a formula_terms-type transform and no term is
            created.
          
          * If the key ``'grid_mapping_name'`` is specified then its
            value is the identity for a grid_mapping-type transform
            and no term is created.
          
          * If the key ``'coords'`` is specified then its value is a
            sequence of coordinate pointers for *all* of the inputs to
            the transform which do not have dedicated terms.

        One of the keys ``'standard_name'`` and
        ``'grid_mapping_name'`` must be specified, but ``'coords'`` is
        optional.

**Examples**

>>> t = cf.Transform(name='rotated_latitude_longitude',
...                  grid_north_pole_latitude=38.0, 
...                  grid_north_pole_longitude=190)

>>> t = cf.Transform(name='atmosphere_ln_pressure_coordinate',
...                  p0=1000, lev='dim1', 

>>> t = cf.Transform(name='atmosphere_ln_pressure_coordinate',
...                  p0=cf.Data(100000, 'Pa'),
...                  lev='atmosphere_ln_pressure_coordinate')

'''
        super(Transform, self).__init__(**kwargs)

#        coords      = set(coords)
#        coord_terms = set(coord_terms)
#       
#        coords.update([value for term, value in kwargs.iteritems()
#                       if term in coord_terms])

        if name in _type:
#            coords.update(_coordinates[name])
            ttype = _type[name]
            if type is not None and type != ttype:
                raise ValueError("345|")
            self.type = ttype
        else:
            self.type = type

        if coords is None:
            coords = set(_coordinates.get(name, ()))
        else:
            coords = set(coords)

        if coord_terms:
            coords.update([kwargs[term] for term in coord_terms if term in kwargs])
            coord_terms = set(coord_terms)
        else:
            coord_terms = set()

        self.name        = name 
        self.coords      = coords
        self.coord_terms = coord_terms
    #--- End: def

    def __delitem__(self, item):
        '''

x.__delitem__(key) <==> del x[key]

'''
        super(Transform, self).__delitem__(item)
        self.coord_terms.discard(item)
    #--- End: def

    def __eq__(self, other):
        '''

The rich comparison operator ``==``

x.__eq__(y) <==> x==y

'''
        return self.equals(other)
    #--- End: def

    def __ne__(self, other):
        '''

The rich comparison operator ``!=``

x.__ne__(y) <==> x!=y

'''
        return not self.equals(other)
    #--- End: def

    def __hash__(self):
        '''

x.__hash__() <==> hash(x)

'''
        if not self.grid_mapping:
            raise ValueError("Can't hash a formula_terms transform")

        h = sorted(self.items())
        h.append(self.identity())

        return hash(tuple(h))
    #--- End: def

    def __repr__(self):
        '''

x.__repr__() <==> repr(x)

''' 
        try:
            return '<CF %s: %s>' % (self.__class__.__name__, self.identity(''))
        except AttributeError:
            return '<CF %s: >' % self.__class__.__name__
    #--- End: def

#    def __setitem__(self, item, value):
#        '''
#
#x.__setitem__(key, value) <==> x[key]=value#
#
#'''
#        if item == 'crs_wkt':
#            # Bodge for crs_wkt
#            super(Transform, self).__setitem__(item, value)
#            return
#
#        if item in self:
#            old = self.pop(item)
#            self.coord_terms.discard(item)
#            if isinstance(old, basestring) and old not in self.values():
#                self.coords.discard(old)
#        #-- End: if
#
#        super(Transform, self).__setitem__(item, value)
#
#        # Add string values to the coordinates sets
#        if isinstance(value, basestring):
#            self.coords.add(value)
#            self.coord_terms.add(item)
#    #--- End: def

    def __str__(self):
        '''
x.__str__() <==> str(x)

'''    
        return 'Transform       : %r' % self
    #--- End: def

    def canonical_units(self, term):
        '''
'''
        return _canonical_units.get(term, None)
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: T (read only)
    # ----------------------------------------------------------------
    @property
    def T(self):
        '''

Returns False.

Provides compatibility with the `cf.Coordinate` API.

.. seealso:: `X`, `Y`, `Z`

**Examples**

>>> print t.T
False

'''              
        return False
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: X (read only)
    # ----------------------------------------------------------------
    @property
    def X(self):
        '''

Returns False.

Provides compatibility with the `cf.Coordinate` API.

.. seealso:: `T`, `Y`, `Z`

**Examples**

>>> print t.X
False

'''              
        return False
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: Y (read only)
    # ----------------------------------------------------------------
    @property
    def Y(self):
        '''

Returns False.

Provides compatibility with the `cf.Coordinate` API.

.. seealso:: `T`, `X`, `Z`

**Examples**

>>> print t.Y
False

'''              
        return False
    #--- End: def

    # ----------------------------------------------------------------
    # Attribute: Z (read only)
    # ----------------------------------------------------------------
    @property
    def Z(self):
        '''

Returns False.

Provides compatibility with the `cf.Coordinate` API.

.. seealso:: `T`, `X`, `Y`

**Examples**

>>> print t.Z
False

'''              
        return False
    #--- End: def

    def close(self):
        '''

Close all files referenced by transform values.

:Returns:

    None

**Examples**

>>> t.close()

'''
        for value in self.itervalues():
            if hasattr(value, 'close'):
                value.close()
    #--- End: def

    def copy(self):
        '''

Return a deep copy.

``t.copy()`` is equivalent to ``copy.deepcopy(t)``.

:Returns:

    out :
        The deep copy.

**Examples**

>>> u = t.copy()

'''       
        X = type(self)
        new = X.__new__(X)

        coord_terms = self.coord_terms.copy()

        new.coord_terms = coord_terms
        new.coords      = self.coords.copy()
        new.name        = self.name
        new.type        = self.type

        _dict = {}
        for term, value in self._dict.iteritems():
            if (term in coord_terms or value is None or
                not hasattr(value, 'copy')):
                _dict[term] = value
            else:
                _dict[term] = value.copy()
        #--- End: for
        new._dict = _dict

        return new
    #---End: def

    def default(self, term):
        '''
'''
        return _default_values.get(term, None)
    #--- End: def

    def dump(self, complete=False, display=True, level=0, domain=None):
        '''

Return a string containing a full description of the transform.

:Parameters:

    complete : bool, optional

    display : bool, optional
        If False then return the description as a string. By default
        the description is printed, i.e. ``t.dump()`` is equivalent to
        ``print t.dump(display=False)``.

    level : int, optional

    domain : cf.Domain, optional

:Returns:

    out : str
        A string containing the description.

**Examples**

'''          
        indent0 = '    ' * level
        indent1 = '    ' * (level+1)

        try:
            string = ['%sTransform: %s' % (indent0, self.identity(''))]
        except AttributeError:
            string = ['%sTransform: ' % indent0]

        if domain:
            coord_keys = domain.items(role='da')
            for key, value in sorted(self.iteritems()):
                if value in coord_keys:
                    string.append("%s%s = %r" % (indent1, key, domain.get(value)))
                else:
                    if complete and hasattr(value, 'domain'):
                        string.append("%s%s = \n%s" % 
                                      (indent1, key, 
                                       value.dump(complete=False, display=False, 
                                                  level=level+2,
                                                  title='Transform field', q='-')))
                    else:
                        string.append("%s%s = %r" % (indent1, key, value))
        else:
            for key, value in sorted(self.iteritems()):
                if complete and hasattr(value, 'domain'):
                    string.append("%s%s = \n%s" % 
                                  (indent1, key, 
                                   value.dump(complete=False, display=False, 
                                              level=level+2, 
                                              title='Transform field', q='-')))
                else:
                    string.append("%s%s = %r" % (indent1, key, value))
        #--- End: if

        string = '\n'.join(string)
       
        if display:
            print string
        else:
            return string
    #--- End: def

    def equals(self, other, rtol=None, atol=None, traceback=False):
        '''

True if two instances are equal, False otherwise.

:Parameters:

    other : 
        The object to compare for equality.

    atol : float, optional
        The absolute tolerance for all numerical comparisons, By
        default the value returned by the `ATOL` function is used.

    rtol : float, optional
        The relative tolerance for all numerical comparisons, By
        default the value returned by the `RTOL` function is used.

    traceback : bool, optional
        If True then print a traceback highlighting where the two
        instances differ.

:Returns: 

    out : bool
        Whether or not the two instances are equal.

**Examples**

'''
        if self is other:
            return True
        
        # Check that each instance is the same type
        if self.__class__ != other.__class__:
            if traceback:
                print("%s: Different types: %s, %s" %
                      (self.__class__.__name__,
                       self.__class__.__name__,
                       other.__class__.__name__))
            return False
        #--- End: if
   
        # ------------------------------------------------------------
        # Check the name
        # ------------------------------------------------------------
        if self.name != other.name:
            if traceback:
                print(
"%s: Different names (%s != %s)" %
(self.__class__.__name__, self.name, other.name))
            return False
        #--- End: if
                
        if rtol is None:
            rtol = RTOL()
        if atol is None:
            atol = ATOL()

        # ------------------------------------------------------------
        # Check that the same terms are present
        # ------------------------------------------------------------
        if set(self) != set(other):
            if traceback:
                print(
"%s: Different collections of terms (%s != %s)" %
(self.__class__.__name__, set(self), set(other)))
            return False
        #--- End: if

        # Check that the coordinate terms match
        coord_terms0 = self.coord_terms
        coord_terms1 = other.coord_terms
        if coord_terms0 != coord_terms1:
            if traceback:
                print(
"%s: Different coordinate-valued terms (%s != %s)" % 
(self.__class__.__name__, coord_terms0, coord_terms1))
            return False
        #--- End: if

        # ------------------------------------------------------------
        # Check that the term values are equal.
        #
        # If the values for a particular term are both undefined or
        # are both pointers to coordinates then they are considered
        # equal.
        # ------------------------------------------------------------
        coords0 = self.coords
        coords1 = other.coords
        if len(coords0) != len(coords1):
            if traceback:
                print(
"%s: Different sized collections of coordinates (%d != %d)" % 
(self.__class__.__name__, len(coords0), len(coords1)))
            return False
        #--- End: if

        for term, value0 in self.iteritems():            
            if term in coord_terms0 and term in coord_terms1:
                # Term values are coordinates in both transforms
                continue
                
            value1 = other[term]  

            if value0 is None and value1 is None:
                # Term values are None in both transforms
                continue
                
            if equals(value0, value1, rtol=rtol, atol=atol,
                      traceback=traceback):
                # Term values are the same in both transforms
                continue

            # Still here? Then the two transforms are not equal.
            if traceback:
                print(
"%s: Unequal '%s' terms (%r != %r)" % 
(self.__class__.__name__, term, value0, value1))
                return False
        #--- End: for

        # ------------------------------------------------------------
        # Still here? Then the two transforms are as equal as can be
        # ascertained in the absence of domains.
        # ------------------------------------------------------------
        return True
    #--- End: def

    def equivalent(self, other, atol=None, rtol=None, traceback=False):
        '''

True if two transforms are logically equal, False otherwise.

:Parameters:

    other : cf.Transform
        The object to compare for equality.

    atol : float, optional
        The absolute tolerance for all numerical comparisons, By
        default the value returned by the `cf.ATOL` function is used.

    rtol : float, optional
        The relative tolerance for all numerical comparisons, By
        default the value returned by the `cf.RTOL` function is used.

    traceback : bool, optional
        If True then print a traceback highlighting where the two
        instances differ.

:Returns:

    out : bool
        Whether or not the two objects are equivalent.

**Examples**

>>>

'''
        if self is other:
            return True
        
        # Check that each instance is the same type
        if self.__class__ != other.__class__:
            if traceback:
                print("%s: Different types (%r != %r)" %
                      (self.__class__.__name__,
                       self.__class__.__name__, other.__class__.__name__))
            return False
        #--- End: if
   
        # ------------------------------------------------------------
        # Check the name
        # ------------------------------------------------------------
        if self.name != other.name:
            if traceback:
                print("%s: Different names (%r != %r)" %
                      (self.__class__.__name__, self.name, other.name))
            return False
        #--- End: if
                
        if rtol is None:
            rtol = RTOL()
        if atol is None:
            atol = ATOL()

        # Check that the term values are equal.
        #
        # If the values for a particular key are both undefined or
        # pointers to coordinates, then they are considered equal.
        coords0 = self.coords
        coords1 = other.coords

        for term in set(self).union(other):

            if term in self.coord_terms and term in other.coord_terms:
                # ----------------------------------------------------
                # Both terms are coordinates
                # ---------------------------------------------------- 
                continue

            value0 = self.get(term, None)
            value1 = other.get(term, None)

            if value1 is None and value0 is None:
                # ----------------------------------------------------
                # Both terms are undefined
                # ----------------------------------------------------
                continue

            if value1 is None:
                t, value = self, value0
            elif value0 is None:
                t, value = other, value1
            else:
                t = None

            if t is not None:
                # ----------------------------------------------------
                # Exactly one term is undefined
                # ----------------------------------------------------
                if term in t.coord_terms:
                    # Term is a coordinate
                    continue

                default = t.default(term)
                if default is None:
                    if traceback:
                        print("%s: Unequivalent %r term" %
                              (self.__class__.__name__,  term))
                    return

                if not allclose(value, default, rtol=rtol, atol=atol): 
                    if traceback:
                        print("%s: Unequivalent %r term" %
                              (self.__class__.__name__,  term))
                    return
            #--- End: if

            # ----------------------------------------------------
            # Both terms are defined and are not coordinates
            # ---------------------------------------------------- 
            if not allclose(value0, value1, rtol=rtol, atol=atol):
                if traceback:
                    print("%s: Unequivalent %r term (%r != %r)" % 
                          (self.__class__.__name__, term, value0, value1))
                return False
        #--- End: for

        # Still here?
        return True
    #--- End: def

    def identity(self, default=None):
        '''

Return the identity of the transform.

The identity is the standard_name of a formula_terms-type transform or
the grid_mapping_name of grid_mapping-type transform.

:Parameters:

    default : optional
        If the transform has no identity then return *default*. By
        default, *default* is None.

:Returns:

    out :
        The identity.

**Examples**

>>> t.identity()
'rotated_latitude_longitude'
>>> t.identity()
'atmosphere_hybrid_height_coordinate'

'''
        return getattr(self, 'name', default)
    #--- End: def

    def inspect(self):
        '''

Inspect the object for debugging.

.. seealso:: `cf.inspect`

:Returns: 

    None

'''
        print cf_inspect(self)
    #--- End: def

    def match(self, match=None, exact=False, match_all=True, regex=False):
        '''

Test whether or not the transform satisfies the given conditions.

:Returns:

    out : bool
        True if the transform satisfies the given criteria, False
        otherwise.

**Examples**

'''
        if not match:
            return True

        found_match = False
         
        if not isinstance(match, dict):
            match = {None: match}

        for prop, value in match.iteritems():           
            if prop is not None:
                # If, in the future, other tests are allowed then this
                # will have to change
                continue
            
            x = self.identity()

            if x is None:
                var_matches = False
            elif isinstance(x, basestring) and isinstance(value, basestring):
                if exact:
                    var_matches = (value == x)
                elif not regex:
                    var_matches = x.startswith(value)
                else:
                    var_matches = re_search(value, x)
            else:	
                var_matches = (value == x)
                if var_matches is not True:
                    var_matches = False
            #--- End: if

            if match_all:
                if var_matches:
                    found_match = True
                else:
                    return var_matches
            elif var_matches:
                return var_matches
        #--- End: for

        return found_match
    #--- End: def

    def change_coord(self, value, new=None):
        '''

Change a pointer to a coordinate object.

If no new value is specified the coordinate object is effectively
removed.

:Parameters:

    value : str

    new : str, optional

:Returns:

    None

**Examples**

>>> t.change_coord('aux1')
>>> t.change_coord('longitude')
>>> t.change_coord('dim1', 'depth')
>>> t.change_coord('depth', 'dim1')

'''
        if value == new:
            return

        for term in self.coord_terms.copy():
            if self[term] == value:
                self[term] = new
        #---End: for

        coords = self.coords
        if value in coords:
            coords.remove(value)
            if new is not None:
                coords.add(new)
        #--- End: if
    #---End: def

    def remove_all_pointers(self):
        '''

Remove all all pointers to coordinates.

All terms referencing coordinates are set to None.

:Returns:

    None

**Examples**

>>> t.remove_all_pointers()

'''
        self.coords.clear()

        for term in self.coord_terms:
            self[term] = None                  
    #---End: def

    def setcoord(self, term, value):
        '''


'''
        super(Transform, self).__setitem__(term, value)
        self.coord_terms.add(term)
        self.coords.add(value)
    #--- End: def

    def set(self, term, value):
        '''


'''
        super(Transform, self).__setitem__(term, value)
    #--- End: def

    def structural_signature(self, rtol=None, atol=None):
        '''
'''     
        name = self.name
        s = [name]
        append = s.append
        
        coord_terms = self.coord_terms
        non_constant_terms = _non_constant_terms.get(name, ())

        for term, value in sorted(self.iteritems()):
            if term in non_constant_terms:
                continue

            if term in coord_terms:
                continue

            if isinstance(value, basestring):
                append((term, value))
                continue
                
            value = Data.asdata(value)

            cu = _canonical_units.get(term, None)
            if cu is not None:
                if value.Units.equivalent(cu):
                    value.Units = cu
                elif value.Units:
                    cu = value.Units
            else:
                cu = value.Units

            if str(cu) in _units:
                cu = _units[str(cu)]
            else:    
                ok = 0
                for units in _units.itervalues():
                    if cu.equals(units):
                        _units[str(cu)] = units
                        cu = units
                        ok = 1
                        break
                if not ok:
                    _units[str(cu)] = cu 


            default = self.default(term)
            if (default is not None and 
                allclose(value, default, rtol=rtol, atol=atol)):
                continue
            
            append((term, value, cu))
        #--- End: for                
                
        return tuple(s)
    #---End: def

#--- End: class
