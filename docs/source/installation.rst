Installation
============

Downloads, installation instructions, installation dependencies and
the source code are all available from the `cf-python home page
<http://cfpython.bitbucket.org>`_.

Installation instructions and hardware and software dependencies are
detailed in the README.md file contained with the downloaded source
code, which may also be read at
`<https://bitbucket.org/cfpython/cf-python>`_.

Please raise any questions or problems through the `cf-python home
page <http://cfpython.bitbucket.org>`_.
