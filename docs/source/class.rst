.. currentmodule:: cf
.. default-role:: obj

.. _class:

Classes of the :mod:`cf` module
===============================

Field classes
-------------

.. autosummary::
   :nosignatures:
   :toctree: classes/

   cf.Field		              
   cf.FieldList		              

Field component classes
-----------------------

.. autosummary::
   :nosignatures:
   :toctree: classes/

   cf.AncillaryVariables
   cf.AuxiliaryCoordinate
   cf.CellMeasure
   cf.CellMethods
   cf.CoordinateBounds
   cf.Data
   cf.DimensionCoordinate
   cf.Domain
   cf.Flags
   cf.Transform
   cf.Units

Miscellaneous classes
---------------------

.. autosummary::
   :nosignatures:
   :toctree: classes/

   cf.Comparison     
   cf.Datetime
   cf.Dict       
   cf.List

Base classes
------------

.. autosummary::
   :nosignatures:
   :toctree: classes/
         
   cf.Coordinate
   cf.Variable       

.. comment
   Data component classes
   ----------------------
   
   .. autosummary::
      :nosignatures:
      :toctree: classes/
   
      cf.Partition
      cf.PartitionMatrix


.. _inheritance_diagrams:

Inheritance diagrams
--------------------

The classes defined by the `cf` package inherit as follows:

----

.. image:: images/inheritance1.png

.. commented out
   .. inheritance-diagram:: cf.Domain
                            cf.Partition		
                            cf.PartitionMatrix
      			    cf.Data
                            cf.Flags	
                            cf.Units
         :parts: 1

----

.. image:: images/inheritance2.png

.. commented out
   .. inheritance-diagram:: cf.CoordinateBounds
   			    cf.AuxiliaryCoordinate
                            cf.DimensionCoordinate
                            cf.Field
                            cf.CellMeasure
      :parts: 1

----

.. image:: images/inheritance3.png

.. commented out
   .. inheritance-diagram:: cf.FieldList
         		    cf.AncillaryVariables
                            cf.CellMethods
         		    cf.Transform
      :parts: 1

----

This inheritance diagrams show, for example:

* `cf.Field` and `cf.DimensionCoordinate` objects have all of the
  features of a `cf.Variable` object, e.g. they may have a data array
  and CF properties.

* `cf.FieldList` and `cf.List` objects are mutable, :ref:`iterable
  <python:typeiter>` objects.

* `cf.Transform` and `cf.Dict` objects are mutable, :ref:`mapping
  <python:typesmapping>` objects.
