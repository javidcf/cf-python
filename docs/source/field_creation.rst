.. currentmodule:: cf
.. default-role:: obj

.. _field_creation:
 
Creating `cf.Field` objects
===========================

A new field may be created by initializing a new `cf.Field`
instance. Data and metadata are provided with the following keyword
parameters, all of which are optional:

=======================  =============================================================
Keyword                  Description
=======================  =============================================================
``ancillary_variables``  Provide the new field with ancillary variable fields in a
                         `cf.AncillaryVariables` object
``attributes``           Provide the new field with attributes in a dictionary
``data``                 Provide the new field with a data array in a `cf.Data` object
``dimensions``           Provide the new field with a data array dimensions
``domain``               Provide the new field with a coordinate system in a
			 `cf.Domain` object
``flags``                Provide the new field with self-describing flag values in a
                         `cf.Flags` object
``properties``           Provide the new field with CF properties in a dictionary
=======================  =============================================================

.. _domain-creation:

Domain creation
---------------

Creating a domain possibly comprises the largest part of field
creation, because the domain itself is composed of many interrelated
items (dimension and auxiliary coordinate, cell measure and transform
objects). It is not, however, difficult and is essentially a
methodical assembly of components. Domain initialization is fully
described in the documention of the `cf.Domain` object.

Each component of the field's domain has an internal identifier
(unique strings such as ``'dim1'``, ``'aux0'``, ``'cm2'`` and
``'trans0'``), but for many field initializations, there is no need to
provide, nor have any knowledge of these. However, they are easy to
set if required (which may be the case if, for example, two
multidimensional auxiliary coordinates span the same dimensions but in
different orders) or if desired for clarity.

.. _inserting-and-removing-components:

Inserting and removing components 
---------------------------------

Inserting field components after initialization is done with the
following methods:

.. autosummary::
   :nosignatures:
   :template: method.rst

   cf.Field.insert_aux
   cf.Field.insert_axis
   cf.Field.insert_cm
   cf.Field.insert_data
   cf.Field.insert_dim
   cf.Field.insert_transform	   

For example:

>>> coord
<CF DimensionCoordinate: time(12) days since 2003-12-1>
>>> f.domain.insert_dim(coord)

Note that inserting domain items during field initialization is likely
to be faster than using the insertion methods afterwards.

Removing field components is done with the following methods:

.. autosummary::
   :nosignatures:
   :template: method.rst

   cf.Field.remove_axis
   cf.Field.remove_axes
   cf.Field.remove_data
   cf.Field.remove_item
   cf.Field.remove_items

For example:

>>> f.domain.remove_item('forecast_reference_time')


.. _field-creation_examples:

Examples
--------

To improve readability, it is recommended that the construction of a
field is done by first creating the components separately (data,
coordinates, properties, *etc.*), and then combining them to make the
field (as in :ref:`example 3 <fc-example3>` and :ref:`example 4
<fc-example4>`), although this may not be necessary for very simple
fields (as in :ref:`example 1 <fc-example1>` and :ref:`example 2
<fc-example2>`).

.. _fc-example1:

Example 1
~~~~~~~~~

An empty field:

>>> f = cf.Field()
>>> print f
 field summary
--------------

.. _fc-example2:

Example 2
~~~~~~~~~

A field with just CF properties:

>>> f = cf.Field(properties={'standard_name': 'air_temperature',
...                          'long_name': 'temperature of air'})
...
>>> print f
air_temperature field summary
-----------------------------

.. _fc-example3:

Example 3
~~~~~~~~~

A field with a simple domain. Note that in this example the data and
coordinates are generated using :py:obj:`range` and `numpy.arange`
simply for the sake of having some numbers to play with. In practice
it is likely the values would have been read from a file in some
arbitrary format:

>>> import numpy
>>> data = cf.Data(numpy.arange(90.).reshape(10, 9), 'm s-1')
>>> properties = {'standard_name': 'eastward_wind'}
>>> dim0 = cf.DimensionCoordinate(data=cf.Data(range(10), 'degrees_north'),
...                               properties={'standard_name': 'latitude'})
...
>>> dim1 = cf.DimensionCoordinate(data=cf.Data(range(9), 'degrees_east'))
>>> dim1.standard_name = 'longitude'
>>> domain = cf.Domain(dim=[dim0, dim1])
>>> f = cf.Field(properties=properties, data=data, domain=domain)
>>> print f
eastward_wind field summary
---------------------------
Data            : eastward_wind(latitude(10), longitude(9)) m s-1
Dimensions      : latitude(10) = [0, ..., 9] degrees_north
                : longitude(9) = [0, ..., 8] degrees_east

Note that the default dimension order of ``['dim0', 'dim1']`` is
applicable to the field's data array.

Adding an auxiliary coordinate to the "latitude" axis and a cell
method may be done with the :ref:`relevant method
<inserting-and-removing-components>` and by simple assignment
respectively (note that these coordinate values are just for
illustration):

>>> aux = cf.AuxiliaryCoordinate(data=cf.Data(['alpha','beta','gamma','delta','epsilon',
...                                             'zeta','eta','theta','iota','kappa']))
...				       
>>> aux.long_name = 'extra'
>>> print f.items()
{'dim0': <CF DimensionCoordinate: latitude(10) degrees_north>,
 'dim1': <CF DimensionCoordinate: longitude(9) degrees_east>}
>>> f.insert_aux(aux, axes=['dim0'])
'aux0'
>>> f.cell_methods = cf.CellMethods('latitude: point')
>>> f.long_name = 'wind'
>>> print f
eastward_wind field summary
---------------------------
Data            : eastward_wind(latitude(10), longitude(9)) m s-1
Cell methods    : latitude: point
Dimensions      : latitude(10) = [0, ..., 9] degrees_north
                : longitude(9) = [0, ..., 8] degrees_east
Auxiliary coords: long_name:extra(latitude(10)) = ['alpha', ..., 'kappa'] 

Removing the auxiliary coordinate and the cell method that were just
added is also done with the :ref:`relevant method
<inserting-and-removing-components>` and by simple deletion
respectively:

>>> f.remove_item({'long_name': 'extra'})
<CF AuxiliaryCoordinate: long_name:extra(10)>
>>> del f.cell_methods
>>> print f
eastward_wind field summary
---------------------------
Data            : eastward_wind(latitude(10), longitude(9)) m s-1
Dimensions      : latitude(10) = [0, ..., 9] degrees_north
                : longitude(9) = [0, ..., 8] degrees_east

.. _fc-example4:

Example 4
~~~~~~~~~

.. highlight:: guess

A more complicated field is created by the following script. Note that
in this example the data and coordinates are generated using
`numpy.arange` simply for the sake of having some numbers to play
with. In practice it is likely the values would have been read from a
file in some arbitrary format::

   import cf
   import numpy
   
   #---------------------------------------------------------------------
   # 1. Create the field's domain items
   #---------------------------------------------------------------------
   # Create a grid_latitude dimension coordinate
   dim0 = cf.DimensionCoordinate(properties={'standard_name': 'grid_latitude'},
                                 data=cf.Data(numpy.arange(10.), 'degrees'))
   
   # Create a grid_longitude dimension coordinate
   dim1 = cf.DimensionCoordinate(data=cf.Data(numpy.arange(9.), 'degrees'))
   dim1.standard_name = 'grid_longitude'
   
   # Create a time dimension coordinate (with bounds)
   bounds = cf.CoordinateBounds(
   data=cf.Data([0.5, 1.5], cf.Units('days since 2000-1-1', calendar='noleap')))
   dim2 = cf.DimensionCoordinate(properties=dict(standard_name='time'),
                                 data=cf.Data(1, cf.Units('days since 2000-1-1',
                                                          calendar='noleap')),
                                 bounds=bounds)
   
   # Create a longitude auxiliary coordinate
   aux0 = cf.AuxiliaryCoordinate(data=cf.Data(numpy.arange(90).reshape(10, 9),
                                              'degrees_north'))
   aux0.standard_name = 'latitude'
   
   # Create a latitude auxiliary coordinate
   aux1 = cf.AuxiliaryCoordinate(properties=dict(standard_name='longitude'),
                                 data=cf.Data(numpy.arange(1, 91).reshape(9, 10),
                                              'degrees_east'))
   
   # Create a rotated_latitude_longitude grid mapping transform
   trans0 = cf.Transform(grid_mapping_name='rotated_latitude_longitude',
                         grid_north_pole_latitude=38.0,
                         grid_north_pole_longitude=190.0)
   
   # --------------------------------------------------------------------
   # 2. Create the field's domain from the previously created items
   # --------------------------------------------------------------------
   domain = cf.Domain(dim=[dim0, dim1, dim2],
                      aux=[aux0, aux1],
                      trans=trans0,
                      assign_axes={'aux1': ['dim1', 'dim0']})
   
   #---------------------------------------------------------------------
   # 3. Create the field
   #---------------------------------------------------------------------
   # Create CF properties
   properties = {'standard_name': 'eastward_wind',
                 'long_name'    : 'East Wind',
                 'cell_methods' : cf.CellMethods('latitude: point')}
   
   # Create the field's data array
   data = cf.Data(numpy.arange(90.).reshape(9, 10), 'm s-1')
   
   # Finally, create the field
   f = cf.Field(properties=properties,
                domain=domain,
                data=data,
                axes=domain.axes(['grid_long', 'grid_lat'], ordered=True))
   
   print "The new field:\n"
   print f

.. highlight:: none

Running this script produces the following output::

   The new field:
   
   eastward_wind field summary
   ---------------------------
   Data            : eastward_wind(grid_longitude(9), grid_latitude(10)) m s-1
   Cell methods    : latitude: point
   Dimensions      : grid_latitude(10) = [0, ..., 9] degrees
                   : grid_longitude(9) = [0, ..., 8] degrees
                   : time(1) = [1] days since 2000-1-1
   Auxiliary coords: latitude(grid_latitude(10), grid_longitude(9)) = [[0, ..., 89]] degrees_north
                   : longitude(grid_longitude(9), grid_latitude(10)) = [[1, ..., 90]] degrees_east
   Transforms      : <CF Transform: rotated_latitude_longitude>

.. highlight:: guess

Note that the default dimension order is not applicable to the
``aux1`` auxiliary coordinate nor the field's data array, but does
apply to the ``aux0`` auxiliary coordinate.

