Introduction
============

`CF
<http://cf-pcmdi.llnl.gov/documents/cf-conventions/1.6/cf-conventions.html>`_
is a `netCDF <http://www.unidata.ucar.edu/software/netcdf>`_
convention which is in wide and growing use for the storage of
model-generated and observational data relating to the atmosphere,
ocean and Earth system.

This package is an implementation of the `CF data model
<https://cf-pcmdi.llnl.gov/trac/ticket/95>`_, and as such it is an API
allows for the full scope of data and metadata interactions described
by the CF conventions.

With this package you can:

    * Read CF-netCDF and `PP
      <http://badc.nerc.ac.uk/help/formats/pp-format/>`_ format files,
      aggregating their contents into as few multidimensional fields
      as possible.

    * Write fields to CF-netCDF files on disk.

    * Create, delete and modify a field's data and metadata.

    * Subset fields by conditions on their metadata.

    * Subspace a field to create a new field.

    * Perform arithmetic and comparison operations with fields.

    * Calculate statisics of fields' data.

All of the above use :ref:`LAMA` functionality, which allows multiple
fields larger than the available memory to exist and be manipulated.

See the `cf-python home page <http://code.google.com/p/cf-python/>`_
for downloads, installation and source code.
