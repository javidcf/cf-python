.. cf-python documentation master file, created by
   sphinx-quickstart on Wed Aug  3 16:28:25 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. currentmodule:: cf
.. default-role:: obj

cf-python |release| documentation
=================================

.. toctree::
   :maxdepth: 2
   
   introduction

----

.. toctree::
   :maxdepth: 2

   getting_started

----

.. toctree::
   :maxdepth: 2

   reference 

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

