.. currentmodule:: cf
.. default-role:: obj


cf.Variable
===========

.. autoclass:: cf.Variable
   :no-members:
   :no-inherited-members:

Variable CF Properties
----------------------
 
.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.Variable.add_offset
   ~cf.Variable.calendar
   ~cf.Variable.comment
   ~cf.Variable._FillValue
   ~cf.Variable.history
   ~cf.Variable.leap_month
   ~cf.Variable.leap_year
   ~cf.Variable.long_name
   ~cf.Variable.missing_value
   ~cf.Variable.month_lengths
   ~cf.Variable.scale_factor
   ~cf.Variable.standard_name
   ~cf.Variable.units
   ~cf.Variable.valid_max
   ~cf.Variable.valid_min
   ~cf.Variable.valid_range


Variable attributes
-------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.Variable.array
   ~cf.Variable.attributes
   ~cf.Variable.data
   ~cf.Variable.day
   ~cf.Variable.dtarray
   ~cf.Variable.dtvarray
   ~cf.Variable.dtype
   ~cf.Variable.hour
   ~cf.Variable.hardmask
   ~cf.Variable.isscalar
   ~cf.Variable.mask
   ~cf.Variable.minute
   ~cf.Variable.month
   ~cf.Variable.ndim
   ~cf.Variable.properties
   ~cf.Variable.second
   ~cf.Variable.shape
   ~cf.Variable.size
   ~cf.Variable.subspace
   ~cf.Variable.unique
   ~cf.Variable.Units
   ~cf.Variable.varray
   ~cf.Variable.year

Variable methods
----------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.Variable.asdatetime
   ~cf.Variable.asreftime
   ~cf.Variable.binary_mask
   ~cf.Variable.chunk
   ~cf.Variable.clip
   ~cf.Variable.close
   ~cf.Variable.copy
   ~cf.Variable.cos
   ~cf.Variable.datum
   ~cf.Variable.delattr
   ~cf.Variable.delprop
   ~cf.Variable.dump
   ~cf.Variable.equals
   ~cf.Variable.expand_dims
   ~cf.Variable.fill_value
   ~cf.Variable.flip
   ~cf.Variable.getattr
   ~cf.Variable.getprop
   ~cf.Variable.hasattr
   ~cf.Variable.hasprop
   ~cf.Variable.identity
   ~cf.Variable.insert_data
   ~cf.Variable.mask_invalid
   ~cf.Variable.match
   ~cf.Variable.name
   ~cf.Variable.override_units
   ~cf.Variable.select
   ~cf.Variable.setattr
   ~cf.Variable.setdata
   ~cf.Variable.setprop
   ~cf.Variable.sin
   ~cf.Variable.squeeze
   ~cf.Variable.transpose
