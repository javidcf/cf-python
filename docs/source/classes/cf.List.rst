.. currentmodule:: cf
.. default-role:: obj

cf.List
=======

.. autoclass:: cf.List
   :no-members:
   :no-inherited-members:

List methods
------------
      
.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.List.copy
   ~cf.List.count
   ~cf.List.delattr
   ~cf.List.equals
   ~cf.List.getattr 
   ~cf.List.index
   ~cf.List.iter
   ~cf.List.method
   ~cf.List.setattr


List list-like methods
----------------------

These methods provide functionality exactly as their counterparts in a
built-in :py:obj:`list`.

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst
     
   ~cf.List.append
   ~cf.List.extend
   ~cf.List.insert
   ~cf.List.pop
   ~cf.List.remove
   ~cf.List.reverse
   ~cf.List.sort

