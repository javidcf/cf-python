.. currentmodule:: cf
.. default-role:: obj

cf.Domain
=========

.. autoclass:: cf.Domain
   :no-members:
   :no-inherited-members:

Domain methods
--------------
  
.. autosummary::
   :nosignatures:
   :toctree: ../generated/	
   :template: method.rst

   ~cf.Domain.analyse
   ~cf.Domain.attach_to_transform
   ~cf.Domain.axes
   ~cf.Domain.axis
   ~cf.Domain.axis_name
   ~cf.Domain.close
   ~cf.Domain.copy
   ~cf.Domain.data_axes
   ~cf.Domain.direction
   ~cf.Domain.directions
   ~cf.Domain.dump
   ~cf.Domain.dump_axes
   ~cf.Domain.dump_components
   ~cf.Domain.equal_transform
   ~cf.Domain.equals
   ~cf.Domain.equivalent
   ~cf.Domain.equivalent_transform
   ~cf.Domain.expand_dims
   ~cf.Domain.finalize
   ~cf.Domain.get
   ~cf.Domain.has
   ~cf.Domain.insert_aux
   ~cf.Domain.insert_axis
   ~cf.Domain.insert_cm
   ~cf.Domain.insert_coord
   ~cf.Domain.insert_dim
   ~cf.Domain.insert_transform
   ~cf.Domain.item
   ~cf.Domain.item_axes
   ~cf.Domain.items
   ~cf.Domain.map_axes
   ~cf.Domain.new_aux_identifier
   ~cf.Domain.new_axis_identifier
   ~cf.Domain.new_cm_identifier
   ~cf.Domain.new_transform_identifier
   ~cf.Domain.remove_axes
   ~cf.Domain.remove_axis
   ~cf.Domain.remove_item
   ~cf.Domain.remove_items
   ~cf.Domain.transform_axes
