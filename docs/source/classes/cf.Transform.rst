.. currentmodule:: cf
.. default-role:: obj

cf.Transform
============

.. autoclass:: cf.Transform
   :no-members:
   :no-inherited-members:

Transform methods
-----------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/	
   :template: method.rst

   ~cf.Transform.close
   ~cf.Transform.copy
   ~cf.Transform.dump
   ~cf.Transform.equals
   ~cf.Transform.equivalent
   ~cf.Transform.identity
   ~cf.Transform.match
   ~cf.Transform.remove_all_pointers
   ~cf.Transform.remove_pointer
   ~cf.Transform.rename_pointer

Transform dict-like methods
---------------------------

These methods provide functionality similar to that of a built-in
:py:obj:`dict`.

Undocumented methods behave exactly as their counterparts in a
built-in :py:obj:`dict`.

.. autosummary::
   :nosignatures:
   :toctree: ../generated/	
   :template: method.rst

   ~cf.Transform.clear
   ~cf.Transform.get
   ~cf.Transform.has_key
   ~cf.Transform.items
   ~cf.Transform.iteritems
   ~cf.Transform.iterkeys
   ~cf.Transform.itervalues
   ~cf.Transform.keys
   ~cf.Transform.pop
   ~cf.Transform.popitem
   ~cf.Transform.setdefault
   ~cf.Transform.update
   ~cf.Transform.values
