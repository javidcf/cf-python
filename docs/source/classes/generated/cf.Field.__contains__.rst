cf.Field.__contains__
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.__contains__