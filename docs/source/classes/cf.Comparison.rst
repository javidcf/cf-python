.. currentmodule:: cf
.. default-role:: obj

cf.Comparison
=============

.. autoclass:: cf.Comparison
   :no-members:
   :no-inherited-members:

Comparison methods
------------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~Comparison.addattr
   ~Comparison.copy	
   ~Comparison.dump	
   ~Comparison.evaluate
