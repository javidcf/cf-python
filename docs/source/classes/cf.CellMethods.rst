.. currentmodule:: cf
.. default-role:: obj

cf.CellMethods
==============

.. autoclass:: CellMethods
   :no-members:
   :no-inherited-members:

CellMethods methods
-------------------

Undocumented methods behave exactly as their counterparts in a
built-in list.
   
.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.CellMethods.copy
   ~cf.CellMethods.dump
   ~cf.CellMethods.equals
   ~cf.CellMethods.equivalent
   ~cf.CellMethods.has_cellmethod
   ~cf.CellMethods.netcdf_translation
   ~cf.CellMethods.parse
   ~cf.CellMethods.strings

CellMethods list-like methods
-----------------------------

These methods provide functionality exactly as their counterparts in a
built-in :py:obj:`list`.

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.CellMethods.append
   ~cf.CellMethods.count
   ~cf.CellMethods.extend
   ~cf.CellMethods.index
   ~cf.CellMethods.insert
   ~cf.CellMethods.pop
   ~cf.CellMethods.remove
   ~cf.CellMethods.reverse

