.. currentmodule:: cf
.. default-role:: obj


cf.CoordinateBounds
===================

.. autoclass:: cf.CoordinateBounds
   :no-members:
   :no-inherited-members:

CoordinateBounds CF Properties
------------------------------
 
.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.CoordinateBounds.add_offset
   ~cf.CoordinateBounds.calendar
   ~cf.CoordinateBounds.comment
   ~cf.CoordinateBounds._FillValue
   ~cf.CoordinateBounds.history
   ~cf.CoordinateBounds.leap_month
   ~cf.CoordinateBounds.leap_year
   ~cf.CoordinateBounds.long_name
   ~cf.CoordinateBounds.missing_value
   ~cf.CoordinateBounds.month_lengths
   ~cf.CoordinateBounds.scale_factor
   ~cf.CoordinateBounds.standard_name
   ~cf.CoordinateBounds.units
   ~cf.CoordinateBounds.valid_max
   ~cf.CoordinateBounds.valid_min
   ~cf.CoordinateBounds.valid_range

CoordinateBounds data attributes
--------------------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.CoordinateBounds.array
   ~cf.CoordinateBounds.data
   ~cf.CoordinateBounds.dtype
   ~cf.CoordinateBounds.hardmask
   ~cf.CoordinateBounds.isscalar
   ~cf.CoordinateBounds.mask
   ~cf.CoordinateBounds.ndim
   ~cf.CoordinateBounds.shape
   ~cf.CoordinateBounds.size
   ~cf.CoordinateBounds.Units
   ~cf.CoordinateBounds.varray

CoordinateBounds attributes
---------------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.CoordinateBounds.attributes
   ~cf.CoordinateBounds.day
   ~cf.CoordinateBounds.hour
   ~cf.CoordinateBounds.minute
   ~cf.CoordinateBounds.month
   ~cf.CoordinateBounds.properties
   ~cf.CoordinateBounds.second
   ~cf.CoordinateBounds.subspace
   ~cf.CoordinateBounds.year


CoordinateBounds methods
------------------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.CoordinateBounds.asdatetime
   ~cf.CoordinateBounds.asreftime
   ~cf.CoordinateBounds.binary_mask
   ~cf.CoordinateBounds.chunk
   ~cf.CoordinateBounds.clip
   ~cf.CoordinateBounds.close
   ~cf.CoordinateBounds.copy
   ~cf.CoordinateBounds.cos
   ~cf.CoordinateBounds.datum
   ~cf.CoordinateBounds.delattr
   ~cf.CoordinateBounds.delprop
   ~cf.CoordinateBounds.dump
   ~cf.CoordinateBounds.equals
   ~cf.CoordinateBounds.expand_dims
   ~cf.CoordinateBounds.fill_value
   ~cf.CoordinateBounds.flip
   ~cf.CoordinateBounds.getattr
   ~cf.CoordinateBounds.getprop
   ~cf.CoordinateBounds.hasattr
   ~cf.CoordinateBounds.hasprop
   ~cf.CoordinateBounds.identity
   ~cf.CoordinateBounds.insert_data
   ~cf.CoordinateBounds.mask_invalid
   ~cf.CoordinateBounds.match
   ~cf.CoordinateBounds.name
   ~cf.CoordinateBounds.override_units
   ~cf.CoordinateBounds.select
   ~cf.CoordinateBounds.setattr
   ~cf.CoordinateBounds.setdata
   ~cf.CoordinateBounds.setprop
   ~cf.CoordinateBounds.sin
   ~cf.CoordinateBounds.squeeze
   ~cf.CoordinateBounds.transpose
