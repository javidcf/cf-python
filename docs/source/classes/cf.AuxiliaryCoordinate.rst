.. currentmodule:: cf
.. default-role:: obj


cf.AuxiliaryCoordinate
======================

.. autoclass:: cf.AuxiliaryCoordinate
   :no-members:
   :no-inherited-members:

AuxiliaryCoordinate CF properties
---------------------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.AuxiliaryCoordinate.add_offset
   ~cf.AuxiliaryCoordinate.axis
   ~cf.AuxiliaryCoordinate.calendar
   ~cf.AuxiliaryCoordinate.comment
   ~cf.AuxiliaryCoordinate._FillValue
   ~cf.AuxiliaryCoordinate.history
   ~cf.AuxiliaryCoordinate.leap_month
   ~cf.AuxiliaryCoordinate.leap_year
   ~cf.AuxiliaryCoordinate.long_name
   ~cf.AuxiliaryCoordinate.missing_value
   ~cf.AuxiliaryCoordinate.month_lengths
   ~cf.AuxiliaryCoordinate.positive
   ~cf.AuxiliaryCoordinate.scale_factor
   ~cf.AuxiliaryCoordinate.standard_name
   ~cf.AuxiliaryCoordinate.units
   ~cf.AuxiliaryCoordinate.valid_max
   ~cf.AuxiliaryCoordinate.valid_min
   ~cf.AuxiliaryCoordinate.valid_range


AuxiliaryCoordinate attributes
------------------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.AuxiliaryCoordinate.array
   ~cf.AuxiliaryCoordinate.attributes
   ~cf.AuxiliaryCoordinate.bounds
   ~cf.AuxiliaryCoordinate.ctype
   ~cf.AuxiliaryCoordinate.data
   ~cf.AuxiliaryCoordinate.day
   ~cf.AuxiliaryCoordinate.dtarray
   ~cf.AuxiliaryCoordinate.dtvarray
   ~cf.AuxiliaryCoordinate.dtype
   ~cf.AuxiliaryCoordinate.hour
   ~cf.AuxiliaryCoordinate.hardmask
   ~cf.AuxiliaryCoordinate.hasbounds
   ~cf.AuxiliaryCoordinate.isauxiliary
   ~cf.AuxiliaryCoordinate.isdimension
   ~cf.AuxiliaryCoordinate.isscalar
   ~cf.AuxiliaryCoordinate.mask
   ~cf.AuxiliaryCoordinate.minute
   ~cf.AuxiliaryCoordinate.month
   ~cf.AuxiliaryCoordinate.ndim
   ~cf.AuxiliaryCoordinate.properties
   ~cf.AuxiliaryCoordinate.second
   ~cf.AuxiliaryCoordinate.shape
   ~cf.AuxiliaryCoordinate.size
   ~cf.AuxiliaryCoordinate.subspace
   ~cf.AuxiliaryCoordinate.T
   ~cf.AuxiliaryCoordinate.unique 
   ~cf.AuxiliaryCoordinate.Units
   ~cf.AuxiliaryCoordinate.varray
   ~cf.AuxiliaryCoordinate.X
   ~cf.AuxiliaryCoordinate.Y
   ~cf.AuxiliaryCoordinate.year
   ~cf.AuxiliaryCoordinate.Z

AuxiliaryCoordinate methods
---------------------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.AuxiliaryCoordinate.asauxiliary
   ~cf.AuxiliaryCoordinate.asdatetime
   ~cf.AuxiliaryCoordinate.asdimension
   ~cf.AuxiliaryCoordinate.asreftime
   ~cf.AuxiliaryCoordinate.binary_mask
   ~cf.AuxiliaryCoordinate.chunk
   ~cf.AuxiliaryCoordinate.clip
   ~cf.AuxiliaryCoordinate.close
   ~cf.AuxiliaryCoordinate.contiguous
   ~cf.AuxiliaryCoordinate.copy
   ~cf.AuxiliaryCoordinate.cos
   ~cf.AuxiliaryCoordinate.datum
   ~cf.AuxiliaryCoordinate.delattr
   ~cf.AuxiliaryCoordinate.delprop
   ~cf.AuxiliaryCoordinate.dump
   ~cf.AuxiliaryCoordinate.equals
   ~cf.AuxiliaryCoordinate.expand_dims
   ~cf.AuxiliaryCoordinate.fill_value
   ~cf.AuxiliaryCoordinate.flip
   ~cf.AuxiliaryCoordinate.getattr
   ~cf.AuxiliaryCoordinate.getprop
   ~cf.AuxiliaryCoordinate.hasattr
   ~cf.AuxiliaryCoordinate.hasprop
   ~cf.AuxiliaryCoordinate.identity
   ~cf.AuxiliaryCoordinate.insert_bounds
   ~cf.AuxiliaryCoordinate.insert_data
   ~cf.AuxiliaryCoordinate.mask_invalid
   ~cf.AuxiliaryCoordinate.match
   ~cf.AuxiliaryCoordinate.name
   ~cf.AuxiliaryCoordinate.override_units
   ~cf.AuxiliaryCoordinate.select
   ~cf.AuxiliaryCoordinate.setattr
   ~cf.AuxiliaryCoordinate.setdata
   ~cf.AuxiliaryCoordinate.setprop
   ~cf.AuxiliaryCoordinate.sin
   ~cf.AuxiliaryCoordinate.squeeze
   ~cf.AuxiliaryCoordinate.transpose
