.. currentmodule:: cf
.. default-role:: obj

cf.AncillaryVariables
=====================

.. autoclass:: cf.AncillaryVariables
   :no-members:
   :no-inherited-members:

AncillaryVariables attributes
-----------------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.AncillaryVariables.subspace

AncillaryVariables methods
--------------------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.AncillaryVariables.append
   ~cf.AncillaryVariables.aux
   ~cf.AncillaryVariables.auxs
   ~cf.AncillaryVariables.axes
   ~cf.AncillaryVariables.binary_mask
   ~cf.AncillaryVariables.chunk
   ~cf.AncillaryVariables.clip
   ~cf.AncillaryVariables.close
   ~cf.AncillaryVariables.cm
   ~cf.AncillaryVariables.cms
   ~cf.AncillaryVariables.coord
   ~cf.AncillaryVariables.coords
   ~cf.AncillaryVariables.copy
   ~cf.AncillaryVariables.cos
   ~cf.AncillaryVariables.count
   ~cf.AncillaryVariables.data_axes
   ~cf.AncillaryVariables.datum
   ~cf.AncillaryVariables.delattr
   ~cf.AncillaryVariables.delprop
   ~cf.AncillaryVariables.dim
   ~cf.AncillaryVariables.dims
   ~cf.AncillaryVariables.dump
   ~cf.AncillaryVariables.equals
   ~cf.AncillaryVariables.expand_dims
   ~cf.AncillaryVariables.fill_value
   ~cf.AncillaryVariables.extend
   ~cf.AncillaryVariables.flip
   ~cf.AncillaryVariables.getattr
   ~cf.AncillaryVariables.getprop
   ~cf.AncillaryVariables.hasattr
   ~cf.AncillaryVariables.hasprop
   ~cf.AncillaryVariables.identity
   ~cf.AncillaryVariables.index
   ~cf.AncillaryVariables.indices
   ~cf.AncillaryVariables.insert
   ~cf.AncillaryVariables.insert_data
   ~cf.AncillaryVariables.item
   ~cf.AncillaryVariables.item_axes
   ~cf.AncillaryVariables.items
   ~cf.AncillaryVariables.iter
   ~cf.AncillaryVariables.mask_invalid
   ~cf.AncillaryVariables.match
   ~cf.AncillaryVariables.method
   ~cf.AncillaryVariables.name
   ~cf.AncillaryVariables.override_units
   ~cf.AncillaryVariables.pop
   ~cf.AncillaryVariables.remove
   ~cf.AncillaryVariables.remove_axes
   ~cf.AncillaryVariables.remove_axis
   ~cf.AncillaryVariables.remove_data
   ~cf.AncillaryVariables.remove_item
   ~cf.AncillaryVariables.remove_items
   ~cf.AncillaryVariables.reverse
   ~cf.AncillaryVariables.select
   ~cf.AncillaryVariables.set_equals
   ~cf.AncillaryVariables.setattr
   ~cf.AncillaryVariables.setdata
   ~cf.AncillaryVariables.setprop
   ~cf.AncillaryVariables.sin
   ~cf.AncillaryVariables.sort
   ~cf.AncillaryVariables.squeeze
   ~cf.AncillaryVariables.subspace
   ~cf.AncillaryVariables.transform
   ~cf.AncillaryVariables.transforms
   ~cf.AncillaryVariables.transpose
   ~cf.AncillaryVariables.unsqueeze
