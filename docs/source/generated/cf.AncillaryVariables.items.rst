cf.AncillaryVariables.items
===========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.items