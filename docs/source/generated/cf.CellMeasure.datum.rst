cf.CellMeasure.datum
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMeasure.datum