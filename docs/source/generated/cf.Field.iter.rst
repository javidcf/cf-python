cf.Field.iter
=============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.iter