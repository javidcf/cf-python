cf.Transform.setdefault
=======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Transform.setdefault