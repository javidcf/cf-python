cf.DimensionCoordinate.binary_mask
==================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.DimensionCoordinate.binary_mask