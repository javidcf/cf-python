cf.Variable.asreftime
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.asreftime