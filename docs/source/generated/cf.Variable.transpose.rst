cf.Variable.transpose
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.transpose