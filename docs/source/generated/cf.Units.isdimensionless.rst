cf.Units.isdimensionless
========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Units.isdimensionless