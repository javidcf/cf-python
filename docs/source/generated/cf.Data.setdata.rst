cf.Data.setdata
===============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Data.setdata