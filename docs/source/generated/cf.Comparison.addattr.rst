cf.Comparison.addattr
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Comparison.addattr