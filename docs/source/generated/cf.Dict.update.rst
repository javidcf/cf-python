cf.Dict.update
==============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Dict.update