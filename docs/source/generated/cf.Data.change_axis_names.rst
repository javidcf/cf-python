cf.Data.change_axis_names
=========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Data.change_axis_names