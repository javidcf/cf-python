cf.Field.equivalent_data
========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.equivalent_data