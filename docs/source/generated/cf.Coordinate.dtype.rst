cf.Coordinate.dtype
===================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.dtype