cf.Coordinate.dtarray
=====================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.dtarray