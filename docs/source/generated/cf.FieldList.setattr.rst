cf.FieldList.setattr
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.setattr