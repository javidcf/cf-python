cf.FieldList.flag_values
========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.FieldList.flag_values