cf.Domain.get
=============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.get