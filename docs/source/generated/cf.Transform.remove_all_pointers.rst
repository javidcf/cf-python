cf.Transform.remove_all_pointers
================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Transform.remove_all_pointers