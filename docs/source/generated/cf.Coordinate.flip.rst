cf.Coordinate.flip
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.flip