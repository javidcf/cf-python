cf.CoordinateBounds.valid_range
===============================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CoordinateBounds.valid_range