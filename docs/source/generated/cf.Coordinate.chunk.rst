cf.Coordinate.chunk
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.chunk