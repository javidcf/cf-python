cf.Transform.equivalent
=======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Transform.equivalent