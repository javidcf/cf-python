cf.Coordinate.datum
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.datum