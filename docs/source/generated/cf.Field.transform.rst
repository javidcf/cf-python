cf.Field.transform
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.transform