cf.CellMeasure.dump_simple_properties
=====================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMeasure.dump_simple_properties