cf.Coordinate.isauxiliary
=========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.isauxiliary