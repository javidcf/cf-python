cf.Domain.new_cm_identifier
===========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.new_cm_identifier