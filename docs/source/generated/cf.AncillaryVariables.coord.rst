cf.AncillaryVariables.coord
===========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.coord