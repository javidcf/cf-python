cf.List.delattr
===============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.List.delattr