cf.CoordinateBounds.clip
========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.clip