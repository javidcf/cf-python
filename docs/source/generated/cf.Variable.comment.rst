cf.Variable.comment
===================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Variable.comment