cf.Coordinate.long_name
=======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.long_name