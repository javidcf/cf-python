cf.FieldList.items
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.items