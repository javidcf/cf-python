cf.Field.setprop
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.setprop