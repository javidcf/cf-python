cf.Dict.iteritems
=================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Dict.iteritems