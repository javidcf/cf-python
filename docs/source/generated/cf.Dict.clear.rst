cf.Dict.clear
=============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Dict.clear