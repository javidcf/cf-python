cf.CoordinateBounds.asdatetime
==============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.asdatetime