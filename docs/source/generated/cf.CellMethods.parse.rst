cf.CellMethods.parse
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMethods.parse