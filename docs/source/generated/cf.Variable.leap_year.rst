cf.Variable.leap_year
=====================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Variable.leap_year