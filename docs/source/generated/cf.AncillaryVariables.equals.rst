cf.AncillaryVariables.equals
============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.equals