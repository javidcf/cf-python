cf.DimensionCoordinate.unique
=============================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.DimensionCoordinate.unique