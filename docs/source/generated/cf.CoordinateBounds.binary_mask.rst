cf.CoordinateBounds.binary_mask
===============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.binary_mask