cf.Transform.close
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Transform.close