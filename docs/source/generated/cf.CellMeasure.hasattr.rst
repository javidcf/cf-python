cf.CellMeasure.hasattr
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMeasure.hasattr