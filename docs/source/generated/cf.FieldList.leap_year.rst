cf.FieldList.leap_year
======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.FieldList.leap_year