cf.Transform.identity
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Transform.identity