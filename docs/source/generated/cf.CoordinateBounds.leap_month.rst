cf.CoordinateBounds.leap_month
==============================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CoordinateBounds.leap_month