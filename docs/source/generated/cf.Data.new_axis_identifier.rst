cf.Data.new_axis_identifier
===========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Data.new_axis_identifier