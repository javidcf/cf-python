cf.Domain.remove_item
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.remove_item