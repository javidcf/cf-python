cf.Transform.items
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Transform.items