cf.Coordinate.valid_max
=======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.valid_max