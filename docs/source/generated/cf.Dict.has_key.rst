cf.Dict.has_key
===============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Dict.has_key