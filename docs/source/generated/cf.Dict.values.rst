cf.Dict.values
==============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Dict.values