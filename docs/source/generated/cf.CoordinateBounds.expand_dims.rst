cf.CoordinateBounds.expand_dims
===============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.expand_dims