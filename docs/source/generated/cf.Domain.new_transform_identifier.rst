cf.Domain.new_transform_identifier
==================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.new_transform_identifier