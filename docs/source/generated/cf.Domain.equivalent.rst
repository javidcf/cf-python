cf.Domain.equivalent
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.equivalent