cf.List.reverse
===============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.List.reverse