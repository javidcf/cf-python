cf.FieldList.dims
=================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.dims