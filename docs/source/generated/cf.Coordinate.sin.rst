cf.Coordinate.sin
=================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.sin