cf.Variable.dump
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.dump