cf.Domain.remove_items
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.remove_items