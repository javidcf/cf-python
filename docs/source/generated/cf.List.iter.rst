cf.List.iter
============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.List.iter