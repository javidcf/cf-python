cf.Transform.itervalues
=======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Transform.itervalues