cf.Coordinate.expand_dims
=========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.expand_dims