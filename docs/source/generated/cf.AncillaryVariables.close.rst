cf.AncillaryVariables.close
===========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.close