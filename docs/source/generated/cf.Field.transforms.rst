cf.Field.transforms
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.transforms