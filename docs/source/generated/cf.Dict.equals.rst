cf.Dict.equals
==============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Dict.equals