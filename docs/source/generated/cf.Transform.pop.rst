cf.Transform.pop
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Transform.pop