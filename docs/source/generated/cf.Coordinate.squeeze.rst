cf.Coordinate.squeeze
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.squeeze