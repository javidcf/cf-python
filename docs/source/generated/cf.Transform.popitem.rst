cf.Transform.popitem
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Transform.popitem