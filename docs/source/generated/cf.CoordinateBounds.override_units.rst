cf.CoordinateBounds.override_units
==================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.override_units