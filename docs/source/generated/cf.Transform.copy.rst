cf.Transform.copy
=================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Transform.copy