cf.FieldList.remove_axis
========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.remove_axis