cf.Variable.fill_value
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.fill_value