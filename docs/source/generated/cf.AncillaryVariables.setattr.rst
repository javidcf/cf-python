cf.AncillaryVariables.setattr
=============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.setattr