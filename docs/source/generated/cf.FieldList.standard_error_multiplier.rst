cf.FieldList.standard_error_multiplier
======================================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.FieldList.standard_error_multiplier