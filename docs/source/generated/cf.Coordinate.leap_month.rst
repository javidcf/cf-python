cf.Coordinate.leap_month
========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.leap_month