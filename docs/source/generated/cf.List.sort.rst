cf.List.sort
============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.List.sort