cf.AncillaryVariables.pop
=========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.pop