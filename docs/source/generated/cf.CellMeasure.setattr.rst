cf.CellMeasure.setattr
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMeasure.setattr