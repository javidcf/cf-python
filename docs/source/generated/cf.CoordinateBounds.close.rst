cf.CoordinateBounds.close
=========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.close