cf.FieldList.flag_masks
=======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.FieldList.flag_masks