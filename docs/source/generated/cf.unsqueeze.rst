cf.unsqueeze
============

.. currentmodule:: cf
.. default-role:: obj

.. autofunction:: cf.unsqueeze