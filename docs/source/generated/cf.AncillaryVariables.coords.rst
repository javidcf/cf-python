cf.AncillaryVariables.coords
============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.coords