cf.Field.datum
==============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.datum