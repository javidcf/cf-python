cf.Coordinate.isscalar
======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.isscalar