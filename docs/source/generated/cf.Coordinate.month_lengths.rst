cf.Coordinate.month_lengths
===========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.month_lengths