cf.Domain.transform_axes
========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.transform_axes