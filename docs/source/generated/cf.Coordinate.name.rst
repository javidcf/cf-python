cf.Coordinate.name
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.name