cf.Field.unique
===============

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Field.unique