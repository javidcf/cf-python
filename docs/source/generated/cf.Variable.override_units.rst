cf.Variable.override_units
==========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.override_units