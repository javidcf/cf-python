cf.Field.method
===============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.method