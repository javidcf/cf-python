cf.FieldList.getattr
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.getattr