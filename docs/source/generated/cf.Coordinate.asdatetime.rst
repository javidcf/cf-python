cf.Coordinate.asdatetime
========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.asdatetime