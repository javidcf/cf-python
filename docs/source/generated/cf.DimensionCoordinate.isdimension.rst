cf.DimensionCoordinate.isdimension
==================================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.DimensionCoordinate.isdimension