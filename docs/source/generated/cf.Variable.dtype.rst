cf.Variable.dtype
=================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Variable.dtype