cf.AuxiliaryCoordinate.isdimension
==================================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.AuxiliaryCoordinate.isdimension