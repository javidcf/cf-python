cf.FieldList.expand_dims
========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.expand_dims