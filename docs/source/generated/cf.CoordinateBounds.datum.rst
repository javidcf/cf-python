cf.CoordinateBounds.datum
=========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.datum