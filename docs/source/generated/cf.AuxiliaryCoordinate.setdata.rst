cf.AuxiliaryCoordinate.setdata
==============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AuxiliaryCoordinate.setdata