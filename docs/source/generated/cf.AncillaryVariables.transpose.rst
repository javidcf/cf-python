cf.AncillaryVariables.transpose
===============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.transpose