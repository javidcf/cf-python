cf.AncillaryVariables.remove_axis
=================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.remove_axis