cf.DimensionCoordinate.asauxiliary
==================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.DimensionCoordinate.asauxiliary