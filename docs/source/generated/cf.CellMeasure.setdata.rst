cf.CellMeasure.setdata
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMeasure.setdata