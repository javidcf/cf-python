cf.AncillaryVariables.squeeze
=============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.squeeze