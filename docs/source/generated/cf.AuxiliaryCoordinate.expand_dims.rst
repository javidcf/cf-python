cf.AuxiliaryCoordinate.expand_dims
==================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AuxiliaryCoordinate.expand_dims