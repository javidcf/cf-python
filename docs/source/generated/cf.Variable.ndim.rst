cf.Variable.ndim
================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Variable.ndim