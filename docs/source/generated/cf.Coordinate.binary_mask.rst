cf.Coordinate.binary_mask
=========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.binary_mask