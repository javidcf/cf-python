cf.Domain.new_axis_identifier
=============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.new_axis_identifier