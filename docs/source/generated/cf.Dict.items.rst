cf.Dict.items
=============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Dict.items