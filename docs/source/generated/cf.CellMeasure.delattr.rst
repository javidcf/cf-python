cf.CellMeasure.delattr
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMeasure.delattr