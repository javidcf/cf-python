cf.Variable.flip
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.flip