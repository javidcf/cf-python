cf.FieldList.insert_data
========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.insert_data