cf.Comparison.evaluate
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Comparison.evaluate