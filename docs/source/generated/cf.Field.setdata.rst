cf.Field.setdata
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.setdata