cf.Coordinate.dump_simple_properties
====================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.dump_simple_properties