cf.List.getattr
===============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.List.getattr