cf.Variable.getattr
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.getattr