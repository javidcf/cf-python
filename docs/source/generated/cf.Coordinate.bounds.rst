cf.Coordinate.bounds
====================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.bounds