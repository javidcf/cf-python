cf.Domain.finalize
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.finalize