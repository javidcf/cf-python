cf.DimensionCoordinate.delattr
==============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.DimensionCoordinate.delattr