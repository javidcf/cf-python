cf.Variable.datum
=================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.datum