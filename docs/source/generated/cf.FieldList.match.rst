cf.FieldList.match
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.match