cf.AncillaryVariables.override_units
====================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.override_units