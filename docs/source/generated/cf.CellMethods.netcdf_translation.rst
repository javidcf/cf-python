cf.CellMethods.netcdf_translation
=================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMethods.netcdf_translation