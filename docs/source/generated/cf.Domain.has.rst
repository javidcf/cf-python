cf.Domain.has
=============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.has