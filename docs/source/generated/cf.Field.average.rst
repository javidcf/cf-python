cf.Field.average
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.average