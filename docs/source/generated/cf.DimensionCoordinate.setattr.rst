cf.DimensionCoordinate.setattr
==============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.DimensionCoordinate.setattr