cf.Variable.hasprop
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.hasprop