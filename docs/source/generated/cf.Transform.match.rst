cf.Transform.match
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Transform.match