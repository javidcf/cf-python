cf.Variable.insert_data
=======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.insert_data