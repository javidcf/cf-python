cf.Coordinate.minute
====================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.minute