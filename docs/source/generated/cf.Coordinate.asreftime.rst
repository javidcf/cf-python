cf.Coordinate.asreftime
=======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.asreftime