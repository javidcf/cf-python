cf.Variable.match
=================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.match