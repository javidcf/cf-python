cf.AuxiliaryCoordinate.asdimension
==================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AuxiliaryCoordinate.asdimension