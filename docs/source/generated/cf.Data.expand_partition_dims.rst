cf.Data.expand_partition_dims
=============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Data.expand_partition_dims