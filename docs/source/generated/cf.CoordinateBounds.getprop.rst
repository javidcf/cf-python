cf.CoordinateBounds.getprop
===========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.getprop