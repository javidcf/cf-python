cf.Coordinate.setattr
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.setattr