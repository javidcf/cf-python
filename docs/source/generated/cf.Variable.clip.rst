cf.Variable.clip
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.clip