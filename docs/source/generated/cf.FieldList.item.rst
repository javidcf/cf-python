cf.FieldList.item
=================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.item