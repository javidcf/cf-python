cf.Variable.missing_value
=========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Variable.missing_value