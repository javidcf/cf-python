cf.Field.finalize
=================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.finalize