cf.FieldList.squeeze
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.squeeze