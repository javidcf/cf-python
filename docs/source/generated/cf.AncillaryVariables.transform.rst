cf.AncillaryVariables.transform
===============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.transform