cf.Transform.remove_pointer
===========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Transform.remove_pointer