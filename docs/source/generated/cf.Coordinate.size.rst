cf.Coordinate.size
==================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.size