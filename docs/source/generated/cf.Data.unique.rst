cf.Data.unique
==============

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Data.unique