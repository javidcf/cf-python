cf.Dict.copy
============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Dict.copy