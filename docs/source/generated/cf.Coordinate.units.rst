cf.Coordinate.units
===================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.units