cf.Data.conform_args
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Data.conform_args