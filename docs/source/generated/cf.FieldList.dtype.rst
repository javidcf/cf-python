cf.FieldList.dtype
==================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.FieldList.dtype