cf.AncillaryVariables.delprop
=============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.delprop