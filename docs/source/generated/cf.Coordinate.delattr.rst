cf.Coordinate.delattr
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.delattr