cf.Dict.setdefault
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Dict.setdefault