cf.Variable.hasattr
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.hasattr