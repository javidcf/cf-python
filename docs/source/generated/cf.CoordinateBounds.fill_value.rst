cf.CoordinateBounds.fill_value
==============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.fill_value