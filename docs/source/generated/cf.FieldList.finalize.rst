cf.FieldList.finalize
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.finalize