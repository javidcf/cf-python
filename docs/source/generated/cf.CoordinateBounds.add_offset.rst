cf.CoordinateBounds.add_offset
==============================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CoordinateBounds.add_offset