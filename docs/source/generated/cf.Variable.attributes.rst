cf.Variable.attributes
======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Variable.attributes