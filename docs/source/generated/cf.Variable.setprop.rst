cf.Variable.setprop
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.setprop