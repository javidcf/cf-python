cf.Coordinate.hardmask
======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.hardmask