cf.Transform.clear
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Transform.clear