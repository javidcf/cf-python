cf.Data.binary_mask
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Data.binary_mask