cf.Coordinate.setdata
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.setdata