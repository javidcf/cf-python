cf.Units.copy
=============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Units.copy