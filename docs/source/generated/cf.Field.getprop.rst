cf.Field.getprop
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.getprop