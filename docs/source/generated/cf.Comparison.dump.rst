cf.Comparison.dump
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Comparison.dump