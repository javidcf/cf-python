cf.Data.unsafe_array
====================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Data.unsafe_array