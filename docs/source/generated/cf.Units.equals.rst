cf.Units.equals
===============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Units.equals