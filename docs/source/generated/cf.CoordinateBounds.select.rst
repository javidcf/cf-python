cf.CoordinateBounds.select
==========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.select