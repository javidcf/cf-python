cf.FieldList.hasattr
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.hasattr