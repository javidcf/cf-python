cf.Transform.equals
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Transform.equals