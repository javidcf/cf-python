cf.Coordinate.select
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.select