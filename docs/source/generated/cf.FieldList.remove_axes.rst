cf.FieldList.remove_axes
========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.remove_axes