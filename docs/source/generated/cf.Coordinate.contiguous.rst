cf.Coordinate.contiguous
========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.contiguous