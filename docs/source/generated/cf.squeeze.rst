cf.squeeze
==========

.. currentmodule:: cf
.. default-role:: obj

.. autofunction:: cf.squeeze