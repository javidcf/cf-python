cf.AncillaryVariables.expand_dims
=================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.expand_dims