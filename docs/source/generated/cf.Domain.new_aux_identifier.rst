cf.Domain.new_aux_identifier
============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.new_aux_identifier