cf.Data.all_axis_names
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Data.all_axis_names