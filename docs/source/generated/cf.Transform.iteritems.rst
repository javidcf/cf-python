cf.Transform.iteritems
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Transform.iteritems