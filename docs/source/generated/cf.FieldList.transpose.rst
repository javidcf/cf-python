cf.FieldList.transpose
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.transpose