cf.CoordinateBounds.insert_data
===============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.insert_data