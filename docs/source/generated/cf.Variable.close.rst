cf.Variable.close
=================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.close