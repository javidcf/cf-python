cf.Units.ispressure
===================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Units.ispressure