cf.Field.expand_dims
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.expand_dims