cf.AncillaryVariables.remove_item
=================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.remove_item