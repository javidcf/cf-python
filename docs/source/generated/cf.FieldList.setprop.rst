cf.FieldList.setprop
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.setprop