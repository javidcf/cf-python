cf.Coordinate.asauxiliary
=========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.asauxiliary