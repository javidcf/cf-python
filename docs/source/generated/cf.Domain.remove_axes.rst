cf.Domain.remove_axes
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.remove_axes