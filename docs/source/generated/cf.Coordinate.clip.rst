cf.Coordinate.clip
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.clip