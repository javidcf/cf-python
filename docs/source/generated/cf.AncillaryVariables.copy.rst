cf.AncillaryVariables.copy
==========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.copy