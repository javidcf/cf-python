cf.CellMethods.pop
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMethods.pop