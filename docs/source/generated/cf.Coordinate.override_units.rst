cf.Coordinate.override_units
============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.override_units