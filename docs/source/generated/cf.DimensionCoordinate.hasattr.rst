cf.DimensionCoordinate.hasattr
==============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.DimensionCoordinate.hasattr