cf.Data.average
===============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Data.average