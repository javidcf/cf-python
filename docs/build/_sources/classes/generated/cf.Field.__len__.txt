cf.Field.__len__
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.__len__