cf.CoordinateBounds.Units
=========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CoordinateBounds.Units