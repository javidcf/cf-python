cf.CoordinateBounds.mask_invalid
================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.mask_invalid