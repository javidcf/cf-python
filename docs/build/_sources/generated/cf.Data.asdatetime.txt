cf.Data.asdatetime
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Data.asdatetime