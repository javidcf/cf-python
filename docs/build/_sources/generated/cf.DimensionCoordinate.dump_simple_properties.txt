cf.DimensionCoordinate.dump_simple_properties
=============================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.DimensionCoordinate.dump_simple_properties