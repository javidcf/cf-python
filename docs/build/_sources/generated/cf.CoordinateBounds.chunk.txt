cf.CoordinateBounds.chunk
=========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.chunk