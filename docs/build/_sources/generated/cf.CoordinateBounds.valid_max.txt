cf.CoordinateBounds.valid_max
=============================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CoordinateBounds.valid_max