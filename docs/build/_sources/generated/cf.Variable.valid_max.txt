cf.Variable.valid_max
=====================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Variable.valid_max