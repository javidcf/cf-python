cf.DimensionCoordinate.hasprop
==============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.DimensionCoordinate.hasprop