cf.unpickle
===========

.. currentmodule:: cf
.. default-role:: obj

.. autofunction:: cf.unpickle