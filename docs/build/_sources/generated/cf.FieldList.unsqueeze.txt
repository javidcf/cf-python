cf.FieldList.unsqueeze
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.unsqueeze