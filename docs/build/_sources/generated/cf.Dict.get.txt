cf.Dict.get
===========

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Dict.get