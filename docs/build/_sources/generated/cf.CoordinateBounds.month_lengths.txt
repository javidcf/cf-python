cf.CoordinateBounds.month_lengths
=================================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CoordinateBounds.month_lengths