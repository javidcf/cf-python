cf.expand_dims
==============

.. currentmodule:: cf
.. default-role:: obj

.. autofunction:: cf.expand_dims