import tempfile
import os
import sys
import itertools
from operator import mul
import numpy
import cf
import unittest
import inspect

class FieldTest(unittest.TestCase):
    filename = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                            'test_file.nc')
    chunk_sizes = (17, 34, 300, 100000)[::-1]
    original_chunksize = cf.CHUNKSIZE()

    f = cf.read(filename)[0]  

    test_only = ()
    test_only = ('test_Field_anchor',)

    def test_Field_equals(self):
        if self.test_only and inspect.stack()[0][3] not in self.test_only:
            return
        for chunksize in self.chunk_sizes:    
            cf.CHUNKSIZE(chunksize)
            f = cf.read(self.filename)[0]       
            g = f.copy()
            self.assertTrue(f.equals(g, traceback=True))
            self.assertFalse(f.equals(g+1, traceback=False))
        #--- End: for    
        cf.CHUNKSIZE(self.original_chunksize)
    #--- End: def

#    def test_Field_anchor(self):    
#        print
#        if self.test_only and inspect.stack()[0][3] not in self.test_only:
#            return
#        dimarray = self.f.dim('grid_lon').array
#      
#        for chunksize in self.chunk_sizes:            
#            f = cf.read(self.filename)[0]
#
#            for period in (dimarray.min()-5, dimarray.min()):
#                anchors = numpy.arange(dimarray.min()-3*period,
#                                       dimarray.max()+3*period, 0.5)
#
#                f.cyclic('grid_lon', period=period)
#
#                for anchor in anchors:
#                    print 'period, anchor=',period, anchor
#                    g = f.anchor('grid_lon', anchor)
#                    x0, x1 = g.coord('grid_lon').array[0:2]
#                    self.assertTrue(x0 <= anchor < x1,
#                                    "INCREASING period=%s, x0=%s, anchor=%s, x1=%s" % \
#                                    (period, x0, anchor, x1))
#                    print'oooooooooooookkkkkkkkkkkkkkk', period, x0, anchor, x1
#    
#                f.flip('grid_lon', i=True)
#                for anchor in anchors:
#                    print'anchor=',anchor
#                    g = f.anchor('grid_lon', anchor)
#                    x0, x1 = g.coord('grid_lon').array[0:2]
#                    self.assertTrue(x0 >= anchor > x1,
#                                    "DECREASING: x0=%s, anchor=%s, x1=%s" % \
#                                    (x0, anchor, x1))
#        #--- End: for    
#        cf.CHUNKSIZE(self.original_chunksize)
#    #--- End: def

    def test_Field_transpose(self):
        if self.test_only and inspect.stack()[0][3] not in self.test_only:
            return
        for chunksize in self.chunk_sizes:            
            cf.CHUNKSIZE(chunksize)            
            f = cf.read(self.filename)[0]
            
            h = f.transpose((1, 2, 0))
            h.transpose((2, 0, 1), i=True)
            h.transpose(('grid_longitude', 'atmos', 'grid_latitude'), i=True)
            h.varray
            h.transpose(('atmos', 'grid_latitude', 'grid_longitude'), i=True)
            self.assertTrue(cf.equals(f, h, traceback=True))
#            print "pmshape =", f.Data._pmshape
        #--- End: for    
        cf.CHUNKSIZE(self.original_chunksize)
    #--- End: def

    def test_Field_match(self):
        if self.test_only and inspect.stack()[0][3] not in self.test_only:
            return
        f = cf.read(self.filename)[0]
        f.long_name = 'qwerty'
        all_kwargs = (
            {'inverse': False}, 
            {'inverse': False, 'match': 'eastward_wind'},
            {'inverse': False, 'match': {'long_name': 'qwerty'}},
            {'inverse': False, 'match': {None: 'east'}},
            {'inverse': False, 'match': 'east'},
            {'inverse': False, 'match': {None: 'east.*'}, 'regex': True},
            {'inverse': False, 'match': 'east.*', 'regex': True},
            {'inverse': False, 'match': cf.eq('east.*', regex=True)},
            {'inverse': False, 'match': {None: cf.eq('east.*', regex=True)}},
            {'inverse': False, 'match': {None: 'east', 'long_name': 'qwe'}},
            {'inverse': False, 'match': {None: 'east', 'long_name': 'qwe'}, 'match_all': False},
            {'inverse': False, 'match': {None: 'east', 'long_name': 'asd'}, 'match_all': False},
            #
            {'inverse': True, 'match': {None: 'east', 'long_name': 'asd'},},
        )
        for kwargs in all_kwargs:
            self.assertTrue(f.match(**kwargs), 
                            "f.match(**%s) failed" % kwargs)
            kwargs['inverse'] = not kwargs['inverse']
            self.assertFalse(f.match(**kwargs),
                             "f.match(**%s) failed" % kwargs)
        #--- End: for
    #--- End: def

#--- End: class

if __name__ == '__main__':
    print 'cf-python version:', cf.__version__
    print 'cf-python path:'   , os.path.abspath(cf.__file__)
    unittest.main(verbosity=2)
