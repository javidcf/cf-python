import cf
import datetime
import numpy
import os
import unittest

class ComparisonTest(unittest.TestCase):
    filename = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                            'test_file.nc')
    chunk_sizes = (17, 34, 300, 100000)[::-1]
    original_chunksize = cf.CHUNKSIZE()
    d= 9
    def test_Comparison_datetime1(self):
        for chunksize in self.chunk_sizes:    
            cf.CHUNKSIZE(chunksize)
            d = cf.Data([[1., 5.], [6, 2]], 'days since 2000-12-29 21:57:57')   
            self.assertTrue((d==cf.eq(cf.dt('2001-01-03 21:57:57'))).equals(cf.Data([[False, True], [False, False]])))
            self.assertTrue((d==cf.ne(cf.dt('2001-01-03 21:57:57'))).equals(cf.Data([[True, False], [True, True]])))
            self.assertTrue((d==cf.ge(cf.dt('2001-01-03 21:57:57'))).equals(cf.Data([[False, True], [True, False]])))
            self.assertTrue((d==cf.gt(cf.dt('2001-01-03 21:57:57'))).equals(cf.Data([[False, False], [True, False]])))
            self.assertTrue((d==cf.le(cf.dt('2001-01-03 21:57:57'))).equals(cf.Data([[True, True], [False, True]])))
            self.assertTrue((d==cf.lt(cf.dt('2001-01-03 21:57:57'))).equals(cf.Data([[True, False], [False, True]]))) 
            self.assertTrue((d==cf.wi(cf.dt('2000-12-31 21:57:57 21:57:57'), cf.dt('2001-01-03 21:57:57'))).equals(cf.Data([[False, True], [False, True]]))) 
            self.assertTrue((d==cf.wo(cf.dt('2000-12-31 21:57:57 21:57:57'), cf.dt('2001-01-03 21:57:57'))).equals(cf.Data([[True, False], [True, False]]))) 
            self.assertTrue((d==cf.set([cf.dt('2000-12-31 21:57:57 21:57:57'), cf.dt('2001-01-03 21:57:57')])).equals(cf.Data([[False, True], [False, True]]))) 

        cf.CHUNKSIZE(self.original_chunksize)
    #--- End: def

    def test_year_month_day_hour_minute_second(self):
        for chunksize in self.chunk_sizes:    
            cf.CHUNKSIZE(chunksize)
            d = cf.Data([[1., 5.], [6, 2]], 'days since 2000-12-29 21:57:57')   
            self.assertTrue((d==cf.year(2000)).equals(cf.Data([[True, False], [False, True]])))
            self.assertTrue((d==cf.month(12)).equals(cf.Data([[True, False], [False, True]])))
            self.assertTrue((d==cf.day(3)).equals(cf.Data([[False, True], [False, False]])))
            d = cf.Data([[1., 5], [6, 2]], 'hours since 2000-12-29 21:57:57')
            self.assertTrue((d==cf.hour(2)).equals(cf.Data([[False, True], [False, False]])))
            d = cf.Data([[1., 5], [6, 2]], 'minutes since 2000-12-29 21:57:57')
            self.assertTrue((d==cf.minute(2)).equals(cf.Data([[False, True], [False, False]])))
            d = cf.Data([[1., 5], [6, 2]], 'seconds since 2000-12-29 21:57:57')
            self.assertTrue((d==cf.second(2)).equals(cf.Data([[False, True], [False, False]]))) 
        
            d = cf.Data([[1., 5.], [6, 2]], 'days since 2000-12-29 21:57:57')   
            self.assertTrue((d==cf.year(cf.ne(-1))).equals(cf.Data([[True, True], [True, True]])))
            self.assertTrue((d==cf.month(cf.ne(-1))).equals(cf.Data([[True, True], [True, True]])))
            self.assertTrue((d==cf.day(cf.ne(-1))).equals(cf.Data([[True, True], [True, True]])))
            d = cf.Data([[1., 5], [6, 2]], 'hours since 2000-12-29 21:57:57')
            self.assertTrue((d==cf.hour(cf.ne(-1))).equals(cf.Data([[True, True], [True, True]])))
            d = cf.Data([[1., 5], [6, 2]], 'minutes since 2000-12-29 21:57:57')
            self.assertTrue((d==cf.minute(cf.ne(-1))).equals(cf.Data([[True, True], [True, True]])))
            d = cf.Data([[1., 5], [6, 2]], 'seconds since 2000-12-29 21:57:57')
            self.assertTrue((d==cf.second(cf.ne(-1))).equals(cf.Data([[True, True], [True, True]]))) 
        #--- End: def
        cf.CHUNKSIZE(self.original_chunksize)  
    #--- End: def

    def test_dteq_dtne_dtge_dtgt_dtle_dtlt(self):
        for chunksize in self.chunk_sizes:    
            cf.CHUNKSIZE(chunksize)
            d = cf.Data([[1., 5.], [6, 2]], 'days since 2000-12-29 21:57:57')   
            self.assertTrue((d==cf.dteq('2001-01-03 21:57:57')).equals(cf.Data([[False, True], [False, False]])))
            self.assertTrue((d==cf.dtne('2001-01-03 21:57:57')).equals(cf.Data([[True, False], [True, True]])))
            self.assertTrue((d==cf.dtge('2001-01-03 21:57:57')).equals(cf.Data([[False, True], [True, False]])))
            self.assertTrue((d==cf.dtgt('2001-01-03 21:57:57')).equals(cf.Data([[False, False], [True, False]])))
            self.assertTrue((d==cf.dtle('2001-01-03 21:57:57')).equals(cf.Data([[True, True], [False, True]])))
            self.assertTrue((d==cf.dtlt('2001-01-03 21:57:57')).equals(cf.Data([[True, False], [False, True]]))) 
        
            self.assertTrue((d==cf.dteq(2001, 1, 3, 21, 57, 57)).equals(cf.Data([[False, True], [False, False]])))
            self.assertTrue((d==cf.dtne(2001, 1, 3, 21, 57, 57)).equals(cf.Data([[True, False], [True, True]])))
            self.assertTrue((d==cf.dtge(2001, 1, 3, 21, 57, 57)).equals(cf.Data([[False, True], [True, False]])))
            self.assertTrue((d==cf.dtgt(2001, 1, 3, 21, 57, 57)).equals(cf.Data([[False, False], [True, False]])))
            self.assertTrue((d==cf.dtle(2001, 1, 3, 21, 57, 57)).equals(cf.Data([[True, True], [False, True]])))
            self.assertTrue((d==cf.dtlt(2001, 1, 3, 21, 57, 57)).equals(cf.Data([[True, False], [False, True]]))) 
        
            d = cf.dt(2002, 6, 16)
            self.assertTrue(not (d == cf.dteq(1990, 1, 1)))
            self.assertTrue(d == cf.dteq(2002, 6, 16))
            self.assertTrue(not(d == cf.dteq('2100-1-1')))
            self.assertTrue(not (d == cf.dteq('2001-1-1') & cf.dteq(2010, 12, 31)))
        
            d = cf.dt(2002, 6, 16)
            self.assertTrue(d == cf.dtge(1990, 1, 1))
            self.assertTrue(d == cf.dtge(2002, 6, 16))
            self.assertTrue(not (d == cf.dtge('2100-1-1')))
            self.assertTrue(not (d == cf.dtge('2001-1-1') & cf.dtge(2010, 12, 31)))
                                        
            d = cf.dt(2002, 6, 16)
            self.assertTrue(d == cf.dtgt(1990, 1, 1))
            self.assertTrue(not (d == cf.dtgt(2002, 6, 16)))
            self.assertTrue(not (d == cf.dtgt('2100-1-1')))
            self.assertTrue(d == cf.dtgt('2001-1-1') & cf.dtle(2010, 12, 31))
        
            d = cf.dt(2002, 6, 16)
            self.assertTrue(d == cf.dtne(1990, 1, 1))
            self.assertTrue(not (d == cf.dtne(2002, 6, 16)))
            self.assertTrue(d == cf.dtne('2100-1-1'))
            self.assertTrue(d == cf.dtne('2001-1-1') & cf.dtne(2010, 12, 31))
        
            d = cf.dt(2002, 6, 16)
            self.assertTrue(not (d == cf.dtle(1990, 1, 1)))
            self.assertTrue(d == cf.dtle(2002, 6, 16))
            self.assertTrue(d == cf.dtle('2100-1-1'))
            self.assertTrue(not (d == cf.dtle('2001-1-1') & cf.dtle(2010, 12, 31)))
        
            d = cf.dt(2002, 6, 16)
            self.assertTrue(not (d == cf.dtlt(1990, 1, 1)))
            self.assertTrue(not (d == cf.dtlt(2002, 6, 16)))
            self.assertTrue(d == cf.dtlt('2100-1-1'))
            self.assertTrue(not (d == cf.dtlt('2001-1-1') & cf.dtlt(2010, 12, 31)))
        #--- End: for
        cf.CHUNKSIZE(self.original_chunksize)
    #--- End: def

    def test_Comparison_evaluate(self):
        
        c = cf.wi(2, 4)
        d = cf.wi(6, 8)

        e = d | c

        self.assertTrue(c.evaluate(3))
        self.assertFalse(c.evaluate(5))

        self.assertTrue(e.evaluate(3))
        self.assertTrue(e.evaluate(7))
        self.assertFalse(e.evaluate(5))

        self.assertTrue(3 == c)
        self.assertFalse(5 == c)

        self.assertTrue(3 == e)
        self.assertTrue(7 == e)
        self.assertFalse(5 == e)
#--- End: class

if __name__ == '__main__':
    print 'cf-python version:', cf.__version__
    print 'cf-python path:'   , os.path.abspath(cf.__file__)
    print
    unittest.main(verbosity=2)
