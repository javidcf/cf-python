from distutils.core import setup, Extension
import os, fnmatch
import imp

def find_package_data_files(directory):
    for root, dirs, files in os.walk(directory):
        for basename in files:
            if fnmatch.fnmatch(basename, '*'):
                filename = os.path.join(root, basename)
#                if filename.find('/.svn') > -1:
#                    continue
                yield filename.replace('cf/', '', 1)

# Check that the dependencies are met
for _module in ('netCDF4', 'numpy'):
    try:
        imp.find_module(_module)
    except ImportError as error:
        raise ImportError("Missing dependency. cf requires package %s. %s" %
                          (_module, error))
#--- End: for

# --------------------------------------------------------------------
# WGDOS unpacking C extension module
# --------------------------------------------------------------------
unwgdos = Extension('cf.pp.umread.umfile',
                    sources=['cf/pp/unwgdos/unwgdos.c',
                             'cf/pp/unwgdos/unwgdos_crayio.c'],
                    include_dirs=['cf/um/umread',
                                  'cf/um/umread/bits'],
                    )
                
unwgdos = Extension('cf.um/umread.lib.libunwgdos',
                    sources=['cf/pp/unwgdos/unwgdos.c',
                             'cf/pp/unwgdos/unwgdos_crayio.c'],
                    define_macros=[('LITTLE__ENDIAN', '1')],
                    extra_link_args=['-shared'],
                    include_dirs=['cf/pp/unwgdos'],
                    )
                
version   = '0.9.8.3'
packages  = ['cf']
etc_files = [f for f in find_package_data_files('cf/etc')]
unwgdos_build_files = [f for f in find_package_data_files('cf/pp/unwgdos')]

package_data = etc_files + unwgdos_build_files

#doc_files          = [f for f in find_package_data_files('docs/build')] + [f for f in find_package_data_files('docs/source')] 
#test_files         = ['test/test1.py']
#man_files          = [f for f in find_package_data_files('scripts/man1')]

with open('README.md') as ldfile:
    long_description = ldfile.read()

setup(name = "cf",
      long_description = long_description,
      version      = version,
      description  = "Python interface to the CF data model",
      author       = "David Hassell",
      author_email = "d.c.hassell at reading.ac.uk",
      url          = "http://cfpython.bitbucket.org/",
      download_url = "https://bitbucket.org/cfpython/cf-python/downloads",
      platforms    = ["linux"],
      license      = ["OSI Approved"],
      keywords     = ['cf', 'numpy','netcdf','data','science','network','oceanography','meteorology','climate'],
      classifiers  = ["Development Status :: 3 - Alpha",
                      "Intended Audience :: Science/Research", 
                      "License :: OSI Approved", 
                      "Topic :: Software Development :: Libraries :: Python Modules",
                      "Topic :: System :: Archiving :: Compression",
                      "Operating System :: OS Independent"],
      packages     = ['cf',
                      'cf.pp',
                      'cf.data',
                      'cf.netcdf',
                      'cf.tools'],
      package_data = {'cf': package_data},
      scripts      = ['scripts/cfa',
                      'scripts/cfdump'],
      requires     = ['netCDF4 (>=0.9.7)', 'numpy (>=1.6)'],
      ext_modules  = [unwgdos],
  )
